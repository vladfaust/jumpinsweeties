package com.mainecoon.jumpinsweets;

import com.vladislavfaust.jumpinsweetis.helpers.advertisment.AdsManager;
import com.vladislavfaust.jumpinsweetis.helpers.advertisment.IPollFish;

/**
 * Created by Faust on 02.04.2015.
 *
 */
public class PollFishIOS implements IPollFish
{
    @Override
    public AdsManager.INCENTIVIZED_TYPE getNetworkType()
    {
        return null;
    }

    @Override
    public AdsManager.INCENTIVIZED_SOURCE getNetworkSource()
    {
        return null;
    }

    @Override
    public boolean showIncentivizedContent()
    {
        return false;
    }

    @Override
    public boolean readyToShowIncentivizedContent()
    {
        return false;
    }
}
