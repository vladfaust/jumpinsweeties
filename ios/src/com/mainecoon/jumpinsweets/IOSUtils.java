package com.mainecoon.jumpinsweets;

import com.vladislavfaust.jumpinsweetis.helpers.IPlatformUtils;

/**
 * Created by Faust on 27.03.2015.
 *
 */
public class IOSUtils implements IPlatformUtils
{
    @Override
    public String getDeviceId()
    {
        return null;
    }

    @Override
    public boolean checkInternetConnection()
    {
        return false;
    }

    @Override
    public void showMessage(String message)
    {

    }

    @Override
    public void shareImageAndText(String filepath, String text)
    {

    }
}
