package com.mainecoon.jumpinsweets;

import com.vladislavfaust.jumpinsweetis.helpers.IAP;
import com.vladislavfaust.jumpinsweetis.helpers.advertisment.AdsManager;
import com.vladislavfaust.jumpinsweetis.helpers.analytics.IGA;

/**
 * Created by Faust on 13.05.2015.
 *
 */
public class GameAnalyticsIOS implements IGA
{
    @Override
    public void trackGameOver(int score, int candies)
    {

    }

    @Override
    public void trackGiftsOffered()
    {

    }

    @Override
    public void trackIncentivizedOffered()
    {

    }

    @Override
    public void trackIncentivizedClick(AdsManager.INCENTIVIZED_TYPE type, boolean success)
    {

    }

    @Override
    public void trackIncentivizedShown(AdsManager.INCENTIVIZED_SOURCE which)
    {

    }

    @Override
    public void trackGiftingScreen()
    {

    }

    @Override
    public void trackUnlockAllGiftsPurchased(boolean dialog)
    {

    }

    @Override
    public void trackBuyMoreTokensClick()
    {

    }

    @Override
    public void trackPurchaseTokensClick(IAP.PURCHASE_TYPE which, boolean success)
    {

    }

    @Override
    public void trackUnlockUniqueClick()
    {

    }

    @Override
    public void trackUnlockUniquePurchase(boolean currency, boolean hadAChoice)
    {

    }

    @Override
    public void logFPS()
    {

    }
}
