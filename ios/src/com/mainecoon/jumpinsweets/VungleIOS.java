package com.mainecoon.jumpinsweets;

import com.vladislavfaust.jumpinsweetis.helpers.advertisment.AdsManager;
import com.vladislavfaust.jumpinsweetis.helpers.advertisment.IVungle;

/**
 * Created by Faust on 30.03.2015.
 *
 */
public class VungleIOS implements IVungle
{
    @Override
    public AdsManager.INCENTIVIZED_TYPE getNetworkType()
    {
        return null;
    }

    @Override
    public AdsManager.INCENTIVIZED_SOURCE getNetworkSource()
    {
        return null;
    }

    @Override
    public boolean showIncentivizedContent()
    {
        return false;
    }

    @Override
    public boolean readyToShowIncentivizedContent()
    {
        return false;
    }
    @Override
    public boolean showInterstitial()
    {
        return false;
    }

    @Override
    public boolean readyToShowInterstitial()
    {
        return false;
    }
}
