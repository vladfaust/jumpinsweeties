package com.mainecoon.jumpinsweets;

import com.vladislavfaust.jumpinsweetis.gameobjects.bonuses.BonusLevelingController;
import com.vladislavfaust.jumpinsweetis.helpers.IAP;
import com.vladislavfaust.jumpinsweetis.helpers.advertisment.AdsManager;
import com.vladislavfaust.jumpinsweetis.helpers.analytics.AnalyticsHelper;
import com.vladislavfaust.jumpinsweetis.helpers.analytics.IFlurry;

/**
 * Created by Faust on 02.04.2015.
 *
 */
public class FlurryIOS implements IFlurry
{

    @Override
    public void trackMainMenuScreen()
    {

    }

    @Override
    public void trackSound(boolean enabled)
    {

    }

    @Override
    public void trackMusic(boolean enabled)
    {

    }

    @Override
    public void trackPromocodeClick(boolean confirmed, String code)
    {

    }

    @Override
    public void trackLeaveFeedbackClick()
    {

    }

    @Override
    public void trackSiteClick()
    {

    }

    @Override
    public void trackProgressClick()
    {

    }

    @Override
    public void trackLeadersClick()
    {

    }

    @Override
    public void trackGameStart()
    {

    }

    @Override
    public void trackGameOver(String skinTexture, int score, int candies)
    {

    }

    @Override
    public void trackShareClick(int score, boolean highscore, boolean confirmed)
    {

    }

    @Override
    public void trackGiftsOffered()
    {

    }

    @Override
    public void trackIncentivizedOffered()
    {

    }

    @Override
    public void trackIncentivizedClick(AdsManager.INCENTIVIZED_TYPE type, boolean success)
    {

    }

    @Override
    public void trackIncentivizedShown(AdsManager.INCENTIVIZED_SOURCE which)
    {

    }

    @Override
    public void trackPollCancel()
    {

    }

    @Override
    public void trackGiftingScreen()
    {

    }

    @Override
    public void trackUnlockAllGiftsPurchased(boolean dialog)
    {

    }

    @Override
    public void trackBoostsScreen()
    {

    }

    @Override
    public void trackUpgradeBoost(BonusLevelingController.BOOST_TYPE which, int toLevel)
    {

    }

    @Override
    public void trackConvert()
    {

    }

    @Override
    public void trackBuyMoreTokensClick()
    {

    }

    @Override
    public void trackPurchaseTokensClick(IAP.PURCHASE_TYPE which, boolean success)
    {

    }

    @Override
    public void trackSkinScreen()
    {

    }

    @Override
    public void trackUnlockUniqueClick()
    {

    }

    @Override
    public void trackUnlockUniquePurchase(boolean currency, boolean hadAChoice)
    {

    }

    @Override
    public void trackUnlockCommon(String texture, int price)
    {

    }

    @Override
    public void trackFeedbackOffer(AnalyticsHelper.FEEDBACK_RESULT result)
    {

    }

    @Override
    public void trackMissionCompleted(int id)
    {

    }

    @Override
    public void trackAchievementUnlock(int id)
    {

    }

    @Override
    public void trackAchievementShareScreen(int id, boolean success, boolean afterDialog)
    {

    }

    @Override
    public void trackNoGPGSInstalled()
    {

    }

    @Override
    public void trackGPGSLogin(boolean success)
    {

    }
}
