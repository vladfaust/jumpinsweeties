package com.mainecoon.jumpinsweets;

import com.badlogic.gdx.backends.iosrobovm.IOSApplication;
import com.badlogic.gdx.backends.iosrobovm.IOSApplicationConfiguration;
import com.vladislavfaust.jumpinsweetis.JumpinSweeties;

import org.robovm.apple.foundation.NSAutoreleasePool;
import org.robovm.apple.uikit.UIApplication;

public class IOSLauncher extends IOSApplication.Delegate {
    @Override
    protected IOSApplication createApplication() {
        IOSApplicationConfiguration config = new IOSApplicationConfiguration();
        return new IOSApplication(new JumpinSweeties(new ChartboostIOS(), new RequestHandlerIOS(), new AdColonyIOS(), new IOSUtils(), new GPGSIOS(), new VungleIOS(), new PollFishIOS(), new FlurryIOS(), new GameAnalyticsIOS()), config);
    }

    public static void main(String[] argv) {
        NSAutoreleasePool pool = new NSAutoreleasePool();
        UIApplication.main(argv, null, IOSLauncher.class);
        pool.close();
    }
}