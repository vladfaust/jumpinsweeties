package com.mainecoon.jumpinsweets;

import com.vladislavfaust.jumpinsweetis.helpers.googleplay.IGPGSActions;
import com.vladislavfaust.jumpinsweetis.helpers.googleplay.OnAchievementCheck;

/**
 * Created by Faust on 27.03.2015.
 *
 */
public class GPGSIOS implements IGPGSActions
{
    @Override
    public boolean getSignedInGPGS()
    {
        return false;
    }

    @Override
    public void loginGPGS()
    {

    }

    @Override
    public void submitScoreGPGS(int score)
    {

    }

    @Override
    public void unlockAchievementGPGS(String achievementId)
    {

    }

    @Override
    public void getLeaderboardGPGS()
    {

    }

    @Override
    public void getAchievementsGPGS()
    {

    }

    @Override
    public void checkIfAchievementUnlocked(String id, OnAchievementCheck callback)
    {

    }

    @Override
    public void incrementAchievement(String id, int steps)
    {

    }
}
