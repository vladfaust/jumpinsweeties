package com.mainecoon.jumpinsweets.desktop;

import com.vladislavfaust.jumpinsweetis.helpers.advertisment.AdsManager;
import com.vladislavfaust.jumpinsweetis.helpers.advertisment.IChartBoost;

/**
 * Created by Faust on 16.03.2015.
 *
 */
public class ChartBoostDesktop implements IChartBoost
{
    @Override
    public AdsManager.INCENTIVIZED_TYPE getNetworkType()
    {
        return null;
    }

    @Override
    public AdsManager.INCENTIVIZED_SOURCE getNetworkSource()
    {
        return null;
    }

    @Override
    public boolean showIncentivizedContent()
    {
        return false;
    }

    @Override
    public boolean readyToShowIncentivizedContent()
    {
        return false;
    }

    @Override
    public boolean showInterstitial()
    {
        return false;
    }

    @Override
    public boolean readyToShowInterstitial()
    {
        return false;
    }

    @Override
    public void cacheInterstitial()
    {

    }

    @Override
    public void cacheRewardedVideo()
    {

    }

    @Override
    public boolean onBackPressed()
    {
        return false;
    }
}
