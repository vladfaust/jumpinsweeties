package com.mainecoon.jumpinsweets.desktop;

import com.vladislavfaust.jumpinsweetis.helpers.dialogs.IPromptDialog;
import com.vladislavfaust.jumpinsweetis.helpers.dialogs.IRequestHandler;
import com.vladislavfaust.jumpinsweetis.helpers.dialogs.IConfirmDialog;
import com.vladislavfaust.jumpinsweetis.helpers.dialogs.IThreeOptionsDialog;

/**
 * Created by Faust on 21.03.2015.
 *
 */
public class RequestHandlerDesktop implements IRequestHandler
{
    @Override
    public void confirm(IConfirmDialog confirmDialog, String title, String message, String ok, String cancel)
    {

    }

    @Override
    public void prompt(IPromptDialog promptDialog, String title, String ok, String cancel, String hint)
    {

    }

    @Override
    public void threeOptions(IThreeOptionsDialog threeOptionsDialog, String title, String message, String positive, String negative, String neutral)
    {

    }
}
