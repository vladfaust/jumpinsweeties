package com.mainecoon.jumpinsweets.desktop;

import com.vladislavfaust.jumpinsweetis.helpers.advertisment.AdsManager;
import com.vladislavfaust.jumpinsweetis.helpers.advertisment.IAdColony;

/**
 * Created by Faust on 25.03.2015.
 *
 */
public class AdColonyDesktop implements IAdColony
{
    @Override
    public AdsManager.INCENTIVIZED_TYPE getNetworkType()
    {
        return null;
    }

    @Override
    public AdsManager.INCENTIVIZED_SOURCE getNetworkSource()
    {
        return null;
    }

    @Override
    public boolean showIncentivizedContent()
    {
        return false;
    }

    @Override
    public boolean readyToShowIncentivizedContent()
    {
        return false;
    }

    @Override
    public boolean showInterstitial()
    {
        return false;
    }

    @Override
    public boolean readyToShowInterstitial()
    {
        return false;
    }
}
