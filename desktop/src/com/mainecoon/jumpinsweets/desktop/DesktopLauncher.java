package com.mainecoon.jumpinsweets.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.vladislavfaust.jumpinsweetis.JumpinSweeties;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();

        config.width = 360;
        config.height = 600;

		new LwjglApplication(new JumpinSweeties(new ChartBoostDesktop(), new RequestHandlerDesktop(), new AdColonyDesktop(), new DesktopUtils(), new GPGSDesktop(), new VungleDesktop(), new PoolFishDesktop(), new FlurryDesktop(), new GameAnalyticsDesktop()), config);
	}
}
