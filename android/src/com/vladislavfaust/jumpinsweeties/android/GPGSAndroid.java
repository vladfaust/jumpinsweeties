package com.vladislavfaust.jumpinsweeties.android;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;

import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.games.Games;
import com.google.android.gms.games.GamesStatusCodes;
import com.google.android.gms.games.achievement.Achievement;
import com.google.android.gms.games.achievement.AchievementBuffer;
import com.google.android.gms.games.achievement.Achievements;
import com.google.example.games.basegameutils.GameHelper;
import com.vladislavfaust.jumpinsweetis.helpers.analytics.AnalyticsHelper;
import com.vladislavfaust.jumpinsweetis.helpers.googleplay.AchievementsHandler;
import com.vladislavfaust.jumpinsweetis.helpers.googleplay.IGPGSActions;
import com.vladislavfaust.jumpinsweetis.helpers.googleplay.OnAchievementCheck;

import java.util.concurrent.TimeUnit;

/**
 * Created by Faust on 27.03.2015.
 *
 */
public class GPGSAndroid implements IGPGSActions, GameHelper.GameHelperListener
{
    // Настройки
    private final static String LEADERBOARD_ID = "CgkIhea2oKYUEAIQCg";

    // Переменные
    private Activity activity;
    private GameHelper gameHelper;

    public GPGSAndroid(Activity activity)
    {
        this.activity = activity;
    }

    public void init ()
    {
        gameHelper = new GameHelper(activity, GameHelper.CLIENT_GAMES);
        gameHelper.setConnectOnStart(false);
        gameHelper.enableDebugLog(true);
        gameHelper.setup(this);

        try {
            gameHelper.getApiClient();
        }
        catch (Exception e)
        {
            AnalyticsHelper.trackNoGPGSInstalled();
        }
    }

    public void onStart ()
    {
        gameHelper.onStart(activity);
    }

    public void onStop ()
    {
        gameHelper.onStop();
    }

    public void onActivityResult (int requestCode, int resultCode, Intent data)
    {
        gameHelper.onActivityResult(requestCode, resultCode, data);
    }

    private boolean firstTime = true;
    @Override
    public void onSignInFailed()
    {
        AnalyticsHelper.trackGPGSLogin(false);
        
        /*if (firstTime)
        {
            firstTime = false;
            return;
        }

        if ((gameHelper.getSignInError() == null) || gameHelper.getSignInError().getServiceErrorCode() != GameHelper.CAUSE_NETWORK_LOST)
        {
            AnalyticsHelper.trackGPGSLogin(false);

            if (!MyPreferences.getGPGSBlocked())
            {
                RequestHandlerAndroid.getInstance().threeOptions(new IThreeOptionsDialog() {
                     @Override
                     public void clickedNegative()
                     {

                     }

                     @Override
                     public void clickedPositive()
                     {
                         loginGPGS();
                     }

                     @Override
                     public void clickedNeutral()
                     {
                         AnalyticsHelper.trackGPGSConfirmationDialogClickedNever();
                         MyPreferences.putGPGSBlocked(true);
                     }
                 },
                    Language.LOCALE.get("confirm"),
                    Language.LOCALE.get("failedLoginMessage"),
                    Language.LOCALE.get("tryAgain"),
                    Language.LOCALE.get("later"),
                    Language.LOCALE.get("never"));

                AnalyticsHelper.trackGPGSConfirmationDialog();
            }
        }*/

        /*if (gameHelper.getSignInError() && !MyPreferences.getGPGSBlocked())
        {
            RequestHandlerAndroid.getInstance().threeOptions(new IThreeOptionsDialog() {
                @Override
                public void clickedNegative()
                {

                }

                @Override
                public void clickedPositive()
                {
                    loginGPGS();
                }

                @Override
                public void clickedNeutral()
                {
                    AnalyticsHelper.trackGPGSConfirmationDialogClickedNever();
                    MyPreferences.putGPGSBlocked(true);
                }
            },
                Language.LOCALE.get("confirm"),
                Language.LOCALE.get("failedLoginMessage"),
                Language.LOCALE.get("tryAgain"),
                Language.LOCALE.get("later"),
                Language.LOCALE.get("never"));

            AnalyticsHelper.trackGPGSConfirmationDialog();
        }*/
    }

    @Override
    public void onSignInSucceeded()
    {
        AnalyticsHelper.trackGPGSLogin(true);

        AchievementsHandler.checkShouldBeUnlockedSkins();
    }

    @Override
    public boolean getSignedInGPGS()
    {
        return gameHelper.isSignedIn();
    }

    @Override
    public void loginGPGS()
    {
        /*if (!AndroidUtils.checkInternetConnectionStatic(activity)) {
            AndroidUtils.showMessageStatic(activity, Language.LOCALE.get("needInternet"));
            return;
        }*/

        try
        {
            activity.runOnUiThread(new Runnable()
            {
                @Override
                public void run()
                {
                    gameHelper.beginUserInitiatedSignIn();
                }
            });
        } catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    @Override
    public void submitScoreGPGS(int score)
    {
        if (!getSignedInGPGS())
            return;

        Games.Leaderboards.submitScoreImmediate(gameHelper.getApiClient(), LEADERBOARD_ID, score);
    }

    @Override
    public void unlockAchievementGPGS(String achievementId)
    {
        if (!getSignedInGPGS())
            return;

        Games.Achievements.unlock(gameHelper.getApiClient(), achievementId);
    }

    @Override
    public void getLeaderboardGPGS()
    {
        if (!getSignedInGPGS())
            return;

        activity.startActivityForResult(Games.Leaderboards.getLeaderboardIntent(gameHelper.getApiClient(), LEADERBOARD_ID), 100);
    }

    @Override
    public void getAchievementsGPGS()
    {
        if (!getSignedInGPGS())
            return;

        activity.startActivityForResult( Games.Achievements.getAchievementsIntent(gameHelper.getApiClient()), 101);
    }

    private class LoadAchievementsBackground extends AsyncTask
    {
        String id;
        OnAchievementCheck callback;

        public LoadAchievementsBackground (String id, OnAchievementCheck callback)
        {
            this.id = id;
            this.callback = callback;
        }

        @Override
        protected Object doInBackground(Object[] params)
        {
            // Таймоут операции
            long waitTime = 60;

            // Загрузить ачивки
            PendingResult p = Games.Achievements.load( gameHelper.getApiClient(), true );
            Achievements.LoadAchievementsResult r = (Achievements.LoadAchievementsResult)p.await(waitTime, TimeUnit.SECONDS);

            int status = r.getStatus().getStatusCode();
            if ( status != GamesStatusCodes.STATUS_OK )
            {
                r.release();
                return null;
            }

            // Кешируем
            AchievementBuffer buf = r.getAchievements();

            // Прогоняем
            int bufSize = buf.getCount();
            for (int i = 0; i < bufSize; i++)
            {
                Achievement achievement = buf.get(i);

                String achievementId = achievement.getAchievementId();

                if (!achievementId.equals(id))
                    continue;

                if (achievement.getState() == Achievement.STATE_UNLOCKED)
                    callback.doSomething(true);
                else
                    callback.doSomething(false);

                break;
            }

            buf.close();
            r.release();

            return null;
        }
    }

    @Override
    public void checkIfAchievementUnlocked (String id, OnAchievementCheck callback)
    {
        if (!getSignedInGPGS())
            return;

        new LoadAchievementsBackground(id, callback).execute();
    }

    @Override
    public void incrementAchievement(String id, int amount)
    {
        if (!getSignedInGPGS())
            return;

        if (amount <= 0)
            return;

        Games.Achievements.increment(gameHelper.getApiClient(), id, amount);
    }
}
