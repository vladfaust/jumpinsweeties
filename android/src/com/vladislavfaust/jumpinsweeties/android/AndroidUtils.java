package com.vladislavfaust.jumpinsweeties.android;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.widget.Toast;

import com.vladislavfaust.jumpinsweetis.helpers.IPlatformUtils;
import com.vladislavfaust.jumpinsweetis.helpers.Language;

import java.io.File;

/**
 * Created by Faust on 27.03.2015.
 *
 */
public class AndroidUtils implements IPlatformUtils
{
    private Activity activity;

    public AndroidUtils (Activity activity)
    {
        this.activity = activity;
    }

    @Override
    public String getDeviceId()
    {
        final TelephonyManager mTelephony = (TelephonyManager) activity.getSystemService(Context.TELEPHONY_SERVICE);
        return (mTelephony.getDeviceId() != null) ? mTelephony.getDeviceId() :
                Settings.Secure.getString(activity.getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    @Override
    public boolean checkInternetConnection()
    {
        ConnectivityManager cm = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();

        return ni != null;
    }

    public static boolean checkInternetConnectionStatic (Activity activity)
    {
        ConnectivityManager cm = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();

        return ni != null;
    }

    private static Toast toast;

    @Override
    public void showMessage(final String message)
    {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run()
            {
                if (toast != null) {
                    toast.cancel();
                    toast = Toast.makeText(activity, message, Toast.LENGTH_SHORT);
                    toast.show();
                }
                else
                {
                    toast = Toast.makeText(activity, message, Toast.LENGTH_SHORT);
                    toast.show();
                }
            }
        });
    }

    @Override
    public void shareImageAndText(String filepath, String text)
    {
        Uri imageUri = Uri.fromFile(new File(filepath));

        Intent shareIntent = new Intent();
        shareIntent.setType("image/png");
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.putExtra(Intent.EXTRA_TEXT, text);
        shareIntent.putExtra(Intent.EXTRA_STREAM, imageUri);
        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

        activity.startActivity(Intent.createChooser(shareIntent, Language.LOCALE.get("shareVia")));
    }
}
