package com.vladislavfaust.jumpinsweeties.android;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.multidex.MultiDex;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.vladislavfaust.jumpinsweeties.android.advertisment.AdColonyAndroid;
import com.vladislavfaust.jumpinsweeties.android.advertisment.ChartboostAndroid;
import com.vladislavfaust.jumpinsweeties.android.analytics.FlurryAndroid;
import com.vladislavfaust.jumpinsweeties.android.analytics.GAAndroid;
import com.vladislavfaust.jumpinsweetis.JumpinSweeties;

public class AndroidLauncher extends AndroidApplication
{
    private ChartboostAndroid chartBoostInstance;
    private AdColonyAndroid adColonyInstance;
    private GPGSAndroid playGamesAndroid;
    private ParseAndroid parseAndroid;
    private JumpinSweeties jumpinSweeties;
    private GAAndroid gameAnalytics;

	@Override
	protected void onCreate (Bundle savedInstanceState)
    {
		super.onCreate(savedInstanceState);
		AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();

        chartBoostInstance = new ChartboostAndroid(this);
        adColonyInstance = new AdColonyAndroid(this);
        playGamesAndroid = new GPGSAndroid(this);
        gameAnalytics = new GAAndroid(this);

        jumpinSweeties = new JumpinSweeties(chartBoostInstance, new RequestHandlerAndroid(this), adColonyInstance, new AndroidUtils(this), playGamesAndroid, null, null, new FlurryAndroid(this), gameAnalytics);
		initialize(jumpinSweeties, config);

        // Init stuff after game
        chartBoostInstance.init();
        adColonyInstance.init();
        playGamesAndroid.init();

        parseAndroid = new ParseAndroid(this);

	}

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    // ChartBoost
    @Override
    public void onStart()
    {
        super.onStart();
        chartBoostInstance.onStart();
        parseAndroid.init();
        playGamesAndroid.onStart();
    }

    @Override
    public void onResume()
    {
        super.onResume();
        chartBoostInstance.onResume();
        adColonyInstance.onResume();
        gameAnalytics.onResume();
    }

    @Override
    public void onPause()
    {
        super.onPause();
        chartBoostInstance.onPause();
        adColonyInstance.onPause();
        gameAnalytics.onPause();
    }

    @Override
    public void onStop()
    {
        super.onStop();
        chartBoostInstance.onStop();
        playGamesAndroid.onStop();
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        chartBoostInstance.onDestroy();
    }

    public void returnControlToGame ()
    {
        jumpinSweeties.resetInputProcessor();
    }

    @Override
    public void onBackPressed ()
    {
        if (chartBoostInstance.onBackPressed())
            return;
        if (!jumpinSweeties.keyDown(Input.Keys.BACK))
            super.onBackPressed();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // здесь gameHelper принимает решение о подключении, переподключении или
        // отключении от игровых сервисов, в зависимости от кода результата
        // Activity
        playGamesAndroid.onActivityResult(requestCode, resultCode, data);
    }
}
