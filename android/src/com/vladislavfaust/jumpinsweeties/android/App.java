package com.vladislavfaust.jumpinsweeties.android;

import android.app.Application;

import com.parse.Parse;
import com.parse.ParseInstallation;

import java.util.Locale;

/**
 * Created by Faust on 03.04.2015.
 *
 */
public class App extends Application
{
    @Override
    public void onCreate() {
        super.onCreate();
        Parse.initialize(this, ParseAndroid.APP_ID, ParseAndroid.CLIENT_KEY);
        ParseInstallation installation = ParseInstallation.getCurrentInstallation();
        installation.put("language", Locale.getDefault().toString());
        installation.saveInBackground();
    }
}
