package com.vladislavfaust.jumpinsweeties.android.advertisment;

import android.app.Activity;
import android.util.Log;

import com.chartboost.sdk.CBLocation;
import com.chartboost.sdk.Chartboost;
import com.chartboost.sdk.ChartboostDelegate;
import com.chartboost.sdk.Model.CBError;
import com.vladislavfaust.jumpinsweetis.helpers.advertisment.AdsManager;
import com.vladislavfaust.jumpinsweetis.helpers.advertisment.IChartBoost;

/**
 * Created by Faust on 16.03.2015.
 *
 */
public class ChartboostAndroid implements IChartBoost
{
    // Settings
    private final static String TAG = "Chartboost";
    private final static String APP_ID = "550420d904b0167e588d9515";
    private final static String APP_SIGNATURE = "f85bc8c9ee69211fbde05d954358e7f013032f7c";

    private Activity activity;

    public ChartboostAndroid(final Activity activity)
    {
        this.activity = activity;
    }

    public void init ()
    {
        Chartboost.startWithAppId(activity, APP_ID, APP_SIGNATURE);
        Chartboost.onCreate(activity);

        ChartboostDelegate delegate = new ChartboostDelegate()
        {

            @Override
            public boolean shouldRequestInterstitial(String location)
            {
                Log.i(TAG, "SHOULD REQUEST INTERSTITIAL '" + (location != null ? location : "null"));
                return true;
            }

            @Override
            public boolean shouldDisplayInterstitial(String location)
            {
                Log.i(TAG, "SHOULD DISPLAY INTERSTITIAL '" + (location != null ? location : "null"));
                return true;
            }

            @Override
            public void didCacheInterstitial(String location)
            {
                Log.i(TAG, "DID CACHE INTERSTITIAL '" + (location != null ? location : "null"));
            }

            @Override
            public void didFailToLoadInterstitial(String location, CBError.CBImpressionError error)
            {
                Log.i(TAG, "DID FAIL TO LOAD INTERSTITIAL '" + (location != null ? location : "null") + " Error: " + error.name());
            }

            @Override
            public void didDismissInterstitial(String location)
            {
                Log.i(TAG, "DID DISMISS INTERSTITIAL: " + (location != null ? location : "null"));
            }

            @Override
            public void didCloseInterstitial(String location)
            {
                AdsManager.giveRewardForIncentivized();
                AdsManager.updateIncentivizedButton();
                cacheRewardedVideo();
                Log.i(TAG, "DID CLOSE INTERSTITIAL: " + (location != null ? location : "null"));
            }

            @Override
            public void didClickInterstitial(String location)
            {
                Log.i(TAG, "DID CLICK INTERSTITIAL: " + (location != null ? location : "null"));
            }

            @Override
            public void didDisplayInterstitial(String location)
            {
                Log.i(TAG, "DID DISPLAY INTERSTITIAL: " + (location != null ? location : "null"));
            }

            @Override
            public boolean shouldRequestMoreApps(String location)
            {
                Log.i(TAG, "SHOULD REQUEST MORE APPS: " + (location != null ? location : "null"));
                return true;
            }

            @Override
            public boolean shouldDisplayMoreApps(String location)
            {
                Log.i(TAG, "SHOULD DISPLAY MORE APPS: " + (location != null ? location : "null"));
                return true;
            }

            @Override
            public void didFailToLoadMoreApps(String location, CBError.CBImpressionError error)
            {
                Log.i(TAG, "DID FAIL TO LOAD MOREAPPS " + (location != null ? location : "null") + " Error: " + error.name());
            }

            @Override
            public void didCacheMoreApps(String location)
            {
                Log.i(TAG, "DID CACHE MORE APPS: " + (location != null ? location : "null"));
            }

            @Override
            public void didDismissMoreApps(String location)
            {
                Log.i(TAG, "DID DISMISS MORE APPS " + (location != null ? location : "null"));
            }

            @Override
            public void didCloseMoreApps(String location)
            {
                Log.i(TAG, "DID CLOSE MORE APPS: " + (location != null ? location : "null"));
            }

            @Override
            public void didClickMoreApps(String location)
            {
                Log.i(TAG, "DID CLICK MORE APPS: " + (location != null ? location : "null"));
            }

            @Override
            public void didDisplayMoreApps(String location)
            {
                Log.i(TAG, "DID DISPLAY MORE APPS: " + (location != null ? location : "null"));
            }

            @Override
            public void didFailToRecordClick(String uri, CBError.CBClickError error)
            {
                Log.i(TAG, "DID FAILED TO RECORD CLICK " + (uri != null ? uri : "null") + ", stackError: " + error.name());
            }

            @Override
            public boolean shouldDisplayRewardedVideo(String location)
            {
                Log.i(TAG, String.format("SHOULD DISPLAY REWARDED VIDEO: '%s'", (location != null ? location : "null")));
                return true;
            }

            @Override
            public void didCacheRewardedVideo(String location)
            {
                Log.i(TAG, String.format("DID CACHE REWARDED VIDEO: '%s'", (location != null ? location : "null")));
            }

            @Override
            public void didFailToLoadRewardedVideo(String location, CBError.CBImpressionError error)
            {
                Log.i(TAG, String.format("DID FAIL TO LOAD REWARDED VIDEO: '%s', Error:  %s", (location != null ? location : "null"), error.name()));
            }

            @Override
            public void didDismissRewardedVideo(String location)
            {
                Log.i(TAG, String.format("DID DISMISS REWARDED VIDEO '%s'", (location != null ? location : "null")));
            }

            @Override
            public void didCloseRewardedVideo(String location)
            {
                Log.i(TAG, String.format("DID CLOSE REWARDED VIDEO '%s'", (location != null ? location : "null")));
            }

            @Override
            public void didClickRewardedVideo(String location)
            {
                Log.i(TAG, String.format("DID CLICK REWARDED VIDEO '%s'", (location != null ? location : "null")));
            }

            @Override
            public void didCompleteRewardedVideo(String location, int reward)
            {
                Log.i(TAG, String.format("DID COMPLETE REWARDED VIDEO '%s' FOR REWARD %d", (location != null ? location : "null"), reward));
                AdsManager.giveRewardForIncentivized();
                AdsManager.updateIncentivizedButton();
                cacheRewardedVideo();
            }

            @Override
            public void didDisplayRewardedVideo(String location)
            {
                Log.i(TAG, String.format("DID DISPLAY REWARDED VIDEO '%s' FOR REWARD", location));
            }

            @Override
            public void willDisplayVideo(String location)
            {
                Log.i(TAG, String.format("WILL DISPLAY VIDEO '%s", location));
            }

        };

        Chartboost.setDelegate(delegate);
        Chartboost.setAutoCacheAds(true);
        Chartboost.setShouldPrefetchVideoContent(true);
    }

    @Override
    public AdsManager.INCENTIVIZED_TYPE getNetworkType()
    {
        return AdsManager.INCENTIVIZED_TYPE.VIDEO;
    }

    @Override
    public AdsManager.INCENTIVIZED_SOURCE getNetworkSource()
    {
        return AdsManager.INCENTIVIZED_SOURCE.CHARTBOOST;
    }

    @Override
    public boolean showInterstitial()
    {
        if (Chartboost.hasInterstitial(CBLocation.LOCATION_DEFAULT)) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run()
                {
                    Chartboost.showInterstitial(CBLocation.LOCATION_DEFAULT);
                }
            });
            return true;
        }
        else return false;
    }

    @Override
    public boolean readyToShowInterstitial()
    {
        return Chartboost.hasInterstitial(CBLocation.LOCATION_DEFAULT);
    }

    @Override
    public void cacheInterstitial()
    {
        Chartboost.cacheInterstitial(CBLocation.LOCATION_DEFAULT);
    }

    @Override
    public boolean showIncentivizedContent()
    {
        return showInterstitial();
        /*if (Chartboost.hasRewardedVideo(CBLocation.LOCATION_DEFAULT)) {
            Chartboost.showRewardedVideo(CBLocation.LOCATION_DEFAULT);
            return true;
        }
        else return false;*/
    }

    @Override
    public void cacheRewardedVideo()
    {
        cacheInterstitial();
        /*Chartboost.cacheRewardedVideo(CBLocation.LOCATION_DEFAULT);*/
    }

    @Override
    public boolean readyToShowIncentivizedContent()
    {
        return readyToShowInterstitial();
        /*return Chartboost.hasRewardedVideo(CBLocation.LOCATION_DEFAULT);*/
    }

    public void onStart()
    {
        Chartboost.onStart(activity);
    }

    public void onResume()
    {
        Chartboost.onResume(activity);
    }

    public void onPause()
    {
        Chartboost.onPause(activity);
    }

    public void onStop()
    {
        Chartboost.onStop(activity);
    }

    public void onDestroy()
    {
        Chartboost.onDestroy(activity);
    }

    @Override
    public boolean onBackPressed()
    {
        // If an interstitial is on screen, close it.
        return Chartboost.onBackPressed();
    }

}
