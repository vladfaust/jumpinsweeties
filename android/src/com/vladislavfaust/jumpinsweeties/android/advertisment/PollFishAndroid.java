/*
package com.vladislavfaust.jumpinsweeties.android.advertisment;

import android.util.Log;

import com.pollfish.constants.Position;
import com.pollfish.interfaces.PollfishClosedListener;
import com.pollfish.interfaces.PollfishOpenedListener;
import com.pollfish.interfaces.PollfishSurveyCompletedListener;
import com.pollfish.interfaces.PollfishSurveyNotAvailableListener;
import com.pollfish.interfaces.PollfishSurveyReceivedListener;
import com.pollfish.interfaces.PollfishUserNotEligibleListener;
import com.pollfish.main.PollFish;
import com.vladislavfaust.jumpinsweeties.android.AndroidLauncher;
import com.vladislavfaust.jumpinsweetis.helpers.advertisment.AdsManager;
import com.vladislavfaust.jumpinsweetis.helpers.advertisment.IPollFish;
import com.vladislavfaust.jumpinsweetis.helpers.analytics.AnalyticsHelper;

*/
/**
 * Created by Faust on 01.04.2015.
 *
 *//*

public class PollFishAndroid implements IPollFish, PollfishSurveyReceivedListener, PollfishSurveyCompletedListener, PollfishSurveyNotAvailableListener, PollfishOpenedListener, PollfishClosedListener, PollfishUserNotEligibleListener
{
    // Настройки
    private final static String API_KEY = "fe060ab0-8f16-40cb-bd3a-789d76afda89";

    // Переменные
    private AndroidLauncher launcher;
    private boolean isReady = false;
    private boolean completed = false;

    public PollFishAndroid (AndroidLauncher launcher)
    {
        this.launcher = launcher;
    }

    public void init ()
    {
        PollFish.customInit(launcher, API_KEY, Position.BOTTOM_LEFT, 0, this, this, this, this, this, this);
        PollFish.hide();
    }

    @Override
    public AdsManager.INCENTIVIZED_TYPE getNetworkType()
    {
        return AdsManager.INCENTIVIZED_TYPE.POLL;
    }

    @Override
    public AdsManager.INCENTIVIZED_SOURCE getNetworkSource()
    {
        return AdsManager.INCENTIVIZED_SOURCE.POLLFISH;
    }

    @Override
    public boolean showIncentivizedContent()
    {
        if (!isReady)
            return false;

        PollFish.show();
        return true;
    }

    @Override
    public boolean readyToShowIncentivizedContent()
    {
        return isReady;
    }

    @Override
    public void onPollfishSurveyCompleted(boolean b, int i)
    {
        Log.d("PollFish1", "onPollfishSurveyCompleted, b = " + b + ", i = " + i);
        AdsManager.giveRewardForIncentivized();
        AdsManager.updateIncentivizedButton();
        isReady = false;
        completed = true;
    }

    @Override
    public void onPollfishClosed()
    {
        Log.d("PollFish1", "onPollfishClosed()");

        if (!completed)
            AnalyticsHelper.trackPollCancel();
        else
            completed = false;

        launcher.returnControlToGame();
    }

    @Override
    public void onPollfishOpened()
    {
        Log.d("PollFish1", "onPollfishOpened()");
    }

    @Override
    public void onPollfishSurveyNotAvailable()
    {
        Log.d("PollFish1", "onPollfishSurveyNotAvailable()");
        isReady = false;
    }

    @Override
    public void onPollfishSurveyReceived(boolean b, int i)
    {
        Log.d("PollFish1", "onPollfishSurveyReceived(), b = " + b + ", i = " + i);
        isReady = true;
    }

    @Override
    public void onUserNotEligible()
    {
        Log.d("PollFish1", "onUserNotEligible()");
    }
}
*/
