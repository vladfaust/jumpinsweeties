/*
package com.vladislavfaust.jumpinsweeties.android.advertisment;

import android.app.Activity;
import android.util.Log;

import com.vladislavfaust.jumpinsweetis.helpers.advertisment.AdsManager;
import com.vladislavfaust.jumpinsweetis.helpers.advertisment.IVungle;
import com.vungle.publisher.AdConfig;
import com.vungle.publisher.EventListener;
import com.vungle.publisher.Orientation;
import com.vungle.publisher.VunglePub;

*/
/**
 * Created by Faust on 30.03.2015.
 *
 *//*

public class VungleAndroid implements IVungle
{
    // Настройки
    private static final String APP_ID = "55194096a630da252f0000aa";

    // Переменные
    private Activity activity;
    private final VunglePub vunglePub = VunglePub.getInstance();
    private AdConfig adConfig;

    public VungleAndroid (Activity activity)
    {
        this.activity = activity;
    }

    public void init ()
    {
        vunglePub.init(activity.getApplicationContext(), APP_ID);
        vunglePub.setEventListeners(new EventListener() {
            @Override
            public void onAdEnd(boolean b)
            {
                if (adConfig.isIncentivized() && b)
                {
                    AdsManager.giveRewardForIncentivized();
                    AdsManager.updateIncentivizedButton();
                }
            }

            @Override
            public void onAdStart()
            {

            }

            @Override
            public void onAdUnavailable(String s)
            {
                Log.d("Vungle", "onAdUnavailable: " + s);
            }

            @Override
            public void onAdPlayableChanged(boolean b)
            {

            }

            @Override
            public void onVideoView(boolean b, int i, int i2)
            {
                Log.d("Vungle", "onVideoView: "+b);
                if (adConfig.isIncentivized() && b)
                {
                    AdsManager.giveRewardForIncentivized();
                    AdsManager.updateIncentivizedButton();
                }
            }
        });
        adConfig = vunglePub.getGlobalAdConfig();

        adConfig.setImmersiveMode(true);
    }

    @Override
    public boolean showIncentivizedContent()
    {
        if (!vunglePub.isAdPlayable())
            return false;

        adConfig.setIncentivized(true);
        adConfig.setOrientation(Orientation.matchVideo);
        vunglePub.playAd(adConfig);
        return true;
    }

    @Override
    public boolean readyToShowIncentivizedContent()
    {
        return vunglePub.isAdPlayable();
    }

    @Override
    public AdsManager.INCENTIVIZED_TYPE getNetworkType()
    {
        return AdsManager.INCENTIVIZED_TYPE.VIDEO;
    }

    @Override
    public AdsManager.INCENTIVIZED_SOURCE getNetworkSource()
    {
        return AdsManager.INCENTIVIZED_SOURCE.VUNGLE;
    }

    @Override
    public boolean showInterstitial()
    {
        if (!vunglePub.isAdPlayable())
            return false;

        adConfig.setIncentivized(false);
        adConfig.setOrientation(Orientation.autoRotate);
        vunglePub.playAd(adConfig);
        return true;
    }

    @Override
    public boolean readyToShowInterstitial()
    {
        return vunglePub.isAdPlayable();
    }

    public void onResume()
    {
        vunglePub.onResume();
    }

    public void onPause()
    {
        vunglePub.onPause();
    }
}
*/
