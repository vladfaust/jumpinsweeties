package com.vladislavfaust.jumpinsweeties.android.advertisment;

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.util.Log;

import com.jirbo.adcolony.AdColony;
import com.jirbo.adcolony.AdColonyAd;
import com.jirbo.adcolony.AdColonyAdAvailabilityListener;
import com.jirbo.adcolony.AdColonyAdListener;
import com.jirbo.adcolony.AdColonyV4VCAd;
import com.jirbo.adcolony.AdColonyV4VCListener;
import com.jirbo.adcolony.AdColonyV4VCReward;
import com.jirbo.adcolony.AdColonyVideoAd;
import com.vladislavfaust.jumpinsweetis.helpers.advertisment.AdsManager;
import com.vladislavfaust.jumpinsweetis.helpers.advertisment.IAdColony;

/**
 * Created by Faust on 25.03.2015.
 *
 */
public class AdColonyAndroid implements IAdColony, AdColonyV4VCListener, AdColonyAdAvailabilityListener, AdColonyAdListener
{
    // Настройки
    private static final String APP_ID = "app3cf0b5ef888647ae9a";
    private static final String INTERSTITIAL_ZONE_ID = "vz2bc65e46719541f5a1";
    private static final String REWARDED_VIDEO_ZONE_ID = "vzd4cc9e8ebac5428bb8";

    // Переменные
    private AdColonyV4VCAd V4VCAd;
    private AdColonyVideoAd videoAd;

    private Activity activity;

    public AdColonyAndroid (Activity activity)
    {
        this.activity = activity;
    }

    public void init ()
    {
        AdColony.configure(activity, "version:1.0,store:google", APP_ID, INTERSTITIAL_ZONE_ID, REWARDED_VIDEO_ZONE_ID);

        AdColony.addV4VCListener(this);
        AdColony.addAdAvailabilityListener(this);

        activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        V4VCAd = new AdColonyV4VCAd();
        videoAd = new AdColonyVideoAd();
    }

    // Проиграть с подливой
    @Override
    public boolean showIncentivizedContent()
    {
        if (V4VCAd.isReady())
        {
            activity.runOnUiThread(new Runnable()
            {
                @Override
                public void run()
                {
                    V4VCAd.show();
                    V4VCAd = new AdColonyV4VCAd();
                }
            });
            return true;
        }
        else
            return false;
    }

    @Override
    public boolean readyToShowIncentivizedContent()
    {
        return V4VCAd.isReady();
    }

    @Override
    public AdsManager.INCENTIVIZED_TYPE getNetworkType()
    {
        return AdsManager.INCENTIVIZED_TYPE.VIDEO;
    }

    @Override
    public AdsManager.INCENTIVIZED_SOURCE getNetworkSource()
    {
        return AdsManager.INCENTIVIZED_SOURCE.ADCOLONY;
    }

    @Override
    public boolean showInterstitial()
    {
        if (videoAd.isReady()) {
            activity.runOnUiThread(new Runnable()
            {
                @Override
                public void run()
                {
                    videoAd.show();
                    videoAd = new AdColonyVideoAd();
                }
            });
            return true;
        }
        else
            return false;
    }

    @Override
    public boolean readyToShowInterstitial()
    {
        return videoAd.isReady();
    }

    @Override
    public void onAdColonyV4VCReward(AdColonyV4VCReward adColonyV4VCReward)
    {
        Log.d("AdColony", "onAdColonyV4VCReward");

        if (adColonyV4VCReward.success())
        {
            AdsManager.giveRewardForIncentivized();
            AdsManager.updateIncentivizedButton();
        }
    }



    @Override
    public void onAdColonyAdAvailabilityChange(boolean b, String s)
    {
        Log.d("AdColony", "onAdColonyAdAvailabilityChange: "+b+", "+s);
    }

    @Override
    public void onAdColonyAdAttemptFinished(AdColonyAd adColonyAd)
    {
        Log.d("AdColony", "onAdColonyAdAttemptFinished");
    }

    @Override
    public void onAdColonyAdStarted(AdColonyAd adColonyAd)
    {
        Log.d("AdColony", "onAdColonyAdStarted");
    }

    public void onPause()
    {
        AdColony.pause();
    }

    public void onResume()
    {
        AdColony.resume(activity);
    }
}
