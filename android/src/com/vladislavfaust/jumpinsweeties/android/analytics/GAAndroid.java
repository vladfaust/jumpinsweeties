package com.vladislavfaust.jumpinsweeties.android.analytics;

import android.app.Activity;

import com.gameanalytics.android.GameAnalytics;
import com.vladislavfaust.jumpinsweetis.helpers.IAP;
import com.vladislavfaust.jumpinsweetis.helpers.TokensHandler;
import com.vladislavfaust.jumpinsweetis.helpers.advertisment.AdsManager;
import com.vladislavfaust.jumpinsweetis.helpers.analytics.IGA;

/**
 * Created by Faust on 12.05.2015.
 *
 */
public class GAAndroid implements IGA
{
    /*
        Настройки
     */

    private final static String SECRET_KEY = "7c3975a6cb8e7e94d910b9160b9dab19d8bbbc7c";
    private final static String GAME_KEY = "d0247d8b08b9132727a2c81125573213";

    /*
        Методы
     */

    private Activity activity;

    public GAAndroid(Activity activity)
    {
        GameAnalytics.initialise(activity, SECRET_KEY, GAME_KEY);
        GameAnalytics.startSession(activity);
        GameAnalytics.logUnhandledExceptions();

        this.activity = activity;
    }

    public void onResume()
    {
        GameAnalytics.startSession(activity);
    }

    public void onPause()
    {
        GameAnalytics.stopLoggingFPS();
        GameAnalytics.stopSession();
    }


    @Override
    public void trackGameOver(int score, int candies)
    {
        GameAnalytics.newDesignEvent("GameOver:Score", (float) score);
        GameAnalytics.newDesignEvent("GameOver:Candies", (float) candies);
    }

    @Override
    public void trackGiftsOffered()
    {
        GameAnalytics.newDesignEvent("Gifts:Offer", 1f);
    }

    @Override
    public void trackIncentivizedOffered()
    {
        GameAnalytics.newDesignEvent("Incentivized:Offer", 1f);
    }

    @Override
    public void trackIncentivizedClick(AdsManager.INCENTIVIZED_TYPE type, boolean success)
    {
        GameAnalytics.newDesignEvent("Incentivized:Click", 1f);
    }

    @Override
    public void trackIncentivizedShown(AdsManager.INCENTIVIZED_SOURCE which)
    {
        GameAnalytics.newDesignEvent("Incentivized:Show", 1f);
    }

    @Override
    public void trackGiftingScreen()
    {
        GameAnalytics.newDesignEvent("Gifts:Show", 1f);
    }

    @Override
    public void trackUnlockAllGiftsPurchased(boolean dialog)
    {
        GameAnalytics.newBusinessEvent("Currency:UnlockAllGifts", "USD", IAP.findByType(IAP.PURCHASE_TYPE.UNLOCK_GIFTS).getPriceUsdInt());
    }

    @Override
    public void trackBuyMoreTokensClick()
    {
        GameAnalytics.newDesignEvent("BuyMoreTokens:Open", 1f);
    }

    @Override
    public void trackPurchaseTokensClick(IAP.PURCHASE_TYPE which, boolean success)
    {
        if (!success) return;

        GameAnalytics.newBusinessEvent("Currency:" + which.name(), "USD", IAP.findByType(which).getPriceUsdInt());
    }

    @Override
    public void trackUnlockUniqueClick()
    {
        GameAnalytics.newDesignEvent("UnlockUniqueSkin:Click", 1f);
    }

    @Override
    public void trackUnlockUniquePurchase(boolean currency, boolean hadAChoice)
    {
        if (currency)
            GameAnalytics.newBusinessEvent("Currency:UnlockUniqueSkin", "USD", IAP.findByType(IAP.PURCHASE_TYPE.UNLOCK_UNIQUE_SKIN).getPriceUsdInt());
        else
            GameAnalytics.newBusinessEvent("Tokens:UnlockUniqueSkin", "Tokens", TokensHandler.UNLOCK_UNIQUE_PRICE);
    }

    @Override
    public void logFPS()
    {
        GameAnalytics.logFPS();
    }
}
