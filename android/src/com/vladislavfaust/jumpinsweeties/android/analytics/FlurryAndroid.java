package com.vladislavfaust.jumpinsweeties.android.analytics;

import android.app.Activity;

import com.flurry.android.FlurryAgent;
import com.vladislavfaust.jumpinsweetis.gameobjects.bonuses.BonusLevelingController;
import com.vladislavfaust.jumpinsweetis.helpers.BackgroundHelper;
import com.vladislavfaust.jumpinsweetis.helpers.IAP;
import com.vladislavfaust.jumpinsweetis.helpers.advertisment.AdsManager;
import com.vladislavfaust.jumpinsweetis.helpers.analytics.AnalyticsHelper;
import com.vladislavfaust.jumpinsweetis.helpers.analytics.IFlurry;
import com.vladislavfaust.jumpinsweetis.helpers.googleplay.AchievementsHandler;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Faust on 02.04.2015.
 *
 */
public class FlurryAndroid implements IFlurry
{
    // Настройки
    private final static String API_KEY = "H79FCBX7XW2Q6RZ4CMYK";

    public FlurryAndroid (Activity activity)
    {
        FlurryAgent.setLogEnabled(true);
        FlurryAgent.init(activity.getApplicationContext(), API_KEY);
    }

    @Override
    public void trackGameStart()
    {
        FlurryAgent.logEvent("Game_Over", true);
    }

    @Override
    public void trackGameOver(String skinTexture, int score, int candies)
    {
        Map<String, String> params = new HashMap<>();

        params.put("Skin", skinTexture);
        params.put("Score", score+"");
        params.put("Candies", candies+"");

        FlurryAgent.endTimedEvent("Game_Over", params);
    }

    @Override
    public void trackShareClick(int score, boolean highscore, boolean confirmed)
    {
        Map<String, String> params = new HashMap<>();

        params.put("Score", score + "");
        params.put("Highscore", highscore+"");
        params.put("Confirmed", confirmed+"");

        FlurryAgent.logEvent("Share_Clicked", params);
    }

    @Override
    public void trackGiftsOffered()
    {
        FlurryAgent.logEvent("Gifts_Offered");
    }

    @Override
    public void trackIncentivizedOffered()
    {
        FlurryAgent.logEvent("Incentivized_Offered");
    }

    @Override
    public void trackIncentivizedClick(AdsManager.INCENTIVIZED_TYPE type, boolean success)
    {
        FlurryAgent.logEvent("Incentivized_Click");

        if (success)
            FlurryAgent.logEvent("Incentivized_Suggested_"+type.name());
    }

    @Override
    public void trackIncentivizedShown(AdsManager.INCENTIVIZED_SOURCE which)
    {
        Map<String, String> params = new HashMap<>();

        params.put("Source", which.name());

        FlurryAgent.logEvent("Incentivized_Shown", params);
    }

    @Override
    public void trackPollCancel()
    {
        FlurryAgent.logEvent("Poll_Cancel");
    }

    @Override
    public void trackGiftingScreen()
    {
        FlurryAgent.logEvent("Gifts_Shown");
    }

    @Override
    public void trackUnlockAllGiftsPurchased(boolean dialog)
    {
        Map<String, String> params = new HashMap<>();

        params.put("After_Dialog", dialog+"");

        FlurryAgent.logEvent("Unlock_Gifts_Purchase", params);
    }

    @Override
    public void trackBoostsScreen()
    {
        FlurryAgent.logEvent("Boosts_Screen_Shown");
    }

    @Override
    public void trackUpgradeBoost(BonusLevelingController.BOOST_TYPE which, int toLevel)
    {
        Map<String, String> params = new HashMap<>();

        params.put("Type", which.name());

        FlurryAgent.logEvent("Boost_Upgrade", params);
    }

    @Override
    public void trackConvert()
    {
        FlurryAgent.logEvent("Candies_Convert");
    }

    @Override
    public void trackBuyMoreTokensClick()
    {
        FlurryAgent.logEvent("BuyMore_Click");
    }

    @Override
    public void trackPurchaseTokensClick(IAP.PURCHASE_TYPE which, boolean success)
    {
        if (!success) return;

        Map<String, String> params = new HashMap<>();

        params.put("Which", which.name());

        FlurryAgent.logEvent("Token_Purchase", params);
    }

    @Override
    public void trackSkinScreen()
    {
        FlurryAgent.logEvent("Skins_Screen_Shown");
    }

    @Override
    public void trackUnlockUniqueClick()
    {
        FlurryAgent.logEvent("Unlock_Unique_Skin_Click");
    }

    @Override
    public void trackUnlockUniquePurchase(boolean currency, boolean hadAChoice)
    {
        if (hadAChoice)
        {
            Map<String, String> params = new HashMap<>();

            params.put("Currency", currency + "");

            FlurryAgent.logEvent("Unlock_Unique_Skin_Purchase_Choice", params);
        }
        else if (currency)
            FlurryAgent.logEvent("Unlock_Unique_Skin_Purchase_Currency");
    }

    @Override
    public void trackUnlockCommon(String texture, int price)
    {
        Map<String, String> params = new HashMap<>();

        params.put("Skin", texture);
        params.put("Price", price + "");

        FlurryAgent.logEvent("Unlock_Common_Skin", params);
    }

    @Override
    public void trackSelectBackground(BackgroundHelper.BACKGROUND background)
    {
        Map<String, String> params = new HashMap<>();

        params.put("Background", background.name());

        FlurryAgent.logEvent("Select_Background", params);
    }

    @Override
    public void trackFeedbackOffer(AnalyticsHelper.FEEDBACK_RESULT result)
    {
        Map<String, String> params = new HashMap<>();

        params.put("Result", result.name());

        FlurryAgent.logEvent("Feedback_Offered", params);
    }

    @Override
    public void trackMainMenuScreen()
    {
        FlurryAgent.logEvent("Main_Menu_Shown");
    }

    @Override
    public void trackSound(boolean enabled)
    {
        Map<String, String> params = new HashMap<>();

        params.put("Enabled", enabled + "");

        FlurryAgent.logEvent("Sound_Switch", params);
    }

    @Override
    public void trackMusic(boolean enabled)
    {
        Map<String, String> params = new HashMap<>();

        params.put("Enabled", enabled + "");

        FlurryAgent.logEvent("Music_Switch", params);
    }

    @Override
    public void trackPromocodeClick(boolean confirmed, String code)
    {
        FlurryAgent.logEvent("Promocode_Click");

        if (confirmed)
        {
            Map<String, String> params = new HashMap<>();

            params.put("Code", (code == null) ? "" : code);

            FlurryAgent.logEvent("Promocode_Enter", params);
        }
    }

    @Override
    public void trackLeaveFeedbackClick()
    {
        FlurryAgent.logEvent("Settings_Leave_Feedback_Click");
    }

    @Override
    public void trackSiteClick()
    {
        FlurryAgent.logEvent("Settings_Site_Click");
    }

    @Override
    public void trackProgressClick()
    {
        FlurryAgent.logEvent("Progress_Click");
    }

    @Override
    public void trackLeadersClick()
    {
        FlurryAgent.logEvent("Leaders_Click");
    }

    @Override
    public void trackMissionCompleted(int id)
    {
        Map<String, String> missionCompletedParams = new HashMap<>();
        missionCompletedParams.put("ID", id+"");

        FlurryAgent.logEvent("Mission_Completed", missionCompletedParams);
    }

    @Override
    public void trackAchievementUnlock(int id)
    {
        Map<String, String> params = new HashMap<>();

        params.put("Name", AchievementsHandler.getAchievements().get(id).getName());

        FlurryAgent.logEvent("Achievement_Unlock", params);
    }

    @Override
    public void trackAchievementShareScreen(int id, boolean success, boolean afterDialog)
    {
        if (!success) return;

        Map<String, String> params = new HashMap<>();

        params.put("Name", AchievementsHandler.getAchievements().get(id).getName());
        params.put("After_Dialog", afterDialog + "");

        FlurryAgent.logEvent("Achievement_Share", params);
    }

    @Override
    public void trackNoGPGSInstalled()
    {

    }

    @Override
    public void trackGPGSLogin(boolean success)
    {
        Map<String, String> params = new HashMap<>();

        params.put("Success", success + "");

        FlurryAgent.logEvent("GPGS_Login", params);
    }
}
