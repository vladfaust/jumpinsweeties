package com.vladislavfaust.jumpinsweeties.android;

import android.app.Activity;
import android.graphics.Color;
import android.widget.EditText;

import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.Theme;
import com.vladislavfaust.jumpinsweetis.helpers.dialogs.IConfirmDialog;
import com.vladislavfaust.jumpinsweetis.helpers.dialogs.IPromptDialog;
import com.vladislavfaust.jumpinsweetis.helpers.dialogs.IRequestHandler;
import com.vladislavfaust.jumpinsweetis.helpers.dialogs.IThreeOptionsDialog;

/**
 * Created by Faust on 21.03.2015.
 *
 */
public class RequestHandlerAndroid implements IRequestHandler
{
    private static RequestHandlerAndroid instance;

    public static RequestHandlerAndroid getInstance()
    {
        return instance;
    }

    private Activity activity;

    public RequestHandlerAndroid(Activity activity)
    {
        this.activity = activity;

        instance = this;
    }

    @Override
    public void confirm(final IConfirmDialog confirmDialog, final String title, final String message, final String ok, final String cancel)
    {
        activity.runOnUiThread(new Runnable()
        {
            @Override
            public void run()
            {
                new MaterialDialog.Builder(activity)
                        .title(title).titleColor(Color.rgb(252, 99, 90))
                        .content(message)
                        .theme(Theme.LIGHT)
                        .positiveText(ok)
                        .negativeText(cancel)
                        .callback(new MaterialDialog.ButtonCallback()
                        {
                            @Override
                            public void onNegative(MaterialDialog dialog)
                            {
                                confirmDialog.clickedCancel();
                                dialog.cancel();
                            }

                            @Override
                            public void onPositive(MaterialDialog dialog)
                            {
                                confirmDialog.clickedOK();
                                dialog.cancel();
                            }
                        })
                        .show();
            }
        });
    }

    @Override
    public void prompt(final IPromptDialog promptDialog, final String title, final String ok, final String cancel, final String hint)
    {
        activity.runOnUiThread(new Runnable()
        {
            @Override
            public void run()
            {
                final EditText input = new EditText(activity.getApplicationContext());

                input.setTextColor(Color.BLACK);
                input.setHintTextColor(Color.LTGRAY);

                if (hint != null) input.setHint(hint);

                new MaterialDialog.Builder(activity)
                        .title(title).titleColor(Color.rgb(252, 99, 90))
                        .theme(Theme.LIGHT)
                        .positiveText(ok)
                        .negativeText(cancel)
                        .customView(input, true)
                        .callback(new MaterialDialog.ButtonCallback()
                        {
                            @Override
                            public void onNegative(MaterialDialog dialog)
                            {
                                promptDialog.clickedCancel();
                                dialog.cancel();
                            }

                            @Override
                            public void onPositive(MaterialDialog dialog)
                            {
                                promptDialog.clickedOK(input.getText().toString());
                                dialog.cancel();
                            }
                        })
                        .show();
            }
        });
    }

    @Override
    public void threeOptions(final IThreeOptionsDialog threeOptionsDialog, final String title, final String message, final String positive, final String negative, final String neutral)
    {
        activity.runOnUiThread(new Runnable()
        {
            @Override
            public void run()
            {
                new MaterialDialog.Builder(activity)
                        .title(title).titleColor(Color.rgb(252, 99, 90))
                        .content(message)
                        .theme(Theme.LIGHT)
                        .positiveText(positive)
                        .negativeText(negative)
                        .neutralText(neutral)
                        .callback(new MaterialDialog.ButtonCallback()
                        {
                            @Override
                            public void onNeutral(MaterialDialog dialog)
                            {
                                threeOptionsDialog.clickedNeutral();
                                dialog.cancel();
                            }

                            @Override
                            public void onNegative(MaterialDialog dialog)
                            {
                                threeOptionsDialog.clickedNegative();
                                dialog.cancel();
                            }

                            @Override
                            public void onPositive(MaterialDialog dialog)
                            {
                                threeOptionsDialog.clickedPositive();
                                dialog.cancel();
                            }
                        })
                        .show();
            }
        });
    }
}
