package com.vladislavfaust.jumpinsweetis.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Array;
import com.vladislavfaust.jumpinsweetis.JumpinSweeties;
import com.vladislavfaust.jumpinsweetis.helpers.AssetLoader;
import com.vladislavfaust.jumpinsweetis.helpers.BackgroundHelper;

/**
 * Created by Faust on 19.01.2015.
 * Гарантируется синглтон.
 */
public class Background
{
    // Цвета
    private final static float
            COLOR_R = 255f/255f,
            COLOR_G = 241f/255f,
            COLOR_B = 217f/255f,
            COLOR_A = 1f;

    // Кол-во элементов всего (предположительно 3)
    private final static int MAX_ELEMENTS = 3;

    // Эффект параллакса
    private final static float PARALLAX = 0.7f;

    // Батч для отрисовки
    private SpriteBatch batch;

    // Лист элементов бэкграунда (предположительно 3 штуки)
    private final Array<Sprite> elements = new Array<Sprite>();

    // Инстанс синглтона
    private static Background _instance;

    // Геттер инстанса синглтона
    public static Background instance ()
    {
        return _instance;
    }

    // Конструктор
    public Background ()
    {
        _instance = this;

        // Создаем камеру
        OrthographicCamera camera = new OrthographicCamera();
        camera.setToOrtho(false, JumpinSweeties.instance().getGameWidth(), JumpinSweeties.instance().getGameHeight());

        // Создаем батч
        batch = new SpriteBatch();
        batch.setProjectionMatrix(camera.combined);
    }

    // Сброс позиции бэкграунда
    public void init ()
    {
        // Первый элемент сдвигаем в самый низ
        elements.get(0).setY(0f);

        // Далее остальные над ним
        for (int i = 1; i < MAX_ELEMENTS; i++)
        {
            elements.get(i).setY(elements.get(i - 1).getY() + elements.get(i - 1).getHeight());
        }
    }

    // Логика бэкграунда
    // Принимает в качестве аргумента сдвиг по y
    public void update (float yDelta)
    {
        // Перво-наперво сдвигаем элементы вниз
        for (int i = 0; i < MAX_ELEMENTS; i++)
        {
            elements.get(i).translateY(-yDelta * PARALLAX);
        }

        // Теперь, если они уехали за экран, то сдвигаем их относительно друг друга
        for (int i = 0; i < MAX_ELEMENTS; i++)
        {
            // Магическая хрень, которая равна (при максимуме = 3) 0 при i = 1, 1 при i = 2, 2 при i = 0
            int a = (i == 0) ? MAX_ELEMENTS - 1 - i : i - 1;

            // Если элемент ушел за край экрана
            if (elements.get(i).getY() + elements.get(i).getHeight() < 0f)
            {
                // Перемещаем его наверх другого
                elements.get(i).setY(elements.get(a).getY() + elements.get(a).getHeight());
            }
        }
    }

    // Рендер бэкграунда
    // Предполагается вызывать самым первым, поэтому очистка экрана обязательна
    public void render ()
    {
        // Очистка экрана (и заливка цветом)
        Gdx.gl.glClearColor(COLOR_R, COLOR_G, COLOR_B,COLOR_A);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        // Теперь отрисовка элементов
        batch.begin();
        for (int i = 0; i < MAX_ELEMENTS; i++)
        {
            elements.get(i).draw(batch);
        }
        batch.end();
    }

    // Смена бэкграунда
    public void setBackground (BackgroundHelper.BACKGROUND which)
    {
        elements.clear();

        // Первый создаем внизу
        Sprite element = new Sprite(AssetLoader.getBackground(which));
        element.setSize(element.getRegionWidth(), element.getRegionHeight());
        elements.add(element);

        // Далее остальные
        for (int i = 1; i < MAX_ELEMENTS; i++)
        {
            element = new Sprite(AssetLoader.getBackground(which));
            element.setSize(element.getRegionWidth(), element.getRegionHeight());
            element.setY(elements.get(i-1).getY() + elements.get(i-1).getHeight());
            elements.add(element);
        }
    }
}
