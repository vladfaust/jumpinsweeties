package com.vladislavfaust.jumpinsweetis.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Array;
import com.vladislavfaust.jumpinsweetis.JumpinSweeties;
import com.vladislavfaust.jumpinsweetis.helpers.ScreenshotHelper;
import com.vladislavfaust.jumpinsweetis.ui.screens.UIScreenBoosts;
import com.vladislavfaust.jumpinsweetis.ui.screens.UIScreenGame;
import com.vladislavfaust.jumpinsweetis.ui.screens.UIScreenGameOver;
import com.vladislavfaust.jumpinsweetis.ui.screens.UIScreenMainMenu;
import com.vladislavfaust.jumpinsweetis.ui.screens.UIScreenSkins;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.alpha;

/**
 * Created by Faust on 19.01.2015.
 * Гарантируется синглтон.
 */
public class UIRenderer
{
    // Заблокирована ли кнопка "назад" интерфейсом
    static boolean backButtonBlocked;
    // Внутренние переменные
    private Stage stage;
    private Skin skin;
    private Table root;    // root-таблица, корневая для сцены
    private Stack rootStack;
    private ShapeRenderer shapeRenderer;

    // Партикл-эффекты
    private static Array<ParticleEffect> particleEffects = new Array<>();

    // Шрифты
    BitmapFont font50, font60, font70, font80, font90;
    BitmapFont font60s, font70s;

    // Инстанс синглтона
    private static UIRenderer _instance;

    // Геттер инстанса синглтона
    public static UIRenderer instance ()
    {
        return _instance;
    }

    // Конструктор
    public UIRenderer ()
    {
        _instance = this;

        // Создаем stage
        stage = new Stage();

        // Настраиваем камеру
        OrthographicCamera camera = new OrthographicCamera();
        camera.setToOrtho(false, JumpinSweeties.instance().getGameWidth(), JumpinSweeties.instance().getGameHeight());

        // Настраиваем viewport
        stage.getViewport().setCamera(camera);
        stage.getViewport().setWorldSize(JumpinSweeties.instance().getGameWidth(), JumpinSweeties.instance().getGameHeight());

        // Включаем shapeRenderer
        shapeRenderer = new ShapeRenderer();
        shapeRenderer.setProjectionMatrix(camera.combined);

        // По необходимости включаем дебаг
        stage.setDebugAll(false);

        // Добавляем обработку нажатий в приложение
        JumpinSweeties.instance().addInputProcessor(stage);

        // Устанавливаем скин (тему) всего приложения
        skin = new Skin();

        // Генерируем 1х1 белую текстуру
        Pixmap pixmap = new Pixmap(1, 1, Pixmap.Format.RGBA8888);
        pixmap.setColor(Color.WHITE);
        pixmap.fill();
        skin.add("dummy", new Texture(pixmap));

        // Создаем root-таблицу
        rootStack = new Stack();
        rootStack.setFillParent(true);
        root = new Table(skin);
        root.setFillParent(true);
        rootStack.add(root);
        stage.addActor(rootStack);

        // Чистим статический массив
        particleEffects.clear();
    }

    public static boolean isBackButtonBlocked()
    {
        return backButtonBlocked;
    }

    public static void setBackButtonBlocked(boolean backButtonBlocked)
    {
        UIRenderer.backButtonBlocked = backButtonBlocked;
    }

    // Генерируем шрифты (медленно)
    public void generateFonts ()
    {
        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/troika.ttf"));
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.characters = "абвгдежзийклмнопрстуфхцчшщъыьэюяabcdefghijklmnopqrstuvwxyzАБВГДЕЖЗИЙКЛМНОПРСТУФХЦЧШЩЪЫЬЭЮЯABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789][_!$%#@|\\/?-+=()*&.;:,{}\"´`'<>\u20BD";
        parameter.kerning = true;

        /*
            Обычные шрифты
         */

        parameter.borderWidth = 0.5f;
        parameter.borderColor = Color.valueOf("ffffff00");

        parameter.size = 50;
        font50 = generator.generateFont(parameter);
        font50.getRegion().getTexture().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);

        parameter.size = 60;
        font60 = generator.generateFont(parameter);
        font60.getRegion().getTexture().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);

        parameter.size = 70;
        font70 = generator.generateFont(parameter);
        font70.getRegion().getTexture().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);

        parameter.size = 80;
        font80 = generator.generateFont(parameter);
        font70.getRegion().getTexture().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);

        parameter.size = 90;
        font90 = generator.generateFont(parameter);
        font90.getRegion().getTexture().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);

        skin.add("regular50", font50);
        skin.add("regular60", font60);
        skin.add("regular70", font70);
        skin.add("regular80", font80);
        skin.add("regular90", font90);

        /*
            Обведенные шрифты
         */

        parameter.borderWidth = 5f;
        parameter.borderColor = Color.valueOf("000000ff");

        parameter.size = 60;
        font60s = generator.generateFont(parameter);
        font60s.getRegion().getTexture().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);

        parameter.size = 70;
        font70s = generator.generateFont(parameter);
        font70s.getRegion().getTexture().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);

        skin.add("stroke60", font60s);
        skin.add("stroke70", font70s);

        /*
            Конец
         */

        generator.dispose();
    }

    // Рендерим stage без очистки экрана
    public void render (float delta)
    {
        stage.act(delta);

        updateEffects(stage.getBatch(), delta);

        stage.draw();

        if (ScreenshotHelper.wantToMakeScreenshot())
            ScreenshotHelper.makeScreenshot();
    }

    // Рендерим stage, предварительно очистив экран (или нет)
    public void render (float delta, boolean cleanScreen)
    {
        if (cleanScreen) {
            Gdx.gl.glClearColor(0.2f, 0.2f, 0.2f, 1f);
            Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        }

        render(delta);
    }

    // Апдейт и отрисовка всех партиклов
    private void updateEffects (Batch batch, float delta)
    {
        ParticleEffect effect;

        batch.begin();
        for (int i = 0; i < particleEffects.size; i++)
        {
            effect = particleEffects.get(i);
            effect.draw(stage.getBatch(), delta);

            if (effect.isComplete())
            {
                effect.dispose();
                particleEffects.removeIndex(i);
            }
        }
        batch.end();
    }

    // Добавить новый эффект
    public void addEffect (ParticleEffect effect)
    {
        particleEffects.add(effect);
        effect.start();
    }

    // Очистка всех эффектов
    public void clearEffects ()
    {
        ParticleEffect effect;
        while (particleEffects.size > 0 && (effect = particleEffects.get(0)) != null)
        {
            effect.dispose();
            particleEffects.removeIndex(0);
        }
    }

    // Этот метод очищает root-таблицу
    public void rootClear()
    {
        root.addAction(alpha(1f));
        root.clearChildren();
        root.pad(0);
        root.center();
        clearEffects();
    }

    // Метод растворяет меню и включает игровой экран
    public void tintMenuToGame ()
    {
        UIScreenMainMenu.fadeOut(root);
    }

    // Вызов метода меняет отображаемый UI-экран
    public void setScreen (JumpinSweeties.Screens which)
    {
        switch (which)
        {
            case LOADING:
                break;
            case MAIN_MENU:
                rootClear();
                UIScreenMainMenu.set(root);

                break;
            case GAME:
                rootClear();
                UIScreenGame.set(root);

                break;
            case GAME_OVER:
                rootClear();
                UIScreenGameOver.setUI(root);

                break;
            case SKINS:
                rootClear();
                UIScreenSkins.setUI(root);

                break;
            case BOOSTS:
                rootClear();
                UIScreenBoosts.setUI(root);

                break;
        }
    }

    // Очистка
    public void dispose ()
    {
        stage.dispose();
        skin.dispose();
    }

    public Stage getStage()
    {
        return stage;
    }

    public Skin getSkin()
    {
        return skin;
    }

    public Stack getRootStack()
    {
        return rootStack;
    }
}
