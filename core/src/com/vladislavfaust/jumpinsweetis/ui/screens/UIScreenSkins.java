package com.vladislavfaust.jumpinsweetis.ui.screens;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Scaling;
import com.vladislavfaust.jumpinsweetis.JumpinSweeties;
import com.vladislavfaust.jumpinsweetis.gamesystems.MissionController;
import com.vladislavfaust.jumpinsweetis.helpers.AssetLoader;
import com.vladislavfaust.jumpinsweetis.helpers.BackgroundHelper;
import com.vladislavfaust.jumpinsweetis.helpers.CandiesHandler;
import com.vladislavfaust.jumpinsweetis.helpers.IAP;
import com.vladislavfaust.jumpinsweetis.helpers.IStandardCallback;
import com.vladislavfaust.jumpinsweetis.helpers.JumperSkinsHandler;
import com.vladislavfaust.jumpinsweetis.helpers.Language;
import com.vladislavfaust.jumpinsweetis.helpers.SoundsHandler;
import com.vladislavfaust.jumpinsweetis.helpers.TokensHandler;
import com.vladislavfaust.jumpinsweetis.helpers.analytics.AnalyticsHelper;
import com.vladislavfaust.jumpinsweetis.helpers.dialogs.IConfirmDialog;
import com.vladislavfaust.jumpinsweetis.helpers.dialogs.IThreeOptionsDialog;
import com.vladislavfaust.jumpinsweetis.helpers.googleplay.AchievementsHandler;
import com.vladislavfaust.jumpinsweetis.ui.UICommonElements;
import com.vladislavfaust.jumpinsweetis.ui.UIRenderer;
import com.vladislavfaust.jumpinsweetis.ui.elements.UIShareAchievement;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.delay;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.moveBy;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.parallel;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.run;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.scaleTo;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;
import static com.vladislavfaust.jumpinsweetis.helpers.JumperSkinsHandler.JumperSkin;
import static com.vladislavfaust.jumpinsweetis.helpers.JumperSkinsHandler.SKINS_TYPE;
import static com.vladislavfaust.jumpinsweetis.helpers.JumperSkinsHandler.getSkins;
import static com.vladislavfaust.jumpinsweetis.helpers.JumperSkinsHandler.getTotalSkinCount;
import static com.vladislavfaust.jumpinsweetis.helpers.JumperSkinsHandler.getUnlockedSkinsCount;
import static com.vladislavfaust.jumpinsweetis.helpers.JumperSkinsHandler.unlockSkin;

/**
 * Created by Faust on 26.01.2015.
 *
 */
public class UIScreenSkins
{
    /*
        Переменные
     */

    public static Label labelCandies, labelCommon, labelUnique, labelAchievement, labelThemes;
    private final static Array<Table> skinsTables = new Array<>(), themesTables = new Array<>();

    /*
        Полезные методы
     */

    public static void updateCounters()
    {
        if (labelCandies != null)
            labelCandies.setText(Language.LOCALE.format("youHave", CandiesHandler.getCandies()));
        if (labelCommon != null)
            labelCommon.setText(Language.LOCALE.format("commonSkins", getUnlockedSkinsCount(SKINS_TYPE.COMMON), getTotalSkinCount(SKINS_TYPE.COMMON)));
        if (labelUnique != null)
            labelUnique.setText(Language.LOCALE.format("uniqueSkins", getUnlockedSkinsCount(SKINS_TYPE.UNIQUE), getTotalSkinCount(SKINS_TYPE.UNIQUE)));
        if (labelAchievement != null)
            labelAchievement.setText(Language.LOCALE.format("achievementSkins", getUnlockedSkinsCount(SKINS_TYPE.ACHIEVEMENT), getTotalSkinCount(SKINS_TYPE.ACHIEVEMENT)));
        if (labelThemes != null)
            labelThemes.setText(Language.LOCALE.format("themes", BackgroundHelper.getUnlockedBackgroundsCount(), BackgroundHelper.getTotalBackgroundsCount()));
    }

    private static void setSkinSelectable (final Table table, final JumperSkin skin)
    {
        table.clearListeners();

        table.setTouchable(Touchable.enabled);

        table.addListener(new ClickListener()
        {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button)
            {
                return true;
            }

            public void touchUp(final InputEvent event, float x, float y, int pointer, int button)
            {
                if (event.isTouchFocusCancel())
                    return;

                JumperSkinsHandler.selectSkin(skin.getId());

                table.setTransform(true);
                table.setOrigin(Align.center);
                table.addAction(parallel(scaleTo(1.7f, 1.7f, 0.2f, Interpolation.swingIn), sequence(delay(0.17f), run(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        JumpinSweeties.instance().gotoMainMenu();
                    }
                }))));

                SoundsHandler.selectSkin();
            }
        });
    }

    private static void unlockSkinVisual (Table table, JumperSkin skin)
    {
        table.setBackground(new TextureRegionDrawable(AssetLoader.getUi().findRegion("whiterectangle")));
        final Image image = new Image(AssetLoader.getJumperSkins().findRegion(skin.getTextureName()));
        image.setAlign(Align.center);

        table.clearChildren();
        table.add(image).fill().expand().center();
    }

    private static void setSkinUnlockable (final Table table, final JumperSkin skin)
    {
        table.clearListeners();

        table.setTouchable(Touchable.enabled);

        table.addListener(new ClickListener()
        {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button)
            {
                return true;
            }

            public void touchUp(final InputEvent event, float x, float y, int pointer, int button)
            {
                if (event.isTouchFocusCancel())
                    return;

                /*
                    Пытаемся анлокнуть
                 */

                if (skin.getType() == SKINS_TYPE.COMMON && CandiesHandler.buySkin(skin)) {

                    table.setTransform(true);
                    table.setOrigin(Align.center);

                    table.addAction(sequence(scaleTo(0.0f, 0.0f, 0.2f, Interpolation.swingIn), run(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            unlockSkin(skin.getId());

                            unlockSkinVisual(table, skin);
                            setSkinSelectable(table, skin);

                            updateCounters();
                        }
                    }), scaleTo(1f, 1f, 0.6f, Interpolation.swingOut)));

                    SoundsHandler.unlockSkin();

                    MissionController.unlockSkin(true);
                    AnalyticsHelper.trackUnlockCommon(skin.getTextureName(), skin.getPrice());
                }

                /*
                    Неудачно
                 */

                else if (skin.getType() == SKINS_TYPE.ACHIEVEMENT) {
                    if (JumpinSweeties.instance().getGooglePlayServices().getSignedInGPGS()) {
                        JumpinSweeties.instance().getPlatformUtils().showMessage(Language.LOCALE.get("youHaveNotUnlockedThisSkinYet"));
                        UICommonElements.error(table);
                    } else
                        JumpinSweeties.instance().getRequestHandler().confirm(new IConfirmDialog()
                                                                              {
                                                                                  @Override
                                                                                  public void clickedOK()
                                                                                  {
                                                                                      JumpinSweeties.instance().getGooglePlayServices().loginGPGS();
                                                                                      JumpinSweeties.instance().gotoMainMenu();
                                                                                  }

                                                                                  @Override
                                                                                  public void clickedCancel()
                                                                                  {
                                                                                      UICommonElements.error(table);
                                                                                  }
                                                                              }, Language.LOCALE.get("confirm"), Language.LOCALE.get("noGPGSmessage"), Language.LOCALE.get("loginGPGS"), Language.LOCALE.get("cancel"));
                } else {
                    UICommonElements.error(table);
                }
            }
        });
    }

    private static void unlockRandomSkin ()
    {
        int id = JumperSkinsHandler.getRandomLockedSkinId(false, true);
        final JumperSkin skin = JumperSkinsHandler.getSkins().get(id);

        final Table skinTable = skinsTables.get(id);

        skinTable.setTransform(true);
        skinTable.setOrigin(Align.center);

        skinTable.addAction(sequence(scaleTo(0.0f, 0.0f, 0.2f, Interpolation.swingIn), run(new Runnable()
        {
            @Override
            public void run()
            {
                unlockSkin(skin.getId());

                unlockSkinVisual(skinTable, skin);
                setSkinSelectable(skinTable, skin);

                updateCounters();
            }
        }), scaleTo(1f, 1f, 0.6f, Interpolation.swingOut)));

        SoundsHandler.unlockSkin();

        MissionController.unlockSkin(true);
    }

    private static BackgroundHelper.BACKGROUND getBackgroundByTable (Table which)
    {
        for (int i = 0; i < themesTables.size; i++)
            if (themesTables.get(i) == which)
                return BackgroundHelper.BACKGROUND.values()[i];

        return null;
    }

    /*
        Установка экрана
     */

    public static void setUI (final Table rootTable)
    {
        final Table scrollTable = new Table();
        final ScrollPane scrollPane = new ScrollPane(scrollTable);
        rootTable.add(scrollPane);
        rootTable.row();

        AnalyticsHelper.trackSkinScreen();

        skinsTables.clear();

        /*
            Счетчик конфеток
         */

        labelCandies = new Label(Language.LOCALE.format("youHave", CandiesHandler.getCandies()), UIRenderer.instance().getSkin(), "regular70", UICommonElements.COLOR_GREEN);
        scrollTable.add(labelCandies).center().padTop(35f);
        scrollTable.row();

        /*
            Обычные скины
         */

        labelCommon = new Label(Language.LOCALE.format("commonSkins", getUnlockedSkinsCount(SKINS_TYPE.COMMON), getTotalSkinCount(SKINS_TYPE.COMMON)), UIRenderer.instance().getSkin(), "regular60", UICommonElements.COLOR_RED);
        scrollTable.add(labelCommon).center().spaceBottom(33f);
        scrollTable.row();

        final Table commonSkinTable = new Table();

        int column = 0;
        for (int i = 0; i < getTotalSkinCount(null); i++)
        {
            final JumperSkin skin = getSkins().get(i);

            if (skin.getType() != SKINS_TYPE.COMMON)
                continue;

            final Table skinTable = new Table();

            if (skin.isLocked())
            {
                skinTable.setBackground(new TextureRegionDrawable(AssetLoader.getUi().findRegion("redrectangle")));

                final Label priceLabel = new Label(skin.getPrice()+"", UIRenderer.instance().getSkin(), "regular70", Color.WHITE);
                priceLabel.setAlignment(Align.center);

                skinTable.add(priceLabel).center().expand().padBottom(3f);

                setSkinUnlockable(skinTable, skin);
            }
            else
            {
                skinTable.setBackground(new TextureRegionDrawable(AssetLoader.getUi().findRegion("whiterectangle")));

                final Image skinIcon = new Image(AssetLoader.getJumperSkins().findRegion(skin.getTextureName()));
                skinIcon.setAlign(Align.center);

                skinTable.add(skinIcon).center().fill();

                setSkinSelectable(skinTable, skin);
            }

            switch (column)
            {
                case 0:
                    commonSkinTable.add(skinTable).spaceBottom(49f).spaceRight(49f).width(184f).height(184f);
                    column++;
                    break;
                case 1:
                    commonSkinTable.add(skinTable).spaceBottom(49f).spaceRight(49f).width(184f).height(184f);
                    column++;
                    break;
                case 2:
                    commonSkinTable.add(skinTable).spaceBottom(49f).width(184f).height(184f);
                    column = 0;
                    commonSkinTable.row();
                    break;
            }

            skinsTables.add(skinTable);
        }

        scrollTable.add(commonSkinTable).padLeft(59f).padRight(59f).padBottom(35f).row();

        /*
            Особые
         */

        labelUnique = new Label(Language.LOCALE.format("uniqueSkins", getUnlockedSkinsCount(SKINS_TYPE.UNIQUE), getTotalSkinCount(SKINS_TYPE.UNIQUE)), UIRenderer.instance().getSkin(), "regular60", UICommonElements.COLOR_UNIQUE);
        scrollTable.add(labelUnique).center().spaceBottom(33f);
        scrollTable.row();
        
        /*
            Кнопка "открыть случайный"
         */

        final Stack unlockRandomButton = new Stack();
        final Image unlockRandomBackground = new Image(AssetLoader.getUi().findRegion("open-all-gifts-background"));
        unlockRandomButton.addActor(unlockRandomBackground);

        final Label unlockRandomLabel = new Label(Language.LOCALE.get("openRandomSkin"), UIRenderer.instance().getSkin(), "regular60", Color.WHITE);
        unlockRandomLabel.setAlignment(Align.center);
        unlockRandomButton.addActor(unlockRandomLabel);

        final Image unlockRandomIcon = new Image(AssetLoader.getUi().findRegion("unlock-random-skin"));
        unlockRandomIcon.setScaling(Scaling.none);
        unlockRandomIcon.setAlign(Align.center);
        unlockRandomButton.addActor(unlockRandomIcon);

        final Table priceTable = new Table();

        final Label unlockRandomPriceCurrency = UICommonElements.priceLabel(IAP.PURCHASE_TYPE.UNLOCK_UNIQUE_SKIN);
        unlockRandomPriceCurrency.setAlignment(Align.left);
        priceTable.add(unlockRandomPriceCurrency).left();

        final Label unlockRandomPriceTokens = new Label("/"+ TokensHandler.UNLOCK_UNIQUE_PRICE, UIRenderer.instance().getSkin(), "stroke60", Color.WHITE);
        unlockRandomPriceTokens.setAlignment(Align.right);
        priceTable.add(unlockRandomPriceTokens).left().padLeft(-5f);

        final Image tokenIcon = new Image(AssetLoader.getUi().findRegion("token"));
        tokenIcon.setAlign(Align.right);
        priceTable.add(tokenIcon).left();

        unlockRandomButton.addActor(priceTable);

        unlockRandomButton.pack();

        unlockRandomLabel.addAction(moveBy(-80f, 0f));
        unlockRandomIcon.addAction(moveBy(218f, 0f));
        priceTable.addAction(moveBy(200f, -103f));

        unlockRandomButton.setOrigin(Align.center);
        unlockRandomButton.setTransform(true);
        unlockRandomButton.setTouchable(Touchable.enabled);
        unlockRandomButton.addListener(new InputListener()
        {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button)
            {
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button)
            {
                if (event.isTouchFocusCancel())
                    return;

                event.cancel();

                UICommonElements.click(unlockRandomButton);

                AnalyticsHelper.trackUnlockUniqueClick();

                /*
                    Если токенов достаточно, то показываем выбор
                 */

                if (TokensHandler.getTotalTokens() >= TokensHandler.UNLOCK_UNIQUE_PRICE)
                {
                    JumpinSweeties.instance().getRequestHandler().threeOptions(new IThreeOptionsDialog() {
                        @Override
                        public void clickedNegative()
                        {
                            TokensHandler.spendTokens(TokensHandler.UNLOCK_UNIQUE_PRICE);
                            unlockRandomSkin();

                            JumpinSweeties.instance().getPlatformUtils().showMessage(Language.LOCALE.format("tokensLeft", TokensHandler.getTotalTokens()));

                            AnalyticsHelper.trackUnlockUniquePurchase(false, true);
                        }

                        @Override
                        public void clickedPositive()
                        {
                            IAP.purchase(new IStandardCallback()
                            {
                                @Override
                                public void success()
                                {
                                    unlockRandomSkin();
                                    JumpinSweeties.instance().getPlatformUtils().showMessage(Language.LOCALE.get("purchaseSuccess"));

                                    AnalyticsHelper.trackUnlockUniquePurchase(true, true);
                                }

                                @Override
                                public void error(String e)
                                {
                                    JumpinSweeties.instance().getPlatformUtils().showMessage(Language.LOCALE.format("purchaseError", e));
                                }

                                @Override
                                public void cancelled()
                                {
                                    JumpinSweeties.instance().getPlatformUtils().showMessage(Language.LOCALE.get("purchaseCancel"));
                                }
                            }, IAP.PURCHASE_TYPE.UNLOCK_UNIQUE_SKIN);
                        }

                        @Override
                        public void clickedNeutral()
                        {

                        }
                    },
                            Language.LOCALE.get("unlockRandomSelectTitle"),
                            Language.LOCALE.format("unlockRandomSelectMessage", TokensHandler.getTotalTokens()),
                            Language.LOCALE.get("currency"),
                            Language.LOCALE.get("tokens"),
                            Language.LOCALE.get("cancel")
                    );
                }
                else
                    IAP.purchase(new IStandardCallback()
                    {
                        @Override
                        public void success()
                        {
                            unlockRandomSkin();
                            JumpinSweeties.instance().getPlatformUtils().showMessage(Language.LOCALE.get("purchaseSuccess"));

                            AnalyticsHelper.trackUnlockUniquePurchase(true, false);
                        }

                        @Override
                        public void error(String e)
                        {
                            JumpinSweeties.instance().getPlatformUtils().showMessage(Language.LOCALE.format("purchaseError", e));
                        }

                        @Override
                        public void cancelled()
                        {
                            JumpinSweeties.instance().getPlatformUtils().showMessage(Language.LOCALE.get("purchaseCancel"));
                        }
                    }, IAP.PURCHASE_TYPE.UNLOCK_UNIQUE_SKIN);
            }
        });

        scrollTable.add(unlockRandomButton).center().spaceBottom(43f).row();
        
        /*
            Список особых скинов
         */

        final Table uniqueSkinsTable = new Table();

        column = 0;
        for (int i = 0; i < getTotalSkinCount(null); i++)
        {
            final JumperSkin skin = getSkins().get(i);

            if (skin.getType() != SKINS_TYPE.UNIQUE)
                continue;

            final Table skinTable = new Table();

            if (skin.isLocked())
            {
                skinTable.setBackground(new TextureRegionDrawable(AssetLoader.getUi().findRegion("unique-skin")));

                setSkinUnlockable(skinTable, skin);
            }
            else
            {
                skinTable.setBackground(new TextureRegionDrawable(AssetLoader.getUi().findRegion("whiterectangle")));

                final Image skinIcon = new Image(AssetLoader.getJumperSkins().findRegion(skin.getTextureName()));
                skinIcon.setAlign(Align.center);

                skinTable.add(skinIcon).center().fill();

                setSkinSelectable(skinTable, skin);
            }

            switch (column)
            {
                case 0:
                    uniqueSkinsTable.add(skinTable).spaceBottom(49f).spaceRight(49f).width(184f).height(184f);
                    column++;
                    break;
                case 1:
                    uniqueSkinsTable.add(skinTable).spaceBottom(49f).spaceRight(49f).width(184f).height(184f);
                    column++;
                    break;
                case 2:
                    uniqueSkinsTable.add(skinTable).spaceBottom(49f).width(184f).height(184f);
                    column = 0;
                    uniqueSkinsTable.row();
                    break;
            }

            skinsTables.add(skinTable);
        }

        scrollTable.add(uniqueSkinsTable).padLeft(59f).padRight(59f).padBottom(35f).row();

        /*
            За достижения
         */

        labelAchievement = new Label(Language.LOCALE.format("achievementSkins", getUnlockedSkinsCount(SKINS_TYPE.ACHIEVEMENT), getTotalSkinCount(SKINS_TYPE.ACHIEVEMENT)), UIRenderer.instance().getSkin(), "regular60", UICommonElements.COLOR_ACHIEVEMENT);
        scrollTable.add(labelAchievement).center().spaceBottom(33f);
        scrollTable.row();

        final Table achievementSkinsTable = new Table();

        column = 0;
        for (int i = 0; i < getTotalSkinCount(null); i++)
        {
            final JumperSkin skin = getSkins().get(i);

            if (skin.getType() != SKINS_TYPE.ACHIEVEMENT)
                continue;

            final Table skinTable = new Table();

            if (skin.isLocked())
            {
                skinTable.setBackground(new TextureRegionDrawable(AssetLoader.getUi().findRegion("achievement-skin")));

                setSkinUnlockable(skinTable, skin);
            }
            else
            {
                skinTable.setBackground(new TextureRegionDrawable(AssetLoader.getUi().findRegion("whiterectangle")));

                final Stack achievementSkinStack = new Stack();

                final Image skinIcon = new Image(AssetLoader.getJumperSkins().findRegion(skin.getTextureName()));
                skinIcon.setScaling(Scaling.fill);
                skinIcon.setAlign(Align.center);
                achievementSkinStack.add(skinIcon);

                final Image achievementIcon = new Image(AssetLoader.getUi().findRegion("achievement", AchievementsHandler.getIdBySkinTexture(skin.getTextureName())));
                achievementIcon.setScaling(Scaling.none);
                achievementIcon.setScale(0.36f);
                achievementIcon.setAlign(Align.center);
                achievementSkinStack.add(achievementIcon);

                achievementSkinStack.pack();

                achievementIcon.addAction(sequence(delay(0f), moveBy(-20f, -20f)));

                skinTable.add(achievementSkinStack).center().fill().row();
                setSkinSelectable(skinTable, skin);
            }

            switch (column)
            {
                case 0:
                    achievementSkinsTable.add(skinTable).spaceBottom(49f).spaceRight(49f).width(184f).height(184f);
                    column++;
                    break;
                case 1:
                    achievementSkinsTable.add(skinTable).spaceBottom(49f).spaceRight(49f).width(184f).height(184f);
                    column++;
                    break;
                case 2:
                    achievementSkinsTable.add(skinTable).spaceBottom(49f).width(184f).height(184f);
                    column = 0;
                    achievementSkinsTable.row();
                    break;
            }

            skinsTables.add(skinTable);
        }

        scrollTable.add(achievementSkinsTable).padLeft(59f).padRight(59f).padBottom(35f).row();

        /*
            Темы
         */

        labelThemes = new Label("", UIRenderer.instance().getSkin(), "regular60", UICommonElements.COLOR_GREEN);
        scrollTable.add(labelThemes).center().spaceBottom(33f);
        scrollTable.row();

        Table themesTable = new Table();
        scrollTable.add(themesTable).center().padBottom(49f);

        themesTables.clear();
        for (int i = 0; i < BackgroundHelper.BACKGROUND.values().length; i++)
        {
            BackgroundHelper.BACKGROUND background = BackgroundHelper.BACKGROUND.values()[i];

            final Table table = new Table();
            table.setBackground(new TextureRegionDrawable(AssetLoader.getBackgroundPreview(background)));

            table.setTransform(true);
            table.setTouchable(Touchable.enabled);

            if (BackgroundHelper.isBackgroundUnlocked(background))
                table.addListener(new InputListener()
                {
                    public boolean touchDown (InputEvent event, float x, float y, int pointer, int button)
                    {
                        return true;
                    }
                    public void touchUp (InputEvent event, float x, float y, int pointer, int button)
                    {
                        if (event.isTouchFocusCancel())
                            return;

                        SoundsHandler.button();
                        BackgroundHelper.setBackground(getBackgroundByTable((Table) event.getListenerActor()));

                        AnalyticsHelper.trackSelectBackground(getBackgroundByTable(((Table) event.getListenerActor())));
                    }
                });
            else
            {
                table.addListener(new InputListener()
                {
                    public boolean touchDown(InputEvent event, float x, float y, int pointer, int button)
                    {
                        return true;
                    }

                    public void touchUp(InputEvent event, float x, float y, int pointer, int button)
                    {
                        if (event.isTouchFocusCancel())
                            return;

                        UICommonElements.error(event.getListenerActor());
                    }
                });

                table.setColor(0.5f, 0.5f, 0.5f, 0.6f);

                Label price = new Label(Language.LOCALE.format("scoreToUnlockTheme", BackgroundHelper.howMuchScoreBackgroundCosts(BackgroundHelper.BACKGROUND.values()[i])), UIRenderer.instance().getSkin(), "regular60", Color.WHITE);
                price.setAlignment(Align.center);

                table.add(price).center().expand();
            }

            themesTable.add(table).top().width(651f).height(184f).spaceBottom(49f).row();

            themesTables.add(table);
        }

        updateCounters();

        if (AchievementsHandler.getNexSharedId() != -1)
            UIShareAchievement.show(AchievementsHandler.getNexSharedId());
    }
}
