package com.vladislavfaust.jumpinsweetis.ui.screens;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.vladislavfaust.jumpinsweetis.ui.UICommonElements;
import com.vladislavfaust.jumpinsweetis.ui.UIRenderer;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.run;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.scaleTo;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;

/**
 * Created by Faust on 25.01.2015.
 *
 */
public class UIScreenGame
{
    // Изменяемые элементы
    private static Label inGameScoresLabel;
    private final static Table scoresTable = new Table();

    public static void set (Table rootTable)
    {
        // Добавим счетчик очков
        inGameScoresLabel = new Label("0", UIRenderer.instance().getSkin(), "regular70", UICommonElements.COLOR_RED);
        scoresTable.clear();
        scoresTable.add(inGameScoresLabel);

        // Добавим его в таблицу
        rootTable.add(scoresTable);

        // Красотень
        rootTable.top().right();
        rootTable.padTop(24f);
        rootTable.padRight(32f);
    }

    // Изменяет счетчик очков
    public static void setScore (final int score)
    {
        scoresTable.setTransform(true);
        scoresTable.setOrigin(Align.center);

        scoresTable.addAction(sequence(
                scaleTo(1.2f, 1.2f, 0.2f, Interpolation.linear),
                run(new Runnable() {
                    @Override
                    public void run()
                    {
                        inGameScoresLabel.setText(score + "");
                    }
                }),
                scaleTo(1f, 1f, 0.4f, Interpolation.elasticOut)
        ));
    }
}
