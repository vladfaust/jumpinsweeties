package com.vladislavfaust.jumpinsweetis.ui.screens;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.scenes.scene2d.ui.Container;
import com.badlogic.gdx.scenes.scene2d.ui.HorizontalGroup;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Scaling;
import com.vladislavfaust.jumpinsweetis.JumpinSweeties;
import com.vladislavfaust.jumpinsweetis.gamesystems.MissionController;
import com.vladislavfaust.jumpinsweetis.helpers.AssetLoader;
import com.vladislavfaust.jumpinsweetis.helpers.JumperSkinsHandler;
import com.vladislavfaust.jumpinsweetis.helpers.Language;
import com.vladislavfaust.jumpinsweetis.helpers.SoundsHandler;
import com.vladislavfaust.jumpinsweetis.helpers.analytics.AnalyticsHelper;
import com.vladislavfaust.jumpinsweetis.helpers.googleplay.AchievementsHandler;
import com.vladislavfaust.jumpinsweetis.ui.UICommonElements;
import com.vladislavfaust.jumpinsweetis.ui.UIHandler;
import com.vladislavfaust.jumpinsweetis.ui.UIRenderer;
import com.vladislavfaust.jumpinsweetis.ui.elements.UISettingsMenu;
import com.vladislavfaust.jumpinsweetis.ui.elements.UIShareAchievement;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.forever;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.moveBy;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.run;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;

/**
 * Created by Faust on 25.01.2015.
 *
 */
public class UIScreenMainMenu
{
    private static Image jumperImage;
    private static Table tapToStartTable;

    public static void fadeOut (Table rootTable)
    {
        jumperImage.setVisible(false);

        tapToStartTable.setTouchable(Touchable.disabled);

        rootTable.addAction(new SequenceAction(Actions.fadeOut(0.3f), run(new Runnable()
        {
            @Override
            public void run()
            {
                UIRenderer.instance().setScreen(JumpinSweeties.Screens.GAME);
            }
        })));
    }

    public static void set (Table rootTable)
    {
        // Верх
        final Table upperTable = new Table();

        // Ачивки
        final Stack achievementsStack = new Stack();

        final Image achievementsImage = new Image(AssetLoader.getUi().findRegion("achievements-background"));
        final Label achievementsTitle = new Label(Language.LOCALE.get("achievements"), UIRenderer.instance().getSkin(), "regular70", Color.WHITE);
        achievementsTitle.setAlignment(Align.center);

        achievementsStack.add(achievementsImage);
        achievementsStack.add(achievementsTitle);

        achievementsStack.addListener(new InputListener()
        {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button)
            {
                if (!JumpinSweeties.instance().getGooglePlayServices().getSignedInGPGS())
                    JumpinSweeties.instance().getGooglePlayServices().loginGPGS();
                else
                    JumpinSweeties.instance().getGooglePlayServices().getAchievementsGPGS();

                SoundsHandler.button();

                AnalyticsHelper.trackProgressClick();
                return false;
            }
        });

        upperTable.add(achievementsStack).expand();
        
        // Лидерборд
        final Stack leaderboardStack = new Stack();

        final Image leaderboardImage = new Image(AssetLoader.getUi().findRegion("leaderboard-background"));
        final Label leaderboardTitle = new Label(Language.LOCALE.get("leaders"), UIRenderer.instance().getSkin(), "regular70", Color.WHITE);
        leaderboardTitle.setAlignment(Align.center);

        leaderboardStack.add(leaderboardImage);
        leaderboardStack.add(leaderboardTitle);

        leaderboardStack.addListener(new InputListener()
        {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button)
            {
                if (!JumpinSweeties.instance().getGooglePlayServices().getSignedInGPGS())
                    JumpinSweeties.instance().getGooglePlayServices().loginGPGS();
                else
                    JumpinSweeties.instance().getGooglePlayServices().getLeaderboardGPGS();

                SoundsHandler.button();

                AnalyticsHelper.trackLeadersClick();
                return false;
            }
        });

        upperTable.add(leaderboardStack).expand();
        rootTable.add(upperTable).top().row();

        // Середина
        // Стак
        final Stack middleStack = new Stack();

        // Задания
        final Label missionBrief = new Label("", UIRenderer.instance().getSkin(), "regular60", UICommonElements.COLOR_ORANGE);
        missionBrief.setText(MissionController.getBrief(MissionController.getCurrentId()));
        Container<Label> container = new Container<>(missionBrief);
        container.align(Align.top);
        middleStack.add(container);

        // Добавим таблицу, клик по которой начинает игру
        tapToStartTable = new Table();

        // Эта таблица должна реагировать на клики
        tapToStartTable.setTouchable(Touchable.enabled);
        tapToStartTable.addListener(new InputListener()
        {
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button)
            {
                UIHandler.tapToStartClicked(
                        jumperImage.localToStageCoordinates(new Vector2(jumperImage.getOriginX(), jumperImage.getOriginY()))
                        , (x > JumpinSweeties.instance().getGameWidth()/2f));
                return false;
            }
        });

        // Таблица с копией джампера
        final Table jumperGroup = new Table();

        // Tap left
        final Table tapLeftGroup = new Table();

        final Label tapLabel = new Label(Language.LOCALE.get("tap"), UIRenderer.instance().getSkin(), "regular60", UICommonElements.COLOR_RED);
        tapLeftGroup.add(tapLabel).right();
        tapLeftGroup.row();

        final Label leftLabel = new Label(Language.LOCALE.get("left"), UIRenderer.instance().getSkin(), "regular60", UICommonElements.COLOR_RED);
        tapLeftGroup.add(leftLabel).right();

        jumperGroup.add(tapLeftGroup);

        // Сам джампер
        jumperImage = new Image(JumperSkinsHandler.getSelectedJumperTexture());

        // Его анимация
        final float duration = 0.330f, xAxis = 17.0f, yAxis = 0.0f;
        jumperImage.addAction(forever(sequence(moveBy(-xAxis, 0f, duration), moveBy(xAxis * 2f, 0f, duration * 2.0f), moveBy(-xAxis, 0f, duration))));
        jumperImage.addAction(forever(sequence(moveBy(0f, -yAxis, duration), moveBy(0f, yAxis, duration))));

        jumperGroup.add(jumperImage).center();

        // Tap right
        final Table orRightGroup = new Table();

        final Label orLabel = new Label(Language.LOCALE.get("or"), UIRenderer.instance().getSkin(), "regular60", UICommonElements.COLOR_RED);
        orRightGroup.add(orLabel).left();
        orRightGroup.row();

        final Label rightLabel = new Label(Language.LOCALE.get("right"), UIRenderer.instance().getSkin(), "regular60", UICommonElements.COLOR_RED);
        orRightGroup.add(rightLabel).left();

        jumperGroup.add(orRightGroup);

        tapToStartTable.add(jumperGroup).expand().center();
        middleStack.add(tapToStartTable);

        // Нижняя таблица
        final Table bottomTable = new Table();

        // Счетчики
        bottomTable.add(UICommonElements.thoseTwoValues(true, true)).center().expandX().bottom();
        bottomTable.align(Align.bottom);
        middleStack.add(bottomTable);

        // Добавим эту таблицу в корень
        rootTable.add(middleStack).expand().fillY().width(768f).padTop(35f).padBottom(35f);
        rootTable.row();

        // Таблица нижних кнопок
        HorizontalGroup horizontalGroup = new HorizontalGroup();

        // Настройки
        final Stack settingsStack = new Stack();

        final Image settingsButtonBackground = new Image(AssetLoader.getUi().findRegion("settings-button-background"));
        final Image settingsButtonIcon = new Image(AssetLoader.getUi().findRegion("settings-icon"));
        settingsButtonIcon.setScaling(Scaling.none);

        settingsStack.add(settingsButtonBackground);
        settingsStack.add(settingsButtonIcon);

        settingsStack.addListener(new InputListener()
        {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button)
            {
                UISettingsMenu.show();
                SoundsHandler.button();
                return false;
            }
        });

        horizontalGroup.addActor(settingsStack);
        
        // Скины
        final Stack skinsStack = new Stack();

        final Image skinsShopImage = new Image(AssetLoader.getUi().findRegion("skins-background"));
        final Label skinsShopTitle = new Label(Language.LOCALE.get("changeSkin"), UIRenderer.instance().getSkin(), "regular70", Color.WHITE);
        skinsShopTitle.setAlignment(Align.center);

        skinsStack.add(skinsShopImage);
        skinsStack.add(skinsShopTitle);

        skinsStack.addListener(new InputListener()
        {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button)
            {
                SoundsHandler.button();
                JumpinSweeties.instance().gotoSkinsScreen();
                return false;
            }
        });

        // Добавляем его
        horizontalGroup.addActor(skinsStack);

        // Бусты
        final Stack boostsStack = new Stack();

        final Image boostsButtonBackground = new Image(AssetLoader.getUi().findRegion("boosts-button-background"));
        final Image boostsButtonIcon = new Image(AssetLoader.getUi().findRegion("boosts-icon"));
        boostsButtonIcon.setScaling(Scaling.none);

        boostsStack.add(boostsButtonBackground);
        boostsStack.add(boostsButtonIcon);

        boostsStack.addListener(new InputListener()
        {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button)
            {
                JumpinSweeties.instance().gotoBoostsScreen();
                SoundsHandler.button();
                return false;
            }
        });

        horizontalGroup.addActor(boostsStack);
        rootTable.add(horizontalGroup).bottom().expandX().fillX();

        // Заполняем белым цветом (для плавного появления)
        final Image whiteImageMainMenu = new Image(AssetLoader.getWhiteScreenTexture());
        UIRenderer.instance().getStage().addActor(whiteImageMainMenu);
        whiteImageMainMenu.addAction(new SequenceAction(Actions.fadeOut(0.2f), run(new Runnable()
        {
            @Override
            public void run()
            {
                whiteImageMainMenu.remove();
            }
        })));

        if (AchievementsHandler.getNexSharedId() != -1)
            UIShareAchievement.show(AchievementsHandler.getNexSharedId());
    }
}
