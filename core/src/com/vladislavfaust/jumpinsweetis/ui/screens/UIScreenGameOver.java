package com.vladislavfaust.jumpinsweetis.ui.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.scenes.scene2d.ui.HorizontalGroup;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Timer;
import com.vladislavfaust.jumpinsweetis.JumpinSweeties;
import com.vladislavfaust.jumpinsweetis.gamesystems.MissionController;
import com.vladislavfaust.jumpinsweetis.gamesystems.ScoreHandler;
import com.vladislavfaust.jumpinsweetis.helpers.AssetLoader;
import com.vladislavfaust.jumpinsweetis.helpers.CandiesHandler;
import com.vladislavfaust.jumpinsweetis.helpers.HighscoreHandler;
import com.vladislavfaust.jumpinsweetis.helpers.Language;
import com.vladislavfaust.jumpinsweetis.helpers.MyPreferences;
import com.vladislavfaust.jumpinsweetis.helpers.ScreenShaker;
import com.vladislavfaust.jumpinsweetis.helpers.SoundsHandler;
import com.vladislavfaust.jumpinsweetis.helpers.advertisment.AdsManager;
import com.vladislavfaust.jumpinsweetis.helpers.analytics.AnalyticsHelper;
import com.vladislavfaust.jumpinsweetis.helpers.dialogs.IConfirmDialog;
import com.vladislavfaust.jumpinsweetis.helpers.dialogs.IThreeOptionsDialog;
import com.vladislavfaust.jumpinsweetis.helpers.gifts.GiftsCooldownHelper;
import com.vladislavfaust.jumpinsweetis.helpers.googleplay.AchievementsHandler;
import com.vladislavfaust.jumpinsweetis.ui.UICommonElements;
import com.vladislavfaust.jumpinsweetis.ui.UIRenderer;
import com.vladislavfaust.jumpinsweetis.ui.elements.UIMissionNotification;
import com.vladislavfaust.jumpinsweetis.ui.elements.UIShareAchievement;
import com.vladislavfaust.jumpinsweetis.ui.elements.UISuggestionMenu;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.alpha;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.delay;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeOut;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.forever;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.moveBy;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.run;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.scaleTo;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;

/**
 * Created by Faust on 25.01.2015.
 *
 */
public class UIScreenGameOver
{
    // Настройки
    private final static float STAR_ANIM_LENGTH = 0.35f;

    // Лист активных звездочек
    private static Array<Image> filledStars = new Array<Image>(ScoreHandler.MAX_STARS);

    private static boolean restartUnclickable = false;

    // Необходимо для грамотного отображения звезд
    private static int starsShown = 0;

    private static boolean thisTimeFreebiesSuggested = false;
    private static boolean thisSessionFeedbackSuggested = false;

    public static void setUI (Table rootTable)
    {
        // Верх
        rootTable.add(UICommonElements.thoseTwoValues(true, true)).expandX().top().padTop(40f);
        rootTable.row();

        // Центр
        // Геймовер, очки и зведы
        // Геймовер
        final Table tableMiddle = new Table();
        final Image gameOverImage = new Image(AssetLoader.getUi().findRegion(Language.getGameOverTextureName()));
        gameOverImage.setOrigin(Align.center);
        gameOverImage.setOrigin(gameOverImage.getOriginX(), gameOverImage.getOriginY() - 100f);
        gameOverImage.addAction(sequence(
            alpha(0f),
            scaleTo(0f, 0f, 0f),
            alpha(1f),
            scaleTo(1f, 1f, 0.6f, Interpolation.exp10Out)
        ));
        SoundsHandler.gameOverWhoosh();
        tableMiddle.add(gameOverImage).spaceBottom(41f);
        tableMiddle.row();

        // Звезды
        final Table tableStars = new Table();

        filledStars.clear();

        Image image; Stack starStack;
        for (int i = 0; i < ScoreHandler.MAX_STARS; i++)
        {
            // Каждая звезда в отдельности
            starStack = new Stack();

            // Сначала плейсхолдер серый
            image = new Image(AssetLoader.getUi().findRegion("star-placeholder"));
            image.setOrigin(image.getWidth() / 2f, image.getHeight() / 2f);
            image.setScale(0.90f);
            starStack.addActor(image);

            // Потом сама звезда
            image = new Image(AssetLoader.getUi().findRegion("star", i));
            filledStars.add(image);
            starStack.addActor(image);

            // Скрываем саму звезду
            image.setVisible(false);

            tableStars.add(starStack).spaceRight(21f);
        }
        tableStars.addAction(sequence(
            alpha(0f),
            delay(0.05f),
            run(new Runnable() {
                @Override
                public void run()
                {
                    tableStars.setTransform(true);
                    tableStars.setOrigin(Align.center);
                    tableStars.addAction(sequence(
                            scaleTo(0f, 0f, 0f),
                            alpha(1f),
                            scaleTo(1f, 1f, 0.6f, Interpolation.exp10Out)
                    ));
                    SoundsHandler.gameOverWhoosh();
                }
            }
            )
        ));
        tableMiddle.add(tableStars).spaceBottom(41f);
        tableMiddle.row();

        final Table scoreTable = new Table();

        final Label youVeScoredLabel = new Label(Language.LOCALE.format("youScored", ScoreHandler.getScore(), CandiesHandler.getLastCandies()), UIRenderer.instance().getSkin(), "regular70", UICommonElements.COLOR_ORANGE);
        youVeScoredLabel.setAlignment(Align.center);
        scoreTable.add(youVeScoredLabel);

        scoreTable.addAction(sequence(
            alpha(0f),
            delay(0.1f),
            run(new Runnable() {
                @Override
                public void run()
                {
                    scoreTable.setTransform(true);
                    scoreTable.setOrigin(Align.center);
                    scoreTable.setOrigin(scoreTable.getOriginX(), scoreTable.getOriginY() + 100f);
                    scoreTable.addAction(sequence(
                        alpha(0f),
                        scaleTo(0f, 0f, 0f),
                        alpha(1f),
                        scaleTo(1f, 1f, 0.6f, Interpolation.exp10Out)
                    ));
                    SoundsHandler.gameOverWhoosh();
                }
        })));

        tableMiddle.add(scoreTable).spaceTop(5f);
        tableMiddle.row();

        // Показываем 'NEW HIGHSCORE' только тогда, когда нужно
        if (HighscoreHandler.isNewHighscore())
        {
            final Label newHighscore = new Label(Language.LOCALE.get("newHighscore"), UIRenderer.instance().getSkin(), "regular70", UICommonElements.COLOR_BLUE);

            final Table highscoreTable = new Table();
            highscoreTable.add(newHighscore);
            tableMiddle.add(highscoreTable).spaceTop(5f);
            tableMiddle.row();

            highscoreTable.addAction(sequence(
                alpha(0f),
                delay(0.15f),
                run(new Runnable() {
                    @Override
                    public void run()
                    {
                        highscoreTable.setTransform(true);
                        highscoreTable.setOrigin(Align.center);
                        highscoreTable.addAction(sequence(alpha(0f), scaleTo(0f, 0f, 0f), alpha(1f), forever(sequence(scaleTo(1.1f, 1.1f, 0.4f, Interpolation.sineIn), scaleTo(1f, 1f, 0.4f, Interpolation.sineOut)))));
                    }
                })
            ));


            // И звук проигрываем
            SoundsHandler.highscore();

            // И эффект еще
            ParticleEffect effect = new ParticleEffect();
            effect.load(Gdx.files.internal("effects/confetti.p"), Gdx.files.internal("effects"));
            effect.setPosition(100f, 0f);
            effect.setFlip(true, false);
            effect.scaleEffect(2.5f);
            UIRenderer.instance().addEffect(effect);
            SoundsHandler.petard(1);

            ParticleEffect effect1 = new ParticleEffect(effect);
            effect1.setFlip(true, false);
            effect1.setPosition(JumpinSweeties.instance().getGameWidth() - 50f, 0f);
            UIRenderer.instance().addEffect(effect1);
            SoundsHandler.petard(2);

        }

        rootTable.add(tableMiddle).expand().center();
        rootTable.row();

        /*
            Низ
         */

        // Таблица нижних кнопок
        HorizontalGroup horizontalGroup = new HorizontalGroup();

        // Назад в меню
        final Image backToMenuButton = new Image(AssetLoader.getUi().findRegion("game-over-back"));

        backToMenuButton.addListener(new InputListener()
        {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button)
            {
                JumpinSweeties.instance().gotoMainMenu();
                SoundsHandler.button();
                return false;
            }
        });

        horizontalGroup.addActor(backToMenuButton);

        // Заново
        final Stack restartStack = new Stack();

        final Image restartShopImage = new Image(AssetLoader.getUi().findRegion("restart-background"));
        final Label restartShopTitle = new Label(Language.LOCALE.get("restart"), UIRenderer.instance().getSkin(), "regular70", Color.WHITE);
        restartShopTitle.setAlignment(Align.center);

        restartStack.add(restartShopImage);
        restartStack.add(restartShopTitle);

        thisTimeFreebiesSuggested = false;

        restartStack.addListener(new InputListener()
        {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button)
            {
                SoundsHandler.button();

                if (restartUnclickable)
                    return false;

                /*
                    Показываем меню бесплатностей
                 */

                if (!thisTimeFreebiesSuggested && (
                        AdsManager.readyToShowIncentivizedContent() || (
                                GiftsCooldownHelper.getCooldown() <= 0 && GiftsCooldownHelper.doesHaveATicket()
                        )
                ))
                {
                    thisTimeFreebiesSuggested = true;
                    UISuggestionMenu.show();
                    return true;
                }

                /*
                    Просим оставить отзыв
                 */

                if (!thisSessionFeedbackSuggested && !MyPreferences.getFeedbackBlocked() && (MyPreferences.getThisSessionLaunchTime() + AnalyticsHelper.FEEDBACK_DELAY_MINUTES * 60 * 1000 - System.currentTimeMillis() <= 0))
                {
                    thisSessionFeedbackSuggested = true;

                    JumpinSweeties.instance().getRequestHandler().threeOptions(new IThreeOptionsDialog() {
                        @Override
                        public void clickedNegative()
                        {
                            AnalyticsHelper.trackFeedbackOffer(AnalyticsHelper.FEEDBACK_RESULT.LATER);
                        }

                        @Override
                        public void clickedPositive()
                        {
                            AnalyticsHelper.trackFeedbackOffer(AnalyticsHelper.FEEDBACK_RESULT.SUCCESS);
                            Gdx.net.openURI(AnalyticsHelper.GOOGLE_PLAY_LINK);
                        }

                        @Override
                        public void clickedNeutral()
                        {
                            AnalyticsHelper.trackFeedbackOffer(AnalyticsHelper.FEEDBACK_RESULT.NEVER);
                            MyPreferences.putFeedbackBlocked(true);
                        }
                    },
                        Language.LOCALE.get("feedbackTitle"),
                        Language.LOCALE.get("feedbackMessage"),
                        Language.LOCALE.get("feedbackConfirm"),
                        Language.LOCALE.get("feedbackLater"),
                        Language.LOCALE.get("feedbackNever"));

                    return true;
                }

                JumpinSweeties.instance().startGame();
                return true;
            }
        });

        // Добавляем его
        horizontalGroup.addActor(restartStack);

        // Шеринг
        final Image shareButton = new Image(AssetLoader.getUi().findRegion("game-over-share"));

        shareButton.addListener(new InputListener()
        {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button)
            {
                SoundsHandler.button();

                JumpinSweeties.instance().getRequestHandler().confirm(new IConfirmDialog()
                {
                    @Override
                    public void clickedOK()
                    {
                        UICommonElements.makeAScreenshot(Language.LOCALE.format("shareGameOver", ScoreHandler.getScore(), AnalyticsHelper.SHARE_LINK), true);
                        AnalyticsHelper.trackShareClick(ScoreHandler.getScore(), HighscoreHandler.isNewHighscore(), true);
                    }

                    @Override
                    public void clickedCancel()
                    {
                        AnalyticsHelper.trackShareClick(ScoreHandler.getScore(), HighscoreHandler.isNewHighscore(), false);
                    }
                },
                    Language.LOCALE.get("confirm"),
                    Language.LOCALE.get("sharingMessage"),
                    Language.LOCALE.get("sharingOK"),
                    Language.LOCALE.get("cancel")
                );


                return false;
            }
        });

        horizontalGroup.addActor(shareButton);
        rootTable.add(horizontalGroup).bottom().expandX().fillX();

        // Все добавили, теперь, если нужно, показываем анимацию звезд
        if (ScoreHandler.getStars() > 0) {
            starsShown = 0;
            Timer.schedule(new Timer.Task()
            {
                @Override
                public void run()
                {
                    filledStars.get(starsShown).setOrigin(filledStars.get(starsShown).getWidth() / 2f, filledStars.get(starsShown).getHeight() / 2f);

                    filledStars.get(starsShown).setVisible(true);
                    filledStars.get(starsShown).setScale(0f, 0f);
                    filledStars.get(starsShown).addAction(sequence(
                            moveBy(0f, -4f),
                            scaleTo(1f, 1f, STAR_ANIM_LENGTH, Interpolation.swingOut)
                    ));

                    SoundsHandler.star(starsShown);
                    ScreenShaker.shake((OrthographicCamera) UIRenderer.instance().getStage().getCamera(), 20f, STAR_ANIM_LENGTH);

                    starsShown++;
                }
            }, 0.1f, STAR_ANIM_LENGTH + 0.02f, ScoreHandler.getStars() - 1);
        }

        // Белый фейд
        final Image whiteImageGame = new Image(AssetLoader.getWhiteScreenTexture());
        UIRenderer.instance().getStage().addActor(whiteImageGame);
        whiteImageGame.addAction(new SequenceAction(fadeOut(0.3f), run(new Runnable()
        {
            @Override
            public void run()
            {
                whiteImageGame.remove();

                UICommonElements.candiesLabel.setText(CandiesHandler.getCandies()+"");
            }
        })));

        /*
            Доп. окна
         */

        // Задание
        if (MissionController.isNeedsToBeShown())
            UIMissionNotification.completeMission();
        else if (AchievementsHandler.getNexSharedId() != -1)
            UIShareAchievement.show(AchievementsHandler.getNexSharedId());
    }
}
