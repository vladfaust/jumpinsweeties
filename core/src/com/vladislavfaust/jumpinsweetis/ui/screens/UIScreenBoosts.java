package com.vladislavfaust.jumpinsweetis.ui.screens;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.vladislavfaust.jumpinsweetis.JumpinSweeties;
import com.vladislavfaust.jumpinsweetis.gameobjects.bonuses.BonusLevelingController;
import com.vladislavfaust.jumpinsweetis.gamesystems.MissionController;
import com.vladislavfaust.jumpinsweetis.helpers.AssetLoader;
import com.vladislavfaust.jumpinsweetis.helpers.CandiesHandler;
import com.vladislavfaust.jumpinsweetis.helpers.Language;
import com.vladislavfaust.jumpinsweetis.helpers.TokensHandler;
import com.vladislavfaust.jumpinsweetis.helpers.analytics.AnalyticsHelper;
import com.vladislavfaust.jumpinsweetis.helpers.googleplay.AchievementsHandler;
import com.vladislavfaust.jumpinsweetis.ui.UICommonElements;
import com.vladislavfaust.jumpinsweetis.ui.UIRenderer;
import com.vladislavfaust.jumpinsweetis.ui.elements.UIBuyTokens;
import com.vladislavfaust.jumpinsweetis.ui.elements.UIShareAchievement;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.delay;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.run;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;

/**
 * Created by Faust on 09.05.2015.
 *
 */
public class UIScreenBoosts
{
    /*
        ��������
     */

    private static Label tokenCounter1, tokenCounter2, rocketLevel, magnetLevel, doublerLevel, rocketPrice, magnetPrice, doublerPrice;
    private final static Stack tokenCounter1Wrapper = new Stack(), tokenCounter2Wrapper = new Stack();
    private final static Stack buyMoreStack1 = new Stack(), buyMoreStack2 = new Stack();
    private final static Table convertTable = new Table();

    /*
        ���������� ������
     */

    private static Table createTokenCounterTable (int n)
    {
        final Table counterTable = new Table();

        /*
            ���-�� �������
         */

        switch (n)
        {
            case 1:
                tokenCounter1 = new Label(TokensHandler.getTotalTokens()+"", UIRenderer.instance().getSkin(), "stroke60", Color.WHITE);
                tokenCounter1Wrapper.addActor(tokenCounter1);
                counterTable.add(tokenCounter1Wrapper).spaceRight(6f);
                break;
            case 2:
                tokenCounter2 = new Label(TokensHandler.getTotalTokens()+"", UIRenderer.instance().getSkin(), "stroke60", Color.WHITE);
                tokenCounter2Wrapper.addActor(tokenCounter2);
                counterTable.add(tokenCounter2Wrapper).spaceRight(6f);
                break;
        }

        /*
            ������ ������
         */

        final Image tokenIcon = new Image(AssetLoader.getUi().findRegion("token"));
        counterTable.add(tokenIcon).spaceRight(17f);

        /*
            ���� "������ ���"
         */

        final Image buyMoreBackground = new Image(AssetLoader.getUi().findRegion("buy-more-tokens-background"));
        final Label buyMoreLabel = new Label(Language.LOCALE.get("buyMoreTokens"), UIRenderer.instance().getSkin(), "regular60", Color.WHITE);
        buyMoreLabel.setAlignment(Align.center);

        switch (n)
        {
            case 1:
                buyMoreStack1.add(buyMoreBackground);
                buyMoreStack1.add(buyMoreLabel);
                buyMoreStack1.pack();

                buyMoreStack1.addListener(new InputListener()
                {
                    public boolean touchDown(InputEvent event, float x, float y, int pointer, int button)
                    {
                        UICommonElements.click(event.getListenerActor());
                        UIBuyTokens.show();

                        AnalyticsHelper.trackBuyMoreTokensClick();
                        return true;
                    }
                });

                counterTable.add(buyMoreStack1);
                break;
            case 2:
                buyMoreStack2.add(buyMoreBackground);
                buyMoreStack2.add(buyMoreLabel);
                buyMoreStack2.pack();

                buyMoreStack2.addListener(new InputListener()
                {
                    public boolean touchDown(InputEvent event, float x, float y, int pointer, int button)
                    {
                        UICommonElements.click(event.getListenerActor());
                        UIBuyTokens.show();

                        AnalyticsHelper.trackBuyMoreTokensClick();
                        return true;
                    }
                });

                counterTable.add(buyMoreStack2);
                break;
        }

        counterTable.align(Align.center);

        return counterTable;
    }

    public static void updateTokenCounters ()
    {
        UICommonElements.updateCounter(tokenCounter1Wrapper, tokenCounter1, TokensHandler.getTotalTokens());
        UICommonElements.updateCounter(tokenCounter2Wrapper, tokenCounter2, TokensHandler.getTotalTokens());
    }

    private static void attentionBuyButtons ()
    {
        final float delayTime = 0.15f;

        UIRenderer.instance().getStage().addAction(sequence(
                run(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        UICommonElements.attention(buyMoreStack1, false);
                    }
                }),
                delay(delayTime),
                run(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        UICommonElements.attention(buyMoreStack2, false);
                    }
                }),
                delay(delayTime),
                run(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        UICommonElements.attention(convertTable, false);
                    }
                })
        ));
    }
    
    /*
        ������� ����������
     */

    public static void setUI (Table rootTable)
    {
        AnalyticsHelper.trackBoostsScreen();

        /*
            �������
         */

        tokenCounter1Wrapper.clear();
        tokenCounter2Wrapper.clear();
        buyMoreStack1.clear();
        buyMoreStack2.clear();
        convertTable.clear();

        Table contentTable = new Table();

        /*
            ������� ������� �������
         */

        contentTable.add(createTokenCounterTable(1)).spaceBottom(35f).row();

        /*
            ������
         */

        final Table rocketTable = new Table();
        rocketTable.setBackground(new TextureRegionDrawable(AssetLoader.getUi().findRegion("rocket-background")));
        rocketTable.align(Align.center);

        final Image rocketIcon = new Image(AssetLoader.getUi().findRegion("rocket-icon"));
        rocketTable.add(rocketIcon).padLeft(-27f).padBottom(3f).spaceRight(25f);

        final Table rocketLabelGroup = new Table();
        rocketLabelGroup.align(Align.left);
        final Label rocketName = new Label(Language.LOCALE.get("rocket"), UIRenderer.instance().getSkin(), "regular70", Color.WHITE);
        rocketName.setAlignment(Align.left);
        rocketLevel = new Label(Language.LOCALE.format("level", BonusLevelingController.getRocketLevel(), BonusLevelingController.MAX_LEVEL), UIRenderer.instance().getSkin(), "regular50", Color.WHITE);
        rocketLevel.setAlignment(Align.left);
        rocketLabelGroup.add(rocketName).left().row();
        rocketLabelGroup.add(rocketLevel).left().padTop(-20f);
        rocketTable.add(rocketLabelGroup).padBottom(3f).width(309f);

        final Table rocketPriceTable = new Table();
        rocketPriceTable.setBackground(new TextureRegionDrawable(AssetLoader.getUi().findRegion("upgrade-boost-background")));
        final Table rocketPriceAmount = new Table();

        rocketPrice = new Label("", UIRenderer.instance().getSkin(), "stroke60", Color.WHITE);
        int price = BonusLevelingController.howMuchUpgradeCosts(BonusLevelingController.BOOST_TYPE.ROCKET);
        rocketPrice.setText((price != -1) ? price + "" : Language.LOCALE.get("max"));

        final Image rocketTokenIcon = new Image(AssetLoader.getUi().findRegion("token"));
        rocketPriceAmount.add(rocketPrice).spaceRight(4f);
        rocketPriceAmount.add(rocketTokenIcon);
        rocketPriceTable.add(rocketPriceAmount).center().padBottom(3f).padLeft(12f);
        rocketPriceTable.setTouchable(Touchable.enabled);
        rocketPriceTable.addListener(new InputListener()
        {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button)
            {
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button)
            {
                if (BonusLevelingController.upgrade(BonusLevelingController.BOOST_TYPE.ROCKET))
                {
                    UICommonElements.click(rocketPriceTable);

                    int price = BonusLevelingController.howMuchUpgradeCosts(BonusLevelingController.BOOST_TYPE.ROCKET);
                    rocketPrice.setText((price != -1) ? price + "" : Language.LOCALE.get("max"));
                    rocketPriceAmount.invalidate();
                    rocketPriceTable.invalidate();

                    rocketLevel.setText(Language.LOCALE.format("level", BonusLevelingController.getRocketLevel(), BonusLevelingController.MAX_LEVEL));

                    updateTokenCounters();

                    MissionController.boostUpgraded(true);

                    AnalyticsHelper.trackUpgradeBoost(BonusLevelingController.BOOST_TYPE.ROCKET, BonusLevelingController.getRocketLevel());
                }
                else {
                    UICommonElements.error(rocketPriceTable);
                    if (BonusLevelingController.howMuchUpgradeCosts(BonusLevelingController.BOOST_TYPE.ROCKET) != -1)
                        attentionBuyButtons();
                }
            }
        });
        rocketTable.add(rocketPriceTable).padRight(33f);

        contentTable.add(rocketTable).width(651f).height(194f).spaceBottom(35f).row();
        
        /*
            ������
         */

        final Table magnetTable = new Table();
        magnetTable.setBackground(new TextureRegionDrawable(AssetLoader.getUi().findRegion("magnet-background")));
        magnetTable.align(Align.center);

        final Image magnetIcon = new Image(AssetLoader.getUi().findRegion("magnet-icon"));
        magnetTable.add(magnetIcon).padLeft(-27f).padBottom(3f).spaceRight(25f);

        final Table magnetLabelGroup = new Table();
        magnetLabelGroup.align(Align.left);
        final Label magnetName = new Label(Language.LOCALE.get("magnet"), UIRenderer.instance().getSkin(), "regular70", Color.WHITE);
        magnetName.setAlignment(Align.left);
        magnetLevel = new Label(Language.LOCALE.format("level", BonusLevelingController.getMagnetLevel(), BonusLevelingController.MAX_LEVEL), UIRenderer.instance().getSkin(), "regular50", Color.WHITE);
        magnetLevel.setAlignment(Align.left);
        magnetLabelGroup.add(magnetName).left().row();
        magnetLabelGroup.add(magnetLevel).left().padTop(-20f);
        magnetTable.add(magnetLabelGroup).padBottom(3f).width(309f);

        final Table magnetPriceTable = new Table();
        magnetPriceTable.setBackground(new TextureRegionDrawable(AssetLoader.getUi().findRegion("upgrade-boost-background")));
        final Table magnetPriceAmount = new Table();

        magnetPrice = new Label("", UIRenderer.instance().getSkin(), "stroke60", Color.WHITE);
        price = BonusLevelingController.howMuchUpgradeCosts(BonusLevelingController.BOOST_TYPE.MAGNET);
        magnetPrice.setText((price != -1) ? price + "" : Language.LOCALE.get("max"));

        final Image magnetTokenIcon = new Image(AssetLoader.getUi().findRegion("token"));
        magnetPriceAmount.add(magnetPrice).spaceRight(4f);
        magnetPriceAmount.add(magnetTokenIcon);
        magnetPriceTable.add(magnetPriceAmount).center().padBottom(3f).padLeft(12f);
        magnetPriceTable.setTouchable(Touchable.enabled);
        magnetPriceTable.addListener(new InputListener()
        {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button)
            {
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button)
            {
                if (BonusLevelingController.upgrade(BonusLevelingController.BOOST_TYPE.MAGNET)) {
                    UICommonElements.click(magnetPriceTable);

                    int price = BonusLevelingController.howMuchUpgradeCosts(BonusLevelingController.BOOST_TYPE.MAGNET);
                    magnetPrice.setText((price != -1) ? price + "" : Language.LOCALE.get("max"));
                    magnetPriceAmount.invalidate();
                    magnetPriceTable.invalidate();

                    magnetLevel.setText(Language.LOCALE.format("level", BonusLevelingController.getMagnetLevel(), BonusLevelingController.MAX_LEVEL));

                    updateTokenCounters();

                    MissionController.boostUpgraded(true);

                    AnalyticsHelper.trackUpgradeBoost(BonusLevelingController.BOOST_TYPE.MAGNET, BonusLevelingController.getMagnetLevel());
                } else {
                    UICommonElements.error(magnetPriceTable);
                    if (BonusLevelingController.howMuchUpgradeCosts(BonusLevelingController.BOOST_TYPE.MAGNET) != -1)
                        attentionBuyButtons();
                }
            }
        });
        magnetTable.add(magnetPriceTable).padRight(33f);

        contentTable.add(magnetTable).width(651f).height(194f).spaceBottom(35f).row();
        
        /*
            ���������
         */

        final Table doublerTable = new Table();
        doublerTable.setBackground(new TextureRegionDrawable(AssetLoader.getUi().findRegion("doubler-background")));
        doublerTable.align(Align.center);

        final Image doublerIcon = new Image(AssetLoader.getUi().findRegion("doubler-icon"));
        doublerTable.add(doublerIcon).padLeft(-27f).padBottom(3f).spaceRight(25f);

        final Table doublerLabelGroup = new Table();
        doublerLabelGroup.align(Align.left);
        final Label doublerName = new Label(Language.LOCALE.get("doubler"), UIRenderer.instance().getSkin(), "regular70", Color.WHITE);
        doublerName.setAlignment(Align.left);
        doublerLevel = new Label(Language.LOCALE.format("level", BonusLevelingController.getMagnetLevel(), BonusLevelingController.MAX_LEVEL), UIRenderer.instance().getSkin(), "regular50", Color.WHITE);
        doublerLevel.setAlignment(Align.left);
        doublerLabelGroup.add(doublerName).left().row();
        doublerLabelGroup.add(doublerLevel).left().padTop(-20f);
        doublerTable.add(doublerLabelGroup).padBottom(3f).width(309f);

        final Table doublerPriceTable = new Table();
        doublerPriceTable.setBackground(new TextureRegionDrawable(AssetLoader.getUi().findRegion("upgrade-boost-background")));
        final Table doublerPriceAmount = new Table();

        doublerPrice = new Label("", UIRenderer.instance().getSkin(), "stroke60", Color.WHITE);
        price = BonusLevelingController.howMuchUpgradeCosts(BonusLevelingController.BOOST_TYPE.DOUBLER);
        doublerPrice.setText((price != -1) ? price + "" : Language.LOCALE.get("max"));

        final Image doublerTokenIcon = new Image(AssetLoader.getUi().findRegion("token"));
        doublerPriceAmount.add(doublerPrice).spaceRight(4f);
        doublerPriceAmount.add(doublerTokenIcon);
        doublerPriceTable.add(doublerPriceAmount).center().padBottom(3f).padLeft(12f);
        doublerPriceTable.setTouchable(Touchable.enabled);
        doublerPriceTable.addListener(new InputListener()
        {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button)
            {
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button)
            {
                if (BonusLevelingController.upgrade(BonusLevelingController.BOOST_TYPE.DOUBLER)) {
                    UICommonElements.click(doublerPriceTable);

                    int price = BonusLevelingController.howMuchUpgradeCosts(BonusLevelingController.BOOST_TYPE.DOUBLER);
                    doublerPrice.setText((price != -1) ? price + "" : Language.LOCALE.get("max"));
                    doublerPriceAmount.invalidate();
                    doublerPriceTable.invalidate();

                    doublerLevel.setText(Language.LOCALE.format("level", BonusLevelingController.getDoublerLevel(), BonusLevelingController.MAX_LEVEL));

                    updateTokenCounters();

                    MissionController.boostUpgraded(true);

                    AnalyticsHelper.trackUpgradeBoost(BonusLevelingController.BOOST_TYPE.DOUBLER, BonusLevelingController.getDoublerLevel());
                } else {
                    UICommonElements.error(doublerPriceTable);
                    if (BonusLevelingController.howMuchUpgradeCosts(BonusLevelingController.BOOST_TYPE.DOUBLER) != -1)
                        attentionBuyButtons();
                }
            }
        });
        doublerTable.add(doublerPriceTable).padRight(33f).width(170f);

        contentTable.add(doublerTable).width(651f).height(194f).spaceBottom(35f).row();

        /*
            ������ ������� �������
         */

        contentTable.add(createTokenCounterTable(2)).spaceBottom(38f).row();

        /*
            ������ �����������
         */

        convertTable.setBackground(new TextureRegionDrawable(AssetLoader.getUi().findRegion("convert-background")));

        convertTable.setTouchable(Touchable.enabled);
        convertTable.addListener(new InputListener()
        {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button)
            {
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button)
            {
                if (CandiesHandler.convertToTokens()) {
                    UICommonElements.click(convertTable);
                    updateTokenCounters();
                    MissionController.tokensCollected(1, true);
                    JumpinSweeties.instance().getPlatformUtils().showMessage(Language.LOCALE.format("candiesLeft", CandiesHandler.getCandies()));

                    AnalyticsHelper.trackConvert();
                } else {
                    UICommonElements.error(convertTable);
                    JumpinSweeties.instance().getPlatformUtils().showMessage(Language.LOCALE.get("notEnoughCandies"));
                }
            }
        });

        contentTable.add(convertTable).width(437f).height(194f);

        /*
            �����
         */

        rootTable.add(contentTable).center();

        if (AchievementsHandler.getNexSharedId() != -1)
            UIShareAchievement.show(AchievementsHandler.getNexSharedId());
    }
}
