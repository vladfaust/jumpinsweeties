package com.vladislavfaust.jumpinsweetis.ui;

import com.badlogic.gdx.math.Vector2;
import com.vladislavfaust.jumpinsweetis.JumpinSweeties;

/**
 * Created by Faust on 19.01.2015.
 * Гарантируется синглтон.
 */
public class UIHandler
{
    // X и Y джампера
    private static Vector2 jumperPosition = new Vector2();
    private static boolean jumperRight = false;

    // Клик по старту в главном меню
    public static void tapToStartClicked (Vector2 pos, boolean right)
    {
        jumperRight = right;
        jumperPosition.set(pos);
        JumpinSweeties.instance().startGame();
    }

    public static Vector2 getJumperPosition()
    {
        return jumperPosition;
    }

    public static boolean isJumperRight()
    {
        return jumperRight;
    }
}
