package com.vladislavfaust.jumpinsweetis.ui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Scaling;
import com.vladislavfaust.jumpinsweetis.JumpinSweeties;
import com.vladislavfaust.jumpinsweetis.helpers.AssetLoader;
import com.vladislavfaust.jumpinsweetis.helpers.CandiesHandler;
import com.vladislavfaust.jumpinsweetis.helpers.HighscoreHandler;
import com.vladislavfaust.jumpinsweetis.helpers.IAP;
import com.vladislavfaust.jumpinsweetis.helpers.Language;
import com.vladislavfaust.jumpinsweetis.helpers.ScreenshotHelper;
import com.vladislavfaust.jumpinsweetis.helpers.SoundsHandler;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.alpha;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.delay;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeIn;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeOut;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.forever;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.moveBy;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.repeat;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.rotateBy;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.run;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.scaleTo;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;

/**
 * Created by Faust on 17.04.2015.
 *
 */
public class UICommonElements
{
    // Цвета
    public final static Color
            COLOR_RED = Color.valueOf("fc635aff"),
            COLOR_ORANGE = Color.valueOf("ffa632ff"),
            COLOR_GREEN = Color.valueOf("31e51eff"),
            COLOR_BLUE = Color.valueOf("638cf6ff"),
            COLOR_UNIQUE = Color.valueOf("0eb5afff"),
            COLOR_DISCOUNT = Color.valueOf("ff3a3aff"),
            COLOR_ACHIEVEMENT = COLOR_ORANGE;

    /*
        Показ сделанного скриншота
     */

    // Как будто сфоткали экран
    public static void makeAScreenshot(final String text, final boolean immediateShare)
    {
        final Image white = new Image(AssetLoader.getWhiteScreenTexture());
        white.setFillParent(true);
        UIRenderer.instance().getRootStack().add(white);
        UIRenderer.instance().getRootStack().pack();

        white.addAction(sequence(
                delay(0.1f),
                alpha(0f, 0.2f),
                run(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        UIRenderer.instance().getRootStack().removeActor(white);

                        /*
                            Свой знак на скриншоте
                         */

                        final Table screenshotOverlayTable = new Table();
                        screenshotOverlayTable.setFillParent(true);
                        screenshotOverlayTable.bottom();
                        final Image screenshotOverlay = new Image(AssetLoader.getUi().findRegion("screenshotOverlay"));
                        screenshotOverlay.setAlign(Align.bottom);
                        screenshotOverlayTable.add(screenshotOverlay).bottom().fillX().expandX();
                        UIRenderer.instance().getRootStack().addActor(screenshotOverlayTable);

                        ScreenshotHelper.prepareToMakeScreenshot();

                        screenshotOverlayTable.addAction(sequence(
                                Actions.run(new Runnable()
                                {
                                    @Override
                                    public void run()
                                    {
                                        UIRenderer.instance().getRootStack().removeActor(screenshotOverlayTable);

                                        if (immediateShare)
                                        {
                                            ScreenshotHelper.saveScreenshot();
                                            JumpinSweeties.instance().getPlatformUtils().shareImageAndText(ScreenshotHelper.getFilePath(), text);
                                        }
                                    }
                                })
                        ));
                    }
                })));

        SoundsHandler.camshot();
    }

    /*
        Генерация элементов
     */

    // Бубль ревард-контента
    public static Actor rewardedContentBubble ()
    {
        final Stack stack = new Stack();

        final Image bubble = new Image(AssetLoader.getUi().findRegion("bubble"));
        stack.add(bubble);
        bubble.setScaling(Scaling.none);

        final Image video = new Image(AssetLoader.getUi().findRegion("video-attention"));
        stack.add(video);
        video.setScaling(Scaling.none);

        stack.pack();

        video.addAction(alpha(0f));
        video.setOrigin(Align.center);

        bubble.setOrigin(Align.bottomLeft);
        bubble.addAction(sequence(
                scaleTo(0f, 0f, 0f),
                delay(0.25f),
                scaleTo(1f, 1f, 0.6f, Interpolation.elasticOut)
        ));

        stack.addAction(sequence(
                delay(0.2f),
                run(new Runnable() {
                    @Override
                    public void run()
                    {
                        video.addAction(sequence(
                                delay(0.2f),
                                sequence(
                                        moveBy(2f, 3f),
                                        scaleTo(0f, 0f),
                                        alpha(1f),
                                        scaleTo(1f, 1f, 0.4f, Interpolation.elasticOut)
                                )));
                        stack.setTransform(true);
                        stack.setOrigin(Align.bottomLeft);
                        stack.addAction(forever(sequence(
                                delay(2f),
                                repeat(3, sequence(
                                        rotateBy(4f, 0.030f),
                                        rotateBy(-8f, 0.060f),
                                        rotateBy(4f, 0.030f)
                                ))
                        )));
                    }
                })
        ));

        return stack;
    }

    // Добавить конфетки и хайскор (или только конфетки или только хайскор)
    public static Label highscoreLabel, candiesLabel;
    public static Stack thoseTwoValues (boolean highscore, boolean candies)
    {
        Stack stack = new Stack();
        final float fade = 0.3000000000001f, delay = 0.6000000000001f;

        // Хайскор
        if (highscore) {
            highscoreLabel = new Label(Language.LOCALE.format("highscore", HighscoreHandler.getHighscore()), UIRenderer.instance().getSkin(), "regular70", COLOR_ORANGE);

            // Анимировать, только если есть конфетки
            if (candies) {
                highscoreLabel.addAction(
                        sequence(
                                alpha(0f),
                                forever(
                                        sequence(
                                                delay(delay),
                                                delay(fade),
                                                fadeIn(fade),
                                                delay(delay),
                                                fadeOut(fade),
                                                delay(fade)
                                        )
                                )
                        )
                );
            }

            stack.add(highscoreLabel);
        }

        // Группа отображения конфеток с картинкой
        if (candies) {
            final Table candiesTable = new Table();
            candiesLabel = new Label(CandiesHandler.getCandies() + "", UIRenderer.instance().getSkin(), "regular70", COLOR_GREEN);
            candiesTable.add(candiesLabel).center().spaceRight(12f);

            final Image candieImage = new Image(AssetLoader.getUi().findRegion("candie"));
            candiesTable.add(candieImage).center();

            candiesTable.setFillParent(true);

            // Анимировать, только если есть хайскор
            if (highscore) {
                candiesTable.addAction(
                        forever(
                                sequence(
                                        delay(delay),
                                        fadeOut(fade),
                                        delay(delay),
                                        delay(fade),
                                        delay(fade),
                                        fadeIn(fade)
                                )
                        )
                );
            }

            stack.add(candiesTable);
        }

        return stack;
    }

    public static Label priceLabel (IAP.PURCHASE_TYPE which)
    {
        return new Label(IAP.getPriceLocalized(which), UIRenderer.instance().getSkin(), "stroke60", Color.WHITE);
    };

    public static Label discountLabel (int howMuch)
    {
        return new Label("-" + howMuch + "%", UIRenderer.instance().getSkin(), "stroke70", COLOR_DISCOUNT);
    };

    /*
        Анимации элементов
     */

    // Анимация ошибки
    public static void error (Actor actor)
    {
        actor.setOrigin(Align.center);

        if (actor instanceof Stack)
            ((Stack) actor).setTransform(true);
        else if (actor instanceof Table)
            ((Table) actor).setTransform(true);

        actor.addAction(repeat(3, (sequence(moveBy(10f, 0f, 0.02f), moveBy(-10f, 0f, 0.02f)))));
        SoundsHandler.error();
    }

    // Анимация обновления счетчика
    public static void updateCounter (Actor counterContainer, final Label label, final int newValue)
    {
        counterContainer.setOrigin(Align.center);

        if (counterContainer instanceof Stack)
            ((Stack) counterContainer).setTransform(true);

        counterContainer.addAction(sequence(scaleTo(1.2f, 1.2f, 0.2f, Interpolation.linear), run(new Runnable()
        {
            @Override
            public void run()
            {
                label.setText(newValue + "");
            }
        }), scaleTo(1f, 1f, 0.4f, Interpolation.elasticOut)));
    }

    // Анимация клика
    public static void click (Actor actor)
    {
        actor.setOrigin(Align.center);

        if (actor instanceof Stack)
            ((Stack) actor).setTransform(true);
        else if (actor instanceof Table)
            ((Table) actor).setTransform(true);

        actor.addAction(sequence(
                scaleTo(1.05f, 1.05f, 0.1f, Interpolation.linear),
                scaleTo(1f, 1f, 0.2f, Interpolation.elasticOut)
        ));

        SoundsHandler.button();
    }

    // Анимация привлечения внимания
    public static void attention (Actor actor, boolean forever)
    {
        if (actor instanceof Stack)
            ((Stack) actor).setTransform(true);
        else if (actor instanceof Table)
            ((Table) actor).setTransform(true);

        actor.setOrigin(Align.center);

        if (actor instanceof Stack)
            ((Stack) actor).setTransform(true);
        else if (actor instanceof Table)
            ((Table) actor).setTransform(true);

        if (forever)
            actor.addAction(forever(sequence(
                    scaleTo(1.02f, 1.02f, 0.2f, Interpolation.sineIn),
                    scaleTo(1f, 1f, 0.3f, Interpolation.sineOut),
                    delay(0.4f)
            )));
        else
            actor.addAction(sequence(
                scaleTo(1.05f, 1.05f, 0.1f, Interpolation.sineIn),
                scaleTo(1f, 1f, 0.2f, Interpolation.sineOut)
            ));
    }
}
