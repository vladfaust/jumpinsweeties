package com.vladislavfaust.jumpinsweetis.ui.elements;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Container;
import com.badlogic.gdx.scenes.scene2d.ui.HorizontalGroup;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.Widget;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Scaling;
import com.vladislavfaust.jumpinsweetis.JumpinSweeties;
import com.vladislavfaust.jumpinsweetis.helpers.AssetLoader;
import com.vladislavfaust.jumpinsweetis.helpers.Language;
import com.vladislavfaust.jumpinsweetis.helpers.MyPreferences;
import com.vladislavfaust.jumpinsweetis.helpers.PromoCodesHelper;
import com.vladislavfaust.jumpinsweetis.helpers.SoundsHandler;
import com.vladislavfaust.jumpinsweetis.helpers.analytics.AnalyticsHelper;
import com.vladislavfaust.jumpinsweetis.helpers.dialogs.IPromptDialog;
import com.vladislavfaust.jumpinsweetis.ui.UIRenderer;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.alpha;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.moveBy;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.run;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;

/**
 * Created by Faust on 22.04.2015.
 *
 */
public class UISettingsMenu
{
    // Параметры
    final static float tableWidth = 618f;
    final static float appearTime = 0.2f;
    final static Interpolation appearInterpolation = Interpolation.sineOut;
    final static float disappearTime = 0.15f;
    final static Interpolation disappearInterpolation = Interpolation.sineIn;

    // Переменные
    public static boolean settingsShown;
    private final static Table
            root = new Table(),
            settingsMenuTable = new Table();

    public static void hide()
    {
        if (!settingsShown)
            return;

        settingsMenuTable.addAction(sequence(
                moveBy(tableWidth, 0f, disappearTime, disappearInterpolation),
                run(new Runnable() {
                    @Override
                    public void run()
                    {
                        root.clear();
                        UIRenderer.instance().getRootStack().removeActor(root);
                        settingsShown = false;
                    }
                })
        ));
    }

    public static void show()
    {
        if (settingsShown)
            return;
        settingsShown = true;

        root.clear();
        settingsMenuTable.clear();

        // Root-таблица
        root.setFillParent(true);
        UIRenderer.instance().getRootStack().add(root);

        // Сама таблица
        settingsMenuTable.setBackground(new TextureRegionDrawable(AssetLoader.getUi().findRegion("settings-menu-light-background")));
        settingsMenuTable.setTransform(true);
        settingsMenuTable.addAction(sequence(alpha(0f), alpha(0f), moveBy(tableWidth, 0f, 0f), alpha(1f), moveBy(-tableWidth, 0f, appearTime, appearInterpolation)));
        settingsMenuTable.addListener(new InputListener()
        {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button)
            {
                return true;
            }
        });
        root.add(settingsMenuTable).width(tableWidth).expandY().fillY();
        root.align(Align.right);

        // Касания
        root.setTouchable(Touchable.enabled);
        root.addListener(new InputListener()
        {
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }

            public void touchUp (InputEvent event, float x, float y, int pointer, int button)
            {
                hide();
            }
        });

        // Верх - хедер с названием
        final Label settingsTitle = new Label(Language.LOCALE.get("settings"), UIRenderer.instance().getSkin(), "regular70", Color.WHITE);
        final Container<Label> settingsHeader = new Container<>(settingsTitle);
        settingsHeader.setBackground(new TextureRegionDrawable(AssetLoader.getUi().findRegion("settings-menu-dark-background")));
        settingsMenuTable.add(settingsHeader).fillX().expandX().height(150f).top();
        settingsMenuTable.row();

        /*
            Звук и музыка
         */

        // Звуки
        final HorizontalGroup volumeSettingsGroup = new HorizontalGroup();
        final Image volumeSettingsIcon = new Image(AssetLoader.getUi().findRegion("settings-menu-volume-icon"));
        final Label volumeSettingsValue = new Label(Language.LOCALE.get("settingsSound")+": "+
                (MyPreferences.getSounds()? Language.LOCALE.get("on") : Language.LOCALE.get("off")),
                UIRenderer.instance().getSkin(), "regular60", Color.WHITE);
        volumeSettingsValue.setAlignment(Align.left);

        volumeSettingsGroup.addActor(volumeSettingsIcon);
        volumeSettingsGroup.addActor(volumeSettingsValue);
        volumeSettingsGroup.setTouchable(Touchable.enabled);
        volumeSettingsGroup.addListener(new InputListener()
        {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button)
            {
                event.stop();
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button)
            {
                MyPreferences.switchSounds(!MyPreferences.getSounds());
                SoundsHandler.switchSound(MyPreferences.getSounds());

                volumeSettingsValue.setText(Language.LOCALE.get("settingsSound") + ": " +
                        (MyPreferences.getSounds() ? Language.LOCALE.get("on") : Language.LOCALE.get("off")));

                AnalyticsHelper.trackSound(MyPreferences.getSounds());
            }

        });
        settingsMenuTable.add(volumeSettingsGroup).fillX().expandX().height(150f).top();
        settingsMenuTable.row();

        // Линия под звуками
        final Image line1 = new Image(AssetLoader.getUi().findRegion("settings-menu-line"));
        line1.setScaling(Scaling.stretch);
        settingsMenuTable.add(line1).fillX().expandX().height(2f).top();
        settingsMenuTable.row();

        // Музыка
        final HorizontalGroup musicSettingsGroup = new HorizontalGroup();
        final Image musicSettingsIcon = new Image(AssetLoader.getUi().findRegion("settings-menu-music-icon"));
        musicSettingsIcon.setAlign(Align.left);
        final Label musicSettingsValue = new Label(Language.LOCALE.get("settingsMusic")+": "+
                (MyPreferences.getMusic()? Language.LOCALE.get("on") : Language.LOCALE.get("off")),
                UIRenderer.instance().getSkin(), "regular60", Color.WHITE);
        musicSettingsValue.setAlignment(Align.left);

        musicSettingsGroup.addActor(musicSettingsIcon);
        musicSettingsGroup.addActor(musicSettingsValue);
        musicSettingsGroup.setTouchable(Touchable.enabled);

        musicSettingsGroup.addListener(new InputListener()
        {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button)
            {
                event.stop();
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button)
            {
                MyPreferences.switchMusic(!MyPreferences.getMusic());
                SoundsHandler.switchMusic(MyPreferences.getMusic());

                musicSettingsValue.setText(Language.LOCALE.get("settingsMusic") + ": " +
                        (MyPreferences.getMusic() ? Language.LOCALE.get("on") : Language.LOCALE.get("off")));

                AnalyticsHelper.trackMusic(MyPreferences.getMusic());
            }

        });
        settingsMenuTable.add(musicSettingsGroup).fillX().expandX().height(150f).top();
        settingsMenuTable.row();

        // Линия под музыкой
        final Image line2 = new Image(AssetLoader.getUi().findRegion("settings-menu-line"));
        line2.setScaling(Scaling.stretch);
        settingsMenuTable.add(line2).fillX().expandX().height(2f).top();
        settingsMenuTable.row();

        /*
            Пустое пространство
         */
        final Widget emptySpace = new Widget();
        settingsMenuTable.add(emptySpace).expand().fill();
        settingsMenuTable.row();

        /*
            Нижние кнопки
         */

        // Линия над промокодом
        final Image line3 = new Image(AssetLoader.getUi().findRegion("settings-menu-line"));
        line3.setScaling(Scaling.stretch);
        settingsMenuTable.add(line3).fillX().expandX().height(2f);
        settingsMenuTable.row();

        // Промокод
        final HorizontalGroup promocodeSettingsGroup = new HorizontalGroup();
        final Image promocodeSettingsIcon = new Image(AssetLoader.getUi().findRegion("settings-menu-promocode-icon"));
        final Label promocodeSettingsValue = new Label(Language.LOCALE.get("settingsPromocode"), UIRenderer.instance().getSkin(), "regular60", Color.WHITE);
        promocodeSettingsValue.setAlignment(Align.left);

        promocodeSettingsGroup.addActor(promocodeSettingsIcon);
        promocodeSettingsGroup.addActor(promocodeSettingsValue);
        promocodeSettingsGroup.setTouchable(Touchable.enabled);

        promocodeSettingsGroup.addListener(new InputListener()
        {
            public void touchUp(InputEvent event, float x, float y, int pointer, int button)
            {
                SoundsHandler.button();

                JumpinSweeties.instance().getRequestHandler().prompt(new IPromptDialog()
                {
                    @Override
                    public void clickedOK(String input)
                    {
                        AnalyticsHelper.trackPromocodeClick(true, input);

                        String result = PromoCodesHelper.enterCode(input);
                        if (result != null) {
                            JumpinSweeties.instance().getPlatformUtils().showMessage(Language.LOCALE.format("promoCodeSuccess", result));
                        } else
                            JumpinSweeties.instance().getPlatformUtils().showMessage(Language.LOCALE.get("wrongCode"));
                    }

                    @Override
                    public void clickedOK()
                    {

                    }

                    @Override
                    public void clickedCancel()
                    {
                        AnalyticsHelper.trackPromocodeClick(false, null);
                    }
                },
                        Language.LOCALE.get("enterPromoCode"),
                        Language.LOCALE.get("proceed"),
                        Language.LOCALE.get("cancel"),
                        "EXAMPLE");
            }

            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button)
            {
                event.stop();
                return true;
            }
        });
        settingsMenuTable.add(promocodeSettingsGroup).fillX().expandX().height(150f).top();
        settingsMenuTable.row();

        // Линия под промокодом
        final Image line4 = new Image(AssetLoader.getUi().findRegion("settings-menu-line"));
        line4.setScaling(Scaling.stretch);
        settingsMenuTable.add(line4).fillX().expandX().height(2f);
        settingsMenuTable.row();

        // Оставить отзыв
        final HorizontalGroup feedbackSettingsGroup = new HorizontalGroup();
        final Image feedbackSettingsIcon = new Image(AssetLoader.getUi().findRegion("settings-menu-feedback-icon"));
        final Label feedbackSettingsValue = new Label(Language.LOCALE.get("settingsFeedback"), UIRenderer.instance().getSkin(), "regular60", Color.WHITE);
        feedbackSettingsValue.setAlignment(Align.left);

        feedbackSettingsGroup.addActor(feedbackSettingsIcon);
        feedbackSettingsGroup.addActor(feedbackSettingsValue);
        feedbackSettingsGroup.setTouchable(Touchable.enabled);

        feedbackSettingsGroup.addListener(new InputListener()
        {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button)
            {
                event.stop();
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button)
            {
                Gdx.net.openURI(AnalyticsHelper.GOOGLE_PLAY_LINK);

                AnalyticsHelper.trackLeaveFeedbackClick();
            }

        });
        settingsMenuTable.add(feedbackSettingsGroup).fillX().expandX().height(150f).top();
        settingsMenuTable.row();

        // Мой сайт
        final Image site = new Image(AssetLoader.getUi().findRegion("settings-menu-site"));
        final Container<Image> siteArea = new Container<>(site);
        siteArea.setBackground(new TextureRegionDrawable(AssetLoader.getUi().findRegion("settings-menu-dark-background")));
        siteArea.addListener(new InputListener()
        {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button)
            {
                event.stop();
                return true;
            }

            public void touchUp(InputEvent event, float x, float y, int pointer, int button)
            {
                Gdx.net.openURI(AnalyticsHelper.SETTINGS_LINK);

                AnalyticsHelper.trackSiteClick();
            }

        });
        settingsMenuTable.add(siteArea).fillX().expandX().height(150f).top();
    }
}
