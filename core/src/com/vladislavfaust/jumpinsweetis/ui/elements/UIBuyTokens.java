package com.vladislavfaust.jumpinsweetis.ui.elements;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.vladislavfaust.jumpinsweetis.JumpinSweeties;
import com.vladislavfaust.jumpinsweetis.gamesystems.MissionController;
import com.vladislavfaust.jumpinsweetis.helpers.AssetLoader;
import com.vladislavfaust.jumpinsweetis.helpers.IAP;
import com.vladislavfaust.jumpinsweetis.helpers.IStandardCallback;
import com.vladislavfaust.jumpinsweetis.helpers.Language;
import com.vladislavfaust.jumpinsweetis.helpers.TokensHandler;
import com.vladislavfaust.jumpinsweetis.helpers.analytics.AnalyticsHelper;
import com.vladislavfaust.jumpinsweetis.ui.UICommonElements;
import com.vladislavfaust.jumpinsweetis.ui.UIRenderer;
import com.vladislavfaust.jumpinsweetis.ui.screens.UIScreenBoosts;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.alpha;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.parallel;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.run;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.scaleTo;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;

/**
 * Created by Faust on 09.05.2015.
 *
 */
public class UIBuyTokens
{
    /*
        ��������
     */

    private final static Table rootTable = new Table(), contentTable = new Table();
    private final static Image overlay = new Image();

    /*
        ����������
     */

    private static boolean shown = false, hiding = false;
    private final static Array<Table> options = new Array<>();

    /*
        ��������� ������
     */

    public static void hide ()
    {
        if (hiding)
            return;
        hiding = true;

        contentTable.addAction(sequence(parallel(
                        alpha(0f, 0.2f, Interpolation.sineIn),
                        scaleTo(2f, 2f, 0.25f, Interpolation.sineIn))
        ));

        overlay.addAction(sequence(
                alpha(0f, 0.25f),
                run(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        contentTable.clear();
                        rootTable.clear();
                        overlay.clear();

                        UIRenderer.instance().getRootStack().removeActor(rootTable);
                        UIRenderer.instance().getRootStack().removeActor(overlay);

                        shown = false;
                        UIRenderer.setBackButtonBlocked(false);
                        hiding = false;
                    }
                })
        ));
    }

    /*
        ������� ������
     */

    public static void show()
    {
        if (shown) return;
        shown = true;

        UIRenderer.setBackButtonBlocked(true);

        /*
            �������
         */

        rootTable.clear();
        contentTable.clear();
        overlay.clear();
        rootTable.setTouchable(Touchable.enabled);
        contentTable.setTouchable(Touchable.enabled);

        /*
            �������
         */

        overlay.setDrawable(new TextureRegionDrawable(AssetLoader.getUi().findRegion("overlay")));
        overlay.setFillParent(true);
        UIRenderer.instance().getRootStack().add(overlay);
        overlay.addAction(sequence(alpha(0f), alpha(1f, 0.3f)));

        /*
            �����
         */

        final Table headerTable = new Table();
        headerTable.setBackground(new TextureRegionDrawable(AssetLoader.getUi().findRegion("buy-more-tokens-header")));

        final Label headerLabel = new Label(Language.LOCALE.get("buyTokensHeader"), UIRenderer.instance().getSkin(), "regular60", Color.WHITE);
        headerLabel.setAlignment(Align.center);
        headerTable.add(headerLabel);

        contentTable.add(headerTable).width(678f).row();

        /*
            ��� ��������-�� �����?
         */

        options.clear();
        final Table allOptionsTable = new Table();
        final float pad = 36f;

        for (int i = 0; i < 4; i++)
        {
            final Table optionTable = new Table();
            optionTable.setBackground(new TextureRegionDrawable(AssetLoader.getUi().findRegion("buy-tokens-small-button")));
            optionTable.align(Align.center);

            final Table amountTable = new Table();
            amountTable.align(Align.center);

            // ���-�� �������

            final Label amountLabel = new Label("", UIRenderer.instance().getSkin(), "stroke70", Color.WHITE);
            amountLabel.setText("x" + TokensHandler.getAmountByPurchaseType(getTokenPurchase(i))+"");
            amountTable.add(amountLabel).spaceRight(4f);

            // ������ ������

            final Image tokenIcon = new Image(AssetLoader.getUi().findRegion("token-shadow"));
            amountTable.add(tokenIcon).padBottom(-3f);

            optionTable.add(amountTable).center().padBottom(3f).padLeft(10f).expandX().row();

            /*
                ������ �������
             */

            final Table priceTable = new Table();

            // ������

            if (IAP.isDiscount(getTokenPurchase(i)) > 0)
            {
                Label discountLabel;
                discountLabel = UICommonElements.discountLabel(IAP.isDiscount(getTokenPurchase(i)));
                discountLabel.setAlignment(Align.left);

                priceTable.add(discountLabel).bottom().left().padLeft(-10f).padBottom(-2f).expandX();

                optionTable.add(priceTable).bottom().expandX().fillX().padBottom(-75f).padTop(-10f);
            }
            else
                optionTable.add(priceTable).bottom().expandX().fillX().padBottom(-75f);



            // ����

            Label priceLabel;
            priceLabel = UICommonElements.priceLabel(getTokenPurchase(i));
            priceLabel.setAlignment(Align.right);

            priceTable.add(priceLabel).bottom().right().padRight(-10f).expandX();



            // ����������

            switch (i)
            {
                case 0:
                    allOptionsTable.add(optionTable).width(280f).height(185f);
                    break;
                case 1:
                    allOptionsTable.add(optionTable).width(280f).height(185f).padLeft(pad).row();
                    break;
                case 2:
                    allOptionsTable.add(optionTable).width(280f).height(185f).padTop(pad);
                    break;
                case 3:
                    allOptionsTable.add(optionTable).width(280f).height(185f).padTop(pad).padLeft(pad);
                    break;
            }

            optionTable.setTouchable(Touchable.enabled);
            optionTable.addListener(new InputListener()
            {
                public boolean touchDown (final InputEvent event, float x, float y, int pointer, int button) {
                    event.cancel();

                    UICommonElements.click(event.getListenerActor());

                    IAP.purchase(new IStandardCallback() {
                        @Override
                        public void success()
                        {
                            int amount = TokensHandler.getAmountByPurchaseType(getTokenPurchase(findOption(event.getListenerActor())));
                            TokensHandler.addTokens(amount);
                            MissionController.tokensCollected(amount, true);

                            UIScreenBoosts.updateTokenCounters();

                            JumpinSweeties.instance().getPlatformUtils().showMessage(Language.LOCALE.get("purchaseSuccess"));

                            AnalyticsHelper.trackPurchaseTokensClick(getTokenPurchase(findOption(event.getListenerActor())), true);
                        }

                        @Override
                        public void error(String e)
                        {
                            JumpinSweeties.instance().getPlatformUtils().showMessage(Language.LOCALE.format("purchaseError", e));
                        }

                        @Override
                        public void cancelled()
                        {
                            JumpinSweeties.instance().getPlatformUtils().showMessage(Language.LOCALE.get("purchaseCancel"));

                            AnalyticsHelper.trackPurchaseTokensClick(getTokenPurchase(findOption(event.getListenerActor())), false);
                        }
                    }, getTokenPurchase(findOption(event.getListenerActor())));

                    return true;
                }
            });

            options.add(optionTable);
        }
        contentTable.add(allOptionsTable).pad(pad);

        /*
            �������
         */

        rootTable.add(contentTable).width(678f).center();
        UIRenderer.instance().getRootStack().addActor(overlay);
        UIRenderer.instance().getRootStack().addActor(rootTable);
        UIRenderer.instance().getRootStack().pack();

        contentTable.setBackground(new TextureRegionDrawable(AssetLoader.getUi().findRegion("dialog-background")));
        contentTable.setVisible(false);
        contentTable.setTransform(true);
        contentTable.setOrigin(Align.center);
        contentTable.addAction(sequence(alpha(0f), scaleTo(2f, 2f), parallel(run(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                contentTable.setVisible(true);
                            }
                        }), alpha(1f, 0.2f, Interpolation.sineIn), scaleTo(1f, 1f, 0.25f, Interpolation.sineIn))));

        contentTable.addListener(new InputListener()
        {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button)
            {
                event.cancel();
                return true;
            }
        });

        rootTable.addListener(new InputListener()
        {
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                hide();
                return true;
            }
        });
    }

    private static IAP.PURCHASE_TYPE getTokenPurchase (int i)
    {
        switch (i)
        {
            case 0:
                return IAP.PURCHASE_TYPE.TOKENS_FEW;
            case 1:
                return IAP.PURCHASE_TYPE.TOKENS_SOME;
            case 2:
                return IAP.PURCHASE_TYPE.TOKENS_MANY;
            case 3:
                return IAP.PURCHASE_TYPE.TOKENS_LOTSOF;
            default:
                return null;
        }
    }

    private static int findOption (Actor option)
    {
        for (int i = 0; i < options.size; i++)
            if (options.get(i).equals(option))
                return i;

        return -1;
    }


    /*
        �������
     */

    public static boolean isShown()
    {
        return shown;
    }
}
