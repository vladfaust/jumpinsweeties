package com.vladislavfaust.jumpinsweetis.ui.elements;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.ui.HorizontalGroup;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.vladislavfaust.jumpinsweetis.gamesystems.MissionController;
import com.vladislavfaust.jumpinsweetis.helpers.AssetLoader;
import com.vladislavfaust.jumpinsweetis.helpers.Language;
import com.vladislavfaust.jumpinsweetis.helpers.SoundsHandler;
import com.vladislavfaust.jumpinsweetis.ui.UIRenderer;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.alpha;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.delay;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.moveBy;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.parallel;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.run;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;

/**
 * Created by Faust on 23.04.2015.
 *
 */
public class UIMissionNotification
{
    private static boolean shown = false;

    public static boolean isShown()
    {
        return shown;
    }

    // Показать выполнение задания
    public static void completeMission ()
    {
        MissionController.showCompletion();

        shown = true;
        UIRenderer.setBackButtonBlocked(true);

        float
                completeMissionAnimationTimeIn = 0.3f,
                completeMissionAnimationTimeOut = 0.3f,
                completeMissionAnimationShow = 1.5f,
                between = 0.1f,
                newMissionAnimationTimeIn = 0.3f,
                newMissionAnimationTimeOut = 0.3f,
                newMissionAnimationShow = 2.5f;

        /*
            Оверлей
         */
        final Image overlay = new Image(AssetLoader.getUi().findRegion("overlay"));
        overlay.setFillParent(true);

        overlay.addAction(parallel(
                alpha(1f, 0.1f),
                sequence(delay(completeMissionAnimationTimeIn), delay(completeMissionAnimationShow), delay(completeMissionAnimationTimeOut - between), delay(newMissionAnimationTimeIn), delay(newMissionAnimationShow), delay(newMissionAnimationTimeOut), run(new Runnable() {
                    @Override
                    public void run()
                    {
                        UIRenderer.instance().getRootStack().removeActor(overlay);

                        shown = false;

                        if (UIShareAchievement.isShown() || UIGifting.isShown() || UIBuyTokens.isShown() || UISuggestionMenu.isSuggestionMenuShown)
                            return;

                        UIRenderer.setBackButtonBlocked(false);
                    }
                }))));
        UIRenderer.instance().getRootStack().add(overlay);

        /*
            Окно выполненной миссии
        */
        // Таблица окна
        final Table missionCompleteTable = new Table();

        // Хедер
        final Label headerLabel = new Label("", UIRenderer.instance().getSkin(), "regular60", Color.WHITE);
        headerLabel.setText(Language.LOCALE.format("missionAccomplished", MissionController.getCurrentId()));
        headerLabel.setAlignment(Align.center);
        final Image headerImage = new Image(AssetLoader.getUi().findRegion("mission-background"));
        final Stack headerStack = new Stack();
        headerStack.add(headerImage);
        headerStack.add(headerLabel);
        missionCompleteTable.add(headerStack).top().row();

        // Тело
        final Table contentTable = new Table();
        final Label rewardLabel = new Label(Language.LOCALE.get("yourRewardAfterMission"), UIRenderer.instance().getSkin(), "regular60", Color.valueOf("1eb55cff"));
        contentTable.add(rewardLabel).row();
        final HorizontalGroup rewardValueGroup = new HorizontalGroup();
        final Label rewardValue = new Label("", UIRenderer.instance().getSkin(), "regular80", Color.valueOf("1eb55cff"));
        rewardValue.setText(" " + MissionController.getPreviousMissionReward());
        final Image candie = new Image(AssetLoader.getUi().findRegion("candie"));
        rewardValueGroup.addActor(candie);
        rewardValueGroup.addActor(rewardValue);
        contentTable.add(rewardValueGroup).padLeft(-15f);
        contentTable.setBackground(new TextureRegionDrawable(AssetLoader.getUi().findRegion("dialog-background")));
        missionCompleteTable.add(contentTable).width(678f).height(240f);

        // Добавляем это все в стак сцены
        UIRenderer.instance().getRootStack().add(missionCompleteTable);

        // Делаем анимацию окна выполненной миссии
        missionCompleteTable.setTransform(true);
        missionCompleteTable.addAction(sequence(
                alpha(0f),
                moveBy(0f, -200f),
                parallel(
                        alpha(1f, completeMissionAnimationTimeIn, Interpolation.sineIn),
                        moveBy(0f, 200f, completeMissionAnimationTimeIn, Interpolation.sineOut),
                        run(new Runnable() {
                            @Override
                            public void run()
                            {
                                SoundsHandler.missionCompleted();
                            }
                        })
                ),
                delay(completeMissionAnimationShow),
                parallel(
                        alpha(0f, completeMissionAnimationTimeOut, Interpolation.sineOut),
                        moveBy(0f, 200f, completeMissionAnimationTimeOut, Interpolation.sineIn)
                )
        ));

        /*
            Окно новой миссии
         */

        // Таблица окна
        final Table newMissionTable = new Table();

        // Хедер
        final Label newMissionHeaderLabel = new Label("", UIRenderer.instance().getSkin(), "regular60", Color.WHITE);
        newMissionHeaderLabel.setText(Language.LOCALE.get("youGotNewMission"));
        newMissionHeaderLabel.setAlignment(Align.center);
        final Image newHeaderImage = new Image(AssetLoader.getUi().findRegion("mission-background"));
        final Stack newHeaderStack = new Stack();
        newHeaderStack.add(newHeaderImage);
        newHeaderStack.add(newMissionHeaderLabel);
        newMissionTable.add(newHeaderStack).top().row();

        // Тело
        final Table newContentTable = new Table();
        final Label newMissionLabel = new Label("", UIRenderer.instance().getSkin(), "regular60", Color.valueOf("1eb55cff"));
        newMissionLabel.setText(MissionController.getDescription(MissionController.getCurrentId()));
        newMissionLabel.setAlignment(Align.center);
        newContentTable.add(newMissionLabel).center();
        newContentTable.setBackground(new TextureRegionDrawable(AssetLoader.getUi().findRegion("dialog-background")));
        newMissionTable.add(newContentTable).width(678f).height(240f);

        // Добавляем это все в стак сцены
        UIRenderer.instance().getRootStack().add(newMissionTable);

        // Делаем анимацию окна выполненной миссии
        newMissionTable.setTransform(true);
        newMissionTable.addAction(sequence(
                alpha(0f),
                delay(completeMissionAnimationTimeIn),
                delay(completeMissionAnimationShow),
                delay(completeMissionAnimationTimeOut),
                moveBy(0f, -200f),
                parallel(
                        alpha(1f, newMissionAnimationTimeIn, Interpolation.sineIn),
                        moveBy(0f, 200f, newMissionAnimationTimeIn, Interpolation.sineOut),
                        run(new Runnable() {
                            @Override
                            public void run()
                            {
                                //SoundsHandler.newMission();
                            }
                        })
                ),
                delay(newMissionAnimationShow),
                parallel(
                        alpha(0f, newMissionAnimationTimeOut, Interpolation.sineOut),
                        moveBy(0f, 200f, newMissionAnimationTimeOut, Interpolation.sineIn)
                ),
                run(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        UIRenderer.instance().getRootStack().removeActor(newMissionTable);
                        UIRenderer.instance().getRootStack().removeActor(missionCompleteTable);
                    }
                })
        ));
    }
}
