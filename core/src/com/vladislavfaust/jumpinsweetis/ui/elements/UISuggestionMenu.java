package com.vladislavfaust.jumpinsweetis.ui.elements;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Scaling;
import com.vladislavfaust.jumpinsweetis.JumpinSweeties;
import com.vladislavfaust.jumpinsweetis.helpers.AssetLoader;
import com.vladislavfaust.jumpinsweetis.helpers.Language;
import com.vladislavfaust.jumpinsweetis.helpers.MyPreferences;
import com.vladislavfaust.jumpinsweetis.helpers.Utils;
import com.vladislavfaust.jumpinsweetis.helpers.advertisment.AdsManager;
import com.vladislavfaust.jumpinsweetis.helpers.analytics.AnalyticsHelper;
import com.vladislavfaust.jumpinsweetis.helpers.gifts.GiftsCooldownHelper;
import com.vladislavfaust.jumpinsweetis.ui.UICommonElements;
import com.vladislavfaust.jumpinsweetis.ui.UIRenderer;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.alpha;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.delay;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.forever;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.moveBy;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.parallel;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.run;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.scaleTo;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;

/**
 * Created by Faust on 23.04.2015.
 *
 */
public class UISuggestionMenu
{
    // Настройки
    private final static float
            SUGGESTION_SCALE_IN_VALUE_X = 1.03f,
            SUGGESTION_SCALE_IN_VALUE_Y = 1.03f,
            SUGGESTION_SCALE_IN_TIME = 0.3f,
            SUGGESTION_SCALE_OUT_TIME = 0.3f;

    private final static Interpolation
            SUGGESTION_SCALE_IN_INTERPOLATION = Interpolation.sineIn,
            SUGGESTION_SCALE_OUT_INTERPOLATION = Interpolation.sineOut;

    // Переменные
    public static boolean isSuggestionMenuShown;
    private final static Table
            rootTable = new Table(),
            contentTable = new Table();
    private final static Image
            overlay = new Image();

    // Содержимое рекламного стака
    private final static Stack incentivizedStack = new Stack();
    private final static Image incentivizedIcon = new Image();
    private final static Image incentivizedBackground = new Image();

    public static void updateIncentivizedStack (boolean ready)
    {
        incentivizedStack.clear();
        incentivizedStack.add(incentivizedBackground);

        if (ready)
        {
            incentivizedIcon.setDrawable(new TextureRegionDrawable(AssetLoader.getUi().findRegion("suggestion-ad")));
            incentivizedIcon.setScaling(Scaling.none);
            final Label incentivizedTitle = new Label(Language.LOCALE.format("getRewardForAction", AdsManager.REWARD_FOR_INCENTIVIZED), UIRenderer.instance().getSkin(), "regular60", Color.WHITE);
            incentivizedTitle.setAlignment(Align.center);

            incentivizedIcon.addAction(moveBy(-207f, 0f));
            incentivizedTitle.addAction(moveBy(68f, 0f));

            incentivizedStack.add(incentivizedIcon);
            incentivizedStack.add(incentivizedTitle);

            incentivizedStack.pack();
            incentivizedStack.setOrigin(Align.center);

            incentivizedStack.addAction(sequence(delay(SUGGESTION_SCALE_IN_TIME / 2f), forever(sequence(scaleTo(SUGGESTION_SCALE_IN_VALUE_X, SUGGESTION_SCALE_IN_VALUE_Y, SUGGESTION_SCALE_IN_TIME, SUGGESTION_SCALE_IN_INTERPOLATION), scaleTo(1f, 1f, SUGGESTION_SCALE_OUT_TIME, SUGGESTION_SCALE_OUT_INTERPOLATION), delay(1f)))));

            incentivizedStack.addListener(new InputListener()
            {
                public boolean touchDown(InputEvent event, float x, float y, int pointer, int button)
                {
                    event.stop();

                    UICommonElements.click(event.getListenerActor());

                    AdsManager.showIncentivizedContent();
                    return false;
                }
            });

            AnalyticsHelper.trackIncentivizedOffered();
        }
        else
        {
            final Label incentivizedReadyInLabel = new Label("", UIRenderer.instance().getSkin(), "regular60", Color.WHITE);
            incentivizedReadyInLabel.setAlignment(Align.center);

            incentivizedReadyInLabel.addAction(forever(sequence(run(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            double time = AdsManager.howMuchLeftToAllowIncentivizedContent();
                            time = (time > 0) ? time : 0;
                            incentivizedReadyInLabel.setText(Language.LOCALE.format("willBeReadyIn", Utils.getHours(time), Utils.getMinutes(time), Utils.getSeconds(time)));
                        }
                    }), delay(0.2f))));

            incentivizedStack.addListener(new InputListener()
            {
                public boolean touchDown(InputEvent event, float x, float y, int pointer, int button)
                {
                    event.stop();
                    UICommonElements.error(incentivizedStack);
                    return false;
                }
            });

            incentivizedStack.add(incentivizedReadyInLabel);
            incentivizedStack.pack();
        }
    }

    // Содержимое гифт стака
    private final static Stack giftStack = new Stack();
    private final static Image giftIcon = new Image();
    private final static Image giftBackground = new Image();

    public static void updateGiftStack (boolean ready, boolean gotATicket)
    {
        giftStack.clear();
        giftStack.add(giftBackground);

        // Все готово
        if (ready && gotATicket)
        {
            giftIcon.setDrawable(new TextureRegionDrawable(AssetLoader.getUi().findRegion("suggestion-gift")));
            giftIcon.setScaling(Scaling.none);
            final Label giftTitle = new Label(Language.LOCALE.get("giftsAvailable"), UIRenderer.instance().getSkin(), "regular60", Color.WHITE);
            giftTitle.setAlignment(Align.center);

            giftIcon.addAction(moveBy(206f, 0f));
            giftTitle.addAction(moveBy(-81f, 0f));

            giftStack.add(giftIcon);
            giftStack.add(giftTitle);

            giftStack.pack();
            giftStack.setOrigin(Align.center);

            giftStack.addAction(forever(sequence(scaleTo(SUGGESTION_SCALE_IN_VALUE_X, SUGGESTION_SCALE_IN_VALUE_Y, SUGGESTION_SCALE_IN_TIME, SUGGESTION_SCALE_IN_INTERPOLATION), scaleTo(1f, 1f, SUGGESTION_SCALE_OUT_TIME, SUGGESTION_SCALE_OUT_INTERPOLATION), delay(1f))));

            giftStack.addListener(new InputListener()
            {
                public boolean touchDown(InputEvent event, float x, float y, int pointer, int button)
                {
                    event.stop();

                    UICommonElements.click(event.getListenerActor());

                    if (UIGifting.isShown())
                        return false;

                    UIGifting.show();
                    GiftsCooldownHelper.afterGiveaway();

                    return false;
                }
            });

            AnalyticsHelper.trackGiftsOffered();
        }

        // Нужен билет
        else if ((ready && !gotATicket) || MyPreferences.getLastGiveawayNumber() < 4)
        {
            final Label waitingForATicketLabel = new Label(Language.LOCALE.get("giftsUnavailable"), UIRenderer.instance().getSkin(), "regular60", Color.WHITE);
            waitingForATicketLabel.setAlignment(Align.center);

            giftStack.addListener(new InputListener()
            {
                public boolean touchDown(InputEvent event, float x, float y, int pointer, int button)
                {
                    event.stop();
                    UICommonElements.error(giftStack);
                    return false;
                }
            });

            giftStack.add(waitingForATicketLabel);
            giftStack.pack();
        }

        // Показать таймер
        else
        {
            final Label giftReadyInLabel = new Label("", UIRenderer.instance().getSkin(), "regular60", Color.WHITE);
            giftReadyInLabel.setAlignment(Align.center);

            giftReadyInLabel.addAction(forever(sequence(run(new Runnable()
            {
                @Override
                public void run()
                {
                    double time = (double) GiftsCooldownHelper.getCooldown() / 1000f;
                    if (time <= 0)
                        updateGiftStack(true, GiftsCooldownHelper.doesHaveATicket());
                    giftReadyInLabel.setText(Language.LOCALE.format("willBeReadyIn", Utils.getHours(time), Utils.getMinutes(time), Utils.getSeconds(time)));
                }
            }), delay(0.2f))));

            giftStack.addListener(new InputListener()
            {
                public boolean touchDown(InputEvent event, float x, float y, int pointer, int button)
                {
                    event.stop();
                    UICommonElements.error(giftStack);
                    return false;
                }
            });

            giftStack.add(giftReadyInLabel);
            giftStack.pack();
        }
    }

    // Скрытие предложений
    private static boolean isHiding;
    public static void hide (boolean immediateGame)
    {
        if (isHiding)
            return;
        isHiding = true;

        if (!immediateGame) {
            contentTable.addAction(
                sequence(
                    parallel(
                        alpha(0f, 0.2f, Interpolation.sineIn),
                        scaleTo(2f, 2f, 0.25f, Interpolation.sineIn))
            ));

            overlay.addAction(
                sequence(
                    alpha(0f, 0.25f),
                    run(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            contentTable.clear();
                            rootTable.clear();
                            overlay.clear();

                            UIRenderer.instance().getRootStack().removeActor(rootTable);
                            UIRenderer.instance().getRootStack().removeActor(overlay);

                            isSuggestionMenuShown = false;
                            UIRenderer.setBackButtonBlocked(false);
                            isHiding = false;
                        }
                    })
            ));
        }
        else
        {
            contentTable.addAction(
                sequence(
                    parallel(
                        alpha(0f, 0.2f, Interpolation.sineIn),
                        scaleTo(2f, 2f, 0.20f, Interpolation.sineIn))
            ));

            overlay.addAction(
                parallel(
                    alpha(0f, 0.25f),
                    sequence(
                        delay(0.14f),
                        run(new Runnable()
                        {
                            @Override
                            public void run()
                            {
                                contentTable.clear();
                                rootTable.clear();
                                overlay.clear();

                                UIRenderer.instance().getRootStack().removeActor(rootTable);
                                UIRenderer.instance().getRootStack().removeActor(overlay);

                                isSuggestionMenuShown = false;
                                UIRenderer.setBackButtonBlocked(false);
                                isHiding = false;

                                JumpinSweeties.instance().startGame();
                            }
                        })
                    )
            ));
        }
    }

    // Показ предложений
    public static void show ()
    {
        // Предварительная очистка
        overlay.clear();
        rootTable.clear();
        contentTable.clear();

        UIRenderer.setBackButtonBlocked(true);
        isSuggestionMenuShown = true;

        rootTable.setTouchable(Touchable.enabled);
        contentTable.setTouchable(Touchable.enabled);

        /*
            Оверлэй
         */

        overlay.setDrawable(new TextureRegionDrawable(AssetLoader.getUi().findRegion("overlay")));
        overlay.setFillParent(true);
        UIRenderer.instance().getRootStack().add(overlay);
        overlay.addAction(sequence(
                alpha(0f),
                alpha(1f, 0.3f)
        ));

        /*
            Хедер
         */

        final Stack headerStack = new Stack();
        final Image headerBackground = new Image(AssetLoader.getUi().findRegion("suggestion-header-background"));
        final Label headerTitle = new Label(Language.LOCALE.get("freebiesTime"), UIRenderer.instance().getSkin(), "regular60", Color.WHITE);
        headerTitle.setAlignment(Align.center);

        headerStack.add(headerBackground);
        headerStack.add(headerTitle);
        headerStack.pack();

        contentTable.add(headerStack).expandX();
        contentTable.row();

        /*
            Сами предложения
         */

        final Table suggestionsTable = new Table();
        suggestionsTable.setBackground(new TextureRegionDrawable(AssetLoader.getUi().findRegion("dialog-background")));

        /*
            Подарки
         */

        giftStack.clear();
        giftBackground.setDrawable(new TextureRegionDrawable(AssetLoader.getUi().findRegion("suggestion-gift-background")));
        giftStack.add(giftBackground);
        giftStack.setTouchable(Touchable.enabled);
        giftStack.setTransform(true);

        updateGiftStack(GiftsCooldownHelper.getCooldown() <= 0, GiftsCooldownHelper.doesHaveATicket());

        suggestionsTable.add(giftStack).pad(34f);
        suggestionsTable.row();

        /*
            Оплачиваемый контент
         */

        incentivizedStack.clear();
        incentivizedBackground.setDrawable(new TextureRegionDrawable(AssetLoader.getUi().findRegion("suggestion-ad-background")));
        incentivizedStack.add(incentivizedBackground);
        incentivizedStack.setTransform(true);
        incentivizedStack.setTouchable(Touchable.enabled);

        updateIncentivizedStack(AdsManager.readyToShowIncentivizedContent());

        suggestionsTable.add(incentivizedStack).pad(34f).padTop(0f);
        suggestionsTable.row();

        contentTable.add(suggestionsTable).width(678f);
        contentTable.row();

        /*
            Кнопка "закрыть"
         *//*

        final Stack closeButtonStack = new Stack();
        final Image closeButtonBackground = new Image(AssetLoader.getUi().findRegion("suggestion-closebutton-background"));
        final Label closeButtonTitle = new Label(Language.LOCALE.get("closeButton"), UIRenderer.instance().getSkin(), "regular70", Color.WHITE);
        closeButtonTitle.setAlignment(Align.center);

        closeButtonStack.add(closeButtonBackground);
        closeButtonStack.add(closeButtonTitle);

        closeButtonStack.addListener(new InputListener()
        {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button)
            {
                event.stop();
                SoundsHandler.button();
                hide(true);
                return true;
            }
        });

        contentTable.add(closeButtonStack).expandX();*/

        rootTable.add(contentTable).expand();
        UIRenderer.instance().getRootStack().add(rootTable);
        UIRenderer.instance().getRootStack().pack();

        /*
            Анимация всей штуки
         */

        contentTable.setVisible(false);
        contentTable.setTransform(true);
        contentTable.setOrigin(Align.center);
        contentTable.addAction(sequence(
                alpha(0f),
                scaleTo(2f, 2f),
                parallel(
                        run(new Runnable() {
                            @Override
                            public void run()
                            {
                                contentTable.setVisible(true);
                            }
                        }),
                        alpha(1f, 0.2f, Interpolation.sineIn),
                        scaleTo(1f, 1f, 0.25f, Interpolation.sineIn)
                )
        ));

        /*
            Реакция на клик по руту
         */

        rootTable.addListener(new InputListener()
        {
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button)
            {
                hide(true);
                return false;
            }
        });
    }
}
