package com.vladislavfaust.jumpinsweetis.ui.elements;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.vladislavfaust.jumpinsweetis.JumpinSweeties;
import com.vladislavfaust.jumpinsweetis.helpers.AssetLoader;
import com.vladislavfaust.jumpinsweetis.helpers.CandiesHandler;
import com.vladislavfaust.jumpinsweetis.helpers.Language;
import com.vladislavfaust.jumpinsweetis.helpers.MyPreferences;
import com.vladislavfaust.jumpinsweetis.helpers.ScreenShaker;
import com.vladislavfaust.jumpinsweetis.helpers.SoundsHandler;
import com.vladislavfaust.jumpinsweetis.helpers.analytics.AnalyticsHelper;
import com.vladislavfaust.jumpinsweetis.helpers.dialogs.IConfirmDialog;
import com.vladislavfaust.jumpinsweetis.helpers.googleplay.AchievementsHandler;
import com.vladislavfaust.jumpinsweetis.ui.UICommonElements;
import com.vladislavfaust.jumpinsweetis.ui.UIRenderer;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.alpha;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.moveBy;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.parallel;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.run;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;

/**
 * Created by Faust on 10.05.2015.
 *
 */
public class UIShareAchievement
{
    /*
        ���������
     */

    private final static Color
            COLOR_LIGHT_ORANGE = Color.valueOf("e78c19ff"),
            COLOR_DARK_ORANGE = Color.valueOf("931700ff");

    /*
        ��������
     */

    private final static Table rootTable = new Table(), contentTable = new Table();
    private final static Image overlay = new Image();
    private static boolean shown = false, hiding = false, shared = false;
    private static int thisTimeShared = -1;

    public static boolean isShown()
    {
        return shown;
    }

    /*
        ���������
     */

    private static void hide ()
    {
        if (hiding) return;
        hiding = true;

        contentTable.addAction(parallel(
                        alpha(0f, 0.3f, Interpolation.sineOut),
                        moveBy(0f, 200f, 0.3f, Interpolation.sineIn)
                ));

        overlay.addAction(sequence(alpha(0f, 0.3f), run(new Runnable()
        {
            @Override
            public void run()
            {
                contentTable.clear();
                rootTable.clear();
                overlay.clear();

                UIRenderer.instance().getRootStack().removeActor(rootTable);
                UIRenderer.instance().getRootStack().removeActor(overlay);

                shown = false;
                UIRenderer.setBackButtonBlocked(false);
                hiding = false;
            }
        })));
    }

    public static void tryToHide ()
    {
        if (!shared && !JumpinSweeties.DEBUG)
        {
            JumpinSweeties.instance().getRequestHandler().confirm(new IConfirmDialog() {
                @Override
                public void clickedOK()
                {
                    shared = true;
                    AnalyticsHelper.trackAchievementShareScreen(thisTimeShared, true, true);
                }

                @Override
                public void clickedCancel()
                {
                    AnalyticsHelper.trackAchievementShareScreen(thisTimeShared, false, true);

                    hide();
                }
            },
                    Language.LOCALE.get("confirm"),
                    Language.LOCALE.format("youReallyWantToClose", AchievementsHandler.ACHIEVEMENT_SHARE_REWARD),
                    Language.LOCALE.get("share"),
                    Language.LOCALE.get("close"));
        }
        else {
            AnalyticsHelper.trackAchievementShareScreen(thisTimeShared, true, false);

            hide();
        }
    }

    public static void show (final int id)
    {
        if (shown) return;
        shown = true;

        shared = false;

        thisTimeShared = id;
        AchievementsHandler.resetNextShared();

        UIRenderer.setBackButtonBlocked(true);

        /*
            �������
         */

        overlay.clear();
        overlay.setDrawable(new TextureRegionDrawable(AssetLoader.getUi().findRegion("overlay")));
        overlay.setFillParent(true);
        overlay.addAction(sequence(alpha(0f), alpha(1f, 0.3f)));

        /*
            ������
         */

        rootTable.clear();

        /*
            �������
         */

        contentTable.clear();
        contentTable.setBackground(new TextureRegionDrawable(AssetLoader.getUi().findRegion("dialog-background")));

        /*
            �����
         */

        final Table headerTable = new Table();
        headerTable.setBackground(new TextureRegionDrawable(AssetLoader.getUi().findRegion("achievements-notification-background")));
        final Label headerLabel = new Label(Language.LOCALE.get("achievementUnlockedHeader"), UIRenderer.instance().getSkin(), "regular60", Color.WHITE);
        headerLabel.setAlignment(Align.center);
        headerTable.add(headerLabel).center();

        contentTable.add(headerTable).row();

        /*
            ������ � ��������
         */

        final Table firstInfoTable = new Table();
        firstInfoTable.align(Align.center);

        final Label spentTime = new Label("", UIRenderer.instance().getSkin(), "regular50", COLOR_LIGHT_ORANGE);
        long timeSpent = System.currentTimeMillis() - MyPreferences.getFirstLaunchTime();
        if (((double) timeSpent) / 86400000f >= 1f)
            spentTime.setText(Language.LOCALE.format("timeSpentDays", ((int) Math.floor(((double) timeSpent) / 86400000f))));
        else if (((double) timeSpent)/3600000f >= 1f)
            spentTime.setText(Language.LOCALE.format("timeSpentHours", ((int) Math.floor(((double) timeSpent) / 3600000f))));
        else if (((double) timeSpent)/60000 >= 1f)
            spentTime.setText(Language.LOCALE.format("timeSpentMin", ((int) Math.floor(((double) timeSpent) / 60000f))));
        else
            spentTime.setText(Language.LOCALE.format("timeSpentMin", 1));

        spentTime.setOrigin(Align.center);
        spentTime.setAlignment(Align.center);
        firstInfoTable.add(spentTime).width(224f);

        final Image achievementIcon = new Image(AssetLoader.getUi().findRegion("achievement", id));
        achievementIcon.setAlign(Align.center);
        firstInfoTable.add(achievementIcon).center();

        final Label percentageLabel = new Label("", UIRenderer.instance().getSkin(), "regular50", COLOR_LIGHT_ORANGE);
        percentageLabel.setText(Language.LOCALE.format("rarePercent", AchievementsHandler.getAchievements().get(id).getPercentage()));
        percentageLabel.setOrigin(Align.center);
        percentageLabel.setAlignment(Align.center);
        firstInfoTable.add(percentageLabel).width(224f);

        contentTable.add(firstInfoTable).expandX().width(678f).padTop(39f).row();

        /*
            ��������
         */

        final Label nameLabel = new Label(AchievementsHandler.getAchievements().get(id).getName(), UIRenderer.instance().getSkin(), "regular60", COLOR_DARK_ORANGE);
        nameLabel.setAlignment(Align.center);
        contentTable.add(nameLabel).padTop(24f).row();

        final Label descriptionLabel = new Label(Language.LOCALE.get(AchievementsHandler.getAchievements().get(id).getDescKey()), UIRenderer.instance().getSkin(), "regular50", COLOR_LIGHT_ORANGE);
        descriptionLabel.setAlignment(Align.center);
        contentTable.add(descriptionLabel).padTop(-6f).row();

        /*
            ������ �������
         */

        final Table shareButton = new Table();
        shareButton.setBackground(new TextureRegionDrawable(AssetLoader.getUi().findRegion("achievements-share-background")));
        shareButton.center();

        final Table shareLabels = new Table();
        shareLabels.center();

        final Label shareItLabel = new Label(Language.LOCALE.get("shareAction"), UIRenderer.instance().getSkin(), "regular60", Color.WHITE);
        shareItLabel.setAlignment(Align.center);
        shareLabels.add(shareItLabel).row();

        final Label getCandiesLabel = new Label(Language.LOCALE.format("andGetCandies", AchievementsHandler.ACHIEVEMENT_SHARE_REWARD), UIRenderer.instance().getSkin(), "regular50", Color.valueOf("ffc377fff"));
        getCandiesLabel.setAlignment(Align.center);
        shareLabels.add(getCandiesLabel).padTop(-12f);

        shareButton.add(shareLabels).padBottom(3f);

        contentTable.add(shareButton).pad(34f).padTop(19f).width(610f).height(185f).expand();

        shareButton.setTouchable(Touchable.enabled);
        shareButton.addListener(new InputListener()
        {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button)
            {
                event.cancel();

                UICommonElements.makeAScreenshot(Language.LOCALE.format("iVeUnlockedIt", AchievementsHandler.getAchievements().get(id).getName(), AnalyticsHelper.SHARE_LINK), true);

                if (!shared)
                {
                    CandiesHandler.addCandiesAsReward(AchievementsHandler.ACHIEVEMENT_SHARE_REWARD);
                    JumpinSweeties.instance().getPlatformUtils().showMessage(Language.LOCALE.format("shared", AchievementsHandler.ACHIEVEMENT_SHARE_REWARD));
                    shared = true;
                }

                return true;
            }
        });

        /*
            ��������
         */

        rootTable.add(contentTable).width(678f).center();

        UIRenderer.instance().getRootStack().add(overlay);
        UIRenderer.instance().getRootStack().add(rootTable);
        UIRenderer.instance().getRootStack().pack();

        contentTable.setTransform(true);
        contentTable.setOrigin(Align.center);

        contentTable.addAction(sequence(
                alpha(0f),
                moveBy(0f, -200f),
                parallel(
                        alpha(1f, 0.3f, Interpolation.sineIn),
                        moveBy(0f, 200f, 0.3f, Interpolation.sineOut),
                        run(new Runnable() {
                            @Override
                            public void run()
                            {
                                ScreenShaker.shake(((OrthographicCamera) UIRenderer.instance().getStage().getCamera()), 30f, 1.5f);
                                SoundsHandler.achievement();
                            }
                        })
                )
        ));

        UICommonElements.attention(shareButton, true);

        /*
            �����
         */

        contentTable.setTouchable(Touchable.enabled);
        contentTable.addListener(new InputListener()
        {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button)
            {
                event.cancel();
                return true;
            }
        });

        rootTable.setTouchable(Touchable.enabled);
        rootTable.addListener(new InputListener()
        {
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button)
            {
                event.cancel();

                tryToHide();

                return true;
            }
        });
    }
}
