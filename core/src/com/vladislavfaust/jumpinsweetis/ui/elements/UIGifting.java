package com.vladislavfaust.jumpinsweetis.ui.elements;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Stack;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Scaling;
import com.vladislavfaust.jumpinsweetis.JumpinSweeties;
import com.vladislavfaust.jumpinsweetis.gamesystems.MissionController;
import com.vladislavfaust.jumpinsweetis.helpers.AssetLoader;
import com.vladislavfaust.jumpinsweetis.helpers.CandiesHandler;
import com.vladislavfaust.jumpinsweetis.helpers.IAP;
import com.vladislavfaust.jumpinsweetis.helpers.IStandardCallback;
import com.vladislavfaust.jumpinsweetis.helpers.JumperSkinsHandler;
import com.vladislavfaust.jumpinsweetis.helpers.Language;
import com.vladislavfaust.jumpinsweetis.helpers.SoundsHandler;
import com.vladislavfaust.jumpinsweetis.helpers.TokensHandler;
import com.vladislavfaust.jumpinsweetis.helpers.analytics.AnalyticsHelper;
import com.vladislavfaust.jumpinsweetis.helpers.dialogs.IConfirmDialog;
import com.vladislavfaust.jumpinsweetis.helpers.gifts.GiftingProcessHelper;
import com.vladislavfaust.jumpinsweetis.helpers.gifts.GiftsCooldownHelper;
import com.vladislavfaust.jumpinsweetis.ui.UICommonElements;
import com.vladislavfaust.jumpinsweetis.ui.UIRenderer;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.alpha;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.delay;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.forever;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.moveBy;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.parallel;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.repeat;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.run;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.scaleTo;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;

/**
 * Created by Faust on 29.04.2015.
 *
 */
public class UIGifting
{
    /*
        Элементы
     */

    final static Table rootTable = new Table();
    final static Table contentTable = new Table();
    final static TextureRegionDrawable backgroundImage = new TextureRegionDrawable();

    static Label text;

    final static Table giftMatrix = new Table();
    final static Array<Table> giftCells = new Array<>();
    final static boolean[] cellLocked = new boolean[9];

    final static Stack openAllButton = new Stack();
    final static Image openAllBackground = new Image();
    static Label openAllLabel;
    final static Image openAllIcon = new Image();

    /*
        Переменные
     */

    private static boolean shown = false, readyToBeClosed = false, stillUnlocking = false, openAllBought = false;
    private static int howMuchLeftToUnlock = 0;

    public static void init ()
    {
        for (int i = 0; i < 9; i++)
            cellLocked[i] = true;

        shown = false;
        readyToBeClosed = false;
        stillUnlocking = false;

        backgroundImage.setRegion(AssetLoader.getGiftingBackground());

        openAllBackground.setDrawable(new TextureRegionDrawable(AssetLoader.getUi().findRegion("open-all-gifts-background")));
        openAllIcon.setDrawable(new TextureRegionDrawable(AssetLoader.getUi().findRegion("open-all-gifts-icon")));
    }

    private static void hide ()
    {
        UISuggestionMenu.updateGiftStack(GiftsCooldownHelper.lastChance(), false);

        rootTable.addAction(sequence(parallel(alpha(0f, 0.2f, Interpolation.sineIn), scaleTo(2f, 2f, 0.25f, Interpolation.sineIn)), run(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        UISuggestionMenu.updateGiftStack(GiftsCooldownHelper.getCooldown() <= 0, GiftsCooldownHelper.doesHaveATicket());
                        rootTable.clear();
                        UIRenderer.instance().getRootStack().removeActor(rootTable);
                        shown = false;
                    }
                })));
    }

    public static void tryToHide ()
    {
        if (GiftsCooldownHelper.lastChance()&& !openAllBought)
        {
            JumpinSweeties.instance().getRequestHandler().confirm(new IConfirmDialog()
            {
                @Override
                public void clickedOK()
                {
                    IAP.purchase(new IStandardCallback()
                    {
                        @Override
                        public void success()
                        {
                            howMuchLeftToUnlock = 9 - (GiftingProcessHelper.MAX_ATTEMPTS - GiftingProcessHelper.getAttemptsLeft());

                            openAll();
                            openAllBought = true;

                            openAllButton.setTouchable(Touchable.disabled);
                            openAllButton.clearActions();

                            JumpinSweeties.instance().getPlatformUtils().showMessage(Language.LOCALE.get("purchaseSuccess"));

                            AnalyticsHelper.trackUnlockAllGiftsPurchased(true);
                        }

                        @Override
                        public void error(String e)
                        {
                            JumpinSweeties.instance().getPlatformUtils().showMessage(Language.LOCALE.format("purchaseError", e));
                        }

                        @Override
                        public void cancelled()
                        {
                            JumpinSweeties.instance().getPlatformUtils().showMessage(Language.LOCALE.get("purchaseCancel"));
                        }
                    }, IAP.PURCHASE_TYPE.UNLOCK_GIFTS);
                }

                @Override
                public void clickedCancel()
                {
                    hide();
                }
            },
                Language.LOCALE.get("confirm"),
                Language.LOCALE.get("giftsLockWarning"),
                Language.LOCALE.format("openAllWhenWarned", IAP.getPriceLocalized(IAP.PURCHASE_TYPE.UNLOCK_GIFTS)),
                Language.LOCALE.get("leave")
            );
        }
        else
            hide();
    }

    private static void openAll ()
    {
        for (int i = 0; i < giftCells.size; i++)
        {
            if (!cellLocked[i])
                continue;

            final Table table = giftCells.get(i);
            table.setOrigin(Align.center);
            final GiftingProcessHelper.GIFT_TYPES giftType = GiftingProcessHelper.getGiftType(true);

            if (giftType == null) {
                UICommonElements.error(table);
                continue;
            }

            MissionController.openGift(false);

            table.clearActions();
            table.addAction(sequence(
                    delay(i * 0.45f),
                    scaleTo(1.13f, 1.13f, 0.4f, Interpolation.sineIn),
                    scaleTo(0.0f, 0.0f, 0.2f, Interpolation.swingIn),
                    run(new Runnable()
                    {
                        @Override
                        public void run()
                        {

                            table.clear();
                            table.setBackground(new TextureRegionDrawable(AssetLoader.getUi().findRegion("whiterectangle")));

                            switch (giftType)
                            {

                                /*
                                    Конфетки
                                 */

                                case CANDIES_FEW:
                                case CANDIES_SOME:
                                case CANDIES_MANY:
                                    final Table candieTable = new Table();

                                    final Image candie = new Image(AssetLoader.getUi().findRegion("candie"));
                                    candie.setAlign(Align.center);

                                    final int amount = GiftingProcessHelper.getGiftValue(giftType);
                                    CandiesHandler.addCandiesAsReward(amount);
                                    UICommonElements.candiesLabel.setText(CandiesHandler.getCandies() + "");

                                    final Label amountLabel = new Label("x" + amount, UIRenderer.instance().getSkin(), "regular60", UICommonElements.COLOR_RED);
                                    amountLabel.setAlignment(Align.center);

                                    candieTable.center();
                                    candieTable.add(candie).row();
                                    candieTable.add(amountLabel).padTop(-6f);
                                    table.add(candieTable).center().padTop(15f);

                                    if (giftType == GiftingProcessHelper.GIFT_TYPES.CANDIES_MANY)
                                        SoundsHandler.coolGift();

                                    break;

                                /*
                                    Скины
                                 */

                                case COMMON_SKIN:
                                case SECRET_SKIN:
                                    final int skinId = GiftingProcessHelper.getGiftValue(giftType);

                                    final Image skin = new Image(AssetLoader.getJumperSkins().findRegion(JumperSkinsHandler.getJumperTextureName(skinId)));
                                    skin.setScaling(Scaling.none);
                                    skin.setOrigin(Align.center);
                                    skin.setAlign(Align.center);
                                    skin.setScale(0.8f);

                                    if (giftType == GiftingProcessHelper.GIFT_TYPES.SECRET_SKIN) {
                                        SoundsHandler.coolGift();

                                        final Table uniqueTable = new Table();
                                        uniqueTable.setBackground(new TextureRegionDrawable(AssetLoader.getUi().findRegion("unique-label-background")));
                                        final Label uniqueLabel = new Label(Language.LOCALE.get("uniqueSkin"), UIRenderer.instance().getSkin(), "regular60", UICommonElements.COLOR_UNIQUE);
                                        uniqueLabel.setAlignment(Align.center);
                                        uniqueTable.add(uniqueLabel).height(70f);

                                        table.center();
                                        table.add(skin).padBottom(-70f).spaceBottom(35f).row();
                                        table.add(uniqueTable).height(70f).padBottom(-35f);
                                    } else
                                        table.add(skin).center().padTop(20f).padRight(20f);

                                    JumperSkinsHandler.unlockSkin(skinId);

                                    if (giftType == GiftingProcessHelper.GIFT_TYPES.SECRET_SKIN)
                                        SoundsHandler.coolGift();

                                    MissionController.unlockSkin(false);

                                    break;

                                /*
                                    Токены
                                 */

                                case TOKENS_FEW:
                                case TOKENS_MANY:
                                    final Table tokenTable = new Table();

                                    final Image tokenIcon = new Image(AssetLoader.getUi().findRegion("token"));

                                    final int tokenAmount = GiftingProcessHelper.getGiftValue(giftType);
                                    TokensHandler.addTokens(tokenAmount);

                                    final Label tokenAmountLabel = new Label(tokenAmount + "", UIRenderer.instance().getSkin(), "stroke60", Color.WHITE);

                                    tokenTable.add(tokenAmountLabel).padRight(4f);
                                    tokenTable.add(tokenIcon);

                                    table.add(tokenTable);

                                    if (giftType == GiftingProcessHelper.GIFT_TYPES.TOKENS_MANY)
                                        SoundsHandler.coolGift();

                                    MissionController.tokensCollected(tokenAmount, false);

                                    break;
                            }

                            table.addAction(scaleTo(1f, 1f, 0.6f, Interpolation.swingOut));
                            SoundsHandler.unlockSkin();

                            howMuchLeftToUnlock--;
                            if (howMuchLeftToUnlock <= 0)
                                readyToBeClosed = true;
                        }
                    })
            ));
        }
    }

    public static void show ()
    {
        if (shown) return;
        shown = true;
        readyToBeClosed = false;
        openAllBought = false;

        AnalyticsHelper.trackGiftingScreen();

        /*
            Подготовка
         */

        rootTable.clear();
        rootTable.setFillParent(true);
        rootTable.setBackground(backgroundImage);
        rootTable.setTouchable(Touchable.enabled);
        rootTable.addListener(new InputListener()
        {
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                return false;
            }
        });

        contentTable.clear();
        GiftingProcessHelper.init();

        /*
            Верхний текст
         */

        text = new Label("", UIRenderer.instance().getSkin(), "regular60", UICommonElements.COLOR_RED);
        text.setText(Language.LOCALE.format("findYourPrize", GiftingProcessHelper.MAX_ATTEMPTS));
        text.setAlignment(Align.center);
        contentTable.add(text).row();

        /*
            Ячейки
         */

        int column = 0;
        int row = 0;

        giftCells.clear();
        for (int i = 0; i < 9; i++)
        {
            Table table = new Table();

            table.setBackground(new TextureRegionDrawable(AssetLoader.getUi().findRegion("gifting-giftstack")));
            table.setOrigin(Align.center);
            table.setTransform(true);
            table.setTouchable(Touchable.enabled);

            giftCells.add(table);
        }

        giftMatrix.clear();

        final float delayBetweenAnimations = 0.1f;

        for (int i = 0; i < giftCells.size; i++)
        {
            switch (column)
            {
                case 0:
                case 1:
                    if (row < 2)
                        giftMatrix.add(giftCells.get(i)).spaceBottom(49f).spaceRight(49f).width(184f).height(184f);
                    else
                        giftMatrix.add(giftCells.get(i)).spaceRight(49f).width(184f).height(184f);

                    column++;
                    break;
                case 2:
                    if (row < 2)
                        giftMatrix.add(giftCells.get(i)).spaceBottom(49f).width(184f).height(184f);
                    else
                        giftMatrix.add(giftCells.get(i)).width(184f).height(184f);

                    giftMatrix.row();
                    row++;
                    column = 0;
                    break;
            }

            giftCells.get(i).setSize(184f, 184f);
            giftCells.get(i).setOrigin(Align.center);
            giftCells.get(i).addAction(sequence(delay(1f), delay(i * delayBetweenAnimations), forever(sequence(scaleTo(1.06f, 1.06f, 0.1f, Interpolation.sineIn), scaleTo(1f, 1f, 0.2f, Interpolation.sineOut), delay(2f)))));

            giftCells.get(i).addListener(new InputListener()
            {
                public boolean touchDown(InputEvent event, float x, float y, int pointer, int button)
                {
                    return true;
                }

                public void touchUp(InputEvent event, float x, float y, int pointer, int button)
                {
                    if (stillUnlocking)
                        return;
                    stillUnlocking = true;

                    final Table table = ((Table) event.getListenerActor());
                    table.setOrigin(Align.center);
                    final GiftingProcessHelper.GIFT_TYPES giftType = GiftingProcessHelper.getGiftType(false);

                    if (giftType == null) {
                        UICommonElements.error(table);
                        stillUnlocking = false;
                        return;
                    }

                    for (int i = 0; i < giftCells.size; i++)
                        if (giftCells.get(i) == table)
                            cellLocked[i] = false;

                    MissionController.openGift(true);

                    SoundsHandler.drumroll();

                    table.clearActions();
                    table.addAction(parallel(parallel(sequence(scaleTo(1.2f, 1.2f, 1.8f, Interpolation.sineIn), scaleTo(0.0f, 0.0f, 0.2f, Interpolation.swingIn)), repeat(35, sequence(moveBy(-3f, 0f, 0.0125f), moveBy(6f, 0f, 0.025f), moveBy(-3f, 0f, 0.0125f)))), sequence(delay(2.0f), run(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            table.clear();
                            table.setBackground(new TextureRegionDrawable(AssetLoader.getUi().findRegion("whiterectangle")));

                            switch (giftType) {

                                                /*
                                                    Конфетки
                                                 */

                                case CANDIES_FEW:
                                case CANDIES_SOME:
                                case CANDIES_MANY:
                                    final Table candieTable = new Table();

                                    final Image candie = new Image(AssetLoader.getUi().findRegion("candie"));
                                    candie.setAlign(Align.center);

                                    final int amount = GiftingProcessHelper.getGiftValue(giftType);
                                    CandiesHandler.addCandiesAsReward(amount);
                                    UICommonElements.candiesLabel.setText(CandiesHandler.getCandies() + "");

                                    final Label amountLabel = new Label("x" + amount, UIRenderer.instance().getSkin(), "regular60", UICommonElements.COLOR_RED);
                                    amountLabel.setAlignment(Align.center);

                                    candieTable.center();
                                    candieTable.add(candie).row();
                                    candieTable.add(amountLabel).padTop(-6f);
                                    table.add(candieTable).center().padTop(15f);

                                    if (giftType == GiftingProcessHelper.GIFT_TYPES.CANDIES_MANY)
                                        SoundsHandler.coolGift();

                                    break;

                                        /*
                                            Скины
                                         */

                                case COMMON_SKIN:
                                case SECRET_SKIN:
                                    final int skinId = GiftingProcessHelper.getGiftValue(giftType);

                                    final Image skin = new Image(AssetLoader.getJumperSkins().findRegion(JumperSkinsHandler.getJumperTextureName(skinId)));
                                    skin.setScaling(Scaling.none);
                                    skin.setOrigin(Align.center);
                                    skin.setAlign(Align.center);
                                    skin.setScale(0.8f);

                                    if (giftType == GiftingProcessHelper.GIFT_TYPES.SECRET_SKIN) {
                                        SoundsHandler.coolGift();

                                        final Table uniqueTable = new Table();
                                        uniqueTable.setBackground(new TextureRegionDrawable(AssetLoader.getUi().findRegion("unique-label-background")));
                                        final Label uniqueLabel = new Label(Language.LOCALE.get("uniqueSkin"), UIRenderer.instance().getSkin(), "regular60", UICommonElements.COLOR_UNIQUE);
                                        uniqueLabel.setAlignment(Align.center);
                                        uniqueTable.add(uniqueLabel).height(70f);

                                        table.center();
                                        table.add(skin).padBottom(-70f).spaceBottom(35f).row();
                                        table.add(uniqueTable).height(70f).padBottom(-35f);
                                    } else
                                        table.add(skin).center().padTop(20f).padRight(20f);

                                    JumperSkinsHandler.unlockSkin(skinId);

                                    if (giftType == GiftingProcessHelper.GIFT_TYPES.SECRET_SKIN)
                                        SoundsHandler.coolGift();

                                    MissionController.unlockSkin(true);

                                    break;

                                        /*
                                            Токены
                                         */

                                case TOKENS_FEW:
                                case TOKENS_MANY:
                                    final Table tokenTable = new Table();

                                    final Image tokenIcon = new Image(AssetLoader.getUi().findRegion("token"));

                                    final int tokenAmount = GiftingProcessHelper.getGiftValue(giftType);
                                    TokensHandler.addTokens(tokenAmount);

                                    final Label tokenAmountLabel = new Label(tokenAmount + "", UIRenderer.instance().getSkin(), "stroke60", Color.WHITE);

                                    tokenTable.add(tokenAmountLabel).padRight(4f);
                                    tokenTable.add(tokenIcon);

                                    table.add(tokenTable);

                                    if (giftType == GiftingProcessHelper.GIFT_TYPES.TOKENS_MANY)
                                        SoundsHandler.coolGift();

                                    MissionController.tokensCollected(tokenAmount, true);

                                    break;
                            }

                            text.setText(Language.LOCALE.format("findYourPrize", GiftingProcessHelper.getAttemptsLeft()));

                            table.addAction(scaleTo(1f, 1f, 0.6f, Interpolation.swingOut));

                            stillUnlocking = false;

                            SoundsHandler.unlockSkin();

                            if (GiftingProcessHelper.getAttemptsLeft() <= 0) {
                                readyToBeClosed = true;
                            }
                        }
                    }))));
                }
            });
        }

        contentTable.add(giftMatrix).padTop(38f).expand().row();

        /*
            Кнопка покупки
         */

        openAllButton.clear();
        openAllButton.addActor(openAllBackground);

        openAllLabel = new Label("", UIRenderer.instance().getSkin(), "regular60", Color.WHITE);
        openAllLabel.setText(Language.LOCALE.get("openAll"));
        openAllLabel.setAlignment(Align.center);
        openAllButton.addActor(openAllLabel);

        openAllIcon.setScaling(Scaling.none);
        openAllIcon.setAlign(Align.center);
        openAllButton.addActor(openAllIcon);

        final Label openAllPrice = UICommonElements.priceLabel(IAP.PURCHASE_TYPE.UNLOCK_GIFTS);
        openAllPrice.setAlignment(Align.right);

        openAllButton.addActor(openAllPrice);

        openAllButton.pack();
        openAllButton.setTransform(true);

        openAllLabel.addAction(moveBy(-80f, 0f));
        openAllIcon.addAction(moveBy(218f, 0f));
        openAllPrice.addAction(moveBy(10f, -103f));

        contentTable.add(openAllButton).padTop(54f);

        openAllButton.setOrigin(Align.center);
        openAllButton.setTransform(true);
        openAllButton.addAction(sequence(delay(1f), delay(giftCells.size * delayBetweenAnimations), forever(sequence(scaleTo(1.06f, 1.06f, 0.1f, Interpolation.sineIn), scaleTo(1f, 1f, 0.2f, Interpolation.sineOut), delay(2f)))));

        openAllButton.setTouchable(Touchable.enabled);
        openAllButton.addListener(new InputListener()
        {
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                return true;
            }
            public void touchUp (InputEvent event, float x, float y, int pointer, int button)
            {
                event.cancel();
                UICommonElements.click(openAllButton);

                IAP.purchase(new IStandardCallback()
                {
                    @Override
                    public void success()
                    {
                        howMuchLeftToUnlock = 9 - (GiftingProcessHelper.MAX_ATTEMPTS - GiftingProcessHelper.getAttemptsLeft());

                        openAll();
                        openAllBought = true;

                        openAllButton.setTouchable(Touchable.disabled);
                        openAllButton.clearActions();

                        JumpinSweeties.instance().getPlatformUtils().showMessage(Language.LOCALE.get("purchaseSuccess"));

                        AnalyticsHelper.trackUnlockAllGiftsPurchased(false);
                    }

                    @Override
                    public void error(String e)
                    {
                        JumpinSweeties.instance().getPlatformUtils().showMessage(Language.LOCALE.format("purchaseError", e));
                    }

                    @Override
                    public void cancelled()
                    {
                        JumpinSweeties.instance().getPlatformUtils().showMessage(Language.LOCALE.get("purchaseCancel"));
                    }
                }, IAP.PURCHASE_TYPE.UNLOCK_GIFTS);
            }
        });

        /*
            В завершение
         */

        rootTable.add(contentTable).padRight(59f).padLeft(59f).padBottom(6f).center();
        UIRenderer.instance().getRootStack().add(rootTable);
        UIRenderer.instance().getRootStack().pack();

        rootTable.setVisible(false);
        rootTable.setTransform(true);
        rootTable.setOrigin(Align.center);
        rootTable.addAction(sequence(
                alpha(0f),
                scaleTo(2f, 2f),
                parallel(
                    run(new Runnable() {
                        @Override
                        public void run()
                        {
                            rootTable.setVisible(true);
                        }
                    }),
                    alpha(1f, 0.2f, Interpolation.sineIn),
                    scaleTo(1f, 1f, 0.25f, Interpolation.sineIn)
                )
        ));

    }

    public static boolean isShown()
    {
        return shown;
    }

    public static boolean isReadyToBeClosed()
    {
        return readyToBeClosed;
    }
}
