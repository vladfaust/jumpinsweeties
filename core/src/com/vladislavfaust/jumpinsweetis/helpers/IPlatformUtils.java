package com.vladislavfaust.jumpinsweetis.helpers;

/**
 * Created by Faust on 27.03.2015.
 *
 */
public interface IPlatformUtils
{
    String getDeviceId();
    boolean checkInternetConnection();

    void showMessage(String message);

    void shareImageAndText (String filepath, String text);
}
