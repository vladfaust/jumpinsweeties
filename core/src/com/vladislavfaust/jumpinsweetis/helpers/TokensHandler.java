package com.vladislavfaust.jumpinsweetis.helpers;

import com.vladislavfaust.jumpinsweetis.helpers.googleplay.AchievementsHandler;

/**
 * Created by Faust on 09.05.2015.
 *
 */
public class TokensHandler
{
    /*
        ���������
     */

    public final static int
            TO_BUY_AMOUNT_1 = 6,
            TO_BUY_AMOUNT_2 = 12,
            TO_BUY_AMOUNT_3 = 24,
            TO_BUY_AMOUNT_4 = 48;

    public final static int CONVERSION_RATE = 200;
    public final static int UNLOCK_UNIQUE_PRICE = 20;

    /*
        ��������� ������
     */

    public static void spendTokens (int value)
    {
        MySecurity.addTokens(-value);
    }

    public static void addTokens (int value)
    {
        AchievementsHandler.addTokenKeeper(value);
        MySecurity.addTokens(value);
    }

    public static int getTotalTokens()
    {
        return MySecurity.getTokens();
    }

    public static int getAmountByPurchaseType (IAP.PURCHASE_TYPE type)
    {
        switch (type)
        {
            case TOKENS_FEW:
                return TO_BUY_AMOUNT_1;
            case TOKENS_SOME:
                return TO_BUY_AMOUNT_2;
            case TOKENS_MANY:
                return TO_BUY_AMOUNT_3;
            case TOKENS_LOTSOF:
                return TO_BUY_AMOUNT_4;
            default:
                return 0;
        }
    }
}
