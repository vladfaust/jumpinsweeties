package com.vladislavfaust.jumpinsweetis.helpers.analytics;

import com.vladislavfaust.jumpinsweetis.helpers.IAP;
import com.vladislavfaust.jumpinsweetis.helpers.advertisment.AdsManager;

/**
 * Created by Faust on 12.05.2015.
 *
 */
public interface IGA
{

    /*
        ��������
     */

    void trackGameOver(int score, int candies);

    /*
        �����������
     */

    void trackGiftsOffered ();

    void trackIncentivizedOffered ();

    void trackIncentivizedClick (AdsManager.INCENTIVIZED_TYPE type, boolean success);

    void trackIncentivizedShown (AdsManager.INCENTIVIZED_SOURCE which);

    /*
        ����� ��������
     */

    void trackGiftingScreen ();

    void trackUnlockAllGiftsPurchased(boolean dialog);

    /*
        ����� ������
     */

    void trackBuyMoreTokensClick ();

    void trackPurchaseTokensClick (IAP.PURCHASE_TYPE which, boolean success);

    /*
        ����� ������
     */

    void trackUnlockUniqueClick();

    void trackUnlockUniquePurchase(boolean currency, boolean hadAChoice);

    /*
        FPS
     */

    void logFPS ();
}
