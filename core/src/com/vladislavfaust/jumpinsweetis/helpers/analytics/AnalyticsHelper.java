package com.vladislavfaust.jumpinsweetis.helpers.analytics;

import com.vladislavfaust.jumpinsweetis.JumpinSweeties;
import com.vladislavfaust.jumpinsweetis.gameobjects.bonuses.BonusLevelingController;
import com.vladislavfaust.jumpinsweetis.helpers.BackgroundHelper;
import com.vladislavfaust.jumpinsweetis.helpers.IAP;
import com.vladislavfaust.jumpinsweetis.helpers.advertisment.AdsManager;

/**
 * Created by Faust on 02.04.2015.
 *
 */
public class AnalyticsHelper
{
    public final static String GOOGLE_PLAY_LINK = "https://play.google.com/store/apps/details?id=com.vladislavfaust.jumpinsweeties.android";
    public final static String SHARE_LINK = "http://jumpinsweeties.com";
    public final static String SETTINGS_LINK = "https://twitter.com/vladfaust";

    public final static int FEEDBACK_DELAY_MINUTES = 15;

    /*
        ������� ����
     */

    public static void trackMainMenuScreen () {
        JumpinSweeties.instance().getFlurry().trackMainMenuScreen();
    }

    public static void trackSound(boolean enabled) {
        JumpinSweeties.instance().getFlurry().trackSound(enabled);
    }

    public static void trackMusic (boolean enabled) {
        JumpinSweeties.instance().getFlurry().trackMusic(enabled);
    }

    public static void trackPromocodeClick (boolean confirmed, String code) {
        JumpinSweeties.instance().getFlurry().trackPromocodeClick(confirmed, code);
    }

    public static void trackLeaveFeedbackClick () {
        JumpinSweeties.instance().getFlurry().trackLeaveFeedbackClick();
    }

    public static void trackSiteClick () {
        JumpinSweeties.instance().getFlurry().trackSiteClick();
    }

    public static void trackProgressClick () {
        JumpinSweeties.instance().getFlurry().trackProgressClick();
    }

    public static void trackLeadersClick () {
        JumpinSweeties.instance().getFlurry().trackLeadersClick();
    }

    /*
        ����
     */

    public static void trackGameStart ()
    {
        JumpinSweeties.instance().getFlurry().trackGameStart();
    }

    /*
        ��������
     */

    public static void trackGameOver(String skinTexture, int score, int candies)
    {
        JumpinSweeties.instance().getFlurry().trackGameOver(skinTexture, score, candies);
        JumpinSweeties.instance().getGameAnalytics().trackGameOver(score, candies);
    }

    public static void trackShareClick (int score, boolean highscore, boolean confirmed) {
        JumpinSweeties.instance().getFlurry().trackShareClick(score, highscore, confirmed);
    }

    /*
        �����������
     */

    public static void trackGiftsOffered () {
        JumpinSweeties.instance().getFlurry().trackGiftsOffered();
        JumpinSweeties.instance().getGameAnalytics().trackGiftsOffered();
    }

    public static void trackIncentivizedOffered () {
        JumpinSweeties.instance().getFlurry().trackIncentivizedOffered();
        JumpinSweeties.instance().getGameAnalytics().trackIncentivizedOffered();
    }

    public static void trackIncentivizedClick (AdsManager.INCENTIVIZED_TYPE type, boolean success) {
        JumpinSweeties.instance().getFlurry().trackIncentivizedClick(type, success);
        JumpinSweeties.instance().getGameAnalytics().trackIncentivizedClick(type, success);
    }

    public static void trackIncentivizedShown (AdsManager.INCENTIVIZED_SOURCE which) {
        JumpinSweeties.instance().getFlurry().trackIncentivizedShown(which);
        JumpinSweeties.instance().getGameAnalytics().trackIncentivizedShown(which);
    }

    public static void trackPollCancel () {
        JumpinSweeties.instance().getFlurry().trackPollCancel();
    }

    /*
        ����� ��������
     */

    public static void trackGiftingScreen () {
        JumpinSweeties.instance().getFlurry().trackGiftingScreen();
        JumpinSweeties.instance().getGameAnalytics().trackGiftingScreen();
    }

    public static void trackUnlockAllGiftsPurchased(boolean dialog) {
        JumpinSweeties.instance().getFlurry().trackUnlockAllGiftsPurchased(dialog);
        JumpinSweeties.instance().getGameAnalytics().trackUnlockAllGiftsPurchased(dialog);
    }

    /*
        ����� ������
     */

    public static void trackBoostsScreen () {
        JumpinSweeties.instance().getFlurry().trackBoostsScreen();
    }

    public static void trackUpgradeBoost (BonusLevelingController.BOOST_TYPE which, int toLevel) {
        JumpinSweeties.instance().getFlurry().trackUpgradeBoost(which, toLevel);
    }

    public static void trackConvert  () {
        JumpinSweeties.instance().getFlurry().trackConvert();
    }

    public static void trackBuyMoreTokensClick () {
        JumpinSweeties.instance().getFlurry().trackBuyMoreTokensClick();
        JumpinSweeties.instance().getGameAnalytics().trackBuyMoreTokensClick();
    }

    public static void trackPurchaseTokensClick (IAP.PURCHASE_TYPE which, boolean success) {
        JumpinSweeties.instance().getFlurry().trackPurchaseTokensClick(which, success);
        JumpinSweeties.instance().getGameAnalytics().trackPurchaseTokensClick(which, success);
    }

    /*
        ����� ������
     */

    public static void trackSkinScreen() {
        JumpinSweeties.instance().getFlurry().trackSkinScreen();
    }

    public static void trackUnlockUniqueClick() {
        JumpinSweeties.instance().getFlurry().trackUnlockUniqueClick();
        JumpinSweeties.instance().getGameAnalytics().trackUnlockUniqueClick();
    }

    public static void trackUnlockUniquePurchase(boolean currency, boolean hadAChoice) {
        JumpinSweeties.instance().getFlurry().trackUnlockUniquePurchase(currency, hadAChoice);
        JumpinSweeties.instance().getGameAnalytics().trackUnlockUniquePurchase(currency, hadAChoice);
    }

    public static void trackUnlockCommon(String texture, int price) {
        JumpinSweeties.instance().getFlurry().trackUnlockCommon(texture, price);
    }

    public static void trackSelectBackground (BackgroundHelper.BACKGROUND background)
    {
        JumpinSweeties.instance().getFlurry().trackSelectBackground(background);
    }

    /*
        ����������� ������� ����
     */

    public enum FEEDBACK_RESULT
    {
        SUCCESS, LATER, NEVER
    }
    public static void trackFeedbackOffer (FEEDBACK_RESULT result) {
        JumpinSweeties.instance().getFlurry().trackFeedbackOffer(result);
    }

    /*
        ���������� ������
     */

    public static void trackMissionCompleted (int id) {
        JumpinSweeties.instance().getFlurry().trackMissionCompleted(id);
    }

    /*
        ��������� ����������
     */

    public static void trackAchievementUnlock (int id) {
        JumpinSweeties.instance().getFlurry().trackAchievementUnlock(id);
    }

    public static void trackAchievementShareScreen (int id, boolean success, boolean afterDialog) {
        JumpinSweeties.instance().getFlurry().trackAchievementShareScreen(id, success, afterDialog);
    }

    /*
        FPS
     */

    public static void logFPS ()
    {
        JumpinSweeties.instance().getGameAnalytics().logFPS();
    }

    /*
        Google Play Games
     */

    public static void trackNoGPGSInstalled () {
        JumpinSweeties.instance().getFlurry().trackNoGPGSInstalled();
    }

    public static void trackGPGSLogin (boolean success) {
        if (JumpinSweeties.instance().getFlurry() != null)
            JumpinSweeties.instance().getFlurry().trackGPGSLogin(success);
    }

    public static void trackGPGSConfirmationDialog ()
    {

    }

    public static void trackGPGSConfirmationDialogClickedNever ()
    {

    }
}
