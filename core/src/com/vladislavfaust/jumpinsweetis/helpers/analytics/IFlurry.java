package com.vladislavfaust.jumpinsweetis.helpers.analytics;

import com.vladislavfaust.jumpinsweetis.gameobjects.bonuses.BonusLevelingController;
import com.vladislavfaust.jumpinsweetis.helpers.BackgroundHelper;
import com.vladislavfaust.jumpinsweetis.helpers.IAP;
import com.vladislavfaust.jumpinsweetis.helpers.advertisment.AdsManager;

/**
 * Created by Faust on 02.04.2015.
 *
 */
public interface IFlurry
{

    /*
        ������� ����
     */

    void trackMainMenuScreen ();

    void trackSound(boolean enabled);

    void trackMusic (boolean enabled);

    void trackPromocodeClick (boolean confirmed, String code);

    void trackLeaveFeedbackClick ();

    void trackSiteClick ();

    void trackProgressClick ();

    void trackLeadersClick ();

    /*
        ����
     */

    void trackGameStart ();

    /*
        ��������
     */

    void trackGameOver(String skinTexture, int score, int candies);

    void trackShareClick (int score, boolean highscore, boolean confirmed);

    /*
        �����������
     */

    void trackGiftsOffered ();

    void trackIncentivizedOffered ();

    void trackIncentivizedClick (AdsManager.INCENTIVIZED_TYPE type, boolean success);

    void trackIncentivizedShown (AdsManager.INCENTIVIZED_SOURCE which);

    void trackPollCancel ();

    /*
        ����� ��������
     */

    void trackGiftingScreen ();

    void trackUnlockAllGiftsPurchased(boolean dialog);

    /*
        ����� ������
     */

    void trackBoostsScreen ();

    void trackUpgradeBoost (BonusLevelingController.BOOST_TYPE which, int toLevel);

    void trackConvert  ();

    void trackBuyMoreTokensClick ();

    void trackPurchaseTokensClick (IAP.PURCHASE_TYPE which, boolean success);

    /*
        ����� ������
     */

    void trackSkinScreen();

    void trackUnlockUniqueClick();

    void trackUnlockUniquePurchase(boolean currency, boolean hadAChoice);

    void trackUnlockCommon(String texture, int price);

    void trackSelectBackground (BackgroundHelper.BACKGROUND background);

    /*
        ����������� ������� ����
     */

    void trackFeedbackOffer (AnalyticsHelper.FEEDBACK_RESULT result);

    /*
        ���������� ������
     */

    void trackMissionCompleted (int id);

    /*
        ��������� ����������
     */

    void trackAchievementUnlock (int id);

    void trackAchievementShareScreen (int id, boolean success, boolean afterDialog);

    /*
        Google Play Games
     */

    void trackNoGPGSInstalled ();

    void trackGPGSLogin (boolean success);
}
