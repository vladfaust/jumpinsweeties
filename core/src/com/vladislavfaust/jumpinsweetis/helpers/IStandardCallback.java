package com.vladislavfaust.jumpinsweetis.helpers;

/**
 * Created by Faust on 12.05.2015.
 *
 */
public interface IStandardCallback
{
    void success ();
    void error (String e);
    void cancelled ();
}
