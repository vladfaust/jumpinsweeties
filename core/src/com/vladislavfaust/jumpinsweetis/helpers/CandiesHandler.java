package com.vladislavfaust.jumpinsweetis.helpers;

import com.vladislavfaust.jumpinsweetis.gameobjects.bonuses.Candie;
import com.vladislavfaust.jumpinsweetis.helpers.googleplay.AchievementsHandler;
import com.vladislavfaust.jumpinsweetis.ui.UICommonElements;

/**
 * Created by Faust on 20.01.2015.
 * Отвечает за внутреннюю валюту
 */
public class CandiesHandler
{

    /*
        Куда могут тратиться конфеты
     */

    public static boolean buySkin (JumperSkinsHandler.JumperSkin skin)
    {
        if (MySecurity.getCandies() < skin.getPrice())
            return false;

        MySecurity.addCandies(-skin.getPrice());

        return true;
    }

    public static boolean convertToTokens ()
    {
        if (((float) CandiesHandler.getCandies()) / ((float) TokensHandler.CONVERSION_RATE) < 1f)
            return false;

        MySecurity.addCandies(-TokensHandler.CONVERSION_RATE);
        MySecurity.addTokens(1);

        return true;
    }

    /*
        И откуда они могут взяться
     */

    public static void addCandiesInTheEndOfGame ()
    {
        MySecurity.addCandies(MySecurity.getLastCandies());
        AchievementsHandler.addCandyLand(MySecurity.getLastCandies());
    }

    public static void pickupCandie (Candie candie)
    {
        MySecurity.addLastCandies(candie);
    }

    public static void addCandiesAsReward (int amount)
    {
        MySecurity.addCandies(amount);
        UICommonElements.candiesLabel.setText(MySecurity.getCandies() + "");
    }

    /*
        Геттеры
     */

    public static int getCandies ()
    {
        return MySecurity.getCandies();
    }

    public static int getLastCandies()
    {
        return MySecurity.getLastCandies();
    }

}
