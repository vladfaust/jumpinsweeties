package com.vladislavfaust.jumpinsweetis.helpers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.I18NBundle;

import java.util.Locale;

/**
 * Created by Faust on 03.04.2015.
 *
 */
public class Language
{
    public static I18NBundle LOCALE;

    public static void init ()
    {
        Locale locale = Locale.getDefault();
        LOCALE = I18NBundle.createBundle(Gdx.files.internal("i18n/localedStrings"), locale);
    }

    public static String getGameOverTextureName ()
    {
        String locale = Locale.getDefault().toString();

        if (locale.equals("ru_RU") || locale.equals("uk") || locale.equals("be"))
            return "gameover-ru";
        else
            return "gameover-en";
    }
}
