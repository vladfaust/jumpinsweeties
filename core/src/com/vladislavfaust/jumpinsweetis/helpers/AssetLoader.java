package com.vladislavfaust.jumpinsweetis.helpers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;
import com.vladislavfaust.jumpinsweetis.gamesystems.BonusesHandler;

/**
 * Created by Faust on 19.01.2015.
 *
 */
public class AssetLoader
{
    private static TextureAtlas jumperSkins, ui, common, backgrounds;
    private static Texture splashTexture, whiteScreenTexture;
    private static TextureRegion giftingBackground;

    private static Array<Sprite> candieValues;
    private static Array<Sprite> doublerCandiesValues;

    // Загрузка ресурсов
    public static void init ()
    {
        jumperSkins = new TextureAtlas(Gdx.files.internal("textures/skins.pack"));
        common = new TextureAtlas(Gdx.files.internal("textures/common.pack"));

        ui = new TextureAtlas(Gdx.files.internal("textures/ui.pack"));
        backgrounds = new TextureAtlas(Gdx.files.internal("textures/backgrounds.pack"));

        whiteScreenTexture = new Texture(Gdx.files.internal("textures/white_screen.jpg"));
        giftingBackground = new TextureRegion(new Texture(Gdx.files.internal("textures/gifting-background.png")));

        candieValues = new Array<>();
        for (int i = 0; i < BonusesHandler.CANDIES_TOTAL_TYPES; i++)
        {
            candieValues.add(common.createSprite("bonus", i));
        }

        doublerCandiesValues = new Array<>();
        for (int i = 0; i < 3; i++)
        {
            doublerCandiesValues.add(common.createSprite("x2", i));
        }
    }

    // Загрузка сплеша
    public static void splashInit ()
    {
        splashTexture = new Texture(Gdx.files.internal("textures/splash.png"));
        splashTexture.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
    }

    // Геттеры атласов
    public static TextureAtlas getJumperSkins()
    {
        return jumperSkins;
    }

    public static TextureAtlas getUi()
    {
        return ui;
    }

    public static TextureAtlas getCommon()
    {
        return common;
    }

    // Геттеры определенных текстур
    public static TextureRegion getBackground ()
    {
        return common.findRegion("background");
    }

    public static TextureRegion getLeftPipe ()
    {
        return common.findRegion("pipe-left");
    }

    public static TextureRegion getRightPipe ()
    {
        return common.findRegion("pipe-right");
    }

    public static Texture getSplashTexture ()
    {
        return splashTexture;
    }

    public static Texture getWhiteScreenTexture()
    {
        return whiteScreenTexture;
    }

    public static Sprite getCandie(int i)
    {
        return candieValues.get(i);
    }

    public static TextureRegion getGiftingBackground()
    {
        return giftingBackground;
    }

    public static Array<Sprite> getDoublerCandiesValues()
    {
        return doublerCandiesValues;
    }

    public static TextureRegion getBackground (BackgroundHelper.BACKGROUND which)
    {
        switch (which)
        {
            case CLASSIC:
                return backgrounds.findRegion("classic");
            case SWIRL:
                return backgrounds.findRegion("swirl");
            case SKY:
                return backgrounds.findRegion("sky");
            case SWEETIES:
                return backgrounds.findRegion("sweets");
            default:
                return backgrounds.findRegion("classic");
        }
    }

    public static TextureRegion getBackgroundPreview (BackgroundHelper.BACKGROUND which)
    {
        switch (which)
        {
            case CLASSIC:
                return backgrounds.findRegion("classic-preview");
            case SWIRL:
                return backgrounds.findRegion("swirl-preview");
            case SKY:
                return backgrounds.findRegion("sky-preview");
            case SWEETIES:
                return backgrounds.findRegion("sweets-preview");
            default:
                return backgrounds.findRegion("classic-preview");
        }
    }
}
