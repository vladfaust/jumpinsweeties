package com.vladislavfaust.jumpinsweetis.helpers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.PixmapIO;
import com.badlogic.gdx.utils.ScreenUtils;

/**
 * Created by Faust on 10.05.2015.
 *
 */
public class ScreenshotHelper
{
    /*
        ����������
     */

    private static Pixmap screenshotPixmap;
    private static FileHandle fileHandle;
    private static boolean wantToMakeScreenshot = false;
    private static boolean wantToShareScreenshot = false;
    private static boolean flipped = false;

    /*
        ��������� ������
     */

    public static Pixmap getScreenshotPixmap()
    {
        screenshotPixmap = flipPixmap(screenshotPixmap);
        flipped = true;
        return screenshotPixmap;
    }

    public static String getFilePath()
    {
        return fileHandle.file().getAbsolutePath();
    }

    public static boolean wantToMakeScreenshot()
    {
        return wantToMakeScreenshot;
    }

    public static void prepareToMakeScreenshot()
    {
        wantToMakeScreenshot = true;
    }

    public static void makeScreenshot ()
    {
        try {
            screenshotPixmap = getScreenshot(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), false);
            flipped = false;
            wantToMakeScreenshot = false;
            wantToShareScreenshot = true;
        }
        catch (Exception e)
        {
            throw new Error("Couldn't make a screenshot");
        }
    }

    public static void saveScreenshot ()
    {
        try
        {
            fileHandle = Gdx.files.external("screenshot.png");
            if (!flipped) screenshotPixmap = flipPixmap(screenshotPixmap);
            PixmapIO.writePNG(fileHandle, screenshotPixmap);
            screenshotPixmap.dispose();
        }
        catch (Exception e)
        {
            throw new Error("Couldn't save a screenshot: "+e.getLocalizedMessage());
        }
    }

    /*
        ��������� ������
     */

    private static Pixmap getScreenshot(int x, int y, int w, int h, boolean yDown)
    {
        final Pixmap pixmap = ScreenUtils.getFrameBufferPixmap(x, y, w, h);
        return (yDown) ? flipPixmap (pixmap) : pixmap;
    }

    private static Pixmap flipPixmap (Pixmap src)
    {
        final int width = src.getWidth();
        final int height = src.getHeight();

        Pixmap flipped = new Pixmap(width, height, src.getFormat());

        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                flipped.drawPixel(x, y, src.getPixel(x, height - y - 1));
            }
        }

        src.dispose();

        return flipped;
    }
}
