package com.vladislavfaust.jumpinsweetis.helpers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.vladislavfaust.jumpinsweetis.gameobjects.bonuses.BonusLevelingController;

import static com.vladislavfaust.jumpinsweetis.JumpinSweeties.DEBUG;

/**
 * Created by Faust on 23.02.2015.
 *
 */
public class MyPreferences
{
    /*
        Настройки
     */

    public final static String NAME = (DEBUG) ? "fsada32342wd23dqaw": "JumpinSweetiesPrefs";
    public enum KEY
    {
        UNLOCKED_SKINS, ACTIVE_SKIN, CANDIES, HIGHSCORE, PREVIOUS_VIEW_TIME, CLASSIFICATIONS_USED,
        MISSION_VAR_1, MISSION_VAR_2, MISSION_VAR_3, MISSION_VAR_4, CURRENT_MISSION, SOUND, MUSIC,
        FIRST_GIVEAWAY_TIME, LAST_GIVEAWAY_TIME, LAST_GIVEAWAY_NUMBER, TOKENS, FIRST_LAUNCH_TIME, SESSION_LAUNCH_TIME,
        FEEDBACK_BLOCKED, ACTIVE_BACKGROUND
    }

    public static Preferences preferences;
    
    /*
        Основные методы put и get
     */
    
    private static void put (KEY key, Object value)
    {
        preferences.putString(key.toString(), MySecurity.encryptValueAES(value));
        preferences.flush();
    }

    private static void put (String key, Object value)
    {
        preferences.putString(key, MySecurity.encryptValueAES(value));
        preferences.flush();
    }
    
    private static Long getLong (KEY key, long def)
    {
        return MySecurity.decryptValueAES(preferences.getString(key.toString(), MySecurity.encryptValueAES(def)), Long.class);
    }

    private static Boolean getBoolean (KEY key, boolean def)
    {
        return MySecurity.decryptValueAES(preferences.getString(key.toString(), MySecurity.encryptValueAES(def)), Boolean.class);
    }

    private static String getString (KEY key, String def)
    {
        return MySecurity.decryptValueAES(preferences.getString(key.toString(), MySecurity.encryptValueAES(def)), String.class);
    }

    private static Integer getInteger (KEY key, int def)
    {
        return MySecurity.decryptValueAES(preferences.getString(key.toString(), MySecurity.encryptValueAES(def)), Integer.class);
    }

    private static Integer getInteger (String key, int def)
    {
        return MySecurity.decryptValueAES(preferences.getString(key, MySecurity.encryptValueAES(def)), Integer.class);
    }

    /*
        Инициализация
     */

    public static void init ()
    {
        preferences = Gdx.app.getPreferences(NAME);

        if (getFirstLaunchTime() == 0)
            putFirstLaunchTime();

        putThisSessionLaunchTime();
    }

    /*
        Время запуска
     */

    public static long getFirstLaunchTime ()
    {
        return getLong(KEY.FIRST_LAUNCH_TIME, 0);
    }

    private static void putFirstLaunchTime()
    {
        put(KEY.FIRST_LAUNCH_TIME, System.currentTimeMillis());
    }

    public static long getThisSessionLaunchTime ()
    {
        return getLong(KEY.SESSION_LAUNCH_TIME, 0);
    }

    private static void putThisSessionLaunchTime()
    {
        put(KEY.SESSION_LAUNCH_TIME, System.currentTimeMillis());
    }

    /*
        Скины
     */

    public static String getUnlockedSkins ()
    {
        return getString(KEY.UNLOCKED_SKINS, "tier1-sweet;");
    }

    public static void putUnlockedSkins(String foo)
    {
        put(KEY.UNLOCKED_SKINS, foo);
    }

    public static String getActiveSkin ()
    {
        return getString(KEY.ACTIVE_SKIN, "tier1-sweet");
    }

    public static void putActiveSkin(String texture)
    {
        put(KEY.ACTIVE_SKIN, texture);
    }

    /*
        Конфетки
     */

    public static int getCandies ()
    {
        return getInteger(KEY.CANDIES, 0);
    }

    public static void putCandies(int amount)
    {
        put(KEY.CANDIES, amount);
    }

    /*
        Рекорд
     */

    public static int getHighscore ()
    {
        return getInteger(KEY.HIGHSCORE, 0);
    }

    public static void putHighscore(int score)
    {
        put(KEY.HIGHSCORE, score);
    }

    /*
        Реклама
     */

    public static void putPreviousViewTime()
    {
        put(KEY.PREVIOUS_VIEW_TIME, System.currentTimeMillis());
    }

    public static long getPreviousViewTime ()
    {
        return getLong(KEY.PREVIOUS_VIEW_TIME, 0);
    }

    /*
        Промокоды
     */

    public static void putPromoCodeClassifications(String value)
    {
        put(KEY.CLASSIFICATIONS_USED, value);
    }

    public static String getPromoCodeClassifications ()
    {
        return getString(KEY.CLASSIFICATIONS_USED, "");
    }

    /*
        Миссии
     */

    public static void putMissionVar(int which, int value)
    {
        switch (which)
        {
            case 1:
                put(KEY.MISSION_VAR_1, value);
                break;
            case 2:
                put(KEY.MISSION_VAR_2, value);
                break;
            case 3:
                put(KEY.MISSION_VAR_3, value);
                break;
            case 4:
                put(KEY.MISSION_VAR_4, value);
                break;
        }
    }

    public static int getMissionVar (int which)
    {
        switch (which)
        {
            case 1:
                return getInteger(KEY.MISSION_VAR_1, 0);
            case 2:
                return getInteger(KEY.MISSION_VAR_2, 0);
            case 3:
                return getInteger(KEY.MISSION_VAR_3, 0);
            case 4:
                return getInteger(KEY.MISSION_VAR_4, 0);
            default:
                return 0;
        }
    }

    public static void putCurrentId(int id)
    {
        put(KEY.CURRENT_MISSION, id);
    }

    public static int getCurrentId()
    {
        return getInteger(KEY.CURRENT_MISSION, 0);
    }

    /*
        Настройки
     */

    public static void switchSounds(boolean enabled)
    {
        put(KEY.SOUND, enabled);
    }

    public static boolean getSounds()
    {
        return getBoolean(KEY.SOUND, true);
    }

    public static void switchMusic (boolean enabled)
    {
        put(KEY.MUSIC, enabled);
    }

    public static boolean getMusic ()
    {
        return getBoolean(KEY.MUSIC, false);
    }

    /*
        Подарочки
     */

    public static void putLastGiveawayTime(long time)
    {
        put(KEY.LAST_GIVEAWAY_TIME, time);
    }

    public static long getLastGiveawayTime()
    {
        return getLong(KEY.LAST_GIVEAWAY_TIME, 0);
    }

    public static void putFirstGiveawayTime(long time)
    {
        put(KEY.FIRST_GIVEAWAY_TIME, time);
    }

    public static long getFirstGiveawayTime()
    {
        return getLong(KEY.FIRST_GIVEAWAY_TIME, 0);
    }

    public static void putLastGiveawayNumber(int num)
    {
        put(KEY.LAST_GIVEAWAY_NUMBER, num);
    }

    public static int getLastGiveawayNumber ()
    {
        return getInteger(KEY.LAST_GIVEAWAY_NUMBER, 0);
    }

    /*
        Токены
     */

    public static int getTokens ()
    {
        return getInteger(KEY.TOKENS, 0);
    }

    public static void putTokens (int value)
    {
        put(KEY.TOKENS, value);
    }

    /*
        Уровни бустов
     */

    public static void putBoostLevel (BonusLevelingController.BOOST_TYPE boostType, int level)
    {
        put("bt"+boostType.toString(), level);
    }

    public static int getBoostLevel (BonusLevelingController.BOOST_TYPE boostType)
    {
        return getInteger("bt"+boostType.toString(), 0);
    }

    /*
        Просьба об отзыве
     */

    public static void putFeedbackBlocked (boolean never)
    {
        put(KEY.FEEDBACK_BLOCKED, never);
    }

    public static boolean getFeedbackBlocked ()
    {
        return getBoolean(KEY.FEEDBACK_BLOCKED, false);
    }

    /*
        Бэкграунды
     */

    public static void putActiveBackground (BackgroundHelper.BACKGROUND which)
    {
        put(KEY.ACTIVE_BACKGROUND, which.name());
    }

    public static BackgroundHelper.BACKGROUND getActiveBackground ()
    {
        return BackgroundHelper.BACKGROUND.valueOf(getString(KEY.ACTIVE_BACKGROUND, BackgroundHelper.BACKGROUND.CLASSIC.name()));
    }
}
