package com.vladislavfaust.jumpinsweetis.helpers;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Timer;

import java.util.Random;

/**
 * Created by Faust on 24.01.2015.
 *
 */
public class ScreenShaker
{
    // Настроечки
    private final static float SHAKE_INTERVAL = 0.02f;

    private static Random random;
    private static float shakeTimePassed, radius, randomAngle, shakeTime;
    private static Vector2 temp, offset;
    private static OrthographicCamera camera;
    private static boolean shaking = false;

    // Статический конструктор
    public static void construct()
    {
        random = new Random();
    }

    // Шейк ит, бейба!
    // Гарантируется, что камера больше никак не двигается
    public static void shake(OrthographicCamera camArg, float shakeRadiusArg, float shakeTimeArg)
    {
        if (shaking)
        {
            radius = shakeRadiusArg;
            shakeTimePassed = 0f;
            shakeTime = shakeTimeArg;

            return;
        }

        shaking = true;

        // Устанавливаем всякие вектора и переменные
        shakeTimePassed = 0f;
        camera = camArg;
        radius = shakeRadiusArg;
        shakeTime = shakeTimeArg;

        randomAngle = random.nextFloat() / 360f;
        offset = new Vector2((float) (Math.sin(randomAngle) * radius), (float) (Math.cos(randomAngle) * radius));

        // Обновляем камеру
        camera.translate(offset);
        camera.update();

        // Начинаем тряску
        Timer.schedule(new Timer.Task()
        {
            @Override
            public void run()
            {
                // Возвращаем камеру в исходное положение
                offset = offset.rotate(180f);
                camera.translate(offset);
                camera.update();
                offset.set(0f, 0f);

                shakeTimePassed += SHAKE_INTERVAL;

                if (shakeTimePassed < shakeTime) {
                    // Находим новые значения радиуса и поворота
                    radius *= 0.9f;
                    randomAngle += (180f + random.nextFloat() % 60f);

                    // Находим новый сдвиг
                    offset.set((float) (Math.sin(randomAngle) * radius), (float) (Math.cos(randomAngle) * radius));

                    // Сдвигаем камеру
                    camera.translate(offset);
                    camera.update();
                }
                else
                {
                    shaking = false;
                    this.cancel();
                }
            }
        }, 0f, SHAKE_INTERVAL);
    }

    public static boolean isShaking()
    {
        return shaking;
    }
}
