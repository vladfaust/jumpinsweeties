package com.vladislavfaust.jumpinsweetis.helpers.dialogs;

/**
 * Created by Faust on 14.05.2015.
 *
 */
public interface IThreeOptionsDialog
{
    void clickedNegative();
    void clickedPositive();
    void clickedNeutral();
}
