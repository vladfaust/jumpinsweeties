package com.vladislavfaust.jumpinsweetis.helpers.dialogs;

/**
 * Created by Faust on 21.03.2015.
 *
 */
public interface IConfirmDialog
{
    void clickedOK ();
    void clickedCancel();
}
