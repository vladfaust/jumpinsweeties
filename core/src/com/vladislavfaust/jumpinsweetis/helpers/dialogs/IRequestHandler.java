package com.vladislavfaust.jumpinsweetis.helpers.dialogs;

/**
 * Created by Faust on 21.03.2015.
 *
 */
public interface IRequestHandler
{
    void confirm(IConfirmDialog confirmDialog, String title, String message, String ok, String cancel);

    void prompt(IPromptDialog promptDialog, String title, String ok, String cancel, String hint);

    void threeOptions(IThreeOptionsDialog threeOptionsDialog, String title, String message, String positive, String negative, String neutral);
}
