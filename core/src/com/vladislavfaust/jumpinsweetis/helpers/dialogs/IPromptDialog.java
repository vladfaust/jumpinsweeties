package com.vladislavfaust.jumpinsweetis.helpers.dialogs;

/**
 * Created by Faust on 02.04.2015.
 *
 */
public interface IPromptDialog extends IConfirmDialog
{
    void clickedOK(String input);
}
