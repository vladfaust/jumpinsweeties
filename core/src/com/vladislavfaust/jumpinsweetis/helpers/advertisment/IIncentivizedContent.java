package com.vladislavfaust.jumpinsweetis.helpers.advertisment;

/**
 * Created by Faust on 31.03.2015.
 *
 */
public interface IIncentivizedContent
{
    AdsManager.INCENTIVIZED_TYPE getNetworkType ();
    AdsManager.INCENTIVIZED_SOURCE getNetworkSource ();
    boolean showIncentivizedContent();
    boolean readyToShowIncentivizedContent();
}
