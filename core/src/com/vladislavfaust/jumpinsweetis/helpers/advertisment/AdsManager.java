package com.vladislavfaust.jumpinsweetis.helpers.advertisment;

import com.badlogic.gdx.utils.Array;
import com.vladislavfaust.jumpinsweetis.JumpinSweeties;
import com.vladislavfaust.jumpinsweetis.helpers.CandiesHandler;
import com.vladislavfaust.jumpinsweetis.helpers.Language;
import com.vladislavfaust.jumpinsweetis.helpers.MyPreferences;
import com.vladislavfaust.jumpinsweetis.helpers.analytics.AnalyticsHelper;
import com.vladislavfaust.jumpinsweetis.ui.UICommonElements;
import com.vladislavfaust.jumpinsweetis.ui.elements.UISuggestionMenu;

import java.util.Random;

import static com.vladislavfaust.jumpinsweetis.JumpinSweeties.DEBUG;

/**
 * Created by Faust on 16.03.2015.
 *
 */
public class AdsManager
{
    public enum INCENTIVIZED_TYPE
    {
        VIDEO, POLL
    }

    public enum INCENTIVIZED_SOURCE
    {
        ADCOLONY, CHARTBOOST, VUNGLE, POLLFISH
    }

    private final static boolean
            ADCOLONY_ENABLED = true,
            CHARTBOOST_ENABLED = true,
//            POLLFISH_ENABLED = false,
            VUNGLE_ENABLED = false;

    // Перерыв (в часах) между показами видео
    private final static float VIDEO_BREAK_TIME = (DEBUG)? 0.05f : 0.17f;

    // Награда в конфетах за просмотр видео
    public final static int REWARD_FOR_INCENTIVIZED = 300;

    // Обновляем таймер недоступности видео
    public static void updateIncentivizedTimer()
    {
        MyPreferences.putPreviousViewTime();
    }

    // Выдаем награду
    public static void giveRewardForIncentivized ()
    {
        CandiesHandler.addCandiesAsReward(REWARD_FOR_INCENTIVIZED);
        JumpinSweeties.instance().getPlatformUtils().showMessage(Language.LOCALE.format("yourReward", REWARD_FOR_INCENTIVIZED));

        UICommonElements.candiesLabel.setText(CandiesHandler.getCandies()+"");

        updateIncentivizedTimer();
    }

    // Изменяем UI в SkinsScreen
    public static void updateIncentivizedButton()
    {
        UISuggestionMenu.updateIncentivizedStack(false);
    }

    // Начинаем загрузку ревард-контента
    public static void preloadIncentivizedContent()
    {
        JumpinSweeties.instance().getChartBoost().cacheRewardedVideo();
    }

    // Узнаем, можно ли показывать ревард, закончился ли таймер
    public static boolean isTimerDone ()
    {
        return System.currentTimeMillis() - MyPreferences.getPreviousViewTime() > VIDEO_BREAK_TIME * 3600000;
    }

    // Показываем ревард-контент
    /*public static void showIncentivizedContent()
    {
        // Делаем рандом
        final Array<IIncentivizedContent> incentivizedContentArray = new Array<>();
        incentivizedContentArray.clear();

        if (ADCOLONY_ENABLED && JumpinSweeties.instance().getAdColony().readyToShowIncentivizedContent())
            incentivizedContentArray.add(JumpinSweeties.instance().getAdColony());

        if (CHARTBOOST_ENABLED && JumpinSweeties.instance().getChartBoost().readyToShowIncentivizedContent())
            incentivizedContentArray.add(JumpinSweeties.instance().getChartBoost());

        if (VUNGLE_ENABLED && JumpinSweeties.instance().getVungle().readyToShowIncentivizedContent())
            incentivizedContentArray.add(JumpinSweeties.instance().getVungle());

        if (POLLFISH_ENABLED && JumpinSweeties.instance().getPollFish().readyToShowIncentivizedContent())
            incentivizedContentArray.add(JumpinSweeties.instance().getPollFish());

        if (incentivizedContentArray.size > 0) {
            final int random = new Random().nextInt(incentivizedContentArray.size);

            String prompt = "";
            switch (incentivizedContentArray.get(random).getNetworkType())
            {
                case VIDEO:
                    prompt = Language.LOCALE.format("watchVideo", REWARD_FOR_INCENTIVIZED);
                    break;
                case POLL:
                    prompt = Language.LOCALE.format("completePoll", REWARD_FOR_INCENTIVIZED);
                    break;
            }

            JumpinSweeties.instance().getRequestHandler().confirm(new IConfirmDialog() {
                @Override
                public void clickedOK()
                {
                    incentivizedContentArray.get(random).showIncentivizedContent();

                    AnalyticsHelper.trackIncentivizedClick(incentivizedContentArray.get(random).getNetworkType(), true);
                    AnalyticsHelper.trackIncentivizedShown(incentivizedContentArray.get(random).getNetworkSource());
                }

                @Override
                public void clickedCancel()
                {
                    AnalyticsHelper.trackIncentivizedClick(incentivizedContentArray.get(random).getNetworkType(), false);
                }
            }, Language.LOCALE.get("confirm"), prompt, Language.LOCALE.get("ok"), Language.LOCALE.get("cancel"));
        }
        else
        {
            JumpinSweeties.instance().getPlatformUtils().showMessage(Language.LOCALE.get("failedToLoadRewardedContent"));
        }
    }*/

    /*
        Только видео
     */
    public static void showIncentivizedContent()
    {
        // Делаем рандом
        final Array<IIncentivizedContent> incentivizedContentArray = new Array<>();
        incentivizedContentArray.clear();

        if (ADCOLONY_ENABLED && JumpinSweeties.instance().getAdColony().readyToShowIncentivizedContent())
            incentivizedContentArray.add(JumpinSweeties.instance().getAdColony());

        if (CHARTBOOST_ENABLED && JumpinSweeties.instance().getChartBoost().readyToShowIncentivizedContent())
            incentivizedContentArray.add(JumpinSweeties.instance().getChartBoost());

        if (incentivizedContentArray.size > 0) {
            final int random = new Random().nextInt(incentivizedContentArray.size);
            incentivizedContentArray.get(random).showIncentivizedContent();

//            AnalyticsHelper.trackIncentivizedClick(incentivizedContentArray.get(random).getNetworkType(), true);
            AnalyticsHelper.trackIncentivizedShown(incentivizedContentArray.get(random).getNetworkSource());
        }
        else
        {
            JumpinSweeties.instance().getPlatformUtils().showMessage(Language.LOCALE.get("failedToLoadRewardedContent"));
        }
    }

    // Узнаем, сколько осталось в секундах до включения возомжности реварда
    public static double howMuchLeftToAllowIncentivizedContent()
    {
        return (VIDEO_BREAK_TIME * 3600000f + (double)MyPreferences.getPreviousViewTime() - (double)System.currentTimeMillis()) / 1000f;
    }

    // Есть ли загруженный ревард-контент?
    public static boolean readyToShowIncentivizedContent()
    {
        if (!AdsManager.isTimerDone())
            return false;

        if (CHARTBOOST_ENABLED && JumpinSweeties.instance().getChartBoost().readyToShowIncentivizedContent())
            return true;

        if (ADCOLONY_ENABLED && JumpinSweeties.instance().getAdColony().readyToShowIncentivizedContent())
            return true;

        /*if (POLLFISH_ENABLED && JumpinSweeties.instance().getPollFish().readyToShowIncentivizedContent())
            return true;*/

        return false;
    }
}
