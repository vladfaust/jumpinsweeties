package com.vladislavfaust.jumpinsweetis.helpers.advertisment;

/**
 * Created by Faust on 14.03.2015.
 *
 */
public interface IChartBoost extends IInterstitial, IIncentivizedContent
{
    void cacheInterstitial();
    void cacheRewardedVideo();
    boolean onBackPressed();
}
