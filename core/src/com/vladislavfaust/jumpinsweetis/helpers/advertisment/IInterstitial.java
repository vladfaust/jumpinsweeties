package com.vladislavfaust.jumpinsweetis.helpers.advertisment;

/**
 * Created by Faust on 31.03.2015.
 *
 */
public interface IInterstitial
{
    boolean showInterstitial();
    boolean readyToShowInterstitial();
}
