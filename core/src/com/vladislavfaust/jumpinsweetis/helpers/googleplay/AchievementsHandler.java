package com.vladislavfaust.jumpinsweetis.helpers.googleplay;

import com.badlogic.gdx.utils.Array;
import com.vladislavfaust.jumpinsweetis.JumpinSweeties;
import com.vladislavfaust.jumpinsweetis.helpers.JumperSkinsHandler;
import com.vladislavfaust.jumpinsweetis.helpers.Language;
import com.vladislavfaust.jumpinsweetis.helpers.ScreenShaker;
import com.vladislavfaust.jumpinsweetis.helpers.analytics.AnalyticsHelper;
import com.vladislavfaust.jumpinsweetis.ui.elements.UIShareAchievement;

/**
 * Created by Faust on 28.03.2015.
 *
 */
public class AchievementsHandler
{
    /*
        Сколько конфет дается за шеринг
     */

    public final static int ACHIEVEMENT_SHARE_REWARD = 200;

    /*
        Хранение всех достижений
     */

    public static class Achievement
    {
        private String achievementId, name, descKey;
        private float percentage;
        private String skinTexture = null;

        public Achievement(String achievementId, String descKey, String name, float percentage)
        {
            this.achievementId = achievementId;
            this.descKey = descKey;
            this.name = name;
            this.percentage = percentage;
        }

        public Achievement(String achievementId, String descKey, String name, float percentage, String skinTexture)
        {
            this.achievementId = achievementId;
            this.descKey = descKey;
            this.name = name;
            this.percentage = percentage;
            this.skinTexture = skinTexture;
        }

        public String getAchievementId()
        {
            return achievementId;
        }

        public String getDescKey()
        {
            return descKey;
        }

        public String getName()
        {
            return name;
        }

        public float getPercentage()
        {
            return percentage;
        }

        public String getSkinTexture()
        {
            return skinTexture;
        }
    }
    private final static Array<Achievement> achievements = new Array<>();
    public static Array<Achievement> getAchievements()
    {
        return achievements;
    }

    public static void init ()
    {
        achievements.clear();

        achievements.add(new Achievement("CgkIhea2oKYUEAIQAQ", "ReadyToRock", "Ready To Rock", 84f, "achievement-ready-to-rock"));
        achievements.add(new Achievement("CgkIhea2oKYUEAIQAg", "BecauseICan", "Because I Can", 20f));
        achievements.add(new Achievement("CgkIhea2oKYUEAIQCw", "Thirtythree", "Thirty-Three", 13f));
        achievements.add(new Achievement("CgkIhea2oKYUEAIQBg", "IronWill", "Iron Will", 56f, "achievement-shield"));
        achievements.add(new Achievement("CgkIhea2oKYUEAIQBw", "Novice", "Novice", 18f));
        achievements.add(new Achievement("CgkIhea2oKYUEAIQCA", "Adept", "Adept", 2f));
        achievements.add(new Achievement("CgkIhea2oKYUEAIQCQ", "GrandMaster", "Grand Master", 0.01f, "achievement-zen"));
        achievements.add(new Achievement("CgkIhea2oKYUEAIQAw", "StarCommander", "Star Commander", 4f, "achievement-star"));
        achievements.add(new Achievement("CgkIhea2oKYUEAIQBA", "TimeFliesBy", "Time Flies By", 2f, "achievement-time"));
        achievements.add(new Achievement("CgkIhea2oKYUEAIQBQ", "CandyLand", "Candy Land", 9f, "achievement-candie"));
        achievements.add(new Achievement("CgkIhea2oKYUEAIQEg", "TokenKeeper", "Token Keeper", 3f, "achievement-token"));
        achievements.add(new Achievement("CgkIhea2oKYUEAIQEw", "15Missions", "15 Missions", 11f, "achievement-15-missions"));
        achievements.add(new Achievement("CgkIhea2oKYUEAIQFA", "30Missions", "30 Missions", 2f, "achievement-30-missions"));
        achievements.add(new Achievement("CgkIhea2oKYUEAIQFQ", "50Missions", "50 Missions", 0.01f, "achievement-50-missions"));
        achievements.add(new Achievement("CgkIhea2oKYUEAIQFg", "Columbus", "Columbus", 0.01f, "achievement-columbus"));
        achievements.add(new Achievement("CgkIhea2oKYUEAIQFw", "MaxBoost", "Max Boost", 0.01f));

        checkShouldBeUnlockedSkins();
    }

    private static String getAchievementIdByName (String name)
    {
        for (int i = 0; i < achievements.size; i++) {
            if (achievements.get(i).getDescKey().equals(name))
                return achievements.get(i).getAchievementId();
        }

        throw new RuntimeException("Achievement '" + name + "' not found!");
    }

    private static int getIdByName (String name)
    {
        for (int i = 0; i < achievements.size; i++) {
            if (achievements.get(i).getDescKey().equals(name))
                return i;
        }

        throw new RuntimeException("Achievement '" + name + "' not found!");
    }

    private static String getSkinByName (String name)
    {
        for (int i = 0; i < achievements.size; i++) {
            if (achievements.get(i).getDescKey().equals(name))
                return achievements.get(i).getSkinTexture();
        }

        throw new RuntimeException("Achievement '" + name + "' not found!");
    }

    public static int getIdBySkinTexture (String texture)
    {
        for (int i = 0; i < achievements.size; i++) {
            if (achievements.get(i).getSkinTexture() != null && achievements.get(i).getSkinTexture().equals(texture))
                return i;
        }

        throw new RuntimeException("Achievement with skin texure '" + texture + "' not found!");
    }

    /*
        Методы разблокировки оных
     */

    private static int nexSharedId = -1;

    public static int getNexSharedId()
    {
        return nexSharedId;
    }

    public static void resetNextShared ()
    {
        nexSharedId = -1;
    }

    public static void checkShouldBeUnlockedSkins()
    {
        if (!JumpinSweeties.instance().getPlatformUtils().checkInternetConnection())
            return;

        for (int i = 0; i < achievements.size; i++)
        {
            if (achievements.get(i).getSkinTexture() == null)
                continue;

            final int a = i;

            JumpinSweeties.instance().getGooglePlayServices().checkIfAchievementUnlocked(achievements.get(i).getAchievementId(), new OnAchievementCheck() {
                @Override
                public void doSomething(boolean unlocked)
                {
                    if (unlocked && JumperSkinsHandler.getSkins().get(JumperSkinsHandler.getSkinIdByTextureName(achievements.get(a).getSkinTexture())).isLocked())
                    {
                        JumperSkinsHandler.unlockSkin(JumperSkinsHandler.getSkinIdByTextureName(achievements.get(a).getSkinTexture()));
                        JumpinSweeties.instance().getPlatformUtils().showMessage(Language.LOCALE.get("skinUnlocked"));
                    }
                }
            });
        }
    }

    private static void unlockSkin (String texture)
    {
        if (JumperSkinsHandler.getSkins().get(JumperSkinsHandler.getSkinIdByTextureName(texture)).isLocked())
        {
            JumperSkinsHandler.unlockSkin(JumperSkinsHandler.getSkinIdByTextureName(texture));
            JumpinSweeties.instance().getPlatformUtils().showMessage(Language.LOCALE.get("skinUnlocked"));
        }
    }

    private static void tryToUnlockAchievement(final String name, final int steps)
    {
        final String achievementId = getAchievementIdByName(name);
        final int id = getIdByName(name);

        /*
            Если интернета нет, то выполняем оффлайн
         */

        if (!JumpinSweeties.instance().getPlatformUtils().checkInternetConnection()) {
            if (steps > 0)
                JumpinSweeties.instance().getGooglePlayServices().incrementAchievement(achievementId, steps);
            else
                JumpinSweeties.instance().getGooglePlayServices().unlockAchievementGPGS(achievementId);
        }

        /*
            Если интернет есть, то выполняем онлайн
         */

        else
            JumpinSweeties.instance().getGooglePlayServices().checkIfAchievementUnlocked(achievementId, new OnAchievementCheck()
            {
                @Override
                public void doSomething(boolean unlocked)
                {
                    if (unlocked)
                        return;

                    if (steps > 0)
                        JumpinSweeties.instance().getGooglePlayServices().incrementAchievement(achievementId, steps);
                    else
                        JumpinSweeties.instance().getGooglePlayServices().unlockAchievementGPGS(achievementId);

                    JumpinSweeties.instance().getGooglePlayServices().checkIfAchievementUnlocked(achievementId, new OnAchievementCheck()
                    {
                        @Override
                        public void doSomething(boolean unlocked)
                        {
                            if (!unlocked)
                                return;

                            if (JumpinSweeties.instance().getAppState() != JumpinSweeties.AppState.GAME && !ScreenShaker.isShaking())
                                UIShareAchievement.show(id);
                            else
                                nexSharedId = id;

                            AnalyticsHelper.trackAchievementUnlock(id);

                            final String skinTexture = getSkinByName(name);
                            if (skinTexture != null)
                                unlockSkin(skinTexture);
                        }
                    });
                }
            });
    }

    /*
        0. Ready To Rock
     */

    public static void unlockReadyToRock ()
    {
        tryToUnlockAchievement("ReadyToRock", 0);
    }

    /*
        1. Because I Can
     */

    public static void unlockBecauseICan ()
    {
        tryToUnlockAchievement("BecauseICan", 0);
    }

    /*
        2. Thirty-Three
     */

    public static void unlock33 ()
    {
        tryToUnlockAchievement("Thirtythree", 0);
    }

    /*
        3. Iron Will
     */

    private static int gamesWithoutAnyStars = 0;

    public static void tryToUnlockIronWill (int stars)
    {
        if (stars > 0)
            gamesWithoutAnyStars = 0;
        else
            gamesWithoutAnyStars++;

        if (gamesWithoutAnyStars >= 20)
        {
            tryToUnlockAchievement("IronWill", 0);
        }
    }

    /*
        4. Novice
        5. Adept
        6. Grand Master
     */

    private static int gamesWithOneStar = 0;
    private static int gamesWithTwoStars = 0;
    private static int gamesWithThreeStars = 0;

    public static void tryToUnlockPathAchievements (int stars)
    {
        switch (stars)
        {
            case 0:
                gamesWithOneStar = 0;
                gamesWithTwoStars = 0;
                gamesWithThreeStars = 0;
                break;
            case 1:
                gamesWithOneStar++;
                gamesWithTwoStars = 0;
                gamesWithThreeStars = 0;
                break;
            case 2:
                gamesWithOneStar++;
                gamesWithTwoStars++;
                gamesWithThreeStars = 0;
                break;
            case 3:
                gamesWithOneStar++;
                gamesWithTwoStars++;
                gamesWithThreeStars++;
                break;
        }

        if (gamesWithOneStar >= 3)
        {
            tryToUnlockAchievement("Novice", 0);
        }

        if (gamesWithTwoStars >= 5)
            tryToUnlockAchievement("Adept", 0);

        if (gamesWithThreeStars >= 10)
        {
            tryToUnlockAchievement("GrandMaster", 0);
        }
    }

    /*
        7. Star Commander
     */

    public static void addStarCommander (int stars)
    {
        tryToUnlockAchievement("StarCommander", stars);
    }

    /*
        8. Time Flies By
     */

    public static void addTimeFliesBy ()
    {
        tryToUnlockAchievement("TimeFliesBy", 1);
    }

    /*
        9. Candy Land
     */

    public static void addCandyLand (int candies)
    {
        tryToUnlockAchievement("CandyLand", candies);
    }

    /*
        10. Token Keeper
     */

    public static void addTokenKeeper (int tokens)
    {
        tryToUnlockAchievement("TokenKeeper", tokens);
    }

    /*
        11. 15 Missions
        12. 30 Missions
        13. 50 Missions
     */

    public static void completeMission (int id)
    {
        if (id >= 14)
            tryToUnlockAchievement("15Missions", 0);
        if (id >= 29)
            tryToUnlockAchievement("30Missions", 0);
        if (id >= 49)
            tryToUnlockAchievement("50Missions", 0);
    }

    /*
        14. Columbus
     */

    public static void unlockColumbus ()
    {
        tryToUnlockAchievement("Columbus", 0);
    }

    /*
        15. Max Boost
     */

    public static void unlockMaxBoost ()
    {
        tryToUnlockAchievement("MaxBoost", 0);
    }
}
