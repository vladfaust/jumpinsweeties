package com.vladislavfaust.jumpinsweetis.helpers;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;
import com.vladislavfaust.jumpinsweetis.helpers.googleplay.AchievementsHandler;

import java.util.Random;

/**
 * Created by Faust on 20.01.2015.
 *
 */
public class JumperSkinsHandler
{
    /*
        Переменные
     */

    public enum SKINS_TYPE
    {
        COMMON, UNIQUE, ACHIEVEMENT
    }

    private static int activeSkinID = 0;
    private static int totalSkins = 0;
    private static final Array<JumperSkin> skins = new Array<>();
    private static final Array<String> unlockedSkins = new Array<>();

    /*
        Публичные методы
     */

    // Получить ID активного скина
    public static int getActiveSkinID()
    {
        return activeSkinID;
    }

    public static void selectSkin (int id)
    {
        MyPreferences.putActiveSkin(skins.get(id).getTextureName());
        activeSkinID = id;
    }

    public static void selectSkin (String textureName)
    {
        MyPreferences.putActiveSkin(textureName);
        activeSkinID = getSkinIdByTextureName(textureName);
    }

    // Получить текстуру любого скина по ID
    public static TextureRegion getSelectedJumperTexture(int id)
    {
        return AssetLoader.getJumperSkins().findRegion(getJumperTextureName(id));
    }

    // Получить текстуру скина в формате строки
    public static String getJumperTextureName (int id)
    {
        return skins.get(id).getTextureName();
    }

    // Получить текстуру АКТИВНОГО скина
    public static TextureRegion getSelectedJumperTexture()
    {
        return getSelectedJumperTexture(getActiveSkinID());
    }

    // Получить "вращается ли" АКТИВНОГО скина
    public static boolean isSelectedJumperRotates()
    {
        return skins.get(getActiveSkinID()).isRotates();
    }

    // Возвращает, есть ли еще свободные для открытия скины
    public static boolean areLockedSkinsLeft(Array<SKINS_TYPE> types)
    {
        int lockedAmount = 0;

        for (int i = 0; i < skins.size; i++)
            if (skins.get(i).isLocked() && (types.contains(skins.get(i).getType(), true)))
                lockedAmount++;

        return (lockedAmount > 0);
    }

    public static boolean areLockedSkinsLeft(SKINS_TYPE type)
    {
        Array<SKINS_TYPE> types = new Array<>();
        types.add(type);

        return areLockedSkinsLeft(types);
    }

    // Возвращает id случайного "незадостижения" скина
    public static int getRandomLockedSkinId (boolean common, boolean unique)
    {
        Array<SKINS_TYPE> types = new Array<>();

        if (common)
            types.add(SKINS_TYPE.COMMON);
        if (unique)
            types.add(SKINS_TYPE.UNIQUE);

        if (!areLockedSkinsLeft(types))
            return -1;

        Random random = new Random();

        while (true)
        {
            int i = random.nextInt(skins.size);

            if (skins.get(i).isLocked() && ((common && skins.get(i).getType() == SKINS_TYPE.COMMON) || (unique && skins.get(i).getType() == SKINS_TYPE.UNIQUE)))
                return i;
        }
    }

    // Функция отвечает за запись скинов в настройки
    public static void unlockSkin (int id)
    {
        // Добавляем в эррей
        unlockedSkins.add(skins.get(id).getTextureName());
        skins.get(id).setLocked(false);

        // Пишем в настройки
        String toWrite = "";

        for (int i = 0; i < unlockedSkins.size; i++)
            toWrite += (unlockedSkins.get(i) + ";");

        MyPreferences.putUnlockedSkins(toWrite);

        if (unlockedSkins.size == skins.size)
            AchievementsHandler.unlockColumbus();
    }

    // Возвращает кол-во разблокированных скинов определенного типа
    public static int getUnlockedSkinsCount(SKINS_TYPE type)
    {
        int result = 0;

        for (int i = 0; i < skins.size; i++)
            if (skins.get(i).getType() == type && !skins.get(i).isLocked())
                result++;

        return result;
    }

    // Возвращает общее кол-во скинов определенного типа
    public static int getTotalSkinCount (SKINS_TYPE type)
    {
        if (type == null)
            return skins.size;

        int result = 0;
        for (int i = 0; i < skins.size; i++)
            if (skins.get(i).getType() == type)
                result++;

        return result;
    }

    public static int getSkinIdByTextureName (String textureName)
    {
        for (int i = 0; i < skins.size; i++)
            if (skins.get(i).getTextureName().equals(textureName))
                return i;
        return -1;
    }

    public static Array<JumperSkin> getSkins()
    {
        return skins;
    }

    /*
        Инициализация
     */

    public static void init()
    {
        // Грузим все скины
        skins.clear();
        totalSkins = 0;

        // Обычные
        skins.add(new JumperSkin("tier1-sweet", 0));
        skins.add(new JumperSkin("tier1-cake1", 100));
        skins.add(new JumperSkin("tier1-cake2", 100));
        skins.add(new JumperSkin("tier1-candy", 100));
        skins.add(new JumperSkin("tier1-choko", 100));

        skins.add(new JumperSkin("tier4-1", 300));
        skins.add(new JumperSkin("tier4-2", 300));
        skins.add(new JumperSkin("tier4-3", 300));
        skins.add(new JumperSkin("tier4-5", 300));
        skins.add(new JumperSkin("tier4-6", 300));

        skins.add(new JumperSkin("tier3-cherry", 500));
        skins.add(new JumperSkin("tier3-buttershit", 500));
        skins.add(new JumperSkin("tier3-chockoshit", 500));
        skins.add(new JumperSkin("tier3-cookie", 500));
        skins.add(new JumperSkin("tier3-donut", 500));
        skins.add(new JumperSkin("tier3-lovecookie", 500));
        skins.add(new JumperSkin("tier3-strawberry2", 500));

        skins.add(new JumperSkin("tier2-basketball", 700));
        skins.add(new JumperSkin("tier2-beachball", 700));
        skins.add(new JumperSkin("tier2-bowlingball", 700));
        skins.add(new JumperSkin("tier2-soccerball", 700));
        skins.add(new JumperSkin("tier2-tennisball", 700));

        skins.add(new JumperSkin("cookie-1", 900));
        skins.add(new JumperSkin("cookie-2", 900));
        skins.add(new JumperSkin("cookie-3", 900));
        skins.add(new JumperSkin("cookie-4", 900));

        skins.add(new JumperSkin("tier5-1", 1000));
        skins.add(new JumperSkin("tier5-2", 1000));
        skins.add(new JumperSkin("tier5-3", 1000));
        skins.add(new JumperSkin("tier5-4", 1000));
        skins.add(new JumperSkin("tier5-5", 1000));
        skins.add(new JumperSkin("tier5-6", 1000));
        skins.add(new JumperSkin("tier5-7", 1000));
        skins.add(new JumperSkin("tier5-8", 1000));

        skins.add(new JumperSkin("ccc-1", 1200));
        skins.add(new JumperSkin("ccc-2", 1200));
        skins.add(new JumperSkin("ccc-3", 1200));
        skins.add(new JumperSkin("ccc-4", 1200));
        skins.add(new JumperSkin("ccc-5", 1200));

        // Особые
        skins.add(new JumperSkin("unique-football", SKINS_TYPE.UNIQUE));
        skins.add(new JumperSkin("unique-king-donut", SKINS_TYPE.UNIQUE));
        skins.add(new JumperSkin("unique-strawberry-blup", SKINS_TYPE.UNIQUE));
        skins.add(new JumperSkin("unique-sweet", SKINS_TYPE.UNIQUE));
        skins.add(new JumperSkin("unique-ying-yang", SKINS_TYPE.UNIQUE));
        skins.add(new JumperSkin("unique-white-choco", SKINS_TYPE.UNIQUE));
        skins.add(new JumperSkin("unique-helmet", SKINS_TYPE.UNIQUE));
        skins.add(new JumperSkin("unique-eye", SKINS_TYPE.UNIQUE));
        skins.add(new JumperSkin("unique-cookie-2", SKINS_TYPE.UNIQUE));
        skins.add(new JumperSkin("unique-cookie-1", SKINS_TYPE.UNIQUE));
        skins.add(new JumperSkin("unique-ccc-1", SKINS_TYPE.UNIQUE));
        skins.add(new JumperSkin("unique-caco", SKINS_TYPE.UNIQUE));

        // Ачивки
        skins.add(new JumperSkin("achievement-ready-to-rock", SKINS_TYPE.ACHIEVEMENT));
        skins.add(new JumperSkin("achievement-shield", SKINS_TYPE.ACHIEVEMENT));
        skins.add(new JumperSkin("achievement-candie", SKINS_TYPE.ACHIEVEMENT));
        skins.add(new JumperSkin("achievement-15-missions", SKINS_TYPE.ACHIEVEMENT));
        skins.add(new JumperSkin("achievement-30-missions", SKINS_TYPE.ACHIEVEMENT));
        skins.add(new JumperSkin("achievement-50-missions", SKINS_TYPE.ACHIEVEMENT));
        skins.add(new JumperSkin("achievement-star", SKINS_TYPE.ACHIEVEMENT));
        skins.add(new JumperSkin("achievement-time", SKINS_TYPE.ACHIEVEMENT));
        skins.add(new JumperSkin("achievement-token", SKINS_TYPE.ACHIEVEMENT));
        skins.add(new JumperSkin("achievement-columbus", SKINS_TYPE.ACHIEVEMENT));
        skins.add(new JumperSkin("achievement-zen", SKINS_TYPE.ACHIEVEMENT));

        // Получаем список открытых скинов
        String openedSkinsString = MyPreferences.getUnlockedSkins();
        String[] openedSkinsStrings = openedSkinsString.split(";");

        unlockedSkins.clear();

        for (int i = 0; i < openedSkinsStrings.length; i++)
            unlockedSkins.add((openedSkinsStrings[i]));

        // Изменяем лист скинов в соответствии с октрытыми/закрытыми
        for (int i = 0; i < skins.size; i++)
            if (unlockedSkins.contains(skins.get(i).getTextureName(), false))
                skins.get(i).setLocked(false);

        activeSkinID = getSkinIdByTextureName(MyPreferences.getActiveSkin());
    }

    /*
        Класс скина
     */

    public static class JumperSkin
    {
        private int id = 0;  // ID

        private String textureName = "";  // Имя текстуры джампера
        private int price = 0;   // Цена в конфетках
        private int tier = 0;

        private boolean rotates = true;

        private SKINS_TYPE type = SKINS_TYPE.COMMON;
        private boolean locked = true;  // Разблокирован ли

        public JumperSkin(String textureName, int price)
        {
            this.id = totalSkins; totalSkins++;
            this.textureName = textureName;
            this.price = price;
            this.type = SKINS_TYPE.COMMON;
        }

        public JumperSkin(String textureName, SKINS_TYPE type)
        {
            this.id = totalSkins; totalSkins++;
            this.textureName = textureName;
            this.type = type;
        }

        // Геттеры
        public int getId()
        {
            return id;
        }

        public String getTextureName()
        {
            return textureName;
        }

        public int getPrice()
        {
            return price;
        }

        public boolean isRotates()
        {
            return rotates;
        }

        public boolean isLocked()
        {
            return locked;
        }

        public int getTier()
        {
            return tier;
        }

        public SKINS_TYPE getType()
        {
            return type;
        }

        // Сеттеры
        public void setLocked(boolean locked)
        {
            this.locked = locked;
        }
    }
}
