package com.vladislavfaust.jumpinsweetis.helpers.gifts;

import com.vladislavfaust.jumpinsweetis.helpers.MyPreferences;

/**
 * Created by Faust on 27.04.2015.
 *
 */
public class GiftsCooldownHelper
{
    // Длительность основного таймера подарков, в миллисекундах
    private final static long
            FULL_REFRESH_IN = 6 /* <= Часы */ * 60 * 60 * 1000;

    // Задержки перед появлением тикета, в миллисекундах
    private final static long
            DELAY_1 = 3 /* <= Минуты */ * 60 * 1000,
            DELAY_2 = 3 /* <= Минуты */ * 60 * 1000,
            DELAY_3 = 9 /* <= Минуты */ * 60 * 1000,
            DELAY_4 = 15 /* <= Минуты */ * 60 * 1000;

    // Имеется ли сейчас билетик
    private static boolean hasATicket;

    // Инициализация
    public static void init ()
    {
        // Сброс кулдауна
        if (FULL_REFRESH_IN + MyPreferences.getFirstGiveawayTime() - System.currentTimeMillis() <= 0)
        {
            MyPreferences.putLastGiveawayNumber(0);
            MyPreferences.putLastGiveawayTime(System.currentTimeMillis());
        }
    }

    // В начале игры спрашивать, можно ли спавнить билетик
    public static boolean readyToSpawnATicket ()
    {
        if (hasATicket)
            return false;

        switch (MyPreferences.getLastGiveawayNumber())
        {
            case 0:
                return (MyPreferences.getLastGiveawayTime() + DELAY_1 < System.currentTimeMillis());
            case 1:
                return (MyPreferences.getLastGiveawayTime() + DELAY_2 < System.currentTimeMillis());
            case 2:
                return (MyPreferences.getLastGiveawayTime() + DELAY_3 < System.currentTimeMillis());
            case 3:
                return (MyPreferences.getLastGiveawayTime() + DELAY_4 < System.currentTimeMillis());
            default:
                return false;
        }
    }

    // Активация тикета при его подборе
    public static void pickupTicket ()
    {
        hasATicket = true;
    }

    // Возвращает кол-во миллисекунд до сброса таймера между спавном билетов
    public static long getCooldown()
    {
        switch (MyPreferences.getLastGiveawayNumber())
        {
            case 0:
                return DELAY_1 + MyPreferences.getLastGiveawayTime() - System.currentTimeMillis();
            case 1:
                return DELAY_2 + MyPreferences.getLastGiveawayTime() - System.currentTimeMillis();
            case 2:
                return DELAY_3 + MyPreferences.getLastGiveawayTime() - System.currentTimeMillis();
            case 3:
                return DELAY_4 + MyPreferences.getLastGiveawayTime() - System.currentTimeMillis();
            default:
                return FULL_REFRESH_IN + MyPreferences.getLastGiveawayTime() - System.currentTimeMillis();
        }
    }

    // Возвращает "есть ли билетик"
    public static boolean doesHaveATicket ()
    {
        return hasATicket;
    }

    // Апдейт значений после розыгрыша
    public static void afterGiveaway ()
    {
        if (MyPreferences.getLastGiveawayNumber() == 0)
            MyPreferences.putFirstGiveawayTime(System.currentTimeMillis());

        /*
            Если прошло более половины кулдауна, а пользователь разыгрывает 3 или 4 раз, сбрасываем кулдаун к нулю
         */

        else if (MyPreferences.getLastGiveawayNumber() > 0 && (FULL_REFRESH_IN + MyPreferences.getFirstGiveawayTime() - System.currentTimeMillis() < ((double) FULL_REFRESH_IN) / 2f))
            MyPreferences.putFirstGiveawayTime(System.currentTimeMillis());

        MyPreferences.putLastGiveawayTime(System.currentTimeMillis());
        MyPreferences.putLastGiveawayNumber(MyPreferences.getLastGiveawayNumber() + 1);

        hasATicket = false;
    }

    // Возвращает, начался ли большой кулдаун
    public static boolean lastChance ()
    {
        return (MyPreferences.getLastGiveawayNumber() > 3);
    }
}
