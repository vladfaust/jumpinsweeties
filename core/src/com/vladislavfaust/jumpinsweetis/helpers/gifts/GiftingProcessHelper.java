package com.vladislavfaust.jumpinsweetis.helpers.gifts;

import com.badlogic.gdx.utils.Array;
import com.vladislavfaust.jumpinsweetis.helpers.JumperSkinsHandler;

import java.util.Random;

/**
 * Created by Faust on 29.04.2015.
 *
 */

public class GiftingProcessHelper
{
    // Кол-во попыток
    final public static int MAX_ATTEMPTS = 3;

    // Переменные
    final static Array<GiftType> giftTypes = new Array<>();
    private static int attemptsLeft = 0;
    private final static Random RANDOM = new Random();

    // Конструктор
    public enum GIFT_TYPES
    {
        CANDIES_FEW, CANDIES_SOME, CANDIES_MANY, COMMON_SKIN, SECRET_SKIN, TOKENS_FEW, TOKENS_MANY
    }

    public static void construct ()
    {
        giftTypes.clear();

        giftTypes.add(new GiftType(GIFT_TYPES.CANDIES_SOME, 36));
        giftTypes.add(new GiftType(GIFT_TYPES.CANDIES_MANY,13));
        giftTypes.add(new GiftType(GIFT_TYPES.TOKENS_FEW, 32));
        giftTypes.add(new GiftType(GIFT_TYPES.TOKENS_MANY, 11));
        giftTypes.add(new GiftType(GIFT_TYPES.SECRET_SKIN, 8));

        // Проверка на 100% шанс
        int chance = 0;
        for (int i = 0; i < giftTypes.size; i++)
            chance += giftTypes.get(i).chance;
        if (chance < 100)
            throw new RuntimeException("Chance < 100!");
    }

    // Подготовка, вызывается при вызове экрана раздачи подарков
    public static void init ()
    {
        attemptsLeft = MAX_ATTEMPTS;
    }

    // Какой тип подарка?
    public static GIFT_TYPES getGiftType(boolean openAll)
    {
        if (--attemptsLeft < 0 && !openAll)
            return null;

        while (true) {
            int random = RANDOM.nextInt(101);

            int used = 0;
            for (int i = 0; i < giftTypes.size; i++) {
                if (random <= used + giftTypes.get(i).chance) {
                    // Нужно сделать проверку на некоторые типы подарков
                    if (giftTypes.get(i).type == GIFT_TYPES.COMMON_SKIN) {
                        if (JumperSkinsHandler.areLockedSkinsLeft(JumperSkinsHandler.SKINS_TYPE.COMMON))
                            return giftTypes.get(i).type;
                        else
                            used += giftTypes.get(i).chance;
                    } else if (giftTypes.get(i).type == GIFT_TYPES.SECRET_SKIN) {
                        if (JumperSkinsHandler.areLockedSkinsLeft(JumperSkinsHandler.SKINS_TYPE.UNIQUE))
                            return giftTypes.get(i).type;
                        else
                            used += giftTypes.get(i).chance;
                    } else {
                        return giftTypes.get(i).type;
                    }
                } else
                    used += giftTypes.get(i).chance;
            }
        }
    }

    // Возвращает значение подарка (кол-во конфет или id скина)
    public static int getGiftValue (GIFT_TYPES which)
    {
        switch (which)
        {
            case CANDIES_FEW:
                return 17 + RANDOM.nextInt(34);
            case CANDIES_SOME:
                return 100 + RANDOM.nextInt(51);
            case CANDIES_MANY:
                return 200 + RANDOM.nextInt(101);
            case COMMON_SKIN:
                return JumperSkinsHandler.getRandomLockedSkinId(true, false);
            case SECRET_SKIN:
                return JumperSkinsHandler.getRandomLockedSkinId(false, true);
            case TOKENS_FEW:
                return 1 + RANDOM.nextInt(3);
            case TOKENS_MANY:
                return 5 + RANDOM.nextInt(8);
            default:
                return -1;
        }
    }

    // Класс подарков
    static class GiftType
    {
        public GIFT_TYPES type;
        public int chance;

        public GiftType(GIFT_TYPES type, int chance)
        {
            this.chance = chance;
            this.type = type;
        }
    }

    public static int getAttemptsLeft()
    {
        return attemptsLeft;
    }
}
