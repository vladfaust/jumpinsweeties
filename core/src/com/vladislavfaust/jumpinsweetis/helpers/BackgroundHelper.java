package com.vladislavfaust.jumpinsweetis.helpers;

import com.vladislavfaust.jumpinsweetis.ui.Background;

/**
 * Created by Faust on 22.05.2015.
 *
 */
public class BackgroundHelper
{
    public enum BACKGROUND
    {
        CLASSIC, SWIRL, SKY, SWEETIES
    }

    /*
        ��������� ��������� �� ��������
     */

    public static void init ()
    {
        Background.instance().setBackground(MyPreferences.getActiveBackground());
    }

    public static void setBackground (BACKGROUND which)
    {
        MyPreferences.putActiveBackground(which);
        Background.instance().setBackground(which);
    }

    public static boolean isBackgroundUnlocked (BACKGROUND which)
    {
        return (MyPreferences.getHighscore() >= howMuchScoreBackgroundCosts(which));
    }

    public static int getTotalBackgroundsCount ()
    {
        return BACKGROUND.values().length;
    }

    public static int howMuchScoreBackgroundCosts (BACKGROUND which)
    {
        switch (which)
        {
            case CLASSIC:
                return 0;
            case SWIRL:
                return 10;
            case SKY:
                return 20;
            case SWEETIES:
                return 30;
            default:
                return 999;
        }
    }

    public static int getUnlockedBackgroundsCount ()
    {
        int result = 0;

        for (int i = 0; i < BACKGROUND.values().length; i++)
            if (isBackgroundUnlocked(BACKGROUND.values()[i]))
                result++;

        return result;
    }
}
