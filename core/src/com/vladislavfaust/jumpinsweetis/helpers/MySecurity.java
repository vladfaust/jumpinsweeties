package com.vladislavfaust.jumpinsweetis.helpers;

import com.badlogic.gdx.utils.Array;
import com.vladislavfaust.jumpinsweetis.gameobjects.bonuses.Candie;
import com.vladislavfaust.jumpinsweetis.gameobjects.bonuses.Doubler;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.util.encoders.Base64;

import java.nio.ByteBuffer;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.ShortBufferException;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by Faust on 19.05.2015.
 *
 */

public class MySecurity
{

    public static void init () throws NoSuchPaddingException, NoSuchAlgorithmException, NoSuchProviderException
    {
        initAES();
    }

    // region AES
    
    /*
        AES
     */

    private static Cipher cipherAES = null;
    private static SecretKeySpec keyAES = null;
    
    private static void initAES () throws NoSuchPaddingException, NoSuchAlgorithmException, NoSuchProviderException
    {
        byte[] keyBytes = "kjb^fd2*#fvn32F&".getBytes();
        keyAES = new SecretKeySpec(keyBytes, "AES");

        java.security.Security.addProvider(new BouncyCastleProvider());
        cipherAES = Cipher.getInstance("AES/ECB/PKCS7Padding", "BC");
    }

    private static byte[] doCrypt (int mode, byte[] input) throws NoSuchPaddingException, NoSuchAlgorithmException, NoSuchProviderException, InvalidKeyException, ShortBufferException, BadPaddingException, IllegalBlockSizeException
    {
        cipherAES.init(mode, keyAES);

        byte[] cipherText = new byte[cipherAES.getOutputSize(input.length)];
        int ctLength = cipherAES.update(input, 0, input.length, cipherText, 0);
        ctLength += cipherAES.doFinal(cipherText, ctLength);

        return cipherText;
    }

    /**
     * ������� ������
     * @param value ��� ����� ���������
     * @return ������ � ������� Base64
     */

    public static String encryptValueAES(Object value)
    {
        try {
            return new String(Base64.encode(doCrypt(Cipher.ENCRYPT_MODE, value.toString().getBytes())));
        } catch (NoSuchPaddingException | NoSuchAlgorithmException | InvalidKeyException | ShortBufferException | BadPaddingException | IllegalBlockSizeException | NoSuchProviderException e) {
            throw new SecurityException("Encryption error: " + e.getLocalizedMessage());
        }
    }

    /**
     * ��������� ������, ���������� �� � �������� ���
     * @param encrypted Base64 ������
     * @param returnType ����� ������������� ���� � ������� ����� (Integer.class, Boolean.class)
     * @param <T> ������������ ���
     * @return ��������� ������� ����
     */

    public static <T> T decryptValueAES(String encrypted, Class<T> returnType)
    {
        if (encrypted != null) {
            String decrypted;

            try {
                decrypted = new String(doCrypt(Cipher.DECRYPT_MODE, Base64.decode(encrypted)));
            } catch (NoSuchPaddingException | NoSuchAlgorithmException | InvalidKeyException | ShortBufferException | BadPaddingException | IllegalBlockSizeException | NoSuchProviderException e) {
                throw new SecurityException("Encryption error: " + e.getLocalizedMessage());
            }

            decrypted = decrypted.replaceAll("\u0000", "");

            if (returnType.equals(Boolean.class))
                return convertInstanceOfObject(Boolean.parseBoolean(decrypted), returnType);
            if (returnType.equals(Long.class))
                return convertInstanceOfObject(Long.parseLong(decrypted), returnType);
            if (returnType.equals(Integer.class))
                return convertInstanceOfObject(Integer.parseInt(decrypted), returnType);
            if (returnType.equals(Float.class))
                return convertInstanceOfObject(Float.parseFloat(decrypted), returnType);
            if (returnType.equals(String.class))
                return convertInstanceOfObject(decrypted, returnType);
        }
        else
        {
            if (returnType.equals(Boolean.class))
                return convertInstanceOfObject(Boolean.parseBoolean("false"), returnType);
            if (returnType.equals(Long.class))
                return convertInstanceOfObject(Long.parseLong("0"), returnType);
            if (returnType.equals(Integer.class))
                return convertInstanceOfObject(Integer.parseInt("0"), returnType);
            if (returnType.equals(Float.class))
                return convertInstanceOfObject(Float.parseFloat("0f"), returnType);
            if (returnType.equals(String.class))
                return convertInstanceOfObject("", returnType);
        }

        return null;
    }

    private static <T> T convertInstanceOfObject(Object o, Class<T> clazz) {
        try {
            return clazz.cast(o);
        } catch(ClassCastException e) {
            return null;
        }
    }
    
    // endregion
    
    // region Game Protector
    
    /*
        ������ ������� ��������
     */

    public enum VARIABLE_TYPE
    {
        SCORE, LAST_CANDIES, CANDIES, TOKENS
    }

    private final static Array<GameVariable> gameVariables = new Array<>();
    private static class GameVariable
    {
        private VARIABLE_TYPE type;
        private int amount = 0;
        private byte[] previousHash;
        private byte previousCheckByte;

        public GameVariable(VARIABLE_TYPE type)
        {
            this.type = type;
        }
    }
    
    private static MessageDigest md5 = null;

    /*
        �������������
     */

    public static void initGameProtector () throws NoSuchAlgorithmException
    {
        gameVariables.clear();
        for (int i = 0; i < VARIABLE_TYPE.values().length; i++)
            gameVariables.add(new GameVariable(VARIABLE_TYPE.values()[i]));

        md5 = MessageDigest.getInstance("MD5");

        putVariable(VARIABLE_TYPE.CANDIES, MyPreferences.getCandies());
        putVariable(VARIABLE_TYPE.TOKENS, MyPreferences.getTokens());
    }

    /*
        ��������� ������
     */

    public static void newGame()
    {
        putVariable(VARIABLE_TYPE.SCORE, 0);
        putVariable(VARIABLE_TYPE.LAST_CANDIES, 0);
    }

    public static int getScore ()
    {
        return getCheckedValue(VARIABLE_TYPE.SCORE);
    }

    public static void addScore ()
    {
        modifyVariable(VARIABLE_TYPE.SCORE, 1);
    }

    public static int getLastCandies ()
    {
        return getCheckedValue(VARIABLE_TYPE.LAST_CANDIES);
    }

    public static void addLastCandies (Candie candie)
    {
        modifyVariable(VARIABLE_TYPE.LAST_CANDIES, (Doubler.isActive() ? candie.getValue() * 2 : candie.getValue()));
    }

    public static int getCandies ()
    {
        return getCheckedValue(VARIABLE_TYPE.CANDIES);
    }

    public static int addCandies (int amount)
    {
        modifyVariable(VARIABLE_TYPE.CANDIES, amount);
        MyPreferences.putCandies(findVariableByType(VARIABLE_TYPE.CANDIES).amount);
        return findVariableByType(VARIABLE_TYPE.CANDIES).amount;
    }

    public static int getTokens ()
    {
        return getCheckedValue(VARIABLE_TYPE.TOKENS);
    }

    public static void addTokens (int amount)
    {
        modifyVariable(VARIABLE_TYPE.TOKENS, amount);
        MyPreferences.putTokens(findVariableByType(VARIABLE_TYPE.TOKENS).amount);
    }

    /*
        ���������� ������
     */

    private static int getCheckedValue (VARIABLE_TYPE type)
    {
        if (checkVariable(type))
            return findVariableByType(type).amount;
        else
            throw new SecurityException("You're a cheater!");
    }

    private static void modifyVariable(VARIABLE_TYPE type, int amount)
    {
        if (checkVariable(type))
            putVariable(type, gameVariables.get(type.ordinal()).amount + amount);
    }

    private static void putVariable(VARIABLE_TYPE type, int amount)
    {
        GameVariable gameVariable = findVariableByType(type);

        gameVariable.amount = amount;
        byte[] previousBytes = ByteBuffer.allocate(4).putInt(amount).array();
        gameVariable.previousHash = md5.digest(previousBytes);
        int x = 6; x++; x--; x++;
        gameVariable.previousCheckByte = (byte) (gameVariable.amount % x);
    }

    private static boolean checkVariable(VARIABLE_TYPE type)
    {
        GameVariable gameVariable = findVariableByType(type);

        byte[] previousBytes = ByteBuffer.allocate(4).putInt(gameVariable.amount).array();

        if (!MessageDigest.isEqual(md5.digest(previousBytes), gameVariable.previousHash))
            return false;

        int i = 5; i--; i++; i++; i++; i--; i++;
        return gameVariable.previousCheckByte == (byte) (gameVariable.amount % i);
    }

    private static GameVariable findVariableByType(VARIABLE_TYPE type)
    {
        for (int i = 0; i < gameVariables.size; i++)
            if (gameVariables.get(i).type == type)
                return gameVariables.get(i);

        throw new RuntimeException("Variable '" + type.toString() + "' not found!");
    }
    
    // endregion
}
