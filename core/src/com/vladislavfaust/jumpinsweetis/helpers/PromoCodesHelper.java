package com.vladislavfaust.jumpinsweetis.helpers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.Array;

/**
 * Created by Faust on 02.04.2015.
 *
 */
public class PromoCodesHelper
{
    private static int totalCodes = 0;
    private static final Array<PromoCode> promoCodes = new Array<PromoCode>();
    private static final Array<String> usedClassifications = new Array<String>();

    public static void init ()
    {
        promoCodes.clear();
        totalCodes = 0;

        promoCodes.add(new PromoCode("REDDIT", "Launch"));
        promoCodes.add(new PromoCode("LIBGDX", "Launch"));
        promoCodes.add(new PromoCode("4PDA", "Launch"));
        promoCodes.add(new PromoCode("GAMEDEV", "Launch"));
        promoCodes.add(new PromoCode("MOBILISM", "Launch"));
        promoCodes.add(new PromoCode("XGM", "Launch"));
        promoCodes.add(new PromoCode("VKONTAKTE", "Launch"));
        promoCodes.add(new PromoCode("SLIDEDB", "Launch"));
        promoCodes.add(new PromoCode("SOLDGAME", "Launch"));
        promoCodes.add(new PromoCode("TRASHBOX", "Launch"));
        promoCodes.add(new PromoCode("XDA", "Launch"));
        promoCodes.add(new PromoCode("DROID", "Launch"));
        promoCodes.add(new PromoCode("ANDROID", "Launch"));
        promoCodes.add(new PromoCode("PIKABU", "Launch"));

        promoCodes.add(new PromoCode("EXAMPLE", "Example"));

        usedClassifications.clear();
        String usedClassificationsSaved = MyPreferences.getPromoCodeClassifications();
        String[] tempArray = usedClassificationsSaved.split(";");
        for (int i = 0; i < tempArray.length; i++)
            usedClassifications.add((tempArray[i]));
    }

    public static String enterCode (String code)
    {
        int id = findCodeWithValue(code);

        if (id == -1 || promoCodes.get(id).isUsed() || isClassificationUsed(promoCodes.get(id).getClassification()))
            return null;

        return useCode(id);
    }

    private static String useCode (int id)
    {
        promoCodes.get(id).setUsed(true);
        useClassification(promoCodes.get(id).getClassification());

        switch (promoCodes.get(id).getClassification())
        {
            case "Launch":
                CandiesHandler.addCandiesAsReward(500);
                return Language.LOCALE.format("code1Reward", 500);
            case "Example":
                CandiesHandler.addCandiesAsReward(300);
                return Language.LOCALE.format("code1Reward", 300);
            /*case "TEST":
                CandiesHandler.addCandiesAsReward(20000);
                return Language.LOCALE.format("code1Reward", 20000);*/
        }

        return null;
    }

    private static int findCodeWithValue (String value)
    {
        value = value.toUpperCase().replaceAll("\\s","");
        for (int i = 0; i < promoCodes.size; i++)
        {
            if (promoCodes.get(i).getValue().equals(value))
                return i;
        }

        return -1;
    }

    private static boolean isClassificationUsed (String classification)
    {
        return usedClassifications.contains(classification, false);
    }

    private static void useClassification (String classification)
    {
        usedClassifications.add(classification);

        String toWrite = "";
        for (int i = 0; i < usedClassifications.size; i++)
            toWrite += (usedClassifications.get(i) + ";");
        MyPreferences.putPromoCodeClassifications(toWrite);

        Gdx.app.log("ToWrite", toWrite);
    }

    private static class PromoCode
    {
        private int id;
        private String value;
        private String classification;
        private boolean used;

        public PromoCode (String value, String classification)
        {
            this.id = totalCodes++;
            this.value = value;
            this.classification = classification;
        }

        public int getId()
        {
            return id;
        }

        public String getValue()
        {
            return value;
        }

        public String getClassification()
        {
            return classification;
        }

        public boolean isUsed()
        {
            return used;
        }

        public void setUsed(boolean used)
        {
            this.used = used;
        }
    }
}
