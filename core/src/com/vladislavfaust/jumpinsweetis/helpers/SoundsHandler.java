package com.vladislavfaust.jumpinsweetis.helpers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Timer;
import com.vladislavfaust.jumpinsweetis.gamesystems.ScoreHandler;

/**
 * Created by Faust on 22.01.2015.
 *
 */
public class SoundsHandler
{
    // Настройки
    private final static float
            JUMP_VOLUME = 0.5f,
            PICKUP_VOLUME = 0.5f,
            FALLING_VOLUME = 0.4f,
            STAR_VOLUME = 0.4f,
            HIGHSCORE_VOLUME = 0.3f,
            BUTTON_VOLUME = 0.5f,
            ERROR_VOLUME = 0.3f,
            SELECT_SKIN_VOLUME = 1.2f,
            UNLOCK_SKIN_VOLUME = 1f,
            PETARD_VOLUME = 0.5f,
            MISSION_COMPLETED_VOLUME = 0.3f,
            ROCKET_VOLUME = 0.5f,
            DRUMROLL_VOLUME = 0.6f,
            COOL_GIFT_VOLUME = 0.4f,
            ACHIEVEMENT_VOLUME = 0.3f,
            SCREENSHOT_VOLUME = 0.5f,
            MAGNET_VOLUME = 0.3f,
            BOOST_PICKUP_VOLUME = 0.4f,
            BACKGROUND_MUTED = 0.07f,
            BACKGROUND_UNMUTING_TIME = 1f;

    private final static float STANDARD_SOUND_VOLUME = 0.8f;
    private static float overallSoundVolume = STANDARD_SOUND_VOLUME;

    private final static float STANDARD_MUSIC_VOLUME = 0.3f;
    private static float overallMusicVolume = STANDARD_MUSIC_VOLUME;

    // Различные звуки
    private static Sound jumpLeft, jumpRight, pickup, falling, button, highscore, error, selectSkin, unlockSkin, petard1, petard2, missionCompleted, rocket, coolGift, drumroll, achievement, camshot, magnetHum, boostPickup;
    private static Array<Sound> star = new Array<>(ScoreHandler.MAX_STARS);

    // Бэкграуд музик
    private static Music backgroundMusic;

    // Инициализатор
    public static void init ()
    {
        jumpLeft = Gdx.audio.newSound(Gdx.files.internal("sounds/jump_0.ogg"));
        jumpRight = Gdx.audio.newSound(Gdx.files.internal("sounds/jump_1.ogg"));
        pickup = Gdx.audio.newSound(Gdx.files.internal("sounds/candie_pickup.ogg"));
        falling = Gdx.audio.newSound(Gdx.files.internal("sounds/falling.ogg"));
        button = Gdx.audio.newSound(Gdx.files.internal("sounds/button.ogg"));
        highscore = Gdx.audio.newSound(Gdx.files.internal("sounds/highscore.ogg"));
        backgroundMusic = Gdx.audio.newMusic(Gdx.files.internal("sounds/background.ogg"));
        error = Gdx.audio.newSound(Gdx.files.internal("sounds/error.ogg"));
        selectSkin = Gdx.audio.newSound(Gdx.files.internal("sounds/skinSelected.ogg"));
        unlockSkin = Gdx.audio.newSound(Gdx.files.internal("sounds/skinUnlocked.ogg"));
        petard1 = Gdx.audio.newSound(Gdx.files.internal("sounds/petard1.ogg"));
        petard2 = Gdx.audio.newSound(Gdx.files.internal("sounds/petard2.ogg"));
        missionCompleted = Gdx.audio.newSound(Gdx.files.internal("sounds/missionCompleted.ogg"));
        rocket = Gdx.audio.newSound(Gdx.files.internal("sounds/rocket.ogg"));
        coolGift = Gdx.audio.newSound(Gdx.files.internal("sounds/coolGift.ogg"));
        drumroll = Gdx.audio.newSound(Gdx.files.internal("sounds/drumroll.ogg"));
        achievement = Gdx.audio.newSound(Gdx.files.internal("sounds/achievement.ogg"));
        camshot = Gdx.audio.newSound(Gdx.files.internal("sounds/camshot.ogg"));
        magnetHum = Gdx.audio.newSound(Gdx.files.internal("sounds/magnetHum.ogg"));
        boostPickup = Gdx.audio.newSound(Gdx.files.internal("sounds/boostPickup.ogg"));

        star.clear();
        for (int i = 0; i < ScoreHandler.MAX_STARS; i++)
        {
            star.add(Gdx.audio.newSound(Gdx.files.internal("sounds/star_"+(i+1)+".ogg")));
        }

        overallSoundVolume = (MyPreferences.getSounds()) ? STANDARD_SOUND_VOLUME : 0f;
        switchMusic(MyPreferences.getMusic());
    }

    // Переключить звуки
    public static void switchSound (boolean enabled)
    {
        overallSoundVolume = (enabled) ? STANDARD_SOUND_VOLUME : 0f;
        if (enabled)
            button();
    }

    // Переключить музыку
    public static void switchMusic (boolean enabled)
    {
        overallMusicVolume = (enabled) ? STANDARD_MUSIC_VOLUME : 0f;
        backgroundMusic.setVolume(overallMusicVolume);
    }

    /*
        Внутриигровые звуки
      */

    public static void jump (boolean right)
    {
        if (right)
            jumpLeft.play(JUMP_VOLUME * overallSoundVolume);
        else
            jumpRight.play(JUMP_VOLUME * overallSoundVolume);
    }

    public static void pickupCandie()
    {
        pickup.play(PICKUP_VOLUME * overallSoundVolume);
    }

    public static void fall ()
    {
        falling.play(FALLING_VOLUME * overallSoundVolume);
    }

    public static void star (int which)
    {
        star.get(which).play(STAR_VOLUME * overallSoundVolume);
    }

    // UI
    public static void button ()
    {
        button.play(BUTTON_VOLUME * overallSoundVolume);
    }

    public static void highscore ()
    {
        highscore.play(HIGHSCORE_VOLUME * overallSoundVolume);
    }

    public static void error ()
    {
        error.play(ERROR_VOLUME * overallSoundVolume);
    }

    public static void selectSkin ()
    {
        selectSkin.play(SELECT_SKIN_VOLUME * overallSoundVolume);
    }

    public static void unlockSkin ()
    {
        unlockSkin.play(UNLOCK_SKIN_VOLUME * overallSoundVolume);
    }

    public static void gameOverWhoosh ()
    {
        selectSkin.play(SELECT_SKIN_VOLUME * overallSoundVolume * 0.5f);
    }

    public static void petard (int which)
    {
        switch (which) {
            case 1:
                petard1.play(PETARD_VOLUME * overallSoundVolume);
                break;
            case 2:
                petard2.play(PETARD_VOLUME * overallSoundVolume);
                break;
        }
    }

    public static void missionCompleted ()
    {
        missionCompleted.play(MISSION_COMPLETED_VOLUME * overallSoundVolume);
    }

    public static void coolGift ()
    {
        coolGift.play(COOL_GIFT_VOLUME * overallSoundVolume);
    }

    public static void drumroll ()
    {
        drumroll.play(DRUMROLL_VOLUME * overallSoundVolume);
    }

    public static void achievement()
    {
        achievement.play(ACHIEVEMENT_VOLUME * overallSoundVolume);
        coolGift.play(COOL_GIFT_VOLUME * overallSoundVolume * 0.7f);
    }

    public static void camshot()
    {
        camshot.play(SCREENSHOT_VOLUME * overallSoundVolume);
    }

    public static void boostPickup ()
    {
        boostPickup.play(BOOST_PICKUP_VOLUME * overallSoundVolume);
    }

    /*
        Эффекты бустов
     */

    public static void activateRocket()
    {
        rocket.play(ROCKET_VOLUME * overallSoundVolume);
    }

    public static void activateMagnet ()
    {
        magnetHum.loop(MAGNET_VOLUME * overallSoundVolume);
    }

    public static void deactivateMagnet ()
    {
        magnetHum.stop();
    }

    // Музыка
    public static void backgroundStart ()
    {
        backgroundMusic.setLooping(true);
        backgroundMusic.setVolume(overallMusicVolume);
        backgroundMusic.play();
    }

    public static void backgroundMute ()
    {
        backgroundMusic.setVolume((overallMusicVolume > 0f) ? BACKGROUND_MUTED : 0f);
    }

    static float currentVolume = 0f;
    public static void backgroundUnMute ()
    {
        if (backgroundMusic.getVolume() == overallMusicVolume)
            return;

        final float interval = 0.05f;
        final int times = (int) Math.ceil((double) (BACKGROUND_UNMUTING_TIME / interval));
        final float deltaVolume = (overallMusicVolume - BACKGROUND_MUTED) / times;

        currentVolume = BACKGROUND_MUTED;

        Timer.schedule(new Timer.Task() {
            @Override
            public void run()
            {
                currentVolume += deltaVolume;
                currentVolume = (currentVolume > overallMusicVolume) ? overallMusicVolume : currentVolume;

                backgroundMusic.setVolume(currentVolume);
            }
        }, 0f, interval, times);
    }

    // Глобальная пауза
    public static void pause ()
    {
        jumpLeft.pause();
        jumpRight.pause();
        pickup.pause();
        backgroundMusic.pause();
    }

    // Глобальный резюме
    public static void resume ()
    {
        backgroundMusic.play();
    }
}
