package com.vladislavfaust.jumpinsweetis.helpers;

/**
 * Created by Faust on 24.01.2015.
 *
 */
public class HighscoreHandler
{
    // Текущий highscore
    private static int highscore;

    // Хранит "является ли предыдущий результат хайскором"
    private static boolean newHighscore = false;

    // Публичный статический конструктор
    public static void construct ()
    {
        highscore = MyPreferences.getHighscore();
    }

    // Вызывается каждую новую игру
    public static void init ()
    {
        newHighscore = false;
    }

    // Пытаемся поставить новый рекорд
    public static void tryToSetNewHighscore (int score)
    {
        if (score > highscore)
        {
            highscore = score;

            newHighscore = true;

            // Переписываем предыдущее значение
            MyPreferences.putHighscore(highscore);
        }
    }

    public static boolean isNewHighscore()
    {
        return newHighscore;
    }

    public static int getHighscore ()
    {
        return highscore;
    }
}
