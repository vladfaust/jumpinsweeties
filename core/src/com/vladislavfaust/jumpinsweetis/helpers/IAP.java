package com.vladislavfaust.jumpinsweetis.helpers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.pay.Offer;
import com.badlogic.gdx.pay.OfferType;
import com.badlogic.gdx.pay.PurchaseManagerConfig;
import com.badlogic.gdx.pay.PurchaseObserver;
import com.badlogic.gdx.pay.PurchaseSystem;
import com.badlogic.gdx.pay.Transaction;
import com.badlogic.gdx.utils.Array;

import java.util.Locale;

/**
 * Created by Faust on 11.05.2015.
 *
 */
public class IAP
{
    /*
        ������ ���� �������
     */

    public enum PURCHASE_TYPE
    {
        TOKENS_FEW, TOKENS_SOME, TOKENS_MANY, TOKENS_LOTSOF, UNLOCK_GIFTS, UNLOCK_UNIQUE_SKIN
    }

    public static int isDiscount (PURCHASE_TYPE which)
    {
        switch (which)
        {
            case TOKENS_MANY:
                return 20;
            case TOKENS_LOTSOF:
                return 25;
            default:
                return 0;
        }
    }

    /*
        ������ ���������
     */

    private final static String GOOGLE_API_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAg8MX3TzlFwlvELesaa4+yjYxqm4VKVY3rbeixQ6FhVZAps1YtvqNFGAhMOuoyTN+S4qL4mTJo9XWXg5eaHryQTPp1XF5lL/y6nzwSDE5AkuVypPzTNrGHGGfbJJqnUROiHSZFGmCc7kZc02s4OVjmMNUsqFZSMHma2vL4t3QoeRlVp2JBN0W7B6wmV0n2T5vY5/7nHVuaywr/RlPqNfRufxTsaoI5ixrO2ebdUKpRgoS5HzwhPg3XrCF7cswHnAk/9A3hEJZ4C9hu5ZNw5cMEeNik+1fkFww92znk25ik31CyOpkN9q+7PFLLWAfjgaTW9b6cKYwbkcDV2N0PKJ81wIDAQAB";

    /*
        ����������
     */

    private final static Array<Purchase> PURCHASES = new Array<>();
    private static IStandardCallback awaitingCallback;

    /*
        �������������
     */

    public static void init ()
    {
        PURCHASES.clear();

        PURCHASES.add(new Purchase(PURCHASE_TYPE.TOKENS_FEW, "tokens_few", 49f, 0.99f));
        PURCHASES.add(new Purchase(PURCHASE_TYPE.TOKENS_SOME, "tokens_some", 99f, 1.99f));
        PURCHASES.add(new Purchase(PURCHASE_TYPE.TOKENS_MANY, "tokens_many", 149f, 2.99f));
        PURCHASES.add(new Purchase(PURCHASE_TYPE.TOKENS_LOTSOF, "tokens_lotsof", 199f, 3.99f));
        PURCHASES.add(new Purchase(PURCHASE_TYPE.UNLOCK_GIFTS, "unlock_gifts", 99f, 1.99f));
        PURCHASES.add(new Purchase(PURCHASE_TYPE.UNLOCK_UNIQUE_SKIN, "unlock_unique_skin", 99f, 1.99f));

        if (PurchaseSystem.hasManager())
        {
            PurchaseManagerConfig config = new PurchaseManagerConfig();

            config.addStoreParam(PurchaseManagerConfig.STORE_NAME_ANDROID_GOOGLE, GOOGLE_API_KEY);

            for (int i = 0; i < PURCHASES.size; i++)
                config.addOffer(new Offer().setIdentifier(PURCHASES.get(i).getIdentifier()).setType(OfferType.CONSUMABLE).putIdentifierForStore(PurchaseManagerConfig.STORE_NAME_ANDROID_GOOGLE, PURCHASES.get(i).getIdentifier()));

            PurchaseSystem.install(new PurchaseObserver()
            {
                @Override
                public void handleInstall()
                {
                    Gdx.app.log("PurchaseObserver", "handleInstall");
                }

                @Override
                public void handleInstallError(Throwable e)
                {
                    Gdx.app.error("PurchaseObserver", "handleInstallError: " + e.getLocalizedMessage());
                }

                @Override
                public void handleRestore(Transaction[] transactions)
                {
                    Gdx.app.log("PurchaseObserver", "handleRestore");
                }

                @Override
                public void handleRestoreError(Throwable e)
                {
                    Gdx.app.error("PurchaseObserver", "handleRestoreError: " + e.getLocalizedMessage());
                }

                @Override
                public void handlePurchase(Transaction transaction)
                {
                    Gdx.app.log("PurchaseObserver", "handlePurchase");
                    awaitingCallback.success();
                }

                @Override
                public void handlePurchaseError(Throwable e)
                {
                    Gdx.app.error("PurchaseObserver", "handlePurchaseError: " + e.getLocalizedMessage());
                    awaitingCallback.error(e.getLocalizedMessage());
                }

                @Override
                public void handlePurchaseCanceled()
                {
                    Gdx.app.log("PurchaseObserver", "handlePurchaseCanceled");
                    awaitingCallback.cancelled();
                }
            }, config);
        }
    }

    /*
        ��������� ������
     */

    public static Purchase findByType (PURCHASE_TYPE type)
    {
        for (int i = 0; i < PURCHASES.size; i++)
            if (PURCHASES.get(i).getType() == type)
                return PURCHASES.get(i);

        throw new NullPointerException("Purchase type '" + type.toString() + "' not found!");
    }

    public static void purchase (IStandardCallback callback, PURCHASE_TYPE which)
    {
        /*if (JumpinSweeties.DEBUG) {
            callback.success();
            return;
        }*/

        if (PurchaseSystem.installed())
        {
            awaitingCallback = callback;
            PurchaseSystem.purchase(findByType(which).getIdentifier());
        }
        else
            callback.error(Language.LOCALE.get("error"));
    }

    public static String getPriceLocalized(PURCHASE_TYPE which)
    {
        String locale = Locale.getDefault().toString();

        if (locale.equals("ru_RU") || locale.equals("uk") || locale.equals("be"))
            return ((int) findByType(which).getPriceRub()) + "\u20BD";
        else
            return "$" + findByType(which).getPriceUsd();
    }

    /*
        ����� �������
     */

    public static class Purchase
    {
        private PURCHASE_TYPE type;
        private String identifier;
        private float priceRub, priceUsd;

        public Purchase(PURCHASE_TYPE type, String identifier, float priceRub, float priceUsd)
        {
            this.type = type;
            this.identifier = identifier;
            this.priceRub = priceRub;
            this.priceUsd = priceUsd;
        }

        public PURCHASE_TYPE getType()
        {
            return type;
        }

        public String getIdentifier()
        {
            return identifier;
        }

        public float getPriceRub()
        {
            return priceRub;
        }

        public float getPriceUsd()
        {
            return priceUsd;
        }

        public int getPriceUsdInt ()
        {
            return (int) (priceUsd * 100);
        }
    }
}
