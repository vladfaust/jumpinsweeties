package com.vladislavfaust.jumpinsweetis.helpers;

import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.ParticleEmitter;
import com.badlogic.gdx.utils.Array;

/**
 * Created by Faust on 23.01.2015.
 *
 */
public class CustomParticleMethods
{
    public static void loadBonus (ParticleEffect effect, int index)
    {
        Array<ParticleEmitter> emitters = effect.getEmitters();

        for (int i = 0, n = emitters.size; i < n; i++)
            if (index < 9)
                emitters.get(i).setSprite(AssetLoader.getCandie(index));
            else
                emitters.get(i).setSprite(AssetLoader.getDoublerCandiesValues().get(index - 9));
    }
}