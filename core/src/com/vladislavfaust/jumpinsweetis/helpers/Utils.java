package com.vladislavfaust.jumpinsweetis.helpers;

/**
 * Created by Faust on 17.03.2015.
 *
 */
public class Utils
{
    // Работа со временем
    public static int getHours (double time)
    {
        return (int)Math.floor(time/3600f);
    }

    public static int getMinutes (double time)
    {
        return (int)Math.floor((time - getHours(time) * 3600f)/60f);
    }

    public static int getSeconds (double time)
    {
        return (int)Math.floor(time - getHours(time) * 3600f - getMinutes(time) * 60f);
    }
    
    public static String getHoursString (double time)
    {
        if (getHours(time) < 10)
            return "0" + getHours(time);
        else 
            return "" + getHours(time);
    }

    public static String getMinutesString (double time)
    {
        if (getMinutes(time) < 10)
            return "0" + getMinutes(time);
        else
            return "" + getMinutes(time);
    }

    public static String getSecondsString (double time)
    {
        if (getSeconds(time) < 10)
            return "0" + getSeconds(time);
        else
            return "" + getSeconds(time);
    }
}
