package com.vladislavfaust.jumpinsweetis.screens;

import com.badlogic.gdx.Screen;
import com.vladislavfaust.jumpinsweetis.JumpinSweeties;
import com.vladislavfaust.jumpinsweetis.helpers.analytics.AnalyticsHelper;
import com.vladislavfaust.jumpinsweetis.ui.Background;
import com.vladislavfaust.jumpinsweetis.ui.UIRenderer;
import com.vladislavfaust.jumpinsweetis.ui.elements.UIBuyTokens;
import com.vladislavfaust.jumpinsweetis.ui.elements.UIShareAchievement;

/**
 * Created by Faust on 09.05.2015.
 *
 */
public class BoostsScreen implements Screen
{
    /*
        ��������
     */

    private static BoostsScreen _instance;
    public static BoostsScreen instance ()
    {
        return _instance;
    }
    public BoostsScreen ()
    {
        _instance = this;
    }

    /*
        ����������� ������
     */

    @Override
    public void show()
    {
        //TODO Analytics
        JumpinSweeties.instance().setAppState(JumpinSweeties.AppState.BOOSTS);
        UIRenderer.instance().setScreen(JumpinSweeties.Screens.BOOSTS);
    }

    @Override
    public void render(float delta)
    {
        AnalyticsHelper.logFPS();
        
        // ������� ���������
        Background.instance().update(0.5f);
        Background.instance().render();

        // ����� UI
        UIRenderer.instance().render(delta, false);
    }

    /*
        ���������� ����������
     */

    @Override
    public void resize(int width, int height)
    {

    }

    @Override
    public void pause()
    {

    }

    @Override
    public void resume()
    {

    }

    @Override
    public void hide()
    {

    }

    @Override
    public void dispose()
    {

    }

    /*
        ������� ������ "�����"
     */

    public void onBackButtonPressed ()
    {
        if (!UIRenderer.isBackButtonBlocked())
            JumpinSweeties.instance().gotoMainMenu();
        else if (UIShareAchievement.isShown())
            UIShareAchievement.tryToHide();
        else if (UIBuyTokens.isShown())
            UIBuyTokens.hide();
    }
}
