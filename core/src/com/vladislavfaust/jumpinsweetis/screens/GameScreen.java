package com.vladislavfaust.jumpinsweetis.screens;

import com.badlogic.gdx.Screen;
import com.vladislavfaust.jumpinsweetis.JumpinSweeties;
import com.vladislavfaust.jumpinsweetis.gameobjects.Jumper;
import com.vladislavfaust.jumpinsweetis.gamesystems.GameInput;
import com.vladislavfaust.jumpinsweetis.gamesystems.GameRenderer;
import com.vladislavfaust.jumpinsweetis.gamesystems.GameWorld;
import com.vladislavfaust.jumpinsweetis.helpers.SoundsHandler;
import com.vladislavfaust.jumpinsweetis.helpers.analytics.AnalyticsHelper;
import com.vladislavfaust.jumpinsweetis.ui.Background;
import com.vladislavfaust.jumpinsweetis.ui.UIHandler;
import com.vladislavfaust.jumpinsweetis.ui.UIRenderer;
import com.vladislavfaust.jumpinsweetis.ui.elements.UIShareAchievement;

/**
 * Created by Faust on 19.01.2015.
 * Гарантируется синглтон.
 */
public class GameScreen implements Screen
{
    // Инстанс синглтона
    private static GameScreen _instance;

    // Геттер инстанса синглтона
    public static GameScreen instance ()
    {
        return _instance;
    }

    // Конструктор
    public GameScreen ()
    {
        _instance = this;

        // Создаем игровые объекты
        new GameWorld();
        new GameRenderer();
        new GameInput();
    }
    
    @Override
    public void show()
    {
        // Включаем реакцию на тач
        JumpinSweeties.instance().addInputProcessor(GameInput.instance());

        // Инициализируем игровой мир (смотрим, был ли предыдущий экран - меню)
        if (JumpinSweeties.instance().getAppState() == JumpinSweeties.AppState.MAIN_MENU)
        {
            UIRenderer.instance().tintMenuToGame();

            GameWorld.instance().init(UIHandler.getJumperPosition().x, UIHandler.getJumperPosition().y);
            Jumper.instance().click(UIHandler.isJumperRight());

            GameWorld.instance().setGameState(GameWorld.GameState.ACTIVE);
        }
        else
        {
            UIRenderer.instance().setScreen(JumpinSweeties.Screens.GAME);
            GameWorld.instance().init();
        }
        JumpinSweeties.instance().setAppState(JumpinSweeties.AppState.GAME);

        // И рендерер
        GameRenderer.instance().init();

        // Анмутим музон
        SoundsHandler.backgroundUnMute();

        // Трекаем начало игры
        AnalyticsHelper.trackGameStart();

        System.gc();
    }

    @Override
    public void render(float delta)
    {
        AnalyticsHelper.logFPS();

        // Игровая логика
        GameWorld.instance().update(delta);

        // Рендеринг
        Background.instance().render();
        GameRenderer.instance().render(delta, false);
        UIRenderer.instance().render(delta, false);

        // Логика бэкграунда
        Background.instance().update(GameRenderer.instance().getPreviousYDelta());
    }

    public void onBackButtonPressed ()
    {
        if (!UIRenderer.isBackButtonBlocked())
            JumpinSweeties.instance().gotoMainMenu();
        else if (UIShareAchievement.isShown())
            UIShareAchievement.tryToHide();
    }

    @Override
    public void resize(int width, int height)
    {

    }

    @Override
    public void pause()
    {

    }

    @Override
    public void resume()
    {

    }

    @Override
    public void hide()
    {
        // Выключаем реакцию на тач
        JumpinSweeties.instance().removeInputProcessor(GameInput.instance());
    }

    @Override
    public void dispose()
    {
        GameRenderer.instance().dispose();
    }
}
