package com.vladislavfaust.jumpinsweetis.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.vladislavfaust.jumpinsweetis.JumpinSweeties;
import com.vladislavfaust.jumpinsweetis.helpers.AssetLoader;
import com.vladislavfaust.jumpinsweetis.helpers.SoundsHandler;
import com.vladislavfaust.jumpinsweetis.ui.UIRenderer;

/**
 * Created by Faust on 21.01.2015.
 * Синглтон
 */
public class SplashScreen implements Screen, InputProcessor
{
    // Настроечки
    private final static float
            MINIMUM_SHOW = 0.1f,
            MAXIMUM_SHOW = 1f,
            FADE_TIME = 0.7f;

    // Анимация
    private float
            fadeTime = 0f;
    private boolean fading = false;

    // Батч
    private SpriteBatch batch;
    private Sprite splash;

    // Закончилась ли загрузка ресурсов
    private boolean fontsLoaded = false;
    private boolean fontsAreLoading = false;

    // Инстанс синглтона
    private static SplashScreen _instance;

    // Геттер инстанса синглтона
    public static SplashScreen instance ()
    {
        return _instance;
    }

    // Конструктор
    public SplashScreen ()
    {
        _instance = this;

        // Настраиваем камеру
        OrthographicCamera camera = new OrthographicCamera();
        camera.setToOrtho(false, JumpinSweeties.instance().getGameWidth(), JumpinSweeties.instance().getGameHeight());

        // Создаем батч
        batch = new SpriteBatch();
        batch.setProjectionMatrix(camera.combined);
    }
    
    @Override
    public void show()
    {
        //JumpinSweeties.instance().addInputProcessor(this);
    }

    private void drawThisShit (float alpha)
    {
        // Заливаем цветом
        Gdx.gl.glClearColor(1f, 1f, 1f, 1f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        // Отрисовываем сплеш
        batch.begin();
        splash.draw(batch, alpha);
        batch.end();
    }

    @Override
    public void render(float delta)
    {
        // Создаем спрайт сплеша
        if (splash == null)
        {
            splash = new Sprite(AssetLoader.getSplashTexture());
            splash.setSize(AssetLoader.getSplashTexture().getWidth(), AssetLoader.getSplashTexture().getHeight());
            splash.setOriginCenter();
            splash.setPosition(JumpinSweeties.instance().getGameWidth()/2f - splash.getWidth()/2f, JumpinSweeties.instance().getGameHeight()/2f - splash.getHeight()/2f);

            drawThisShit(1f);
            return;
        }

        if (!fontsLoaded)
        {
            if (!fontsAreLoading)
            {
                fontsAreLoading = true;
                UIRenderer.instance().generateFonts();
                fontsLoaded = true;
            }

            return;
        }

        // Настало время фейда?
        if (!fading && fontsLoaded)
        {
            SoundsHandler.backgroundStart();
            fading = true;
        }

        // Го фейд, битч
        if (fading)
        {
            // Защита от огромной дельты
            if (delta > FADE_TIME)
                return;

            fadeTime += delta;
        }

        // Если фейд все, то...
        if (fadeTime >= FADE_TIME)
        {
            // Заходим в GPGS
            if (!JumpinSweeties.instance().getGooglePlayServices().getSignedInGPGS())
                JumpinSweeties.instance().getGooglePlayServices().loginGPGS();

            JumpinSweeties.instance().gotoMainMenu();
            return;
        }

        drawThisShit(1f - fadeTime/FADE_TIME);
    }

    @Override
    public void resize(int width, int height)
    {

    }

    @Override
    public void pause()
    {

    }

    @Override
    public void resume()
    {

    }

    @Override
    public void hide()
    {
        JumpinSweeties.instance().removeInputProcessor(this);
    }

    @Override
    public void dispose()
    {
        batch.dispose();
    }

    // Методы тача
    @Override
    public boolean keyDown(int keycode)
    {
        return false;
    }

    @Override
    public boolean keyUp(int keycode)
    {
        return false;
    }

    @Override
    public boolean keyTyped(char character)
    {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button)
    {
        /*if (showTime < MINIMUM_SHOW)
            return false;

        fading = true;

        SoundsHandler.backgroundStart();*/

        return true;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button)
    {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer)
    {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY)
    {
        return false;
    }

    @Override
    public boolean scrolled(int amount)
    {
        return false;
    }
}
