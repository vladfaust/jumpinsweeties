package com.vladislavfaust.jumpinsweetis.screens;

import com.badlogic.gdx.Screen;
import com.vladislavfaust.jumpinsweetis.JumpinSweeties;
import com.vladislavfaust.jumpinsweetis.gamesystems.MissionController;
import com.vladislavfaust.jumpinsweetis.gamesystems.ScoreHandler;
import com.vladislavfaust.jumpinsweetis.helpers.CandiesHandler;
import com.vladislavfaust.jumpinsweetis.helpers.JumperSkinsHandler;
import com.vladislavfaust.jumpinsweetis.helpers.analytics.AnalyticsHelper;
import com.vladislavfaust.jumpinsweetis.helpers.googleplay.AchievementsHandler;
import com.vladislavfaust.jumpinsweetis.ui.Background;
import com.vladislavfaust.jumpinsweetis.ui.UIRenderer;
import com.vladislavfaust.jumpinsweetis.ui.elements.UIGifting;
import com.vladislavfaust.jumpinsweetis.ui.elements.UIShareAchievement;
import com.vladislavfaust.jumpinsweetis.ui.elements.UISuggestionMenu;

/**
 * Created by Faust on 19.01.2015.
 * Гарантируется синглтон.
 */
public class GameOverScreen implements Screen
{
    // Инстанс синглтона
    private static GameOverScreen _instance;

    // Геттер инстанса синглтона
    public static GameOverScreen instance ()
    {
        return _instance;
    }

    // Конструктор
    public GameOverScreen ()
    {
        _instance = this;
    }

    @Override
    public void show()
    {
        // Добавляем конфетки
        CandiesHandler.addCandiesInTheEndOfGame();

        // Задания
        MissionController.playGame(CandiesHandler.getLastCandies(), ScoreHandler.getScore(), ScoreHandler.getStars());

        JumpinSweeties.instance().setAppState(JumpinSweeties.AppState.GAME_OVER);
        UIRenderer.instance().setScreen(JumpinSweeties.Screens.GAME_OVER);

        // Тут значит ачивки можно
        if (ScoreHandler.getScore() >= 1)
            AchievementsHandler.unlockReadyToRock();

        if (ScoreHandler.getStars() == 3)
            AchievementsHandler.unlockBecauseICan();

        if (ScoreHandler.getScore() == 33)
            AchievementsHandler.unlock33();

        AchievementsHandler.tryToUnlockIronWill(ScoreHandler.getStars());
        AchievementsHandler.tryToUnlockPathAchievements(ScoreHandler.getStars());

        AchievementsHandler.addStarCommander(ScoreHandler.getStars());
        AchievementsHandler.addTimeFliesBy();

        // Трекаем конец игры
        AnalyticsHelper.trackGameOver(JumperSkinsHandler.getJumperTextureName(JumperSkinsHandler.getActiveSkinID()), ScoreHandler.getScore(), CandiesHandler.getLastCandies());
    }

    @Override
    public void render(float delta)
    {
        AnalyticsHelper.logFPS();

        // Сначала бэкграунд
        Background.instance().update(0f);
        Background.instance().render();

        // Потом UI
        UIRenderer.instance().render(delta, false);
    }

    public void onBackButtonPressed ()
    {
        if (!JumpinSweeties.instance().getChartBoost().onBackPressed()) {
            if (!UIRenderer.isBackButtonBlocked())
                JumpinSweeties.instance().gotoMainMenu();
            else if (UIShareAchievement.isShown())
                UIShareAchievement.tryToHide();
            else if (UIGifting.isShown())
            {
                if (UIGifting.isReadyToBeClosed())
                    UIGifting.tryToHide();
            }
            else if (UISuggestionMenu.isSuggestionMenuShown)
                UISuggestionMenu.hide(false);
        }
    }

    @Override
    public void resize(int width, int height)
    {

    }

    @Override
    public void pause()
    {

    }

    @Override
    public void resume()
    {

    }

    @Override
    public void hide()
    {

    }

    @Override
    public void dispose()
    {

    }
}
