package com.vladislavfaust.jumpinsweetis.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.vladislavfaust.jumpinsweetis.JumpinSweeties;
import com.vladislavfaust.jumpinsweetis.helpers.Language;
import com.vladislavfaust.jumpinsweetis.helpers.SoundsHandler;
import com.vladislavfaust.jumpinsweetis.helpers.analytics.AnalyticsHelper;
import com.vladislavfaust.jumpinsweetis.helpers.dialogs.IConfirmDialog;
import com.vladislavfaust.jumpinsweetis.helpers.googleplay.AchievementsHandler;
import com.vladislavfaust.jumpinsweetis.ui.Background;
import com.vladislavfaust.jumpinsweetis.ui.UIRenderer;
import com.vladislavfaust.jumpinsweetis.ui.elements.UISettingsMenu;
import com.vladislavfaust.jumpinsweetis.ui.elements.UIShareAchievement;

/**
 * Created by Faust on 19.01.2015.
 * Гарантируется синглтон.
 */
public class MainMenuScreen implements Screen
{
    // Инстанс синглтона
    private static MainMenuScreen _instance;

    // Геттер инстанса синглтона
    public static MainMenuScreen instance ()
    {
        return _instance;
    }

    // Конструктор
    public MainMenuScreen ()
    {
        _instance = this;
    }
    
    @Override
    public void show()
    {
        JumpinSweeties.instance().setAppState(JumpinSweeties.AppState.MAIN_MENU);
        UIRenderer.instance().setScreen(JumpinSweeties.Screens.MAIN_MENU);

        // Анмутим музыку на всякий случай
        SoundsHandler.backgroundUnMute();

        AnalyticsHelper.trackMainMenuScreen();

        AchievementsHandler.checkShouldBeUnlockedSkins();
    }

    @Override
    public void render(float delta)
    {
        AnalyticsHelper.logFPS();

        // Сначала бэкграунд
        Background.instance().update(0.5f);
        Background.instance().render();

        // Потом UI
        UIRenderer.instance().render(delta, false);
    }

    public void onBackButtonPressed ()
    {
        if (UIShareAchievement.isShown())
            UIShareAchievement.tryToHide();
        else if (UISettingsMenu.settingsShown)
            UISettingsMenu.hide();
        else
            JumpinSweeties.instance().getRequestHandler().confirm(new IConfirmDialog() {
                @Override
                public void clickedOK()
                {
                    Gdx.app.exit();
                }

                @Override
                public void clickedCancel()
                {
                    return;
                }
            }, Language.LOCALE.get("confirm"), Language.LOCALE.get("exit"), Language.LOCALE.get("yes"), Language.LOCALE.get("cancel"));
    }

    @Override
    public void resize(int width, int height)
    {

    }

    @Override
    public void pause()
    {

    }

    @Override
    public void resume()
    {

    }

    @Override
    public void hide()
    {

    }

    @Override
    public void dispose()
    {

    }
}
