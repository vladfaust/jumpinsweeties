package com.vladislavfaust.jumpinsweetis.screens;

import com.badlogic.gdx.Screen;
import com.vladislavfaust.jumpinsweetis.JumpinSweeties;
import com.vladislavfaust.jumpinsweetis.helpers.analytics.AnalyticsHelper;
import com.vladislavfaust.jumpinsweetis.ui.Background;
import com.vladislavfaust.jumpinsweetis.ui.UIRenderer;
import com.vladislavfaust.jumpinsweetis.ui.elements.UIShareAchievement;

/**
 * Created by Faust on 19.01.2015.
 * Гарантируется синглтон.
 */
public class SkinsScreen implements Screen
{
    // Инстанс синглтона
    private static SkinsScreen _instance;

    // Геттер инстанса синглтона
    public static SkinsScreen instance ()
    {
        return _instance;
    }

    // Конструктор
    public SkinsScreen ()
    {
        _instance = this;
    }
    
    @Override
    public void show()
    {
        JumpinSweeties.instance().setAppState(JumpinSweeties.AppState.SKINS);
        UIRenderer.instance().setScreen(JumpinSweeties.Screens.SKINS);
    }

    @Override
    public void render(float delta)
    {
        AnalyticsHelper.logFPS();

        // Сначала бэкграунд
        Background.instance().update(0.5f);
        Background.instance().render();

        // Потом UI
        UIRenderer.instance().render(delta, false);
    }

    public void onBackButtonPressed ()
    {
        if (!UIRenderer.isBackButtonBlocked())
            JumpinSweeties.instance().gotoMainMenu();
        else if (UIShareAchievement.isShown())
            UIShareAchievement.tryToHide();
    }

    @Override
    public void resize(int width, int height)
    {

    }

    @Override
    public void pause()
    {

    }

    @Override
    public void resume()
    {

    }

    @Override
    public void hide()
    {

    }

    @Override
    public void dispose()
    {

    }
}
