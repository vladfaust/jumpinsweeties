package com.vladislavfaust.jumpinsweetis;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.vladislavfaust.jumpinsweetis.gameobjects.bonuses.BonusLevelingController;
import com.vladislavfaust.jumpinsweetis.gamesystems.MissionController;
import com.vladislavfaust.jumpinsweetis.helpers.AssetLoader;
import com.vladislavfaust.jumpinsweetis.helpers.BackgroundHelper;
import com.vladislavfaust.jumpinsweetis.helpers.HighscoreHandler;
import com.vladislavfaust.jumpinsweetis.helpers.IAP;
import com.vladislavfaust.jumpinsweetis.helpers.IPlatformUtils;
import com.vladislavfaust.jumpinsweetis.helpers.JumperSkinsHandler;
import com.vladislavfaust.jumpinsweetis.helpers.Language;
import com.vladislavfaust.jumpinsweetis.helpers.MyPreferences;
import com.vladislavfaust.jumpinsweetis.helpers.MySecurity;
import com.vladislavfaust.jumpinsweetis.helpers.PromoCodesHelper;
import com.vladislavfaust.jumpinsweetis.helpers.ScreenShaker;
import com.vladislavfaust.jumpinsweetis.helpers.SoundsHandler;
import com.vladislavfaust.jumpinsweetis.helpers.advertisment.IAdColony;
import com.vladislavfaust.jumpinsweetis.helpers.advertisment.IChartBoost;
import com.vladislavfaust.jumpinsweetis.helpers.advertisment.IPollFish;
import com.vladislavfaust.jumpinsweetis.helpers.advertisment.IVungle;
import com.vladislavfaust.jumpinsweetis.helpers.analytics.IFlurry;
import com.vladislavfaust.jumpinsweetis.helpers.analytics.IGA;
import com.vladislavfaust.jumpinsweetis.helpers.dialogs.IRequestHandler;
import com.vladislavfaust.jumpinsweetis.helpers.gifts.GiftingProcessHelper;
import com.vladislavfaust.jumpinsweetis.helpers.gifts.GiftsCooldownHelper;
import com.vladislavfaust.jumpinsweetis.helpers.googleplay.AchievementsHandler;
import com.vladislavfaust.jumpinsweetis.helpers.googleplay.IGPGSActions;
import com.vladislavfaust.jumpinsweetis.screens.BoostsScreen;
import com.vladislavfaust.jumpinsweetis.screens.GameOverScreen;
import com.vladislavfaust.jumpinsweetis.screens.GameScreen;
import com.vladislavfaust.jumpinsweetis.screens.MainMenuScreen;
import com.vladislavfaust.jumpinsweetis.screens.SkinsScreen;
import com.vladislavfaust.jumpinsweetis.screens.SplashScreen;
import com.vladislavfaust.jumpinsweetis.ui.Background;
import com.vladislavfaust.jumpinsweetis.ui.UIHandler;
import com.vladislavfaust.jumpinsweetis.ui.UIRenderer;
import com.vladislavfaust.jumpinsweetis.ui.elements.UIGifting;

import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;

import javax.crypto.NoSuchPaddingException;

public class JumpinSweeties extends Game implements InputProcessor
{
    /*
        Настройки
     */

    public static final boolean DEBUG = false;

    private int gameWidth = 768;
    private int gameHeight = 0;

    private AppState appState;

    public enum AppState
    {
        LOADING, MAIN_MENU, GAME, GAME_OVER, SKINS, BOOSTS
    }

    public enum Screens
    {
        LOADING, MAIN_MENU, GAME, GAME_OVER, SKINS, BOOSTS
    }

    /*
        Синглтон
     */

    private static JumpinSweeties _instance;

    public static JumpinSweeties instance ()
    {
        return _instance;
    }

    /*
        Инициализация
     */

    private IChartBoost chartBoost;
    private IRequestHandler requestHandler;
    private IAdColony adColony;
    private IPlatformUtils platformUtils;
    private IGPGSActions googlePlayServices;
    private IFlurry flurry;
    private IGA gameAnalytics;

    public JumpinSweeties(IChartBoost chartBoost, IRequestHandler requestHandler, IAdColony adColony, IPlatformUtils platformUtils, IGPGSActions googlePlayServices, IVungle vungle, IPollFish pollFish, IFlurry flurry, IGA gameAnalytics)
    {
        // Интерфейсы
        this.chartBoost = chartBoost;
        this.requestHandler = requestHandler;
        this.adColony = adColony;
        this.platformUtils = platformUtils;
        this.googlePlayServices = googlePlayServices;
        this.flurry = flurry;
        this.gameAnalytics = gameAnalytics;
    }

	@Override
	public void create ()
    {
        _instance = this;

        // Устанавливаем начальные значения
        appState = AppState.LOADING;

        // Рассчитываем высоту игры
        gameHeight = (int) (Gdx.graphics.getHeight() * gameWidth / Gdx.graphics.getWidth());

        // Делаем инпут
        inputMultiplexer = new InputMultiplexer();
        Gdx.input.setInputProcessor(inputMultiplexer);
        Gdx.input.setCatchBackKey(true);
        inputMultiplexer.addProcessor(this);

        // Показываем сплеш
        AssetLoader.splashInit();
        new SplashScreen();
        setScreen(SplashScreen.instance());

        // Грузим все ассеты
        AssetLoader.init();

        try {
            MySecurity.init();
        } catch (NoSuchPaddingException | NoSuchAlgorithmException | NoSuchProviderException e) {
            e.printStackTrace();
        }

        // Преференсцес
        MyPreferences.init();

        try {
            MySecurity.initGameProtector();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        SoundsHandler.init();

        // Инициализируем скины
        JumperSkinsHandler.init();

        // Создаем экземпляры различных штук
        new UIRenderer();
        new UIHandler();
        new Background();
        BackgroundHelper.init();
        ScreenShaker.construct();
        HighscoreHandler.construct();
        PromoCodesHelper.init();
        MissionController.init();
        GiftsCooldownHelper.init();
        GiftingProcessHelper.construct();
        BonusLevelingController.init();
        AchievementsHandler.init();
        IAP.init();

        // Создаем экземпляры экранов
        new MainMenuScreen();
        new GameScreen();
        new GameOverScreen();
        new SkinsScreen();
        new BoostsScreen();

        // Инициализация UI-элементов
        UIGifting.init();

        // Язык
        Language.init();
	}

    /*
        Смена экранов
     */

    public void gotoMainMenu ()
    {
        setScreen(MainMenuScreen.instance());
    }

    public void startGame ()
    {
        setScreen(GameScreen.instance());
    }

    public void gameOver ()
    {
        setScreen(GameOverScreen.instance());
    }

    public void gotoSkinsScreen()
    {
        setScreen(SkinsScreen.instance());
    }

    public void gotoBoostsScreen()
    {
        setScreen(BoostsScreen.instance());
    }

    /*
        Работа с инпутом
     */

    private InputMultiplexer inputMultiplexer;

    public void addInputProcessor (InputProcessor inputProcessor)
    {
        inputMultiplexer.addProcessor(inputProcessor);
    }

    public void removeInputProcessor (InputProcessor inputProcessor)
    {
        inputMultiplexer.removeProcessor(inputProcessor);
    }

    public void resetInputProcessor ()
    {
        Gdx.input.setInputProcessor(inputMultiplexer);
        Gdx.input.setCatchBackKey(true);
        inputMultiplexer.addProcessor(this);
    }

    @Override
    public boolean keyDown(int keycode)
    {
        if(keycode == Input.Keys.BACK || keycode == Input.Keys.BACKSPACE)
        {
            switch (appState)
            {
                case LOADING:
                    break;
                case MAIN_MENU:
                    MainMenuScreen.instance().onBackButtonPressed();
                    break;
                case GAME:
                    GameScreen.instance().onBackButtonPressed();
                    break;
                case GAME_OVER:
                    GameOverScreen.instance().onBackButtonPressed();
                    break;
                case SKINS:
                    SkinsScreen.instance().onBackButtonPressed();
                    break;
                case BOOSTS:
                    BoostsScreen.instance().onBackButtonPressed();
                    break;
            }
            return true;
        }
        return false;
    }

    @Override
    public boolean keyUp(int keycode)
    {
        return false;
    }

    @Override
    public boolean keyTyped(char character)
    {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button)
    {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button)
    {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer)
    {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY)
    {
        return false;
    }

    @Override
    public boolean scrolled(int amount)
    {
        return false;
    }

    /*
        Реализация интерфейсов
     */

    @Override
    public void pause ()
    {
        super.pause();
        SoundsHandler.pause();
    }

    @Override
    public void resume ()
    {
        super.resume();
        SoundsHandler.resume();
    }

    @Override
    public void dispose ()
    {
        super.dispose();
    }

    /*
        Геттеры
     */

    public int getGameWidth()
    {
        return gameWidth;
    }

    public int getGameHeight()
    {
        return gameHeight;
    }

    public AppState getAppState()
    {
        return appState;
    }

    public IChartBoost getChartBoost()
    {
        return chartBoost;
    }

    public IAdColony getAdColony()
    {
        return adColony;
    }

    public IRequestHandler getRequestHandler()
    {
        return requestHandler;
    }

    public IPlatformUtils getPlatformUtils()
    {
        return platformUtils;
    }

    public IGPGSActions getGooglePlayServices()
    {
        return googlePlayServices;
    }

    public IFlurry getFlurry()
    {
        return flurry;
    }

    public IGA getGameAnalytics()
    {
        return gameAnalytics;
    }
/*
        Сеттеры
     */

    public void setAppState(AppState appState)
    {
        this.appState = appState;
    }
}
