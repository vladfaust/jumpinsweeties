package com.vladislavfaust.jumpinsweetis.gameobjects;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Vector2;
import com.vladislavfaust.jumpinsweetis.gameobjects.bonuses.Rocket;
import com.vladislavfaust.jumpinsweetis.helpers.JumperSkinsHandler;
import com.vladislavfaust.jumpinsweetis.helpers.SoundsHandler;

/**
 * Created by Faust on 20.01.2015.
 * Синглтон!
 */
public class Jumper extends Sprite
{
    // Константы
    // Размеры
    public final static float
            COLLISION_RADIUS = 140f,
            SPRITE_SIZE_X = COLLISION_RADIUS * 256f / 150f,
            SPRITE_SIZE_Y = SPRITE_SIZE_X;

    // Перемещение
    private final static float
            GRAVITY = -2954f,   // -2954f
            BURST_X = 250f,     // 248
            BURST_Y = 1525f,    // 1550
            BOUNCE = 1000f,
            MAX_FALL_SPEED = -20000f;

    // Поворот
    private final static float
            ROTATION_MAX = 60f,
            ROTATION_MIN = 0f,
            ROTATION_SPEED = 200f;

    // Вектора различные
    private Vector2 position, velocity, acceleration, temp;

    // Поворот
    private boolean rotates = true;
    private float rotation = 0f;
    private boolean right = true;

    // Максимальное достигнутое y
    private float maxYReached = 0f;

    // Коллижн сиркл
    private com.badlogic.gdx.math.Circle collisionCircle;

    // Инстанс синглтона
    private static Jumper _instance;

    // Геттер инстанса синглтона
    public static Jumper instance ()
    {
        return _instance;
    }

    // Конструктор
    public Jumper ()
    {
        _instance = this;

        // Создание различных штук
        position = new Vector2();
        velocity = new Vector2();
        acceleration = new Vector2(0f, GRAVITY);

        // Устанавливаем размер спрайта
        setSize(SPRITE_SIZE_X, SPRITE_SIZE_Y);

        // Устанавливаем origin прямо в центр
        setOrigin(SPRITE_SIZE_X /2f, SPRITE_SIZE_Y /2f);

        // Коллижн
        collisionCircle = new Circle(0f, 0f, COLLISION_RADIUS/2f);
    }

    // Инициализатор
    public void init (float x, float y)
    {
        // Сброс максимально достигнутого y
        maxYReached = 0f;

        // Сброс векторов
        position.set(x, y);
        velocity.set(0f, 0f);

        // Сброс позиции спрайта
        setPosition(position.x, position.y);

        // И коллижна
        collisionCircle.setPosition(position);

        // Сброс поворота спрайта (если он вообще поворачивается)
        rotates = (JumperSkinsHandler.isSelectedJumperRotates());
        if (rotates)
        {
            if (!right)
                flip(true, false);
            right = true;
            rotation = 0f;
            setRotation(rotation);
        }

        // Установка текстуры
        setRegion(JumperSkinsHandler.getSelectedJumperTexture());
    }

    // Апдейтер (смещает джампер)
    public void update (float delta)
    {
        // Воздействие гравитации на скорость
        velocity.add(acceleration.x * delta, acceleration.y * delta);

        if (Rocket.isActive())
            velocity.y = Rocket.getBurst();

        // Ограничить скорость
        velocity.y = (velocity.y < MAX_FALL_SPEED) ? MAX_FALL_SPEED : velocity.y;

        // Изменить вектор позиции
        position.add(velocity.x * delta, velocity.y * delta);

        // Подправить maxYReached
        if (position.y > maxYReached)
            maxYReached = position.y;

        // Поворот
        if (rotates) {
            // Поворачивать только тогда, когда он летит вниз
            if (velocity.y < 0f) {
                rotation += (right) ? -ROTATION_SPEED * delta : ROTATION_SPEED * delta;

                // Поворот нужно ограничить
                rotation = (rotation < -ROTATION_MAX) ? -ROTATION_MAX : rotation;
                rotation = (rotation > ROTATION_MAX) ? ROTATION_MAX : rotation;
            }
        }

        // Обновить позицию спрайта
        setPosition(position.x, position.y);

        // И поворот тоже
        setRotation(rotation);

        // И коллижн
        collisionCircle.setPosition(position.x + getOriginX(), position.y + getOriginY());
    }

    // По клику нужно делать бурст
    // Если right = true, то вправо, иначе влево
    public void click(boolean right)
    {
        // По y всегда постоянно
        velocity.y = (velocity.y < BURST_Y) ? BURST_Y : velocity.y;

        if (rotates) {
            // Флиппим, если нужно
            if (this.right != right) {
                flip(true, false);
                this.right = right;
            }
        }

        // По х зависит от направления
        if  (right) {
            velocity.x = BURST_X;

            if (rotates)
                rotation = ROTATION_MIN;

            // Звук
            SoundsHandler.jump(true);
        }
        else
        {
            velocity.x = -BURST_X;

            if (rotates)
                rotation = -ROTATION_MIN;

            // Звук
            SoundsHandler.jump(false);
        }
    }

    // Подпрыгивание при столкновении
    public void bounce ()
    {
        // Разворот на 180 градусов, нормализация до определенного значения
        temp = velocity.nor().rotate(180f).scl(BOUNCE);
        velocity.set(temp);
    }

    // Геттеры
    public float getMaxYReached()
    {
        return maxYReached;
    }

    public Circle getCollisionCircle()
    {
        return collisionCircle;
    }

    public Vector2 getPosition()
    {
        return position;
    }
}
