package com.vladislavfaust.jumpinsweetis.gameobjects;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.vladislavfaust.jumpinsweetis.gamesystems.PipesController;

/**
 * Created by Faust on 23.01.2015.
 *
 */
public class Pipes
{
    // Спрайты
    private Sprite leftPipe, rightPipe;

    // Скаляры, векторы
    private float pipeHeight, pipeWidth;
    private Vector2 position;

    // Фигурки
    private com.badlogic.gdx.math.Rectangle
            collisionRectLeft,
            collisionRectRight;

    // Переменная, отвечающая за то, прошел ли джампер через данную трубу
    private boolean passed;

    // Конструктор
    public Pipes (TextureRegion leftPipeRegion, TextureRegion rightPipeRegion)
    {
        this.leftPipe = new Sprite(leftPipeRegion);
        this.leftPipe.setSize(leftPipeRegion.getRegionWidth(), leftPipeRegion.getRegionHeight());

        this.rightPipe = new Sprite(rightPipeRegion);
        this.rightPipe.setSize(rightPipeRegion.getRegionWidth(), rightPipeRegion.getRegionHeight());

        this.position = new Vector2();

        this.pipeHeight = leftPipeRegion.getRegionHeight();
        this.pipeWidth = leftPipeRegion.getRegionWidth();

        this.collisionRectLeft = new Rectangle(0f, 0f, this.leftPipe.getWidth(), this.leftPipe.getHeight());
        this.collisionRectRight = new Rectangle(0f, 0f, this.rightPipe.getWidth(), this.rightPipe.getHeight());

        this.passed = false;
    }

    // Инициализатор (вызывается при каждом обновлении труб)
    // Принимает тот самый сдвиг по x
    public void init (float xPos, float yPos, float difficulty)
    {
        // Вектор
        position.set(xPos, yPos);

        // Позиции спрайтов
        leftPipe.setPosition(-xPos, yPos);
        rightPipe.setPosition(-xPos + pipeWidth + PipesController.HORIZONTAL_GAP / difficulty, yPos);

        // И коллижионов
        collisionRectLeft.setPosition(-xPos, yPos);
        collisionRectRight.setPosition(-xPos + pipeWidth + PipesController.HORIZONTAL_GAP / difficulty, yPos);

        // Сбросим
        passed = false;
    }

    // Геттеры
    public float getEdgeY ()
    {
        return position.y + pipeHeight;
    }

    public Sprite getLeftPipe()
    {
        return leftPipe;
    }

    public Sprite getRightPipe()
    {
        return rightPipe;
    }

    public Rectangle getCollisionRectLeft()
    {
        return collisionRectLeft;
    }

    public Rectangle getCollisionRectRight()
    {
        return collisionRectRight;
    }

    public float getY ()
    {
        return position.y;
    }

    public void passed ()
    {
        passed = true;
    }

    public boolean isPassed()
    {
        return passed;
    }
}
