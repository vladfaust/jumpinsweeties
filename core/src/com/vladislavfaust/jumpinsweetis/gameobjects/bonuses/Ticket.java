package com.vladislavfaust.jumpinsweetis.gameobjects.bonuses;

import com.vladislavfaust.jumpinsweetis.helpers.AssetLoader;
import com.vladislavfaust.jumpinsweetis.helpers.SoundsHandler;
import com.vladislavfaust.jumpinsweetis.helpers.gifts.GiftsCooldownHelper;

/**
 * Created by Faust on 27.04.2015.
 *
 */
public class Ticket extends Bonus
{
    public final static float PICK_RADIUS = 70f;

    public void init(float x, float y, float rotation)
    {
        super.init(AssetLoader.getCommon().findRegion("ticket"), x, y, rotation, PICK_RADIUS);
    }

    public void pickup ()
    {
        GiftsCooldownHelper.pickupTicket();
        SoundsHandler.pickupCandie();
    }
}
