package com.vladislavfaust.jumpinsweetis.gameobjects.bonuses;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.utils.Timer;
import com.vladislavfaust.jumpinsweetis.gameobjects.Jumper;
import com.vladislavfaust.jumpinsweetis.gamesystems.BonusesHandler;
import com.vladislavfaust.jumpinsweetis.gamesystems.MissionController;
import com.vladislavfaust.jumpinsweetis.helpers.AssetLoader;
import com.vladislavfaust.jumpinsweetis.helpers.SoundsHandler;

/**
 * Created by Faust on 08.05.2015.
 *
 */
public class Doubler extends Bonus
{
    /*
        ���������
     */

    private final static float
            SPAWN_CHANCE = 0.05f,
            MIN_DURATION = 3f,
            MAX_DURATION = 8f;

    public final static float PICK_RADIUS = 59f;

    /*
        ����������
     */

    private static boolean active = false;
    private static float timeLeft = 0f;
    private static Timer timer;
    private final static float TIMER_PERIOD = 0.05f;
    private static ParticleEffect effect;

    /*
        ������ ������������
     */

    // ���������� ������ ����
    public static void initialize()
    {
        active = false;
        timeLeft = 0f;

        if (effect != null)
        {
            effect.allowCompletion();
            effect.setDuration(0);
            effect = null;
        }
    }

    // ���������� ���� ���
    public static void construct ()
    {
        initialize();
    }

    // ���������� ��� ������
    public void init(float x, float y, float rotation)
    {
        super.init(AssetLoader.getCommon().findRegion("x2-bonus"), x, y, rotation, PICK_RADIUS);
    }

    /*
        ������, ���������� �������
     */

    public static float getSpawnChance ()
    {
        return SPAWN_CHANCE;
    }

    public static void activate ()
    {
        MissionController.boostActivated();
        SoundsHandler.boostPickup();

        timeLeft = getDuration();
        active = true;

        if (timer != null)
            timer.clear();

        timer = new Timer();
        timer.scheduleTask(new MyTask(), 0f, TIMER_PERIOD);
        timer.start();

        if (effect == null) {
            effect = new ParticleEffect();
            effect.load(Gdx.files.internal("effects/stars.p"), Gdx.files.internal("effects"));
            effect.setPosition(Jumper.instance().getX() + Jumper.instance().getWidth() / 2f, Jumper.instance().getY() + Jumper.instance().getHeight() / 2f);
            effect.scaleEffect(1.5f);
            effect.setDuration(50000);
            BonusesHandler.getActiveBoostsEffects().add(effect);
            effect.start();
        }
    }

    public static boolean isActive()
    {
        return active;
    }

    /*
        ����������
     */

    private static float getDuration ()
    {
        return MIN_DURATION + ((float) BonusLevelingController.getDoublerLevel()) * ((MAX_DURATION - MIN_DURATION) / ((float) BonusLevelingController.MAX_LEVEL));
    }

    /*
        ������ �������
     */

    private static class MyTask extends Timer.Task
    {
        @Override
        public void run()
        {
            if (effect != null)
                effect.setPosition(Jumper.instance().getX() + Jumper.instance().getWidth() / 2f, Jumper.instance().getY() + Jumper.instance().getHeight() / 2f);

            timeLeft -= TIMER_PERIOD;

            if (timeLeft <= 0f)
            {
                if (effect != null) {
                    effect.allowCompletion();
                    effect.setDuration(0);
                    effect = null;
                }

                active = false;

                timer.stop();
                timer.clear();
            }
        }
    }
}


