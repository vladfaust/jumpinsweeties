package com.vladislavfaust.jumpinsweetis.gameobjects.bonuses;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Timer;
import com.vladislavfaust.jumpinsweetis.gameobjects.Jumper;
import com.vladislavfaust.jumpinsweetis.gamesystems.BonusesHandler;
import com.vladislavfaust.jumpinsweetis.gamesystems.MissionController;
import com.vladislavfaust.jumpinsweetis.helpers.AssetLoader;
import com.vladislavfaust.jumpinsweetis.helpers.SoundsHandler;

/**
 * Created by Faust on 08.05.2015.
 *
 */
public class Magnet extends Bonus
{
    /*
        ���������
     */

    private final static float
            SPAWN_CHANCE = 0.05f,
            MIN_DURATION = 3f,
            MAX_DURATION = 8f,

            GRAVITATION_RADIUS = 800f,
            MIN_SPEED = 0f,
            MIN_MAX_SPEED = 800f,
            MAX_MAX_SPEED = 1600f;

    public final static float PICK_RADIUS = 64f;

    /*
        ����������
     */

    private static boolean active = false;
    private static float durationLeft = 0f;
    private static Timer timer;
    private final static float TIMER_PERIOD = 0.01f;
    private static ParticleEffect activeEffect = null, passiveEffect = null;

    /*
        �������������
     */

    public static void initialize()
    {
        SoundsHandler.deactivateMagnet();

        active = false;
        durationLeft = 0f;

        if (activeEffect != null)
        {
            activeEffect.allowCompletion();
            activeEffect.setDuration(0);
            activeEffect = null;
        }

        if (passiveEffect != null) {
            passiveEffect.allowCompletion();
            passiveEffect.setDuration(0);
            passiveEffect = null;
        }
    }

    public static void construct ()
    {
        initialize();
    }

    public void init(float x, float y, float rotation)
    {
        super.init(AssetLoader.getCommon().findRegion("magnet"), x, y, rotation, PICK_RADIUS);
    }

    /*
        �������� �������
     */

    public static float getChance ()
    {
        return SPAWN_CHANCE;
    }

    private static float getDuration ()
    {
        return MIN_DURATION + BonusLevelingController.getMagnetLevel() * ((MAX_DURATION - MIN_DURATION) / ((float) BonusLevelingController.MAX_LEVEL));
    }

    private static float getGravitationMaxSpeed ()
    {
        return MIN_MAX_SPEED + BonusLevelingController.getMagnetLevel() * ((MAX_MAX_SPEED - MIN_MAX_SPEED) / ((float) BonusLevelingController.MAX_LEVEL));
    }

    public static Vector2 getBonusVelocity(Bonus bonus)
    {
        if (!active)
            return null;

        float distance = (float) Math.sqrt(Math.pow(Jumper.instance().getX() - bonus.getPosition().x, 2) + Math.pow(Jumper.instance().getY() - bonus.getPosition().y, 2));

        if (distance > GRAVITATION_RADIUS)
            return null;

        float speed = MIN_SPEED + (GRAVITATION_RADIUS - distance) / GRAVITATION_RADIUS * (getGravitationMaxSpeed() - MIN_SPEED);

        Vector2 betweenVector = Jumper.instance().getPosition().cpy().sub(bonus.getPosition().cpy()).nor();

        return betweenVector.scl(speed);
    }

    public static void switchMagnet (boolean on)
    {
        if (on && activeEffect == null)
        {
            activeEffect = new ParticleEffect();
            activeEffect.load(Gdx.files.internal("effects/magnetActive.p"), Gdx.files.internal("effects"));
            activeEffect.setPosition(Jumper.instance().getX() + Jumper.instance().getWidth() / 2f, Jumper.instance().getY() + Jumper.instance().getHeight() / 2f);
            activeEffect.scaleEffect(2f);
            BonusesHandler.getActiveBoostsEffects().add(activeEffect);
            activeEffect.start();
        }
        else if (!on && activeEffect != null)
        {
            activeEffect.allowCompletion();
            activeEffect.setDuration(0);
            activeEffect = null;
        }
    }

    public static void activate ()
    {
        if (!active)
            SoundsHandler.activateMagnet();

        MissionController.boostActivated();
        SoundsHandler.boostPickup();

        durationLeft = getDuration();
        active = true;

        if (timer != null)
            timer.clear();

        timer = new Timer();
        timer.scheduleTask(new MyTask(), 0f, TIMER_PERIOD);
        timer.start();

        if (passiveEffect == null) {
            passiveEffect = new ParticleEffect();
            passiveEffect.load(Gdx.files.internal("effects/magnetPassive.p"), Gdx.files.internal("effects"));
            passiveEffect.setPosition(Jumper.instance().getX() + Jumper.instance().getWidth() / 2f, Jumper.instance().getY() + Jumper.instance().getHeight() / 2f);
            passiveEffect.scaleEffect(2f);
            BonusesHandler.getActiveBoostsEffects().add(passiveEffect);
            passiveEffect.start();
        }
    }

    /*
        ������
     */

    private static class MyTask extends Timer.Task
    {
        @Override
        public void run()
        {
            if (passiveEffect != null)
                passiveEffect.setPosition(Jumper.instance().getX() + Jumper.instance().getWidth() / 2f, Jumper.instance().getY() + Jumper.instance().getHeight() / 2f);

            durationLeft -= TIMER_PERIOD;

            if (durationLeft <= 0f)
            {
                if (passiveEffect != null) {
                    passiveEffect.allowCompletion();
                    passiveEffect.setDuration(0);
                    passiveEffect = null;
                }

                active = false;

                SoundsHandler.deactivateMagnet();

                timer.stop();
                timer.clear();
            }
        }
    }
}
