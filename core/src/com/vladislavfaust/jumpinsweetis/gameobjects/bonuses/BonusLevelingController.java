package com.vladislavfaust.jumpinsweetis.gameobjects.bonuses;

import com.vladislavfaust.jumpinsweetis.helpers.MyPreferences;
import com.vladislavfaust.jumpinsweetis.helpers.TokensHandler;
import com.vladislavfaust.jumpinsweetis.helpers.googleplay.AchievementsHandler;

/**
 * Created by Faust on 08.05.2015.
 *
 */
public class BonusLevelingController
{
    public enum BOOST_TYPE
    {
        ROCKET, MAGNET, DOUBLER
    }

    private static int
            rocketLevel = 0,
            magnetLevel = 0,
            doublerLevel = 0;

    public final static int MAX_LEVEL = 9;

    public static void init ()
    {
        rocketLevel = MyPreferences.getBoostLevel(BOOST_TYPE.ROCKET);
        magnetLevel = MyPreferences.getBoostLevel(BOOST_TYPE.MAGNET);
        doublerLevel = MyPreferences.getBoostLevel(BOOST_TYPE.DOUBLER);
    }

    /*
          �������, ��������� � ��������
     */

    private static void putLevels ()
    {
        MyPreferences.putBoostLevel(BOOST_TYPE.ROCKET, rocketLevel);
        MyPreferences.putBoostLevel(BOOST_TYPE.MAGNET, magnetLevel);
        MyPreferences.putBoostLevel(BOOST_TYPE.DOUBLER, doublerLevel);
    }

    public static int howMuchUpgradeCosts (BOOST_TYPE boostType)
    {
        switch (boostType)
        {
            case ROCKET:
                return (rocketLevel < 9) ? (int) Math.pow(2, rocketLevel) : -1;
            case MAGNET:
                return (magnetLevel < 9) ? (int) Math.pow(2, magnetLevel) : -1;
            case DOUBLER:
                return (doublerLevel < 9) ? (int) Math.pow(2, doublerLevel) : -1;
            default:
                return -1;
        }
    }

    public static boolean upgrade (BOOST_TYPE boostType)
    {
        int cost = howMuchUpgradeCosts(boostType);

        if (cost > TokensHandler.getTotalTokens() || cost == -1)
            return false;

        TokensHandler.spendTokens(cost);

        switch (boostType)
        {
            case ROCKET:
                rocketLevel++;
                break;
            case MAGNET:
                magnetLevel++;
                break;
            case DOUBLER:
                doublerLevel++;
                break;
        }

        putLevels();

        if (rocketLevel == MAX_LEVEL && magnetLevel == MAX_LEVEL && doublerLevel == MAX_LEVEL)
            AchievementsHandler.unlockMaxBoost();

        return true;
    }

    /*
        �������
     */

    public static int getDoublerLevel()
    {
        return (doublerLevel > MAX_LEVEL) ? 0 : doublerLevel;
    }

    public static int getMagnetLevel()
    {
        return (magnetLevel > MAX_LEVEL) ? 0 : magnetLevel;
    }

    public static int getRocketLevel()
    {
        return (rocketLevel > MAX_LEVEL) ? 0 : rocketLevel;
    }
}
