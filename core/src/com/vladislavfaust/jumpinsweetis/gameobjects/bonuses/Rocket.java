package com.vladislavfaust.jumpinsweetis.gameobjects.bonuses;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.utils.Timer;
import com.vladislavfaust.jumpinsweetis.gameobjects.Jumper;
import com.vladislavfaust.jumpinsweetis.gamesystems.BonusesHandler;
import com.vladislavfaust.jumpinsweetis.gamesystems.CollisionsHandler;
import com.vladislavfaust.jumpinsweetis.gamesystems.MissionController;
import com.vladislavfaust.jumpinsweetis.helpers.AssetLoader;
import com.vladislavfaust.jumpinsweetis.helpers.SoundsHandler;

/**
 * Created by Faust on 05.04.2015.
 *
 */
public class Rocket extends Bonus
{
    /*
        Настройки
     */

    private final static float
            MIN_SPAWN_CHANCE = 0.05f,
            MAX_SPAWN_CHANCE = 0.3f,

            MIN_BURST = 2000f,
            MAX_BURST = 2000f,

            ACTIVE_TIME = 0.1f,
            INVULNERABILITY_TIME = 1f;

    public final static float PICK_RADIUS = 64f;

    /*
        Переменные
     */

    private static boolean active = false, invulnerable = false;
    private static float timeLeft = 0f, invulnerabilityTimeLeft = 0f;
    private static Timer timer;
    private final static float TIMER_PERIOD = 0.01f;

    private static ParticleEffect fire = null;

    /*
        Методы инициализации
     */

    // Вызывается каждую игру
    public static void initialize()
    {
        if (fire != null) {
            fire.allowCompletion();
            fire.setDuration(0);
            fire = null;
        }

        active = false;
        invulnerable = false;
        timeLeft = 0f;
        invulnerabilityTimeLeft = 0f;
    }

    // Вызывается один раз
    public static void construct ()
    {
        initialize();
    }

    // Вызывается при спавне
    public void init(float x, float y, float rotation)
    {
        super.init(AssetLoader.getCommon().findRegion("rocket"), x, y, rotation, PICK_RADIUS);
    }

    /*
        Методы, доступные снаружи
     */

    public static float getBurst ()
    {
        return MIN_BURST + BonusLevelingController.getRocketLevel() * ((MAX_BURST - MIN_BURST) / ((float) BonusLevelingController.MAX_LEVEL));
    }

    public static float getSpawnChance ()
    {
        return MIN_SPAWN_CHANCE + BonusLevelingController.getRocketLevel() * ((MAX_SPAWN_CHANCE - MIN_SPAWN_CHANCE) / ((float) BonusLevelingController.MAX_LEVEL));
    }

    public static void activate ()
    {
        SoundsHandler.activateRocket();

        if (fire == null) {
            fire = new ParticleEffect();
            fire.load(Gdx.files.internal("effects/fire.p"), Gdx.files.internal("effects"));
            fire.setPosition(Jumper.instance().getX() + Jumper.instance().getWidth() / 2f, Jumper.instance().getY() + Jumper.instance().getHeight() / 2f);
            fire.scaleEffect(2f);
            BonusesHandler.getActiveBoostsEffects().add(fire);
            fire.start();
        }

        MissionController.boostActivated();

        timeLeft = ACTIVE_TIME;
        active = true;

        invulnerabilityTimeLeft = INVULNERABILITY_TIME;

        if (!invulnerable)
        {
            invulnerable = true;

            if (timer != null)
                timer.clear();

            timer = new Timer();
            timer.scheduleTask(new MyTask(), 0f, TIMER_PERIOD);
            timer.start();

            Gdx.app.log("Timer", "Scheduled");
        }
    }

    public static boolean isActive()
    {
        return active;
    }

    public static boolean isInvulnerable()
    {
        return invulnerable;
    }

    /*
        Класс задания таймера
     */

    private static class MyTask extends Timer.Task
    {
        @Override
        public void run()
        {
            if (fire != null)
                fire.setPosition(Jumper.instance().getX() + Jumper.instance().getWidth() / 2f, Jumper.instance().getY() + Jumper.instance().getHeight() / 2f);

            timeLeft -= TIMER_PERIOD;
            invulnerabilityTimeLeft -= TIMER_PERIOD;

            if (active && timeLeft <= 0f)
                active = false;

            if (invulnerable && invulnerabilityTimeLeft <= 0 && CollisionsHandler.checkObstacles() == 0) {
                invulnerable = false;

                timer.stop();
                timer.clear();

                if (fire != null) {
                    fire.allowCompletion();
                    fire.setDuration(0);
                    fire = null;
                }
            }
        }
    }
}
