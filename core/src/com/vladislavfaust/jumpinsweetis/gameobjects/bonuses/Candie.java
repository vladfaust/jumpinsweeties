package com.vladislavfaust.jumpinsweetis.gameobjects.bonuses;

import com.vladislavfaust.jumpinsweetis.gamesystems.BonusesHandler;
import com.vladislavfaust.jumpinsweetis.helpers.AssetLoader;

/**
 * Created by Faust on 23.01.2015.
 *
 */
public class Candie extends Bonus
{
    // Ценность (от 1 до 3)
    private int value;
    private int id;

    public void init (int id, float x, float y, float rotation)
    {
        super.init(AssetLoader.getCommon().findRegion("candie", id), x, y, rotation, BonusesHandler.CANDIE_PICK_RADIUS);

        // Устанавливаем ценность в зависимости от id
        if (id < 3)
            value = 1;
        else if (id < 6)
            value = 2;
        else
            value = 3;

        this.id = id;
    }

    // Геттеры сеттеры
    public int getValue()
    {
        return value;
    }

    public int getId()
    {
        return id;
    }
}
