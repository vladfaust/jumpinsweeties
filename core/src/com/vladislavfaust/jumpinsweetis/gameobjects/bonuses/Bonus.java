package com.vladislavfaust.jumpinsweetis.gameobjects.bonuses;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Pool;

/**
 * Created by Faust on 04.04.2015.
 *
 */
public class Bonus implements Pool.Poolable
{
    // Настроечки
    public final static float
            FADE_ANIM_LENGTH = 0.15f,
            BOUNCE_ANIM_LENGTH = 1f,
            BOUNCE_ANIM_POWER = 70f;

    // Спрайт
    protected Sprite sprite;

    // Отвечают за анимации
    private float bounceTime, fadeTime;
    private boolean bounceReversed = false;
    private boolean dead = false;
    private boolean totallyDead = false;

    // Вектор позиции
    protected Vector2 position;

    // Кружок коллизии
    protected Circle collisionCircle;

    // Еще немножк
    private float height;

    // Конструктор
    public Bonus ()
    {
        sprite = new Sprite();
        position = new Vector2();
        collisionCircle = new Circle();
    }

    @Override
    public void reset()
    {
        // Убираем поворот
        sprite.setRotation(0f);

        // Обновляем значения анимации
        bounceTime = 0f;
        fadeTime = 0f;
        bounceReversed = false;
        dead = false;
        totallyDead = false;
        sprite.setAlpha(1f);
    }

    public void init (TextureRegion textureRegion, float x, float y, float rotation, float collisionRadius)
    {
        // Устанавливаем вектор позиции
        position.set(x, y);

        // Устанавливаем спрайт
        sprite.setRegion(textureRegion);
        sprite.setSize(sprite.getRegionWidth(), sprite.getRegionHeight());
        sprite.setOriginCenter();

        // Устанавливаем позицию спрайта
        sprite.setPosition(x, y);

        // Поворачиваем спрайт
        sprite.setRotation(rotation);

        height = sprite.getRegionHeight();

        // Двигаем кружочек
        collisionCircle.setPosition(sprite.getOriginX(), sprite.getOriginY());
        collisionCircle.setRadius(collisionRadius);
    }

    // Сдвигаем, если нужно
    public void moveBy (Vector2 velocity)
    {
        position.add(velocity);
        sprite.setPosition(position.x, position.y);
    }

    // Анимируем по-четкому
    public void updateAnimation (float delta)
    {
        // Время баунса вверх
        if (!bounceReversed)
        {
            bounceTime += delta;
            if (bounceTime >= BOUNCE_ANIM_LENGTH)
                bounceReversed = true;
        }

        // Время баунса вниз
        else
        {
            bounceTime -= delta;
            if (bounceTime <= 0f)
                bounceReversed = false;
        }

        // Баунсим
        sprite.setY(position.y + bounceTime * BOUNCE_ANIM_POWER);

        // Не забываем про кружок
        collisionCircle.setPosition(sprite.getX() + sprite.getOriginX(), sprite.getY() + sprite.getOriginY());

        // Теперь фейд
        // Фейдить, только если бонус "мертв"
        if (dead)
        {
            fadeTime += delta;
            if (fadeTime > FADE_ANIM_LENGTH)
                fadeTime = FADE_ANIM_LENGTH;

            sprite.setAlpha(1f - fadeTime/FADE_ANIM_LENGTH);

            // Фейд закончился, убьем ее полностью
            if (fadeTime >= FADE_ANIM_LENGTH)
            {
                totallyDead = true;
            }
        }
    }

    // Геттеры сеттеры
    public float getEdgeY ()
    {
        return position.y + height;
    }

    public boolean isDead()
    {
        return dead;
    }

    public void setIsDead(boolean dead)
    {
        this.dead = dead;
    }

    public boolean isTotallyDead()
    {
        return totallyDead;
    }

    public Sprite getSprite()
    {
        return sprite;
    }

    public Circle getCollisionCircle()
    {
        return collisionCircle;
    }

    public Vector2 getPosition()
    {
        return position;
    }
}
