package com.vladislavfaust.jumpinsweetis.gamesystems;

import com.vladislavfaust.jumpinsweetis.JumpinSweeties;
import com.vladislavfaust.jumpinsweetis.helpers.MySecurity;

/**
 * Created by Faust on 20.01.2015.
 * Отвечает за начисление очков
 */
public class ScoreHandler
{

    /*
        Настройки
     */

    public final static int MAX_STARS = 3;
    public final static int STARS_INTERVAL = 10;

    // Вызывается каждую новую игру
    public static void init ()
    {
        MySecurity.newGame();
    }

    public static void addScore ()
    {
        MySecurity.addScore();
    }

    // Возвращает кол-во звезд для отображения
    public static int getStars ()
    {
        int stars = (int)Math.floor(MySecurity.getScore()/STARS_INTERVAL);
        stars = (stars > MAX_STARS) ? MAX_STARS : stars;
        return stars;
    }

    // Отправляет score на GP
    public static void submitScore ()
    {
        if (JumpinSweeties.instance().getGooglePlayServices().getSignedInGPGS())
            JumpinSweeties.instance().getGooglePlayServices().submitScoreGPGS(MySecurity.getScore());
    }

    // Возвращает последние очки
    public static int getScore ()
    {
        return MySecurity.getScore();
    }
}
