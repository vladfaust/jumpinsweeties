package com.vladislavfaust.jumpinsweetis.gamesystems;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.ParticleEffectPool;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Pool;
import com.vladislavfaust.jumpinsweetis.JumpinSweeties;
import com.vladislavfaust.jumpinsweetis.gameobjects.bonuses.Bonus;
import com.vladislavfaust.jumpinsweetis.gameobjects.bonuses.Candie;
import com.vladislavfaust.jumpinsweetis.gameobjects.bonuses.Doubler;
import com.vladislavfaust.jumpinsweetis.gameobjects.bonuses.Magnet;
import com.vladislavfaust.jumpinsweetis.gameobjects.bonuses.Rocket;
import com.vladislavfaust.jumpinsweetis.gameobjects.bonuses.Ticket;
import com.vladislavfaust.jumpinsweetis.helpers.AssetLoader;
import com.vladislavfaust.jumpinsweetis.helpers.CustomParticleMethods;
import com.vladislavfaust.jumpinsweetis.helpers.SoundsHandler;
import com.vladislavfaust.jumpinsweetis.helpers.gifts.GiftsCooldownHelper;

import java.util.Random;

/**
 * Created by Faust on 20.01.2015.
 * Класс отвечает за спавн бонусов
 * Без отрисовки, только спавн
 */
public class BonusesHandler
{
    // Различные настройки
    public final static float
            CANDIE_PICK_RADIUS = 30f,
            BOOSTER_PICK_RADIUS = 30f,
            EDGE_OFFSET_X = 150f,
            EDGE_OFFSET_Y = 150f,
            ROTATION = 15f,
            CANDIES_SECOND_LEVEL = 7,   // Сколько очков нужно для перехода на 2 очка
            CANDIES_THIRD_LEVEL = 17;    // Сколько нужно для перехода на 3 очка

    // Шанс спавна
    private static final float
            CANDIE_CHANCE = 0.75f,
            JACKPOT_CHANCE = 0.02f,
            TICKET_CHANCE = 0.2f;

    private static final int
            JACKPOT_CANDIES = 9;

    private static boolean ticketAlreadySpawned, readyToSpawnATicket;

    // Всего типов конфет
    public static final int CANDIES_TOTAL_TYPES = 9;

    // Бонусы
    private final static Array<Bonus> activeBonuses = new Array<Bonus>();
    private final static Pool<Candie> candiePool = new Pool<Candie>() {
        @Override
        protected Candie newObject()
        {
            return new Candie();
        }
    };
    private final static Pool<Rocket> rocketPool = new Pool<Rocket>() {
        @Override
        protected Rocket newObject()
        {
            return new Rocket();
        }
    };
    private final static Pool<Magnet> magnetPool = new Pool<Magnet>() {
        @Override
        protected Magnet newObject()
        {
            return new Magnet();
        }
    };
    private final static Pool<Doubler> doublerPool = new Pool<Doubler>() {
        @Override
        protected Doubler newObject()
        {
            return new Doubler();
        }
    };
    private final static Pool<Ticket> ticketPool = new Pool<Ticket>() {
        @Override
        protected Ticket newObject()
        {
            return new Ticket();
        }
    };
    
    // Пул партиклов конфеток
    private static Array<ParticleEffectPool> candieEffectPools = new Array<>(CANDIES_TOTAL_TYPES + 3);
    private static Array<ParticleEffectPool.PooledEffect> activeCandieEffects = new Array<>();
    private static Array<ParticleEffect> activeBoostsEffects = new Array<>();

    // Всякое
    private static Random random;

    // Статичный конструктор
    public static void construct()
    {
        random = new Random();

        // Здесь инициализируем стандартные партиклы конфет
        for (int i = 0; i < CANDIES_TOTAL_TYPES + 3; i++) {
            ParticleEffect effect = new ParticleEffect();
            effect.load(Gdx.files.internal("effects/bonus.p"), AssetLoader.getCommon());
            CustomParticleMethods.loadBonus(effect, i);
            candieEffectPools.add(new ParticleEffectPool(effect, 1, JACKPOT_CANDIES * 2));
        }

        activeBoostsEffects.clear();

        Rocket.construct();
        Magnet.construct();
        Doubler.construct();

        init();
    }

    // Инициализатор (служит для сброса значений)
    public static void init ()
    {
        activeCandieEffects.clear();
        activeBonuses.clear();
        ticketAlreadySpawned = false;
        readyToSpawnATicket = GiftsCooldownHelper.readyToSpawnATicket();

        Rocket.initialize();
        Magnet.initialize();
        Doubler.initialize();
    }

    // Спавн Джекпота
    private static void spawnJackpot (float yMin, float yMax)
    {
        // Джекпот!
        for (int i = 0; i < JACKPOT_CANDIES; i++)
        {
            int value = 0;

            // Выбираем ID в зависимости от набранных очков
            if (ScoreHandler.getScore() > CANDIES_SECOND_LEVEL)
                if (ScoreHandler.getScore() > CANDIES_THIRD_LEVEL)
                    value = 6 + random.nextInt(3);
                else
                    value = 3 + random.nextInt(3);
            else
                value = random.nextInt(3);

            // Сначала находим рандомный x
            float x = EDGE_OFFSET_X + random.nextInt((int) (JumpinSweeties.instance().getGameWidth() - EDGE_OFFSET_X * 2));

            // А потом рандомный y
            float y = yMin + EDGE_OFFSET_Y + random.nextInt((int) (yMax - yMin - EDGE_OFFSET_Y * 2));

            while (!isAreaFree(x, y, CANDIE_PICK_RADIUS))
            {
                x = EDGE_OFFSET_X + random.nextInt((int) (JumpinSweeties.instance().getGameWidth() - EDGE_OFFSET_X * 2));
                y = yMin + EDGE_OFFSET_Y + random.nextInt((int) (yMax - yMin - EDGE_OFFSET_Y * 2));
            }

            // Теперь спавним!
            Candie candie = candiePool.obtain();
            candie.init(value, x, y, -ROTATION + random.nextInt((int) ROTATION * 2));
            activeBonuses.add(candie);
        }
    }

    // Спавн ракеты
    private static void spawnRocket(float yMin, float yMax)
    {
        // Сначала находим рандомный x
        float x = EDGE_OFFSET_X + random.nextInt((int) (JumpinSweeties.instance().getGameWidth() - EDGE_OFFSET_X * 2));

        // А потом рандомный y
        float y = yMin + EDGE_OFFSET_Y + random.nextInt((int) (yMax - yMin - EDGE_OFFSET_Y * 2));

        while (!isAreaFree(x, y, Rocket.PICK_RADIUS))
        {
            x = EDGE_OFFSET_X + random.nextInt((int) (JumpinSweeties.instance().getGameWidth() - EDGE_OFFSET_X * 2));
            y = yMin + EDGE_OFFSET_Y + random.nextInt((int) (yMax - yMin - EDGE_OFFSET_Y * 2));
        }

        // Теперь спавним!
        Rocket rocket = rocketPool.obtain();
        rocket.init(x, y, -ROTATION + random.nextInt((int) ROTATION * 2));
        activeBonuses.add(rocket);
    }

    // Спавн магнита
    private static void spawnMagnet(float yMin, float yMax)
    {
        // Сначала находим рандомный x
        float x = EDGE_OFFSET_X + random.nextInt((int) (JumpinSweeties.instance().getGameWidth() - EDGE_OFFSET_X * 2));

        // А потом рандомный y
        float y = yMin + EDGE_OFFSET_Y + random.nextInt((int) (yMax - yMin - EDGE_OFFSET_Y * 2));

        while (!isAreaFree(x, y, Magnet.PICK_RADIUS))
        {
            x = EDGE_OFFSET_X + random.nextInt((int) (JumpinSweeties.instance().getGameWidth() - EDGE_OFFSET_X * 2));
            y = yMin + EDGE_OFFSET_Y + random.nextInt((int) (yMax - yMin - EDGE_OFFSET_Y * 2));
        }

        // Теперь спавним!
        Magnet magnet = magnetPool.obtain();
        magnet.init(x, y, -ROTATION + random.nextInt((int) ROTATION * 2));
        activeBonuses.add(magnet);
    }

    // Спавн х2
    private static void spawnDoubler(float yMin, float yMax)
    {
        // Сначала находим рандомный x
        float x = EDGE_OFFSET_X + random.nextInt((int) (JumpinSweeties.instance().getGameWidth() - EDGE_OFFSET_X * 2));

        // А потом рандомный y
        float y = yMin + EDGE_OFFSET_Y + random.nextInt((int) (yMax - yMin - EDGE_OFFSET_Y * 2));

        while (!isAreaFree(x, y, Doubler.PICK_RADIUS))
        {
            x = EDGE_OFFSET_X + random.nextInt((int) (JumpinSweeties.instance().getGameWidth() - EDGE_OFFSET_X * 2));
            y = yMin + EDGE_OFFSET_Y + random.nextInt((int) (yMax - yMin - EDGE_OFFSET_Y * 2));
        }

        // Теперь спавним!
        Doubler doubler = doublerPool.obtain();
        doubler.init(x, y, -ROTATION + random.nextInt((int) ROTATION * 2));
        activeBonuses.add(doubler);
    }

    // Спавн билетика
    private static void spawnTicket (float yMin, float yMax)
    {
        // Сначала находим рандомный x
        float x = EDGE_OFFSET_X + random.nextInt((int) (JumpinSweeties.instance().getGameWidth() - EDGE_OFFSET_X * 2));

        // А потом рандомный y
        float y = yMin + EDGE_OFFSET_Y + random.nextInt((int) (yMax - yMin - EDGE_OFFSET_Y * 2));

        while (!isAreaFree(x, y, Ticket.PICK_RADIUS))
        {
            x = EDGE_OFFSET_X + random.nextInt((int) (JumpinSweeties.instance().getGameWidth() - EDGE_OFFSET_X * 2));
            y = yMin + EDGE_OFFSET_Y + random.nextInt((int) (yMax - yMin - EDGE_OFFSET_Y * 2));
        }

        // Теперь спавним!
        Ticket ticket = ticketPool.obtain();
        ticket.init(x, y, -ROTATION + random.nextInt((int) ROTATION * 2));
        activeBonuses.add(ticket);

        // Спавнить билетик можно только раз за игру
        ticketAlreadySpawned = true;
    }

    // Спавн конфетки
    private static void spawnCandie (float yMin, float yMax)
    {
        // Выбираем value в зависимости от набранных очков
        int value = 0;
        if (ScoreHandler.getScore() > CANDIES_SECOND_LEVEL)
            if (ScoreHandler.getScore() > CANDIES_THIRD_LEVEL)
                value = 6 + random.nextInt(3);
            else
                value = 3 + random.nextInt(3);
        else
            value = random.nextInt(3);

        // Сначала находим рандомный x
        float x = EDGE_OFFSET_X + random.nextInt((int) (JumpinSweeties.instance().getGameWidth() - EDGE_OFFSET_X * 2));

        // А потом рандомный y
        float y = yMin + EDGE_OFFSET_Y + random.nextInt((int) (yMax - yMin - EDGE_OFFSET_Y * 2));

        while (!isAreaFree(x, y, CANDIE_PICK_RADIUS))
        {
            x = EDGE_OFFSET_X + random.nextInt((int) (JumpinSweeties.instance().getGameWidth() - EDGE_OFFSET_X * 2));
            y = yMin + EDGE_OFFSET_Y + random.nextInt((int) (yMax - yMin - EDGE_OFFSET_Y * 2));
        }

        // Теперь спавним!
        Candie candie = candiePool.obtain();
        candie.init(value, x, y, -ROTATION + random.nextInt((int) ROTATION * 2));
        activeBonuses.add(candie);
    }

    // Собственно, метод запроса на спавн бонусов в заданном вертикальном регионе
    public static void tryToSpawn (float yMin, float yMax)
    {
        float theRandom = random.nextFloat();

        // Если заспавнен джекпот, то больше ничего не спавнится
        if (theRandom <= JACKPOT_CHANCE) {
            spawnJackpot(yMin, yMax);
            return;
        }

        // Ракета?
        else if (theRandom <= Rocket.getSpawnChance() + JACKPOT_CHANCE)
            spawnRocket(yMin, yMax);

        // Магнит?
        else if (theRandom <= Magnet.getChance() + Rocket.getSpawnChance() + JACKPOT_CHANCE)
            spawnMagnet(yMin, yMax);

        // doubler
        else if (theRandom <= Doubler.getSpawnChance() + Magnet.getChance() + Rocket.getSpawnChance() + JACKPOT_CHANCE)
            spawnDoubler(yMin, yMax);

        // Билетик?
        else if (readyToSpawnATicket && !ticketAlreadySpawned && theRandom <= TICKET_CHANCE + Doubler.getSpawnChance() + Magnet.getChance() + Rocket.getSpawnChance() + JACKPOT_CHANCE)
            spawnTicket(yMin, yMax);

        // Можно спавнить конфетку каждый просвет
        if (random.nextFloat() <= CANDIE_CHANCE)
            spawnCandie(yMin, yMax);
    }

    // Апдейт метод для анимации конфеток и логики
    public static void update (float delta)
    {
        Bonus bonus;

        // Пробежимся по всем бонусам
        for (int i = 0; i < activeBonuses.size; i++)
        {
            bonus = activeBonuses.get(i);

            // Если бонус за пределами экрана или полностью уничтожен, убираем его нафиг
            if (bonus.isTotallyDead() || bonus.getEdgeY() < GameRenderer.instance().getYOffset() - Bonus.BOUNCE_ANIM_POWER)
            {
                activeBonuses.removeIndex(i);
                bonus.reset();

                if (bonus instanceof Candie)
                    candiePool.free((Candie) bonus);
                else if (bonus instanceof Rocket)
                    rocketPool.free((Rocket) bonus);
                else if (bonus instanceof Magnet)
                    magnetPool.free((Magnet) bonus);
                else if (bonus instanceof Doubler)
                    doublerPool.free((Doubler) bonus);
            }
            else
            {
                // Если магнит активен, то можно все притягивать
                Vector2 velocity = Magnet.getBonusVelocity(bonus);

                if (velocity != null) {
                    bonus.moveBy(velocity.scl(delta));
                    Magnet.switchMagnet((velocity.len() > 0f));
                }
                else
                    Magnet.switchMagnet(false);

                bonus.updateAnimation(delta);
            }
        }
        
        // Партиклы конфеток
        ParticleEffectPool.PooledEffect effect;

        for (int i = 0; i < activeCandieEffects.size; i++)
        {
            effect = activeCandieEffects.get(i);
            effect.update(delta);

            if (effect.isComplete())
            {
                effect.free();
                activeCandieEffects.removeIndex(i);
            }
        }

        // Партиклы бонусов
        for (int i = 0; i < activeBoostsEffects.size; i++)
        {
            if (!activeBoostsEffects.get(i).isComplete())
                activeBoostsEffects.get(i).update(delta);
            else {
                /*activeBoostsEffects.get(i).dispose();
                activeBoostsEffects.removeIndex(i);*/
            }
        }
    }

    // Метод, вызываемый, когда джампер таки касается бонуса
    public static void killBonus(Bonus bonus)
    {
        // "Убиваем" бонус, вследствие чего он начнет фейдится
        bonus.setIsDead(true);

        // Узнаем, конфетка ли
        if (bonus instanceof Candie)
        {
            // Воспроизводим звук конфетки
            SoundsHandler.pickupCandie();

            // Показываем партикл
            int id = (Doubler.isActive()) ? (int) Math.floor(((double) ((Candie) bonus).getId()) / 3f) + 9 : ((Candie)bonus).getId();
            ParticleEffectPool.PooledEffect effect = candieEffectPools.get(id).obtain();
            effect.setPosition(bonus.getCollisionCircle().x, bonus.getCollisionCircle().y);
            activeCandieEffects.add(effect);
        }

        // А может все таки ракета
        else if (bonus instanceof Rocket)
            Rocket.activate();

        // Или магнит
        else if (bonus instanceof Magnet)
            Magnet.activate();

        // Или Doubler
        else if (bonus instanceof Doubler)
            Doubler.activate();

        // Или, допустим, билетик
        else if (bonus instanceof Ticket)
            ((Ticket) bonus).pickup();
    }

    // Смотрит, есть ли в данном кругу бонусы
    private static boolean isAreaFree (float x, float y, float radius)
    {
        for (int i = 0; i < activeBonuses.size; i++)
        {
            if (Math.sqrt(Math.pow(activeBonuses.get(i).getPosition().x - x, 2) + Math.pow(activeBonuses.get(i).getPosition().y - y, 2)) < radius + activeBonuses.get(i).getCollisionCircle().radius)
                return false;
        }

        return true;
    }

    // Геттеры
    public static Array<ParticleEffectPool.PooledEffect> getActiveCandieEffects()
    {
        return activeCandieEffects;
    }

    public static Array<Bonus> getActiveBonuses()
    {
        return activeBonuses;
    }

    public static Array<ParticleEffect> getActiveBoostsEffects()
    {
        return activeBoostsEffects;
    }
}
