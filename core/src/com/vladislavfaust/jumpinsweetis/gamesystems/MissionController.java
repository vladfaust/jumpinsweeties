package com.vladislavfaust.jumpinsweetis.gamesystems;

import com.badlogic.gdx.utils.Array;
import com.vladislavfaust.jumpinsweetis.helpers.CandiesHandler;
import com.vladislavfaust.jumpinsweetis.helpers.Language;
import com.vladislavfaust.jumpinsweetis.helpers.MyPreferences;
import com.vladislavfaust.jumpinsweetis.helpers.analytics.AnalyticsHelper;
import com.vladislavfaust.jumpinsweetis.helpers.googleplay.AchievementsHandler;
import com.vladislavfaust.jumpinsweetis.ui.elements.UIMissionNotification;

/**
 * Created by Faust on 05.04.2015.
 *
 */
public class MissionController
{
    // Настройки
    private enum TYPE
    {
        PLAY_GAMES, CANDIES_TOTAL, CANDIES_IN_ONE_GAME, SCORE_TOTAL, SCORE_IN_ONE_GAME, SCORE_IN_ROW, STARS_TOTAL, STARS_IN_GAME, UNLOCK_SKINS,
        OPEN_GIFTS, BOOSTS_TOTAL, TOKENS_TOTAL, UPGRADE_BOOST
    }

    // Переменные
    private final static Array<Mission> missions = new Array<>();
    private static int currentId;
    private static boolean needsToBeShown;

    /*
        Внешние методы
     */

    // Конструктор
    public static void init ()
    {
        // Инициализируем все миссии
        missions.clear();

        missions.add(new Mission(TYPE.PLAY_GAMES, 3, 10));
        missions.add(new Mission(TYPE.CANDIES_TOTAL, 3, 10));
        missions.add(new Mission(TYPE.SCORE_IN_ONE_GAME, 3, 10));
        missions.add(new Mission(TYPE.CANDIES_IN_ONE_GAME, 3, 10));
        missions.add(new Mission(TYPE.UNLOCK_SKINS, 1, 10));

        missions.add(new Mission(TYPE.CANDIES_TOTAL, 20, 25));
        missions.add(new Mission(TYPE.SCORE_IN_ROW, 5, 2, 25));
        missions.add(new Mission(TYPE.SCORE_TOTAL, 20, 25));
        missions.add(new Mission(TYPE.PLAY_GAMES, 10, 25));
        missions.add(new Mission(TYPE.SCORE_IN_ONE_GAME, 7, 25));

        missions.add(new Mission(TYPE.CANDIES_TOTAL, 30, 50));
        missions.add(new Mission(TYPE.SCORE_TOTAL, 30, 50));
        missions.add(new Mission(TYPE.CANDIES_IN_ONE_GAME, 10, 50));
        missions.add(new Mission(TYPE.UNLOCK_SKINS, 2, 50));
        missions.add(new Mission(TYPE.STARS_IN_GAME, 1, 50));   // 15!

        missions.add(new Mission(TYPE.PLAY_GAMES, 50, 75));
        missions.add(new Mission(TYPE.SCORE_IN_ROW, 8, 2, 75));
        missions.add(new Mission(TYPE.SCORE_IN_ONE_GAME, 15, 75));
        missions.add(new Mission(TYPE.STARS_TOTAL, 7, 75));
        missions.add(new Mission(TYPE.CANDIES_IN_ONE_GAME, 15, 75));

        missions.add(new Mission(TYPE.STARS_IN_GAME, 2, 100));
        missions.add(new Mission(TYPE.SCORE_IN_ROW, 13, 2, 100));
        missions.add(new Mission(TYPE.PLAY_GAMES, 100, 100));
        missions.add(new Mission(TYPE.CANDIES_IN_ONE_GAME, 20, 100));
        missions.add(new Mission(TYPE.CANDIES_TOTAL, 300, 100));

        missions.add(new Mission(TYPE.UNLOCK_SKINS, 2, 150));
        missions.add(new Mission(TYPE.SCORE_TOTAL, 150, 150));
        missions.add(new Mission(TYPE.SCORE_IN_ROW, 14, 5, 150));
        missions.add(new Mission(TYPE.STARS_IN_GAME, 3, 150));
        missions.add(new Mission(TYPE.CANDIES_TOTAL, 500, 150)); // 30!

        missions.add(new Mission(TYPE.PLAY_GAMES, 300, 200));
        missions.add(new Mission(TYPE.SCORE_TOTAL, 400, 200));
        missions.add(new Mission(TYPE.SCORE_IN_ROW, 20, 5, 200));
        missions.add(new Mission(TYPE.SCORE_IN_ROW, 30, 10, 200));
        missions.add(new Mission(TYPE.STARS_TOTAL, 999, 200));

        missions.add(new Mission(TYPE.STARS_IN_GAME, 4, 200));

        /*
            NEW

            missions.add(new Mission(TYPE.PLAY_GAMES, 3, 10));
        missions.add(new Mission(TYPE.CANDIES_TOTAL, 3, 10));
        missions.add(new Mission(TYPE.SCORE_IN_ONE_GAME, 3, 10));
        missions.add(new Mission(TYPE.CANDIES_IN_ONE_GAME, 3, 10));
        missions.add(new Mission(TYPE.UNLOCK_SKINS, 1, 10));

        missions.add(new Mission(TYPE.CANDIES_TOTAL, 20, 25));
        missions.add(new Mission(TYPE.SCORE_IN_ONE_GAME, 5, 25));
        missions.add(new Mission(TYPE.OPEN_GIFTS, 3, 25));
        missions.add(new Mission(TYPE.SCORE_IN_ROW, 3, 2, 25));
        missions.add(new Mission(TYPE.CANDIES_TOTAL, 20, 25));

        missions.add(new Mission(TYPE.SCORE_TOTAL, 20, 50));
        missions.add(new Mission(TYPE.UNLOCK_SKINS, 1, 50));
        missions.add(new Mission(TYPE.BOOSTS_TOTAL, 10, 50));
        missions.add(new Mission(TYPE.CANDIES_IN_ONE_GAME, 5, 50));
        missions.add(new Mission(TYPE.STARS_IN_GAME, 1, 50));   // 15!

        missions.add(new Mission(TYPE.TOKENS_TOTAL, 5, 75));
        missions.add(new Mission(TYPE.OPEN_GIFTS, 6, 75));
        missions.add(new Mission(TYPE.UPGRADE_BOOST, 1, 75));
        missions.add(new Mission(TYPE.STARS_TOTAL, 5, 75));
        missions.add(new Mission(TYPE.SCORE_TOTAL, 25, 75));

        missions.add(new Mission(TYPE.SCORE_IN_ROW, 6, 2, 100));
        missions.add(new Mission(TYPE.BOOSTS_TOTAL, 20, 100));
        missions.add(new Mission(TYPE.PLAY_GAMES, 50, 100));
        missions.add(new Mission(TYPE.SCORE_TOTAL, 100, 100));
        missions.add(new Mission(TYPE.CANDIES_IN_ONE_GAME, 12, 100));

        missions.add(new Mission(TYPE.STARS_TOTAL, 10, 125));
        missions.add(new Mission(TYPE.OPEN_GIFTS, 9, 125));
        missions.add(new Mission(TYPE.UNLOCK_SKINS, 2, 125));
        missions.add(new Mission(TYPE.SCORE_IN_ROW, 12, 125));
        missions.add(new Mission(TYPE.STARS_IN_GAME, 3, 125)); // 30!

        missions.add(new Mission(TYPE.TOKENS_TOTAL, 20, 150));
        missions.add(new Mission(TYPE.UPGRADE_BOOST, 1, 150));
        missions.add(new Mission(TYPE.PLAY_GAMES, 100, 150));
        missions.add(new Mission(TYPE.CANDIES_TOTAL, 100, 150));
        missions.add(new Mission(TYPE.SCORE_IN_ROW, 999
         */

        // Устанавливаем текущую миссию
        currentId = MyPreferences.getCurrentId();

        switch (missions.get(currentId).getType())
        {
            case SCORE_IN_ROW:
                missions.get(currentId).setVar3(MyPreferences.getMissionVar(3));
                break;
            default:
                missions.get(currentId).setVar2(MyPreferences.getMissionVar(2));
        }
    }

    // Возвращет id активного задания
    public static int getCurrentId()
    {
        return currentId;
    }

    // Возвращает полное описание миссии
    public static String getDescription (int id)
    {
        Mission mission = missions.get(id);

        switch (mission.getType()) {
            case PLAY_GAMES:
                return "#" + (id + 1) + ": " + Language.LOCALE.format("playNGames", mission.getVar1());
            case CANDIES_TOTAL:
                return "#" + (id + 1) + ": " + Language.LOCALE.format("totalCandies", mission.getVar1());
            case CANDIES_IN_ONE_GAME:
                return "#" + (id + 1) + ": " + Language.LOCALE.format("candiesPerGame", mission.getVar1());
            case SCORE_TOTAL:
                return "#" + (id + 1) + ": " + Language.LOCALE.format("totalScore", mission.getVar1());
            case SCORE_IN_ONE_GAME:
                return "#" + (id + 1) + ": " + Language.LOCALE.format("scorePerGame", mission.getVar1());
            case SCORE_IN_ROW:
                return "#" + (id + 1) + ": " + Language.LOCALE.format("scoreInRow", mission.getVar1(), mission.getVar2());
            case STARS_TOTAL:
                return "#" + (id + 1) + ": " + Language.LOCALE.format("totalStars", mission.getVar1());
            case STARS_IN_GAME:
                return "#" + (id + 1) + ": " + Language.LOCALE.format("starsInOneGame", mission.getVar1());
            case UNLOCK_SKINS:
                return "#" + (id + 1) + ": " + Language.LOCALE.format("unlockSkins", mission.getVar1());
            case OPEN_GIFTS:
                return "#" + (id + 1) + ": " + Language.LOCALE.format("openGifts", mission.getVar1());
            case BOOSTS_TOTAL:
                return "#" + (id + 1) + ": " + Language.LOCALE.format("boostsTotal", mission.getVar1());
            case TOKENS_TOTAL:
                return "#" + (id + 1) + ": " + Language.LOCALE.format("tokensTotal", mission.getVar1());
            case UPGRADE_BOOST:
                return "#" + (id + 1) + ": " + Language.LOCALE.get("upgradeBoost");
        }

        return "Error!";
    }

    // Возвращает бриф на главном экране
    public static String getBrief (int id)
    {
        Mission mission = missions.get(id);

        switch (mission.getType())
        {
            case PLAY_GAMES:
                return "#" + (id + 1) + ": " + Language.LOCALE.format("playNGamesBrief", mission.getVar1(), mission.getVar2());
            case CANDIES_TOTAL:
                return "#" + (id + 1) + ": " + Language.LOCALE.format("totalCandiesBrief", mission.getVar1(), mission.getVar2());
            case CANDIES_IN_ONE_GAME:
                return "#" + (id + 1) + ": " + Language.LOCALE.format("candiesPerGameBrief", mission.getVar1());
            case SCORE_TOTAL:
                return "#" + (id + 1) + ": " + Language.LOCALE.format("totalScoreBrief", mission.getVar1(), mission.getVar2());
            case SCORE_IN_ONE_GAME:
                return "#" + (id + 1) + ": " + Language.LOCALE.format("scorePerGameBrief", mission.getVar1());
            case SCORE_IN_ROW:
                return "#" + (id + 1) + ": " + Language.LOCALE.format("scoreInRowBrief", mission.getVar1(), mission.getVar2(), mission.getVar3());
            case STARS_TOTAL:
                return "#" + (id + 1) + ": " + Language.LOCALE.format("totalStarsBrief", mission.getVar1(), mission.getVar2());
            case STARS_IN_GAME:
                return "#" + (id + 1) + ": " + Language.LOCALE.format("starsInOneGameBrief", mission.getVar1());
            case UNLOCK_SKINS:
                return "#" + (id + 1) + ": " + Language.LOCALE.format("unlockSkinsBrief", mission.getVar1(), mission.getVar2());
            case OPEN_GIFTS:
                return "#" + (id + 1) + ": " + Language.LOCALE.format("openGiftsBrief", mission.getVar1(), mission.getVar2());
            case BOOSTS_TOTAL:
                return "#" + (id + 1) + ": " + Language.LOCALE.format("boostsTotalBrief", mission.getVar1(), mission.getVar2());
            case TOKENS_TOTAL:
                return "#" + (id + 1) + ": " + Language.LOCALE.format("tokensTotalBrief", mission.getVar1(), mission.getVar2());
            case UPGRADE_BOOST:
                return "#" + (id + 1) + ": " + Language.LOCALE.get("upgradeBoostBrief");
        }

        return "Error!";
    }

    /*
        Не знаю как назвать. Контрольные методы
     */

    // Игра сыграна
    public static void playGame (int candies, int score, int stars)
    {
        Mission currentMission = missions.get(currentId);

        switch (currentMission.getType()) {
            case PLAY_GAMES:
                currentMission.setVar2(currentMission.getVar2() + 1);
                if (currentMission.getVar2() >= currentMission.getVar1())
                    completeCurrentMission();
                break;
            case CANDIES_TOTAL:
                currentMission.setVar2(currentMission.getVar2() + candies);
                if (currentMission.getVar2() >= currentMission.getVar1())
                    completeCurrentMission();
                break;
            case CANDIES_IN_ONE_GAME:
                if (candies >= currentMission.getVar1())
                    completeCurrentMission();
                break;
            case SCORE_TOTAL:
                currentMission.setVar2(currentMission.getVar2() + score);
                if (currentMission.getVar2() >= currentMission.getVar1())
                    completeCurrentMission();
                break;
            case SCORE_IN_ONE_GAME:
                if (score >= currentMission.getVar1())
                    completeCurrentMission();
                break;
            case SCORE_IN_ROW:
                if (score >= currentMission.getVar1())
                    currentMission.setVar3(currentMission.getVar3() + 1);
                else
                    currentMission.setVar3(0);

                if (currentMission.getVar3() >= currentMission.getVar2())
                    completeCurrentMission();
                break;
            case STARS_TOTAL:
                currentMission.setVar2(currentMission.getVar2() + stars);
                if (currentMission.getVar2() >= currentMission.getVar1())
                    completeCurrentMission();
                break;
            case STARS_IN_GAME:
                if (stars >= currentMission.getVar1())
                    completeCurrentMission();
                break;
            case UNLOCK_SKINS:
                break;
        }

        MyPreferences.putMissionVar(1, currentMission.getVar1());
        MyPreferences.putMissionVar(2, currentMission.getVar2());
        MyPreferences.putMissionVar(3, currentMission.getVar3());
        MyPreferences.putMissionVar(4, currentMission.getVar4());
    }

    /*
        События
     */

    public static void unlockSkin(boolean showCompletion)
    {
        missionPlusOne(TYPE.UNLOCK_SKINS, 0);

        if (showCompletion && MissionController.isNeedsToBeShown())
            UIMissionNotification.completeMission();
    }

    public static void openGift(boolean showCompletion)
    {
        missionPlusOne(TYPE.OPEN_GIFTS, 0);

        if (showCompletion && MissionController.isNeedsToBeShown())
            UIMissionNotification.completeMission();
    }

    public static void boostActivated ()
    {
        missionPlusOne(TYPE.BOOSTS_TOTAL, 0);
    }

    public static void tokensCollected(int value, boolean showCompletion)
    {
        missionPlusOne(TYPE.TOKENS_TOTAL, value);

        if (showCompletion && MissionController.isNeedsToBeShown())
            UIMissionNotification.completeMission();
    }

    public static void boostUpgraded(boolean showCompletion)
    {
        Mission mission = missions.get(currentId);

        if (mission.getType() == TYPE.UPGRADE_BOOST)
            completeCurrentMission();

        if (showCompletion && MissionController.isNeedsToBeShown())
            UIMissionNotification.completeMission();
    }

    private static void missionPlusOne(TYPE type, int amount)
    {
        Mission mission = missions.get(currentId);

        if (mission.getType() != type)
            return;

        mission.setVar2(mission.getVar2() + ((amount == 0) ? 1 : amount));

        if (mission.getVar2() >= mission.getVar1())
            completeCurrentMission();

        MyPreferences.putMissionVar(currentId, mission.getVar1());
        MyPreferences.putMissionVar(currentId, mission.getVar2());
    }

    // Нужно ли показывать выполнение
    public static boolean isNeedsToBeShown()
    {
        return needsToBeShown;
    }

    // Показать выполнение
    public static void showCompletion ()
    {
        needsToBeShown = false;
    }

    // Получить награду за предыдущую миссию
    public static int getPreviousMissionReward ()
    {
        return missions.get(currentId - 1).getRewardCandies();
    }

    /*
        Внутренние методы
     */

    // Выполнение миссии
    private static void completeCurrentMission ()
    {
        needsToBeShown = true;

        CandiesHandler.addCandiesAsReward(missions.get(currentId).getRewardCandies());

        AchievementsHandler.completeMission(currentId);
        AnalyticsHelper.trackMissionCompleted(currentId);

        MyPreferences.putMissionVar(1, 0);
        MyPreferences.putMissionVar(2, 0);
        MyPreferences.putMissionVar(3, 0);
        MyPreferences.putMissionVar(4, 0);

        currentId++;
        MyPreferences.putCurrentId(currentId);
    }

    // Класс задания
    private static class Mission
    {
        private TYPE type;
        private int var1, var2, var3, var4;
        private int rewardCandies;

        public Mission(TYPE type, int var1, int rewardCandies)
        {
            this.type = type;
            this.var1 = var1;
            this.rewardCandies = rewardCandies;
        }

        public Mission(TYPE type, int var1, int var2, int rewardCandies)
        {
            this.type = type;
            this.var1 = var1;
            this.var2 = var2;
            this.rewardCandies = rewardCandies;
        }

        public Mission(TYPE type, int var1, int var2, int var3, int rewardCandies)
        {
            this.type = type;
            this.var1 = var1;
            this.var2 = var2;
            this.var3 = var3;
            this.rewardCandies = rewardCandies;
        }

        public Mission(TYPE type, int var1, int var2, int var3, int var4, int rewardCandies)
        {
            this.type = type;
            this.var1 = var1;
            this.var2 = var2;
            this.var3 = var3;
            this.var4 = var4;
            this.rewardCandies = rewardCandies;
        }

        public TYPE getType()
        {
            return type;
        }

        public int getVar1()
        {
            return var1;
        }

        public void setVar1(int var1)
        {
            this.var1 = var1;
        }

        public int getVar2()
        {
            return var2;
        }

        public void setVar2(int var2)
        {
            this.var2 = var2;
        }

        public int getVar3()
        {
            return var3;
        }

        public void setVar3(int var3)
        {
            this.var3 = var3;
        }

        public int getVar4()
        {
            return var4;
        }

        public void setVar4(int var4)
        {
            this.var4 = var4;
        }

        public int getRewardCandies()
        {
            return rewardCandies;
        }
    }
}
