package com.vladislavfaust.jumpinsweetis.gamesystems;

import com.badlogic.gdx.math.Intersector;
import com.vladislavfaust.jumpinsweetis.JumpinSweeties;
import com.vladislavfaust.jumpinsweetis.gameobjects.Jumper;
import com.vladislavfaust.jumpinsweetis.gameobjects.Pipes;
import com.vladislavfaust.jumpinsweetis.gameobjects.bonuses.Bonus;

/**
 * Created by Faust on 22.01.2015.
 *
 */
public class CollisionsHandler
{
    public static int checkObstacles ()
    {
        Pipes pipes;

        // Сначала проверяем трубы
        for (int i = 0; i < PipesController.MAX_PIPES; i++)
        {
            pipes = PipesController.getPipes().get(i);

            if (Intersector.overlaps(Jumper.instance().getCollisionCircle(), pipes.getCollisionRectLeft()))
                return 1;

            if (Intersector.overlaps(Jumper.instance().getCollisionCircle(), pipes.getCollisionRectRight()))
                return 1;
        }

        // Потом бока
        if (Jumper.instance().getX() + (Jumper.SPRITE_SIZE_X - Jumper.COLLISION_RADIUS)/2f < 0f ||
            Jumper.instance().getX() + (Jumper.SPRITE_SIZE_X + Jumper.COLLISION_RADIUS)/2f > JumpinSweeties.instance().getGameWidth())
            return 2;

        // И низ
        if (Jumper.instance().getY() + (Jumper.SPRITE_SIZE_Y - Jumper.COLLISION_RADIUS)/2f < GameRenderer.instance().getYOffset())
            return 2;

        // Все успешно
        return 0;
    }

    public static Bonus checkBonuses()
    {
        Bonus bonus;

        for (int i = 0; i < BonusesHandler.getActiveBonuses().size; i++)
        {
            bonus = BonusesHandler.getActiveBonuses().get(i);

            if (!bonus.isDead() && Intersector.overlaps(Jumper.instance().getCollisionCircle(), bonus.getCollisionCircle()))
                return bonus;
        }

        // Все не очень успешно
        return null;
    }
}
