package com.vladislavfaust.jumpinsweetis.gamesystems;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Circle;
import com.vladislavfaust.jumpinsweetis.JumpinSweeties;
import com.vladislavfaust.jumpinsweetis.gameobjects.Jumper;

/**
 * Created by Faust on 20.01.2015.
 *
 */
public class GameRenderer
{
    // Смягчение движения камеры
    private final static float CAMERA_SMOOTH = 0.1f;

    // Дебаг
    private static final boolean DEBUG = false;

    // Всякие переменные
    private SpriteBatch batch;
    private ShapeRenderer shapeRenderer;
    private OrthographicCamera camera;

    // Сдвиг по y
    private float
            yOffset = 0f,
            previousYDelta = 0f;

    // Инстанс синглтона
    private static GameRenderer _instance;

    // Геттер инстанса синглтона
    public static GameRenderer instance ()
    {
        return _instance;
    }

    // Конструктор
    public GameRenderer ()
    {
        _instance = this;

        // Настраиваем камеру
        camera = new OrthographicCamera();
        camera.setToOrtho(false, JumpinSweeties.instance().getGameWidth(), JumpinSweeties.instance().getGameHeight());

        // Настраиваем батч
        batch = new SpriteBatch();
        batch.setProjectionMatrix(camera.combined);

        // Настраиваем отрисовщик фигур (нужно для дебага, скорей всего)
        shapeRenderer = new ShapeRenderer();
        shapeRenderer.setProjectionMatrix(camera.combined);
    }

    // Инициализатор, вызывается каждую новую игру
    public void init ()
    {
        camera.translate(0f, -yOffset);
        camera.update();

        batch.setProjectionMatrix(camera.combined);
        shapeRenderer.setProjectionMatrix(camera.combined);

        yOffset = 0f;
        previousYDelta = 0f;
    }

    // Рендер-метод, вызывается каждый фрейм
    public void render (float delta)
    {
        // Рисуем спрайты
        batch.begin();

        // Тут все очевидно
        drawPipes();
        drawBonuses();
        drawBoostsParticles();
        drawJumper();
        drawCandiesParticles();

        // Это необходимо
        batch.end();

        // Теперь дебаг отрисовка
        if (DEBUG)
        {
            shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);

            drawJumperDebug();
            drawPipesDebug();
            drawBonusesDebug();

            shapeRenderer.end();
        }

        // Работа с перемещением камеры
        float yDelta = Jumper.instance().getMaxYReached() - yOffset - JumpinSweeties.instance().getGameHeight() / 2.7f;
        yDelta *= CAMERA_SMOOTH;

        if (yDelta > 0f)
        {
            yOffset += yDelta;

            camera.translate(0f, yDelta);
            camera.update();

            batch.setProjectionMatrix(camera.combined);
            shapeRenderer.setProjectionMatrix(camera.combined);

            previousYDelta = yDelta;
        }
        else
            previousYDelta = 0f;

    }

    // Рендер с предварительной очисткой экрана
    public void render (float delta, boolean clearScreen)
    {
        if (clearScreen)
        {
            Gdx.gl.glClearColor(0.2f, 0.2f, 0.2f, 1f);
            Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        }

        render(delta);
    }

    // Функции отрисовки
    private void drawJumper ()
    {
        batch.enableBlending();
        Jumper.instance().draw(batch);
    }

    private void drawPipes ()
    {
        batch.enableBlending();

        for (int i = 0; i < PipesController.MAX_PIPES; i++)
        {
            PipesController.getPipes().get(i).getLeftPipe().draw(batch);
            PipesController.getPipes().get(i).getRightPipe().draw(batch);
        }
    }

    private void drawBonuses()
    {
        batch.enableBlending();

        for (int i = 0; i < BonusesHandler.getActiveBonuses().size; i++)
        {
            BonusesHandler.getActiveBonuses().get(i).getSprite().draw(batch);
        }
    }

    private void drawCandiesParticles()
    {
        batch.enableBlending();

        for (int i = 0; i < BonusesHandler.getActiveCandieEffects().size; i++)
        {
            BonusesHandler.getActiveCandieEffects().get(i).draw(batch);
        }
    }

    private void drawBoostsParticles()
    {
        batch.enableBlending();

        for (int i = 0; i < BonusesHandler.getActiveBoostsEffects().size; i++)
        {
            BonusesHandler.getActiveBoostsEffects().get(i).draw(batch);
        }
    }

    // Дебаг
    private void drawJumperDebug ()
    {
        shapeRenderer.setColor(Color.RED);
        shapeRenderer.circle(Jumper.instance().getCollisionCircle().x, Jumper.instance().getCollisionCircle().y, Jumper.instance().getCollisionCircle().radius);
    }

    private void drawPipesDebug ()
    {
        com.badlogic.gdx.math.Rectangle rectangle;

        for (int i = 0; i < PipesController.MAX_PIPES; i++)
        {
            shapeRenderer.setColor(Color.YELLOW);

            rectangle = PipesController.getPipes().get(i).getCollisionRectLeft();
            shapeRenderer.rect(rectangle.x, rectangle.y, rectangle.width, rectangle.height);

            rectangle = PipesController.getPipes().get(i).getCollisionRectRight();
            shapeRenderer.rect(rectangle.x, rectangle.y, rectangle.width, rectangle.height);
        }
    }

    private void drawBonusesDebug()
    {
        Circle circle;

        for (int i = 0; i < BonusesHandler.getActiveBonuses().size; i++)
        {
            circle = BonusesHandler.getActiveBonuses().get(i).getCollisionCircle();

            shapeRenderer.setColor(Color.GREEN);
            shapeRenderer.circle(circle.x, circle.y, circle.radius);
        }
    }

    public void dispose ()
    {
        batch.dispose();
        shapeRenderer.dispose();
    }

    // Геттеры
    public float getYOffset()
    {
        return yOffset;
    }

    public float getPreviousYDelta()
    {
        return previousYDelta;
    }

    public OrthographicCamera getCamera()
    {
        return camera;
    }
}
