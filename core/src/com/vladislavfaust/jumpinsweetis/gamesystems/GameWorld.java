package com.vladislavfaust.jumpinsweetis.gamesystems;

import com.vladislavfaust.jumpinsweetis.JumpinSweeties;
import com.vladislavfaust.jumpinsweetis.gameobjects.bonuses.Bonus;
import com.vladislavfaust.jumpinsweetis.gameobjects.bonuses.Rocket;
import com.vladislavfaust.jumpinsweetis.gameobjects.bonuses.Candie;
import com.vladislavfaust.jumpinsweetis.gameobjects.Jumper;
import com.vladislavfaust.jumpinsweetis.helpers.CandiesHandler;
import com.vladislavfaust.jumpinsweetis.helpers.HighscoreHandler;
import com.vladislavfaust.jumpinsweetis.helpers.ScreenShaker;
import com.vladislavfaust.jumpinsweetis.helpers.SoundsHandler;

/**
 * Created by Faust on 20.01.2015.
 *
 */
public class GameWorld
{
    // Константа, определяющая, сколько времени длится падение при проигрыше
    private final static float FALLING_TIME = 1f;

    // Сколько времени прошло в падении
    private float fallingTime = 0f;

    // Начальная позиция, когда не с игрового меню
    private float startX, startY;

    // Стейт игры
    private GameState gameState;
    public enum GameState
    {
        READY, ACTIVE, FALLING
    }

    // Инстанс синглтона
    private static GameWorld _instance;

    // Геттер инстанса синглтона
    public static GameWorld instance ()
    {
        return _instance;
    }

    // Конструктор
    public GameWorld ()
    {
        _instance = this;

        // Создаем джампера
        new Jumper();

        // Создаем трубы
        PipesController.construct();
        BonusesHandler.construct();

        // Задаем стартовые x и y
        startX = JumpinSweeties.instance().getGameWidth()/2f - Jumper.SPRITE_SIZE_X/2f;
        startY = 150f;
    }

    // Инициализация, вызывается заново каждую игру
    public void init (float x, float y)
    {
        // Инициализируем системы
        ScoreHandler.init();
        HighscoreHandler.init();
        BonusesHandler.init();
        PipesController.init();

        // Перемещаем джампера
        Jumper.instance().init(x, y);

        // Сбрасываем стейт и время падения
        gameState = GameState.READY;
        fallingTime = 0f;
    }

    // Инициализация со стандартными x и y
    public void init ()
    {
        init(startX, startY);
    }

    // Апдейт игрового мира
    public void update (float delta)
    {
        // Если только не "подготовка" - обновлять позицию джампера
        // Если все еще подготовка, то джампер стоит на месте
        if (gameState != GameState.READY)
            Jumper.instance().update(delta);

        PipesController.update();
        BonusesHandler.update(delta);

        // Проверяем коллизию
        // Сначала - конфетки
        Bonus bonus = CollisionsHandler.checkBonuses();
        if (bonus != null)
        {
            if (bonus instanceof Candie)
                CandiesHandler.pickupCandie((Candie) bonus);

            BonusesHandler.killBonus(bonus);
        }

        // Потом - трубы и границы экрана
        // Проверяем, только если игар еще не закончилась
        if (gameState != GameState.FALLING) {
            int obstacles = CollisionsHandler.checkObstacles();
            if ((obstacles > 0 && !Rocket.isInvulnerable()) || (obstacles == 2 && Rocket.isInvulnerable())) {
                // Если препятствие, то заканчиваем игру!
                // Удаляем инпут
                JumpinSweeties.instance().removeInputProcessor(GameInput.instance());

                // Джампер мило подпрыгивает
                Jumper.instance().bounce();

                // Меняем стейт
                gameState = GameState.FALLING;

                // Приглушаем фоновую музыку
                SoundsHandler.backgroundMute();

                // Играем звук удара
                SoundsHandler.fall();
                SoundsHandler.deactivateMagnet();

                // Трясем экран
                ScreenShaker.shake(GameRenderer.instance().getCamera(), 25f, 0.5f);

                // Записываем новый хайскор
                HighscoreHandler.tryToSetNewHighscore(ScoreHandler.getScore());
            }
        }
        // Если джампер падает
        else
        {
            fallingTime += delta;

            if (fallingTime >= FALLING_TIME)
            {
                ScoreHandler.submitScore();
                JumpinSweeties.instance().gameOver();
            }
        }
    }

    // Геттеры и даже сеттеры
    public GameState getGameState()
    {
        return gameState;
    }

    public void setGameState(GameState gameState)
    {
        this.gameState = gameState;
    }
}
