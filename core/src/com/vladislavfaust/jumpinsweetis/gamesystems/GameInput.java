package com.vladislavfaust.jumpinsweetis.gamesystems;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.vladislavfaust.jumpinsweetis.gameobjects.Jumper;

/**
 * Created by Faust on 20.01.2015.
 * Процессор, поэтому должен быть синглтоном
 */
public class GameInput implements InputProcessor
{
    // Инстанс синглтона
    private static GameInput _instance;

    // Геттер инстанса синглтона
    public static GameInput instance ()
    {
        return _instance;
    }

    // Конструктор
    public GameInput ()
    {
        _instance = this;
    }
    
    @Override
    public boolean keyDown(int keycode)
    {
        return false;
    }

    @Override
    public boolean keyUp(int keycode)
    {
        return false;
    }

    @Override
    public boolean keyTyped(char character)
    {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button)
    {
        if (GameWorld.instance() != null)
        {

        }
        switch (GameWorld.instance().getGameState())
        {
            case READY:
                GameWorld.instance().setGameState(GameWorld.GameState.ACTIVE);
            case ACTIVE:
                Jumper.instance().click(screenX > Gdx.graphics.getWidth() / 2);
                break;
            case FALLING:
                break;
        }

        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button)
    {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer)
    {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY)
    {
        return false;
    }

    @Override
    public boolean scrolled(int amount)
    {
        return false;
    }
}
