package com.vladislavfaust.jumpinsweetis.gamesystems;

import com.badlogic.gdx.utils.Array;
import com.vladislavfaust.jumpinsweetis.JumpinSweeties;
import com.vladislavfaust.jumpinsweetis.gameobjects.Jumper;
import com.vladislavfaust.jumpinsweetis.gameobjects.Pipes;
import com.vladislavfaust.jumpinsweetis.helpers.AssetLoader;
import com.vladislavfaust.jumpinsweetis.ui.screens.UIScreenGame;

import java.util.Random;

/**
 * Created by Faust on 20.01.2015.
 * Класс отвечает за логику перемещения труб при движении джампера
 * Без отрисовки, только перемещение
 */
public class PipesController
{
    // Максимальное кол-во труб в игре (рекомендуется 5)
    public static final int MAX_PIPES = 5;

    // Разные настройки
    public static final float
            START_DIFFICULTY = 0.94f,
            HARDEST_AT = 12f,       // Когда самое сложное
            START_Y = 100f,
            BORDER_MIN = 100f,
            HORIZONTAL_GAP = 270f,  // Самое сложное значение
            VERTICAL_GAP = 590f;    // Самое сложное значение

    // Сколько труб преодолел
    private static int pipesScored = 0;

    // Лист труб
    private static Array<Pipes> pipes = new Array<Pipes>(MAX_PIPES);

    // Его величество Рандом
    private static Random random;

    // Статический конструктор, заполняет лист труб
    public static void construct ()
    {
        // Инициализируем Рандом
        random = new Random();

        pipes.clear();
        // Суем трубы в лист
        for (int i = 0; i < MAX_PIPES; i++)
        {
            pipes.add(new Pipes(AssetLoader.getLeftPipe(), AssetLoader.getRightPipe()));
        }
    }

    // Получает коэффициент сложности в зависимости от пройденных труб
    private static float getDifficulty ()
    {
        float difficulty = START_DIFFICULTY + (1f - START_DIFFICULTY) * ((float)(pipesScored + MAX_PIPES) / (HARDEST_AT + MAX_PIPES));
        difficulty = (difficulty > 1f) ? 1f : difficulty;
        return difficulty;
    }

    // Получает рандомный сдвиг по x в заданных пределах
    private static float getRandomXOffset ()
    {
        return BORDER_MIN + HORIZONTAL_GAP + random.nextInt((int)(JumpinSweeties.instance().getGameWidth() - BORDER_MIN * 2 - HORIZONTAL_GAP));
    }

    // Инициализатор, вызывается, предположительно, каждую новую игру
    public static void init ()
    {
        // Сначала указываем позицию первых труб
        pipesScored = -MAX_PIPES - 1;
        pipes.get(0).init(getRandomXOffset(), JumpinSweeties.instance().getGameHeight() + START_Y, getDifficulty());

        // Потом всех остальных
        pipesScored++;
        for (int i = 1; i < MAX_PIPES; i++)
        {
            pipes.get(i).init(getRandomXOffset(), pipes.get(i-1).getEdgeY() + VERTICAL_GAP / getDifficulty(), getDifficulty());
            pipesScored++;

            // И еще справним конфетки
            BonusesHandler.tryToSpawn(pipes.get(i - 1).getEdgeY(), pipes.get(i).getY());
        }

        pipesScored = 0;
    }

    // Апдейт, который перемещает трубы, если нужно
    public static void update ()
    {
        // Если трубы уехали за экран, то сдвигаем их относительно друг друга
        for (int i = 0; i < MAX_PIPES; i++)
        {
            // Начисляем очки
            if (!pipes.get(i).isPassed() && pipes.get(i).getY() < Jumper.instance().getMaxYReached())
            {
                ScoreHandler.addScore();
                UIScreenGame.setScore(ScoreHandler.getScore());
                pipes.get(i).passed();
                pipesScored++;
            }

            // Магическая хрень, которая равна (при максимуме = 3) 0 при i = 1, 1 при i = 2, 2 при i = 0
            int a = (i == 0) ? MAX_PIPES - 1 - i : i - 1;

            // Если элемент ушел за край экрана
            if (pipes.get(i).getEdgeY() < GameRenderer.instance().getYOffset())
            {
                // Перемещаем его наверх другого
                pipes.get(i).init(getRandomXOffset(), pipes.get(a).getEdgeY() + VERTICAL_GAP / getDifficulty(), getDifficulty());

                // И еще справним конфетки
                BonusesHandler.tryToSpawn(pipes.get(a).getEdgeY(), pipes.get(i).getY());
            }
        }
    }

    // Класс труб, объединяющий два спрайта


    public static Array<Pipes> getPipes()
    {
        return pipes;
    }
}
